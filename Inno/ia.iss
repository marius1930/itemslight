[Setup]
AppVerName=AO Item Assistant (IA:Light)
AppName=IA:Light (c) EvilSoft
AppId=ialight
DefaultDirName={code:DefDirRoot}\AO Item Assistant
Uninstallable=Yes
OutputDir=..\Installer
SetupIconFile=../client/Client0.ico


[Tasks]
Name: desktopicon; Description: "Create a &desktop icon"; GroupDescription: "Icons:"
Name: starticon; Description: "Create a &startmenu icon"; GroupDescription: "Icons:"


[Icons]
Name: "{commonprograms}\AO Item Assistant"; Filename: "{app}\\ItemsLight.exe"; Tasks: starticon
Name: "{commondesktop}\AO Item Assistant"; Filename: "{app}\\ItemsLight.exe"; Tasks: desktopicon


[Files]
Source: "..\client\bin\Release\*"; Excludes: "*.pdb, *.exe.config"; DestDir: "{app}"; Flags: overwritereadonly replacesameversion recursesubdirs createallsubdirs touch


[Run]
Filename: "{app}\dotNetFx40_Full_setup.exe"; Description: "Install .NET 4"; Flags: postinstall unchecked runascurrentuser
Filename: "{app}\ItemsLight.exe"; Description: "Launch IA:Light"; Flags: postinstall nowait


[Setup]
UseSetupLdr=yes
DisableProgramGroupPage=yes
DiskSpanning=no
AppVersion=1
PrivilegesRequired=admin
DisableWelcomePage=Yes
ArchitecturesInstallIn64BitMode=x64
AlwaysShowDirOnReadyPage=Yes
DisableDirPage=No
OutputBaseFilename=IALightInstaller

[UninstallDelete]
Type: filesandordirs; Name: {app}

[Languages]
Name: eng; MessagesFile: compiler:Default.isl

[Code]
function IsRegularUser(): Boolean;
begin
Result := not (IsAdminLoggedOn or IsPowerUserLoggedOn);
end;

function DefDirRoot(Param: String): String;
begin
if IsRegularUser then
Result := ExpandConstant('{localappdata}')
else
Result := ExpandConstant('{pf}')
end;

