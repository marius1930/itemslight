﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AORipper.Structures {
    enum ResourceType {
        AODB_TYP_UNKNOWN = 0x00000000,
        AODB_TYP_ITEM = 0x000f4254,
        AODB_TYP_TEXTURE = 0x000f6954,
        AODB_TYP_ICON = 0x000f6958,
        AODB_TYP_PF = 0x000f4241,
        AODB_TYP_PFGROUND = 0x000f4249,
        AODB_TYP_OBJMODEL = 0x000f424d,	// unsure: object model data? (.biff filenames)
        AODB_TYP_NANO = 0x000fde85,
    };
}
