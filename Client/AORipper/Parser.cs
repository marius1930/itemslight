﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using AORipper.Structures;
using log4net;
using System.Diagnostics;
using EvilsoftCommons;

namespace AORipper {
    class Parser : IDisposable {
        static ILog logger = LogManager.GetLogger(typeof(Parser));
        private byte[] buffer;
        private Dictionary<long, string> fileOffsets = new Dictionary<long, string>();
        private FileStream resourceFile;
        private long currentFileOffset;
        private long currentFileSize;
        private uint fatalities = 0;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resourceFiles">Resource files</param>
        public Parser(List<string> resourceFiles) {
            buffer = new byte[1024 * 1024];

            long accumulatedOffset = 0;
            foreach (string file in resourceFiles) {
                FileInfo fi = new FileInfo(file);
                fileOffsets[accumulatedOffset] = file;
                accumulatedOffset += fi.Length - 4096; // HACK: Should not hardcode 0x1000. Datafile size can be found in the index file.
            }
        }
        
        public AOItem GetItem(uint offset) {
            FileStream fs = GetFileHandle(offset);
            fs.Seek(offset - currentFileOffset, SeekOrigin.Begin);

            int tmp1 = fs.ReadByte();
            int tmp2 = fs.ReadByte();
            if (tmp1 != 0xFA || tmp2 != 0xFA) {
                byte[] tmp = new byte[2048*2];
                logger.FatalFormat("Expected 0xFAFA, got 0x{0}{1}", tmp1.ToString("X"), tmp2.ToString("X"));
                logger.FatalFormat("Item excluded from database parse ({0} so far)", ++fatalities); // This happens when parsing the new engine, 1778 items lost
                return null;
            }

            uint recordSize = IOHelper.ReadUInteger(fs);
            uint payloadSize = IOHelper.ReadUInteger(fs);
            uint payloadType = IOHelper.ReadUInteger(fs);
            logger.DebugFormat("Item parse at offset {0}, size {1}", offset, recordSize);

            if (payloadSize > recordSize) {
                logger.DebugFormat("Skipping item at offset {0}, Payload > Record with {0} > {1}", offset, payloadSize, recordSize);
            }
            else if (payloadSize > buffer.Length) {
                logger.DebugFormat("Skipping item at offset {0}, buffer overflow.", offset);
            }
            else {
                int payloadRead = fs.Read(buffer, 0, (int)payloadSize);
                if (payloadRead != payloadSize) {
                    logger.WarnFormat("Attempted to read {0} byte payload, but only {1} was read.", payloadSize, payloadRead);
                }
                
                return new ItemParser().Parse(buffer, payloadRead);
            }

            return null;            
        }



        /// <summary>
        /// Get the FileStream handle to the resource file responsible for this offset
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        private FileStream GetFileHandle(uint offset) {
            // May need to open a different file, if so, close this one.
            if (resourceFile != null) {
                if (offset < currentFileOffset || offset > currentFileOffset + currentFileSize) {
                    resourceFile.Close();
                    resourceFile = null;
                }
            }

            // Open the appropriate resource file
            if (resourceFile == null) {
                var key = fileOffsets.Keys.Where(m => m < offset).Max();
                resourceFile = File.OpenRead(fileOffsets[key]);
                currentFileSize = resourceFile.Length;
                currentFileOffset = key;
                return resourceFile;
            }

            return resourceFile;
        }


        /// <summary>
        /// Get a list of the resource files for a given Anarchy Online install
        /// </summary>
        /// <param name="path">Full path to anarchy online</param>
        /// <returns></returns>
        public static List<string> GetResourceFiles(string path) {
            List<string> files = new List<string>();
            files.Add(string.Format("{0}/cd_image/data/db/ResourceDatabase.dat", path));
            for (uint i = 1; i < 1000; i++) {
                string filename = string.Format("{0}/cd_image/data/db/ResourceDatabase.dat.{1}", path, i.ToString("000"));
                if (File.Exists(filename))
                    files.Add(filename);
                else
                    break;
            }

            return files;
        }


        public void Dispose() {
            if (resourceFile != null) {
                try {
                    resourceFile.Close();
                }
                catch (Exception ex) {
                    logger.Warn(ex.Message);
                    logger.Warn(ex.StackTrace);
                }
            }
        }
    }
}
