﻿using AORipper.Structures;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using AORipper;
using NHibernate;
using ItemsLight.Dao;
using log4net;
using ItemsLight.Model;
using System.Threading;
using System.Collections.Concurrent;
using NHibernate.Criterion;
using EvilsoftCommons.Exceptions;



namespace ItemsLight.AORipper {
    class BackgroundRipper : IDisposable {
        private static ILog logger = LogManager.GetLogger(typeof(BackgroundRipper));
        private BackgroundWorker bw = new BackgroundWorker();
        private ulong numItemsSaved = 0;
        private ConcurrentQueue<List<AOItem>> queue = new ConcurrentQueue<List<AOItem>>();
        private volatile bool SaveTimestamp = false;
        private const int NUM_ITEMS_PER_TRANSACTION = 2500;





        /// <summary>
        /// 
        /// </summary>
        /// <param name="path">Path to anarchy online</param>
        public BackgroundRipper(string path) {
            long lastUpdated = DatabaseItemDao.GetLastUpdated();
            long lastModified = (System.IO.File.GetLastWriteTime(path).Ticks - DateTime.Parse("01/01/1970 00:00:00").Ticks) / 10000000;


            if (lastModified > lastUpdated) {
                bw.DoWork += new DoWorkEventHandler(bw_DoWork);
                bw.WorkerSupportsCancellation = true;
                bw.RunWorkerCompleted += bw_RunWorkerCompleted;
                bw.RunWorkerAsync(path);
                logger.Info("Starting to rip AO database..");
            }
            else {
                logger.Info("AO database up to date..");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            bw = null;
            SaveTimestamp = true;

            logger.Info("Finished ripping ao database");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bw_DoWork(object sender, DoWorkEventArgs e) {
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "BackgroundRipper";

            BackgroundWorker worker = sender as BackgroundWorker;
            string path = e.Argument as string;

            string indexFile = string.Format("{0}/cd_image/data/db/ResourceDatabase.idx", path);
            List<ResourceType> resources = new List<ResourceType>(new ResourceType[] { ResourceType.AODB_TYP_ITEM, ResourceType.AODB_TYP_NANO });
            Indexer s = new Indexer(indexFile, resources);

            SortedSet<uint> itemOffsets = s.GetOffsets(ResourceType.AODB_TYP_ITEM);
            //SortedSet<uint> nanoOffsets = s.GetOffsets(ResourceType.AODB_TYP_NANO);


            List<AOItem> items = new List<AOItem>();
            Parser parser = new Parser(Parser.GetResourceFiles(path));
            foreach (uint offset in itemOffsets.Reverse()) {
                AOItem item = parser.GetItem(offset);
                if (item == null || string.IsNullOrEmpty(item.name))
                    logger.WarnFormat("{0}: Skipping item save.. item is null..", Thread.CurrentThread.Name);
                else
                    items.Add(item);

                if (items.Count >= NUM_ITEMS_PER_TRANSACTION) {
                    queue.Enqueue(items);
                    items = new List<AOItem>();
                }

                // Don't fill up the queue, eats memory!
                while (!worker.CancellationPending && queue.Count > 5) {
                    Thread.Sleep(1);
                }

                // Abort!
                if (worker.CancellationPending)
                    return;
            }

            if (items.Count > 0) {
                queue.Enqueue(items);
            }
        }


        public void Process() {
            List<AOItem> tmp;
            if (queue.TryDequeue(out tmp)) {
                try {
                    SaveProcessedItems(tmp);
                }
                catch (Exception ex) {
                    logger.Warn(ex.Message);
                    logger.Warn(ex.StackTrace);
                    
                    ExceptionReporter.ReportException(ex);
                }
                //logger.InfoFormat("Stored item batch.. {0} batches in queue..", queue.Count);
            }

            // Only update the timestamp if the queue has been fully processed
            if (SaveTimestamp && queue.IsEmpty) {
                // Update the 'last updated' timestamp, to prevent this from having to run every launch.
                using (ISession session = SessionFactory.OpenSession()) {
                    using (ITransaction transaction = session.BeginTransaction()) {
                        session.CreateQuery("UPDATE DatabaseSetting SET V2 = :v2 WHERE Id = 'lastupdate'").SetParameter("v2", ExceptionReporter.Timestamp).ExecuteUpdate();
                        transaction.Commit();
                    }
                }

                logger.Info("Updated internal database timestamp.");
                SaveTimestamp = false;
            }
        }


        /// <summary>
        /// Split a list into a series of smaller lists
        /// Mainly due to a SQLite limitation
        /// </summary>
        /// <param name="items"></param>
        /// <param name="segmentSize"></param>
        /// <returns></returns>
        private ICollection<List<long>> SplitIntoSegments(IEnumerable<long> items, int segmentSize) {
            ICollection<List<long>> result = new List<List<long>>();
            List<long> current = new List<long>();
            foreach (var item in items) {
                current.Add(item);
                if (current.Count >= segmentSize) {
                    result.Add(current);
                    current = new List<long>();
                }
            }
            result.Add(current);
            return result;
        }

        /// <summary>
        /// Persist processed data to database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveProcessedItems(List<AOItem> items) {
            // Filter effects by desired types only
            // Will reduce insert time slightly
            SortedSet<long> DesiredEffectFilter = new SortedSet<long>(new long[]{17,20,19,21,16,18,124,101,134,161,148,167,151,150,112,114,115,116,113});

            using (ISession session = SessionFactory.OpenSession()) {

                using (ITransaction transaction = session.BeginTransaction()) {
                    foreach (var list in SplitIntoSegments(items.Select(m => m.aoid), 800)) {
                        session.CreateQuery("DELETE FROM AOItemEffect WHERE Parent.Id IN ( :items )")
                            .SetParameterList("items", list)
                            .ExecuteUpdate();
                    }
                    transaction.Commit();
                }

                using (ITransaction transaction = session.BeginTransaction()) {

                    // Check if we already have all the items, if so, we can update instead of SaveOrUpdate
                    long count = 0;
                    foreach (var list in SplitIntoSegments(items.Select(m => m.aoid), 800)) {
                        count += (long)session.CreateCriteria<DatabaseItem>()
                             .Add(Restrictions.In("Id", list))
                             .SetProjection(Projections.RowCountInt64())
                             .UniqueResult();
                    }

                    bool saveOrUpdate = count != items.Count;


                    foreach (AOItem item in items) {
                        if (string.IsNullOrEmpty(item.name)) {
                            //ItemsLight.AOIA.ExceptionReporter.ReportIssue(string.Format("Item AOID:{0} has item.name == null", item.aoid));
                        }
                        else {
                            try {
                                // Create the base item
                                var SavedItem = new DatabaseItem {
                                    Flags = item.flags,
                                    Id = item.aoid,
                                    ISlot = item.slot,
                                    Name = item.name,
                                    PropertyFlags = item.props,
                                    QL = item.ql,
                                    Type = "Unknown",
                                    //Attributes = item.attmap,
                                    Effects = new SortedSet<ItemsLight.Model.AOItemEffect>()
                                };
                                
                                // Add the effects
                                var effects = item.effects;
                                foreach (var effect in effects) {
                                    if (effect.values != null && effect.values.Count >= 2 && DesiredEffectFilter.Contains(effect.values[0])) {
                                        var newEffect = new ItemsLight.Model.AOItemEffect {
                                            Parent = SavedItem,
                                            Text = effect.text,
                                            Type = effect.values[0],
                                            Value = effect.values[1]
                                        };
                                        SavedItem.Effects.Add(newEffect);
                                        //session.Save(newEffect);
                                    }
                                }

                                // Store item and effects
                                if (saveOrUpdate)
                                    session.SaveOrUpdate(SavedItem);
                                else
                                    session.Update(SavedItem);

                                if (++numItemsSaved % 1000 == 0) {
                                    logger.DebugFormat("Stored {0} items to database..", numItemsSaved);
                                }
                            }
                            catch (Exception ex) {
                                logger.WarnFormat("On thread {0}:", Thread.CurrentThread.Name);
                                ExceptionReporter.ReportException(ex, string.Format("Item({0}, {1}, {2}, {3}, {4}, {5})", item.flags, item.aoid, item.slot, item.name, item.props, item.ql));
                                logger.Warn(ex.Message);
                                logger.Warn(ex.StackTrace);
                                return; // Commit will fail anyways
                            }
                        }
                    }
                    transaction.Commit();
                }
            }
        }

        public void Dispose() {
            if (bw != null) {
                try {
                    bw.CancelAsync();
                    bw = null;
                }
                catch (Exception) {
                    // Nothing to do, already disposing.
                }
            }
        }
    }
}
