﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using AORipper.Structures;
using log4net;
using EvilsoftCommons;

namespace AORipper {
    class Indexer {
        private static ILog logger = LogManager.GetLogger(typeof(Indexer));
        private FileStream fs;
        private Dictionary<ResourceType, SortedSet<uint>> record_index;

        public SortedSet<uint> GetOffsets(ResourceType type) {
            if (record_index.ContainsKey(type)) {
                return record_index[type];
            }
            else
                return null;
        }

        #region READERS
        uint ReadInteger(bool endian = false) {
            return IOHelper.ReadUInteger(fs, endian);
        }

        ushort ReadShort() {
            return IOHelper.ReadShort(fs);
        }
        #endregion

        #region INTERNALS

        public Indexer(string file, List<ResourceType> resources) {
            fs = File.OpenRead(file);
            long currentPosition = fs.Position;


            List<uint> types = new List<uint>();
            record_index = new Dictionary<ResourceType, SortedSet<uint>>();
            foreach (var type in resources) {
                record_index[type] = new SortedSet<uint>();
                types.Add((uint)type);
            }

            long lastOffset = ReadInteger();
            fs.Seek(4, SeekOrigin.Current); // Unknown / 0
            long dataEnd = ReadInteger();
            long blockSize = ReadInteger();

            fs.Seek(56, SeekOrigin.Current);
            long dataStart = ReadInteger();

            currentPosition += dataStart;
            fs.Seek(dataStart, SeekOrigin.Begin);

            while (currentPosition + blockSize < fs.Length) {
                long end = Math.Min(currentPosition + blockSize, fs.Length);
                long next_block = ReadIndexBlock(currentPosition, end, types);

                if (next_block > 0) {
                    currentPosition = next_block;
                    fs.Seek(next_block, SeekOrigin.Begin);
                }
                else {
                    break;
                }
            }

            fs.Close();
            fs = null;

        }
        long ReadIndexBlock(long pos, long end, List<uint> types) {
            // Index blocks are stored in a double linked list. 
            // Extract pointer to next block.
            
            uint next_block = ReadInteger();

            // Extract pointer to previous block. [unused]
            ulong prev_block = ReadInteger();

            // Number of active record indexes in this block
            int count = ReadShort();

            // Skip block header
            fs.Seek(0x12, SeekOrigin.Current);

    
            while (count-- > 0 && pos + 2 < end) {
		        uint ignored = ReadInteger();
                

                uint offset = ReadInteger();
                uint resource_type = ReadInteger(true);                
                uint resource_id = ReadInteger(true);
                

                if (types.Count == 0 || types.Contains(resource_type)) {
                    ResourceType rt = (ResourceType)resource_type;
#if DEBUG
                    if (record_index[rt].Contains(offset))
                        logger.WarnFormat("Index already contains offset {0}, may need the resource id.", offset);
#endif

                    record_index[rt].Add(offset);
                    
                }
            }


            return next_block;
        }
        #endregion
    }
}
