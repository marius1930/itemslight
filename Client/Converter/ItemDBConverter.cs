﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using log4net;
using ItemsLight.Model;
using ItemsLight.Dao;
using EvilsoftCommons.Exceptions;

namespace ItemsLight.AOIA {
    class ItemDBConverter {
        static ILog logger = LogManager.GetLogger(typeof(ItemDBConverter));


        /// <summary>
        /// Load inventory and store to SQL
        /// </summary>
        /// <param name="pathToDatabase"></param>
        /// <param name="sql"></param>
        /// <param name="callback"></param>
        public static void ImportAOIA(string pathToDatabase, Action<string> callback, bool wipeExistingDatabase) {
            List<Item> itemdata = new List<Item>();
            List<Character> chardata = new List<Character>();
            SQLiteConnection dbConnection;
            
            callback("Loading AOIA database..");


            dbConnection = new SQLiteConnection(string.Format("Data Source=\"{0}\";Version=3;", pathToDatabase));
            dbConnection.Open();

            callback("Loading AOIA database...");
            
            SQLiteCommand command;
            try {

                CharacterDao characterDao = new CharacterDao();
                if (wipeExistingDatabase) {
                    characterDao.DeleteEverything();
                }

                // Check if this dump has dimensions, some versions of aoia might not.
                bool hasDimensions = false;
                command = new SQLiteCommand("PRAGMA table_info(tToons)", dbConnection);
                using (SQLiteDataReader reader = command.ExecuteReader()) {
                    while (reader.Read()) {
                        if (reader.GetString(1).Equals("dimensionid"))
                            hasDimensions = true;
                    }
                }

                if (hasDimensions)
                    command = new SQLiteCommand("select charid, charname, dimensionid from tToons", dbConnection);
                else
                    command = new SQLiteCommand("select charid, charname from tToons", dbConnection);
                
                using (SQLiteDataReader reader = command.ExecuteReader()) {
                    while (reader.Read()) {
                        Character character = new Character {
                            Id = (long)reader.GetInt32(0),
                            Dimension = (hasDimensions) ? reader.GetInt32(2) : 0,
                            Name = reader.GetString(1),
                            LastSeen = new DateTime(1980, 1, 1)
                        };

                        chardata.Add(character);
                        callback(string.Format("Loading AOIA database.. {0} characters..", chardata.Count));
                    }
                }
                characterDao.Save(chardata);


                command = new SQLiteCommand("select keylow, keyhigh, ql, flags, stack, parent, slot, children, owner from tItems", dbConnection);
                using (SQLiteDataReader reader = command.ExecuteReader()) {
                    while (reader.Read()) {
                        long characterId = (long)reader["owner"];

                        try {
                            var item = new Item {
                                LowKey = (long)reader["keylow"],
                                HighKey = (long)reader["keyhigh"],
                                QL = (int)(long)reader["ql"],
                                Flags = (long)reader["flags"],
                                Stack = (uint)(long)reader["stack"],
                                ParentContainerId = (long)reader["parent"],
                                Slot = (int)(long)reader["slot"],
                                Children = (long)reader["children"],
                                CharacterId = characterId
                            };
                            itemdata.Add(item);


                            if (itemdata.Count % 50 == 0) {

                                callback(string.Format("Loading AOIA database.. {0} characters and {1} items..", chardata.Count, itemdata.Count));
                            }
                        }
                        catch (InvalidCastException ex) {
                            var LowKey = reader["keylow"];
                            var HighKey = reader["keyhigh"];
                            var QL = reader["ql"];
                            var Flags = reader["flags"];
                            var Stack = reader["stack"];
                            var ParentContainerId = reader["parent"];
                            var Slot = reader["slot"];
                            var Children = reader["children"];
                            logger.WarnFormat("Could not import item: ({0}/{1}/{2}, Flags:{3}, Stack:{4}, Parent:{5}, Slot:{6}, Children:{7}", LowKey, HighKey, QL, Flags, Stack, ParentContainerId, Slot, Children);
                        }
                    }
                }


                // Try to load credits
                try {
                    command = new SQLiteCommand("select charid, statvalue, statid from tToonStats where statid IN (61,54,669,60,368)", dbConnection);

                    using (SQLiteDataReader reader = command.ExecuteReader()) {
                        while (reader.Read()) {

                            long characterId = reader.GetInt32(0);
                            int value = reader.GetInt32(1);
                            int statid = reader.GetInt32(2);
                            for (int i = 0; i < chardata.Count; i++) {
                                if (chardata[i].Id == characterId) {
                                    if (statid == 61)
                                        chardata[i].Credits = value;
                                    else if (statid == 669)
                                        chardata[i].VP = value;
                                    else if (statid == 54)
                                        chardata[i].Level = value;
                                    else if (statid == 60 || statid == 368)
                                        chardata[i].ProfessionId = value;
                                    
                                    break;
                                }
                            }
                        }
                    }

                }
                catch (Exception e) {
                    logger.Warn(e.Message + e.StackTrace);
                    ExceptionReporter.ReportException(e, "THIS IS OKAY");
                }

            }

            catch (Exception e) {
                logger.Warn(e.Message + e.StackTrace);
                throw;
            }
            finally {
                dbConnection.Close();
            }
            ItemDao itemDao = new ItemDao();

            callback(string.Format("Saving {0} items and {1} characters..", itemdata.Count, chardata.Count));
            itemDao.Save(itemdata);
            callback("Import complete!");
        }

        
    }
}
