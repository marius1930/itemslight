﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using ItemsLight.AOIA;
using System.ComponentModel;
using log4net;
using System.IO;
using Microsoft.Win32;
using System.Threading;
using EvilsoftCommons.DllInjector;
using EvilsoftCommons.Exceptions;

namespace ItemsLight {
    [Obsolete]
    class AOInjector : IDisposable {
        static ILog logger = LogManager.GetLogger(typeof(AOInjector));
        BackgroundWorker bw = new BackgroundWorker();
        HashSet<uint> previouslyInjected = new HashSet<uint>();
        HashSet<uint> dontLog = new HashSet<uint>();
        private Dictionary<uint, IntPtr> pidModuleHandleMap = new Dictionary<uint, IntPtr>();

        public AOInjector(ConcurrentQueue<MessageData> resultQueue) {
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.WorkerSupportsCancellation = true;
            bw.RunWorkerAsync(resultQueue);
        }

        public void Cancel() {
            if (bw != null) {
                bw.CancelAsync();
                bw = null;
            }
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e) {
            BackgroundWorker worker = sender as BackgroundWorker;
            ConcurrentQueue<MessageData> arguments = (ConcurrentQueue<MessageData>)e.Argument;
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "AOInjector";

            while (!worker.CancellationPending) {
                process(arguments);
            }
        }

        public void Dispose() {
            if (bw != null) {
                bw.CancelAsync();
                bw = null;
            }

            // Unload the DLL from any still running GD instance
            // DO NOT ENABLE THIS!
            // DLL HAS NOT YET BEEN MODIFIED TO REMOVE HOOKS!
            // UNLOADING DLL WITH HOOKS ACTIVE WILL CRASH AO
/*
            HashSet<uint> pids = DllInjector.FindProcessForWindow("Grim Dawn");
            foreach (uint pid in pidModuleHandleMap.Keys) {
                if (pids.Contains(pid)) {
                    if (DllInjector.UnloadDll(pid, pidModuleHandleMap[pid])) {
                        logger.InfoFormat("Unloaded module from pid {0}", pid);
                    }
                    else {
                        logger.InfoFormat("Failed to unload module from pid {0}", pid);
                    }
                }
            }*/
        }


        private void process(ConcurrentQueue<MessageData> resultQueue) {
            System.Threading.Thread.Sleep(1200);

            HashSet<uint> pids = DllInjector.FindProcessForWindow("Anarchy client");
            string dll = Path.Combine(Directory.GetCurrentDirectory(), "ItemAssistantHook.dll");
            if (!File.Exists(dll)) {
                logger.Fatal("Could not find ItemAssistantHook.dll at " + dll);
            }
            else {
                foreach (uint pid in pids) {
                    if (!previouslyInjected.Contains(pid)) {
                        string pathToAnarchy = DllInjector.GetProcessPath(pid);
                        if (!string.IsNullOrEmpty(pathToAnarchy)) {
                            pathToAnarchy = Path.GetDirectoryName(pathToAnarchy);
                            if (Directory.Exists(pathToAnarchy)) {
                                Properties.Settings.Default["pathToAnarchy"] = pathToAnarchy;
                                Properties.Settings.Default.Save();
                                logger.InfoFormat("Updated the path to anarchy online: \"{0}\"", pathToAnarchy);
                            }
                        }

                        DllInjector.adjustDebugPriv(pid);
                        IntPtr remoteModuleHandle = DllInjector.NewInject(pid, dll);
                        if (remoteModuleHandle == IntPtr.Zero) {
                            if (!dontLog.Contains(pid))
                                logger.Warn("Could not inject dll into process " + pid);
                        }
                        else {
                            if (!dontLog.Contains(pid))
                                logger.Info("Injected dll into process " + pid);

                            if (!InjectionVerifier.VerifyInjection(pid, "ItemAssistantHook.dll")) {
                                if (!dontLog.Contains(pid)) {
                                    logger.Warn("InjectionVerifier reports injection failed.");
                                    ExceptionReporter.ReportIssue("InjectionVerifier reports injection failed into PID " + pid);
                                }

                                resultQueue.Enqueue(new MessageData {
                                    Type = MessageDataType.RES_SET_STATUS_MESSAGE,
                                    Data = "InjectionVerifier reports injection failed into PID " + pid
                                });

                                dontLog.Add(pid);
                            }
                            else {
                                logger.Info("InjectionVerifier reports injection succeeded.");
                                previouslyInjected.Add(pid);
                                pidModuleHandleMap[pid] = remoteModuleHandle;
                            }

                        }
                    }
                }
            }
        }


        public static void FixRegistryNagOnListDlls() {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software", true);


            key.CreateSubKey("Sysinternals");
            key = key.OpenSubKey("Sysinternals", true);

            key.CreateSubKey("ListDLLs");
            key = key.OpenSubKey("ListDLLs", true);

            key.SetValue("EulaAccepted", 1);

            key.Close();
        }
    }
}


