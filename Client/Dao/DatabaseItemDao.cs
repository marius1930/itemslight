﻿using ItemsLight.AOIA;
using ItemsLight.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using ItemsLight.AOIA.Misc;
using System.Data;
using System.IO;
using NHibernate;
using NHibernate.Exceptions;
using EvilsoftCommons.Exceptions;

namespace ItemsLight.Dao {
    class DatabaseItemDao {
        static ILog logger = LogManager.GetLogger(typeof(DatabaseItemDao));
        
        public ICollection<DatabaseItem> ListAllItems() {
            var items = ItemNameCache.Cache;
            if (items != null)
                return items;

            using (ISession session = SessionFactory.OpenSession()) {
                using (session.BeginTransaction()) {
                    var criteria = session.CreateCriteria<DatabaseItem>();
                    items = new List<DatabaseItem>(criteria.List<DatabaseItem>());
                    ItemNameCache.Cache = items;
                    return items;
                }
            }
        }




        /// <summary>
        /// Retrieve the last time the internal item database was updated
        /// This is checked against the Anarchy Online itemdb timestamp
        /// 
        /// If no effects are stored in the database, returns 0.
        /// (To force an update, its IA that didn't use to parse this data)
        /// </summary>
        /// <returns></returns>
        public static Int64 GetLastUpdated() {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {

                    if (session.CreateQuery("SELECT COUNT(*) FROM AOItemEffect").UniqueResult<long>() <= 1)
                        return 0;


                    DatabaseSetting setting = session.CreateQuery("FROM DatabaseSetting WHERE Id = 'lastupdate'").UniqueResult<DatabaseSetting>();
                    return setting.V2;
                }
            }
        }


        public void UpdateGameItemDatabase(List<DownloadedItemdata> data) {

            logger.Info(string.Format("Storing {0} items to local item database", data.Count));
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    foreach (var item in data) {
                        session.Save(new DatabaseItem {
                            Id = item.aoid,
                            Name = item.name,
                            QL = 1,
                            Type = "Unknown",
                            PropertyFlags = item.properties,
                            ISlot = 0
                        });
                    }
                    transaction.Commit();
                }
            }
            logger.Info("All items stored.");
        }


        [Obsolete("No longer loads all the items in a single batch")]
        public void UpdateGameItemDatabase(List<DatabaseItem> data) {
            logger.Info(string.Format("Storing {0} items to local item database", data.Count));
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    foreach (var item in data) {
                        session.Save(item);
                    }

                    session.CreateQuery("UPDATE DatabaseSetting SET V2 = :v2 WHERE Id = 'lastupdate'").SetParameter("v2", ExceptionReporter.Timestamp).ExecuteUpdate();
                    transaction.Commit();
                }
            }
            logger.Info("All items stored.");
        }


        /// <summary>
        /// Filter database items for insertion, ignoring any existing items.
        /// This is problematic for name changes, but a significant speed boost for insertions
        /// </summary>
        /// <param name="data"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public List<DatabaseItem> FilterGameItemDatabase(List<DatabaseItem> data, Action<string> callback) {
            try {
                using (ISession session = SessionFactory.OpenSession()) {
                    HashSet<long> existingItems = new HashSet<long>(session.CreateQuery("SELECT Id FROM DatabaseItem").List<long>());

                    var elems = data.Where(m => !existingItems.Contains(m.Id));
                    return new List<DatabaseItem>(elems);
                }
            }
            catch (GenericADOException ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                ExceptionReporter.ReportException(ex, " - Handled just fine, but please do fix");
                return new List<DatabaseItem>();
            }
        }



    }
}
