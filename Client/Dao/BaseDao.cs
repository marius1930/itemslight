﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using ItemsLight.AOIA;
using log4net;
using EvilsoftCommons.Exceptions;

namespace ItemsLight.Dao {
    
    public class BaseDao<T> where T : class {
        static ILog logger = LogManager.GetLogger(typeof(BaseDao<T>));

        public virtual void Save(T obj) {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    session.Save(obj);
                    transaction.Commit();
                }
            }
        }

        public virtual void Update(T obj) {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    session.Update(obj);
                    transaction.Commit();
                }
            }
        }


        public virtual void SaveOrUpdate(T obj) {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    session.SaveOrUpdate(obj);
                    transaction.Commit();
                }
            }
        }

        public virtual void Remove(T obj) {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    session.Delete(obj);
                    transaction.Commit();
                }
            }
        }

        public virtual T GetById(long id) {
            using (ISession session = SessionFactory.OpenSession()) {
                return session.Get<T>(id);
            }
        }


        public virtual IList<T> ListAll() {
            try {
                using (ISession session = SessionFactory.OpenSession()) {
                    return session.CreateCriteria<T>().List<T>();
                }
            }
            catch (Exception ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                ExceptionReporter.ReportException(ex);
                throw ex;
            }
        }

    


        protected struct Parameter {
            //public Parameter() { }
            public Parameter(string field, object value) {
                this.field = field;
                this.value = value;
            }

            public string field;
            public object value;
        }

        protected void SingleQuery(String hql, Parameter[] bindings) {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    var query = session.CreateQuery(hql);
                    foreach (var binding in bindings) {
                        query.SetParameter(binding.field, binding.value);
                    }
                    query.ExecuteUpdate();
                    transaction.Commit();
                }
            }
        }

        protected void SingleQuery(String hql, Parameter binding) {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    var query = session.CreateQuery(hql);
                    query.SetParameter(binding.field, binding.value);
                    query.ExecuteUpdate();              
                    transaction.Commit();
                }
            }
        }


        protected void SingleQuery(String hql, List<Parameter> bindings) {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    var query = session.CreateQuery(hql);
                    foreach (var binding in bindings) {
                        query.SetParameter(binding.field, binding.value);
                    }
                    query.ExecuteUpdate();
                    transaction.Commit();
                }
            }
        }

        public T GetByField(long id) {
            using (ISession session = SessionFactory.OpenSession()) {
                return session.Get<T>(id);
            }
        }

    }
    
}
