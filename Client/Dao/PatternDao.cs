﻿using ItemsLight.AOIA;
using ItemsLight.Model;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Dao {
    class PatternDao : BaseDao<Pattern> {


        /// <summary>
        /// Get the patterns for a given character
        /// If the dimension is -1, all dimensions are included
        /// If the characterId is 0, all characters are included (and dimensions)
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public List<PatternPiece> GetPiecesForPattern(string pattern, int dimension, long characterId = 0) {
            List<PatternPiece> result = new List<PatternPiece>();

            ItemDao itemDao = new ItemDao();
            using (ISession session = SessionFactory.OpenSession()) {
                {
                    var criteria = session.CreateCriteria<Item>();

                    if (characterId != 0)
                        criteria.Add(Restrictions.Eq("Owner.Id", characterId));

                    else if (dimension >= 0) {
                        criteria.CreateAlias("Owner", "o");
                        criteria.Add(Restrictions.Eq("o.Dimension", dimension));
                    }
                    
                    criteria.Add(Subqueries.PropertyIn("LowKey", DetachedCriteria.For<Pattern>().Add(Restrictions.Eq("Name", pattern)).SetProjection(Projections.Property("Id"))));

                    var items = criteria.List();
                    foreach (Item item in items) {
                        var p = session.CreateCriteria<Pattern>().Add(Restrictions.Eq("Id", item.LowKey));
                        PatternPiece piece = new PatternPiece {
                            item = item,
                            pattern = p.UniqueResult<Pattern>(),
                        };

                        result.Add(piece);
                    }
                }




                // Finally, select the already completed novictalized patterns
                // These are not included above as the novictum has already been added
                {
                    var criteria = session.CreateCriteria<Item>();

                    if (characterId != 0)
                        criteria.Add(Restrictions.Eq("Owner.Id", characterId));

                    else if (dimension >= 0) {
                        criteria.CreateAlias("Owner", "o");
                        criteria.Add(Restrictions.Eq("o.Dimension", dimension));
                    }

                    criteria.Add(Subqueries.PropertyIn("LowKey", DetachedCriteria.For<PocketBoss>().Add(Restrictions.Eq("Name", pattern)).SetProjection(Projections.Property("Id"))));

                    var items = criteria.List();
                    foreach (Item item in items) {
                        var p = session.CreateCriteria<PocketBoss>().Add(Restrictions.Eq("Id", item.LowKey));
                        PatternPiece piece = new PatternPiece {
                            item = item,
                            pocketboss = p.UniqueResult<PocketBoss>(),
                        };

                        result.Add(piece);
                    }
                }
            }
            return result;
        }


        /// <summary>
        /// Get the patterns for a given character
        /// If the dimension is -1, all dimensions are included
        /// If the characterId is 0, all characters are included
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public Dictionary<string, Pattern> GetPatternsForCharacter(int dimension, long characterId = 0) {
            Dictionary<string, Pattern> result = new Dictionary<string, Pattern>();

            // This query is horribly slow when using HQL, due to the inability to properly left join
            const bool useHql = false;
            using (ISession session = SessionFactory.OpenSession()) {
                if (useHql) {
                    string hql;

                    if (characterId != 0)
                        hql = "SELECT p.Piece, p.Name FROM Pattern p LEFT JOIN Item i WHERE p.Id = i.LowKey AND i.Owner.Id = :owner";
                    else if (dimension >= 0)
                        hql = "SELECT p.Piece, p.Name FROM Pattern p, Item i WHERE i.LowKey = p.Id AND i.Owner.Dimension = :dimension";
                    else
                        hql = "SELECT p.Piece, p.Name FROM Pattern p, Item i WHERE i.LowKey = p.Id";

                    var query = session.CreateQuery(hql);

                    if (hql.Contains(":owner"))
                        query.SetParameter("owner", characterId);

                    else if (hql.Contains(":dimension"))
                        query.SetParameter("dimension", dimension);

                    foreach (var obj in query.List()) {
                        var arr = (object[])obj;
                        string pattern = (string)arr[0];
                        string name = (string)arr[1];

                        if (!result.ContainsKey(name))
                            result[name] = new Pattern { a = 0, b = 0, c = 0, d = 0, Name = name };

                        result[name].Add(pattern);
                    }
                }
                else {
                    string sql;

                    if (characterId != 0)
                        sql = @"select p.pattern, p.name from items i 
                            left join pbPatterns p on i.lowkey = p.aoid
                            where lowkey in (select aoid from pbPatterns)
                            and i.owner = :owner";
                    else if (dimension >= 0)
                        sql = @"select p.pattern, p.name from items i 
                            left join pbPatterns p on i.lowkey = p.aoid
                            where lowkey in (select aoid from pbPatterns)
                            and i.owner in (select characterId from characters where dimension = :dimension)";
                    else
                        sql = @"select p.pattern, p.name from items i 
                            left join pbPatterns p on i.lowkey = p.aoid
                            where lowkey in (select aoid from pbPatterns)";

                    var query = session.CreateSQLQuery(sql);

                    if (sql.Contains(":owner"))
                        query.SetParameter("owner", characterId);

                    else if (sql.Contains(":dimension"))
                        query.SetParameter("dimension", dimension);

                    foreach (var obj in query.List()) {
                        var arr = (object[])obj;
                        string pattern = (string)arr[0];
                        string name = (string)arr[1];

                        if (!result.ContainsKey(name))
                            result[name] = new Pattern { a = 0, b = 0, c = 0, d = 0, Name = name };

                        result[name].Add(pattern);
                    }
                }

                /*if (!useHql) {
                    string hql;
                        hql = @"SELECT p.Name FROM Item i, PocketBoss p WHERE i.LowKey = p.Id AND i.LowKey IN (SELECT Id FROM PocketBoss)";

                    var criteria = session.CreateCriteria<Item>();
                    if (characterId != 0)
                        criteria.Add(Restrictions.Eq("Owner.Id", characterId));
                    else if (dimension >= 0)
                        criteria.Add(Restrictions.Eq("Owner.Dimension", dimension));


                    criteria.CreateCriteria("PocketBoss", "p", NHibernate.SqlCommand.JoinType.LeftOuterJoin);



                    foreach (var name in criteria.List<string>()) {
                        if (!result.ContainsKey(name))
                            result[name] = new Pattern { a = 0, b = 0, c = 0, d = 0, Name = name };

                        result[name].Add("ABCD");
                    }

                }
                else*/ {
                        string hql;
                        if (characterId != 0)
                            hql = @"SELECT p.Name FROM Item i, PocketBoss p WHERE i.LowKey = p.Id AND i.LowKey IN (SELECT Id FROM PocketBoss) AND i.Owner.Id = :owner";
                        else if (dimension >= 0)
                            hql = @"SELECT p.Name FROM Item i, PocketBoss p WHERE i.LowKey = p.Id AND i.LowKey IN (SELECT Id FROM PocketBoss) AND i.Owner.Dimension = :dimension";
                        else
                            hql = @"SELECT p.Name FROM Item i, PocketBoss p WHERE i.LowKey = p.Id AND i.LowKey IN (SELECT Id FROM PocketBoss)";

                        var query = session.CreateQuery(hql);

                        if (hql.Contains(":owner"))
                            query.SetParameter("owner", characterId);

                        else if (hql.Contains(":dimension"))
                            query.SetParameter("dimension", dimension);


                        foreach (var name in query.List<string>()) {
                            if (!result.ContainsKey(name))
                                result[name] = new Pattern { a = 0, b = 0, c = 0, d = 0, Name = name };

                            result[name].Add("ABCD");
                        }
                    }

            }

            return result;
        }




        public Dictionary<string, Pattern> GetAllPBPatternsOmni() {
            var omni = new int[] { 234386, 234280, 234170, 234179, 239678, 234188, 234271, 234318, 234309 };
            return GetAllPBPatterns(omni);
        }
        public Dictionary<string, Pattern> GetAllPBPatternsClan() {
            var clan = new int[] { 234280, 234170, 234125, 234197, 234224, 234152, 234251, 239489, 234327 };
            return GetAllPBPatterns(clan);

        }
        private Dictionary<string, Pattern> GetAllPBPatterns(int[] restrictions) {
            const string query = "SELECT Name FROM PocketBoss WHERE Id IN ( :ids ) ORDER BY Name";
            Dictionary<string, Pattern> result = new Dictionary<string, Pattern>();

            using (ISession session = SessionFactory.OpenSession()) {
                foreach (string name in session.CreateQuery(query).SetParameterList("ids", restrictions).List<string>()) {
                    result[name] = new Pattern { a = 0, b = 0, c = 0, d = 0 };
                }
            }
            return result;
        }


        /// <summary>
        /// Returns all patterns as an empty set
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, Pattern> GetAllPatterns() {
            const string query = "SELECT Name FROM PocketBoss ORDER BY Name";
            Dictionary<string, Pattern> result = new Dictionary<string, Pattern>();

            using (ISession session = SessionFactory.OpenSession()) {
                // TODO: Move this into a database upgrade class!
                using (ITransaction t = session.BeginTransaction()) {
                    session.CreateQuery("UPDATE Pattern SET Name = :name WHERE Id = 294003")
                        .SetParameter("name", "Wistful Apparition")
                        .ExecuteUpdate();

                    t.Commit();
                }
                foreach (string name in session.CreateQuery(query).List<string>()) {
                    result[name] = new Pattern { a = 0, b = 0, c = 0, d = 0 };
                }
            }
            return result;
        }




        /// <summary>
        /// Return the drop locations for a pocket boss
        /// </summary>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public IList<PocketBossLocation> GetPatternDropLocations(string pattern) {
            using (ISession session = SessionFactory.OpenSession()) {
                var criteria = session.CreateCriteria<PocketBossLocation>();
                criteria.Add(Restrictions.Eq("Name", pattern));
                criteria.AddOrder(Order.Asc("Name"));
                return criteria.List<PocketBossLocation>();
            }
        }
    }
}
