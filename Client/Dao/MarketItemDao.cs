﻿using ItemsLight.Model;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace ItemsLight.Dao {
    class MarketItemDao : BaseDao<object> {
        static ILog logger = LogManager.GetLogger(typeof(MarketItemDao));


        /// <summary>
        /// List every item a character owns on the GMI
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public IList<Item> ListGMI(long characterId) {
            using (ISession session = SessionFactory.OpenSession()) {
                var criteria = session.CreateCriteria<GMIItem>();
                criteria.CreateAlias("db", "d");



                criteria.CreateAlias("Owner", "o");
                criteria.Add(Restrictions.Eq("o.Id", characterId));
                
                var items = criteria.List<GMIItem>();
                List<Item> result = new List<Item>();
                foreach (var item in items) {
                    result.Add(
                        new Item {
                            Name = item.Name,
                            Owner = item.Owner,
                            QL = item.QL,
                            ParentContainerId = 0x100,
                            ParentContainerName = "GMI"
                        });
                }

                return result;
            }
        }

        /// <summary>
        /// Search for items owned on the GMI
        /// </summary>
        /// <param name="itemname"></param>
        /// <param name="dimension"></param>
        /// <returns></returns>
        public IList<Item> SearchForItems(string itemname, int dimension) {
            using (ISession session = SessionFactory.OpenSession()) {
                var criteria = session.CreateCriteria<GMIItem>();
                criteria.CreateAlias("db", "d");
                criteria.Add(Restrictions.InsensitiveLike("d.Name", string.Format("%{0}%", itemname.Replace(" ", "%"))));

                if (dimension > 0) {
                    criteria.CreateAlias("Owner", "o");
                    criteria.Add(Restrictions.Eq("o.Dimension", dimension));
                }
                var items = criteria.List<GMIItem>();
                List<Item> result = new List<Item>();
                foreach (var item in items) {
                    result.Add(
                        new Item {
                            Name = item.Name,
                            Owner = item.Owner,
                            QL = item.QL,
                            ParentContainerId = 0x100,
                            ParentContainerName = "GMI"
                        });
                }

                return result;
            }
        }



        /// <summary>
        /// Detect GMI items with no cached item name
        /// These items will need to be synchronized with the AO GMI
        /// </summary>
        /// <returns></returns>
        public IList<long> GetItemsMissingName() {
            using (ISession session = SessionFactory.OpenSession()) {
                string hql = "SELECT Id FROM GMIItem WHERE NOT Id IN (SELECT Id FROM GMIDatabaseItem)";
                var query = session.CreateQuery(hql);
                return query.List<long>();
            }
        }

        /// <summary>
        /// Store the name of a new item
        /// The IDs does not match that of the local item database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public void UpdateName(long id, string name) {
            using (ISession session = SessionFactory.OpenSession()) {
                var item = session.Get<GMIDatabaseItem>(id);
                using (ITransaction transaction = session.BeginTransaction()) {
                    if (item == null) {
                        item = new GMIDatabaseItem { Id = id, Name = name };
                        session.Save(item);
                    }
                    else {
                        item.Name = name;
                        session.Update(item);
                    }
                    transaction.Commit();
                }
            }
        }


        /// <summary>
        /// Store GMI items
        /// </summary>
        /// <param name="items"></param>
        public void SaveGMIItems(List<GMIItem> items, long characterId) {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    session.CreateQuery("DELETE FROM GMIItem WHERE Owner.Id = :owner")
                        .SetParameter("owner", characterId)
                        .ExecuteUpdate();

                    foreach (var item in items) {
                        session.Save(item);
                    }

                    transaction.Commit();
                }
            }
        }
    }
}
