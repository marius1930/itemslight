﻿using ItemsLight.AOIA;
using ItemsLight.AOIA.HelperStructs;
using ItemsLight.Model;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using NHibernate.Exceptions;
using EvilsoftCommons.Exceptions;

namespace ItemsLight.Dao {
    public class CharacterDao : BaseDao<Character> {
        static ILog logger = LogManager.GetLogger(typeof(CharacterDao));


        /// <summary>
        /// Get the character summary, for display on HTML summary page.
        /// </summary>
        /// <param name="dimension"></param>
        /// <returns></returns>
        public IList<Character> ListByDimensionOrderByLastSeen(int dimension) {
            try {
                using (ISession session = SessionFactory.OpenSession()) {
                    var criteria = session.CreateCriteria<Character>();
                    if (dimension >= 0)
                        criteria.Add(Restrictions.Eq("Dimension", dimension));
                    criteria.AddOrder(Order.Desc("LastSeen"));
                    return criteria.List<Character>();
                }
            }
            catch (GenericADOException ex) {
                ExceptionReporter.ReportException(ex, "ListByDimensionOrderByLastSeen");
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                return new List<Character>();
            }
        }

        /// <summary>
        /// Return the name of a given character
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public String GetName(long characterId) {
            using (ISession session = SessionFactory.OpenSession()) {
                var criteria = session.CreateCriteria<Character>();
                criteria.Add(Restrictions.Eq("Id", characterId));
                return criteria.UniqueResult<Character>().Name;
            }
        }

        /// <summary>
        /// List every character, and return them in a ComboBoxItem format
        /// </summary>
        /// <param name="dimension"></param>
        /// <returns></returns>
        public IList<ComboBoxItem> ListAllAsComboBox(int dimension) {
            try {
                using (ISession session = SessionFactory.OpenSession()) {
                    var criteria = session.CreateCriteria<Character>();
                    if (dimension >= 0)
                        criteria.Add(Restrictions.Eq("Dimension", dimension));

                    criteria.AddOrder(Order.Asc("Name"));
                    IList<Character> result = criteria.List<Character>();

                    List<ComboBoxItem> items = new List<ComboBoxItem>();
                    foreach (var item in result) {
                        items.Add(new ComboBoxItem(item));
                    }

                    return items;
                }
            }
            catch (GenericADOException ex) {
                ExceptionReporter.ReportException(ex, "ListAllAsComboBox");
                logger.Fatal(ex.Message);
                logger.Fatal(ex.StackTrace);
                System.Windows.Forms.MessageBox.Show("Oooops! You've run into an unresolved issue..\n\nPlease contact the developer on the AO forum.\n\nI have been trying to narrow down this issue, but I require your help!\n\nThank you.");
                return new List<ComboBoxItem>();
            }
        }


        /// <summary>
        /// Delete a character and all pertaining data
        /// Irrevocable operation
        /// </summary>
        /// <param name="characterId"></param>
        public void Delete(long characterId) {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    session.CreateQuery("DELETE FROM Item WHERE Owner.Id = :id").SetInt64("id", characterId).ExecuteUpdate();
                    session.CreateQuery("DELETE FROM GMIItem WHERE Owner.Id = :id").SetInt64("id", characterId).ExecuteUpdate();
                    session.CreateQuery("DELETE FROM Character WHERE Id = :id").SetInt64("id", characterId).ExecuteUpdate();
                    transaction.Commit();
                }
            }
        }

        /// <summary>
        /// Delete all characters and pertaining items
        /// Used for wiping the database prior to an AOIA import
        /// Irrevocable operation
        /// </summary>
        public void DeleteEverything() {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    session.CreateQuery("DELETE FROM Item").ExecuteUpdate();
                    session.CreateQuery("DELETE FROM GMIItem").ExecuteUpdate();
                    session.CreateQuery("DELETE FROM Character").ExecuteUpdate();
                    transaction.Commit();
                }
            }
        }

        /// <summary>
        /// Set a character as dimension=500, this marks him as "archived" (magic number)
        /// </summary>
        /// <param name="characterId"></param>
        public void AchieveCharacter(long characterId) {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    session.CreateQuery("UPDATE Character SET Dimension = 500 WHERE Id = :id").SetInt64("id", characterId).ExecuteUpdate();
                    transaction.Commit();
                }
            }

        }

        /// <summary>
        /// List every dimension available
        /// </summary>
        /// <returns></returns>
        public ICollection<int> ListAllDimensions() {
            try {
                using (ISession session = SessionFactory.OpenSession()) {
                    return new HashSet<int>(session.CreateQuery("SELECT Dimension FROM Character WHERE Dimension IS NOT NULL").List<int>());
                }
            }
            // This should now be impossible, was related to the missing IS NOT NULL
            catch (GenericADOException ex) {
                ExceptionReporter.ReportException(ex, "ListAllDimensions");
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                return new HashSet<int>(new int[] { 0 });
            }
        }


        public Dictionary<long, String> ListAll(int dimension = -1) {
            Dictionary<long, String> result = new Dictionary<long, String>();
            foreach (Character character in ListByDimensionOrderByLastSeen(dimension)) {
                result[character.Id] = character.Name;
            }
            return result;
        }

        
        /// <summary>
        /// Get the character summary, for display on HTML summary page.
        /// </summary>
        /// <param name="dimension"></param>
        /// <returns></returns>
        public List<Character> GetCharacterSummary(int dimension) {
            List<Character> result;
            try {
                using (ISession session = SessionFactory.OpenSession()) {
                    Dictionary<long, long> numberOfBackpacks = new Dictionary<long, long>();
                    Dictionary<long, long> numberOfItems = new Dictionary<long, long>();

                    {
                        string numberOfBackpacksHql = "SELECT COUNT(*), Owner.Id FROM Item WHERE (LowKey IN ( SELECT Id FROM BackpackIdentifier )) GROUP BY Owner.Id";
                        var query = session.CreateQuery(numberOfBackpacksHql);

                        foreach (object[] row in query.List<object[]>()) {
                            if (row[0] != null && row[1] != null)
                                numberOfBackpacks[(long)row[1]] = (long)row[0];
                        }
                    }
                    {
                        string numberOfItemsHql = "SELECT COUNT(*), Owner.Id FROM Item WHERE NOT (LowKey IN ( SELECT Id FROM BackpackIdentifier )) GROUP BY Owner.Id";
                        var query = session.CreateQuery(numberOfItemsHql);

                        foreach (object[] row in query.List<object[]>()) {
                            if (row[0] != null && row[1] != null)
                                numberOfItems[(long)row[1]] = (long)row[0];
                        }
                    }

                    // Grab the characters
                    var criteria = session.CreateCriteria<Character>();
                    if (dimension >= 0) {
                        criteria.Add(Restrictions.Eq("Dimension", dimension));
                    }
                    criteria.AddOrder(Order.Desc("LastSeen"));

                    result = new List<Character>(criteria.List<Character>());

                    for (int i = 0; i < result.Count; i++) {
                        if (numberOfBackpacks.ContainsKey(result[i].Id))
                            result[i].NumContainers = numberOfBackpacks[result[i].Id];

                        if (numberOfItems.ContainsKey(result[i].Id))
                            result[i].NumItems = numberOfItems[result[i].Id];
                    }
                }
            }
            catch (GenericADOException ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                ExceptionReporter.ReportException(ex, "GetCharacterSummary");
                result = new List<Character>();
            }

            return result;
        }

        /// <summary>
        /// Update the name and dimension of a character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public void UpdateNameAndDimension(long characterId, string name, int dimension) {
            using (ISession session = SessionFactory.OpenSession()) {
                var character = session.Get<Character>(characterId);
                try {
                    using (ITransaction transaction = session.BeginTransaction()) {
                        if (character == null) {
                            character = new Character {
                                Id = characterId,
                                Name = name,
                                Dimension = dimension,
                                LastSeen = DateTime.Now
                            };
                            session.Save(character);
                        }
                        else {
                            character.Name = name;
                            character.Dimension = dimension;
                            session.Update(character);
                        }
                        transaction.Commit();
                    }
                }
                catch (GenericADOException ex) {
                    string message = string.Format("UpdateNameAndDimension({0}, {1}, {2}) with character = {3}", characterId, name, dimension, character);
                    ExceptionReporter.ReportException(ex, message);
                    logger.Warn(message);
                    logger.Warn(ex.Message);
                    logger.Warn(ex.StackTrace);
                }
            }
        }

        public void UpdateCredits(long characterId, long credits) {
            String query = "UPDATE Character SET Credits = :value WHERE Id = :owner";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("value", credits)
            };
            SingleQuery(query, parameters);
        }
        public void UpdateGMICredits(long characterId, long credits) {
            String query = string.Format("UPDATE Character SET CreditsGMI = :value WHERE Id = :owner", credits, characterId);
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("value", credits)
            };
            SingleQuery(query, parameters);
        }
        public void UpdateLevel(long characterId, int level) {
            String query = string.Format("UPDATE Character SET Level = :value WHERE Id = :owner", level, characterId);
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("value", level)
            };
            SingleQuery(query, parameters);
        }
        public void UpdateVP(long characterId, long vp) {
            String query = string.Format("UPDATE Character SET VP = :value WHERE Id = :owner", vp, characterId);
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("value", vp)
            };
            SingleQuery(query, parameters);
        }
        public void UpdateProfession(long characterId, int profession) {
            String query = string.Format("UPDATE Character SET ProfessionId = :value WHERE Id = :owner", profession, characterId);
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("value", profession)
            };
            SingleQuery(query, parameters);
        }


        /// <summary>
        /// Updates the character positions in DB
        /// Data is retrieved from the PositionTracker class
        /// </summary>
        public void UpdateCharacterPositionsFromPositionTracker() {
            const string hql = "UPDATE Character SET ZoneId = :zoneid, SubZoneId = :subzoneid, PositionX = :x, PositionZ = :z, LastSeen = :lastseen WHERE Id = :id";


            try {
                using (ISession session = SessionFactory.OpenSession()) {
                    var query = session.CreateQuery(hql);
                    using (ITransaction transaction = session.BeginTransaction()) {
                        foreach (var elem in PositionTracker.RecentPositions) {                            
                            query.SetParameter("zoneid", elem.zoneid);
                            query.SetParameter("subzoneid", elem.subzoneid);
                            query.SetParameter("x", elem.x);
                            query.SetParameter("z", elem.z);
                            query.SetParameter("lastseen", new DateTime(1970, 1, 1).AddSeconds(elem.timestamp));
                            query.SetParameter("id", elem.characterId);
                            query.ExecuteUpdate();
                        }


                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                ExceptionReporter.ReportException(ex, "Character position tracker");
            }
        }


        /// <summary>
        /// Save a collection of new characters
        /// </summary>
        /// <param name="items"></param>
        public void Save(ICollection<Character> items) {
            try {
                using (ISession session = SessionFactory.OpenSession()) {
                    using (ITransaction transaction = session.BeginTransaction()) {
                        foreach (Character character in items) {
                            session.Save(character);
                        }
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex) {
                logger.Error(ex.Message);
                logger.Error(ex.StackTrace);
                ExceptionReporter.ReportException(ex, "Save Characters");
            }
        }

    }
}
