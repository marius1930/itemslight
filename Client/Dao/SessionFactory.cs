﻿using NHibernate;
using NHibernate.Cfg;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using NHibernate.Tool.hbm2ddl;

namespace ItemsLight.Dao {
    public class SessionFactory {
        private static ISessionFactory _sessionFactory;
#if DEBUG
        public static string DATABASE_FILE = "db1-test.sdf";
        public static string USERDATA_FILE = "userdata-test.db";
#else 
        public static string DATABASE_FILE = "db1.sdf";
        public static string USERDATA_FILE = "userdata.db";
#endif

        private static string ConnectionString {
            get {
                return string.Format("Data Source = {0};Version=3", UserdataLocation);
            }
        }

        [Obsolete]
        public static string DatabaseLocation {
            get {
                String appdata = System.Environment.GetEnvironmentVariable("LocalAppData");
                string path = Path.Combine(appdata, "EvilSoft", "IALight", "data");
                Directory.CreateDirectory(path);
                return Path.Combine(path, DATABASE_FILE);
            }
        }

        public static string UserdataLocation {
            get {
                String appdata = System.Environment.GetEnvironmentVariable("LocalAppData");
                string path = Path.Combine(appdata, "EvilSoft", "IALight", "data");
                Directory.CreateDirectory(path);

                // If the file for whatever reason does not exist, copy in a replacement
                string file = Path.Combine(path, USERDATA_FILE);
                if (!File.Exists(file) && File.Exists("restore")) {
                    File.Copy("restore", file);
                }

                return file;
            }
        }


        private static ISessionFactory sessionFactory {
            get {
                if (_sessionFactory == null) {
                    var configuration = new Configuration();
                    configuration.Configure();
                    configuration.AddAssembly(Assembly.GetExecutingAssembly());
                    configuration.SetProperty("connection.connection_string", ConnectionString);
                    new SchemaUpdate(configuration).Execute(true, true);
                    _sessionFactory = configuration.BuildSessionFactory();
                }
                return _sessionFactory;
            }
        }

        public static ISession OpenSession() {
            return sessionFactory.OpenSession();
        }
        public static IStatelessSession OpenStatelessSession() {
            return sessionFactory.OpenStatelessSession();
        }
    }
}
