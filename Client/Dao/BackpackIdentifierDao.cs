﻿using ItemsLight.Model;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Dao {
    public class BackpackIdentifierDao : BaseDao<BackpackIdentifier> {

        public void UpdateBackpackDatabase() {
            HashSet<long> aoid = new HashSet<long>();

            const string query = @"SELECT LowKey FROM Item i
                            WHERE (
                                (Children > 0 AND EXISTS (SELECT Id FROM Item i2 WHERE i2.ParentContainerId = i.Children))
                                OR LowKey IN (SELECT Id FROM DatabaseItem WHERE Flags = 1 AND ISlot = 8)
                            ) 
                            AND NOT LowKey IN (select Id FROM BackpackIdentifier)";


            using (ISession session = SessionFactory.OpenSession()) {
                var aoids = session.CreateQuery(query).List<long>();

                using (ITransaction transaction = session.BeginTransaction()) {
                    foreach (long id in aoids) {
                        session.Save(new BackpackIdentifier { Id = id });
                    }

                    transaction.Commit();
                }
            }


        }

    }
}
