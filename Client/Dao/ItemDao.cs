﻿using ItemsLight.Model;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using ItemsLight.AOIA;
using ItemsLight.AOIA.Misc;
using ItemsLight.AOIA.Parsers;
using NHibernate.Exceptions;
using NHibernate.Transform;
using EvilsoftCommons.Exceptions;

namespace ItemsLight.Dao {
    class ItemDao : BaseDao<Item> {
        static ILog logger = LogManager.GetLogger(typeof(ItemDao));

        /// <summary>
        /// Lists all the containers in the database (for updating treeview)
        /// </summary>
        /// <param name="dimension">Dimension ID to filter by, if any</param>
        /// <returns></returns>
        public IList<Item> ListAllContainers(int dimension = -1) {
            using (ISession session = SessionFactory.OpenSession()) {

                if (dimension >= 0) {
                    IList<long> charactersInDimension = session.CreateQuery("SELECT Id FROM Character WHERE Dimension = :dimension")
                        .SetParameter("dimension", dimension).List<long>();

                    String hql = "FROM Item WHERE LowKey IN ( SELECT Id FROM BackpackIdentifier ) AND Owner.Id IN ( :characters )";
                    var query = session.CreateQuery(hql);
                    query.SetParameterList("characters", charactersInDimension);
                    return query.List<Item>();
                }
                else {
                    String hql = "FROM Item WHERE LowKey IN ( SELECT Id FROM BackpackIdentifier )";
                    var query = session.CreateQuery(hql);
                    return query.List<Item>();
                }
            }
        }


        /// <summary>
        /// Get the item in a specific inventory (equipped) slot
        /// Will return null if empty/unknown
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="slot"></param>
        /// <returns></returns>
        public Item GetByOwnerAndSlot(long characterId, int slot) {
            using (ISession session = SessionFactory.OpenSession()) {
                String hql = "FROM Item WHERE Slot = :slot AND Owner.Id = :owner AND ParentContainerId = 2";
                var query = session.CreateQuery(hql);
                query.SetParameter("owner", characterId);
                query.SetParameter("slot", slot);
                foreach (var item in query.List<Item>())
                    return item;
            }
            return null;
        }

        public IList<Item> ListContainersFor(long characterId, long containerId) {
            using (ISession session = SessionFactory.OpenSession()) {
                String hql = "FROM Item WHERE LowKey IN ( SELECT Id FROM BackpackIdentifier ) AND Owner.Id = :owner AND ParentContainerId = :parent";
                var query = session.CreateQuery(hql);
                query.SetParameter("owner", characterId);
                query.SetParameter("parent", containerId);
                return query.List<Item>();               
            }
        }

        private List<Item> IMapNamesToItems(IList<Item> items) {
            return new List<Item>(items);
        }
        private List<Item> MapNamesToItems(List<Item> items) {
            /*for (int i = 0; i < items.Count; i++) {
                items[i].Name = MemoryDB.Instance.GetNameForItem(items[i].Id);
            }*/
            return items;
        }


        public int FindFirstItemOfSameType(long charId, long containerId, uint slot, long containerIdToSearchIn) {
            const string hql = @"SELECT target.Slot FROM Item target, Item src WHERE src.Owner.Id = :owner 
                                    AND src.ParentContainerId = :container AND src.Slot = :slot AND target.LowKey = src.LowKey 
                                    AND target.QL = src.QL AND target.Owner.Id = :owner AND target.ParentContainerId = :containerSearchIn 
                                    ORDER BY target.Slot";
            try {
                using (ISession session = SessionFactory.OpenSession()) {
                    var result = session.CreateQuery(hql)
                        .SetParameter("owner", charId)
                        .SetParameter("container", containerId)
                        .SetParameter("slot", slot)
                        .SetParameter("containerSearchIn", containerIdToSearchIn)
                        .SetMaxResults(1)
                        .UniqueResult();

                    if (result != null) {
                        return (int)result;
                    }
                }
            }
            
            catch (InvalidCastException ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                ExceptionReporter.ReportException(ex, "FindFirstItemOfSameType");
            }
            catch (GenericADOException ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                ExceptionReporter.ReportException(ex, string.Format("FindFirstItemOfSameType({0}, {1}, {2}, {3})", charId, containerId, slot, containerIdToSearchIn));
            }

            return 0;
        }


        public long GetNumContainers() {
            using (ISession session = SessionFactory.OpenSession()) {
                String hql = "SELECT COUNT(Id) FROM Item WHERE LowKey IN ( SELECT Id FROM BackpackIdentifier )";
                return session.CreateQuery(hql)
                    .UniqueResult<long>();
            }
        }

        /// <summary>
        /// For showing the container contents in the UI
        /// </summary>
        /// <param name="containerId">ID of the container</param>
        /// <param name="characterId">Character ID, required for listing the "inventory container"</param>
        /// <returns></returns>
        public List<Item> ListContainerContent(long containerId, long characterId = 0) {
            
            using (ISession session = SessionFactory.OpenSession()) {
                var criteria = session.CreateCriteria<Item>();

                // Inventory container, need to query by specific character.
                if (containerId < 14 && characterId != 0) {
                    criteria.CreateAlias("Owner", "o");
                    criteria.Add(Restrictions.Eq("o.Id", characterId));
                }


                if (containerId == (uint)InventoryIds.INV_TOONINV) {
                    criteria.Add(Restrictions.Ge("Slot", 64));
                    var subquery = Subqueries.PropertyNotIn("LowKey", DetachedCriteria.For<BackpackIdentifier>().SetProjection(Projections.Property("Id")));
                    criteria.Add(subquery);
                }

                // Special tabs
                else if (containerId >= 10 && containerId <= 13) {
                    
                    if (containerId == (uint)InventoryIds.VIRTUAL_WEAPONS)
                        criteria.Add(Restrictions.Lt("Slot", 16));
                    else if (containerId == (uint)InventoryIds.VIRTUAL_ARMOR)
                        criteria.Add(Restrictions.And(Restrictions.Ge("Slot", 16), Restrictions.Lt("Slot", 32)));                        
                    else if (containerId == (uint)InventoryIds.VIRTUAL_IMPLANTS)
                        criteria.Add(Restrictions.And(Restrictions.Ge("Slot", 32), Restrictions.Lt("Slot", 47)));
                    else if (containerId == (uint)InventoryIds.VIRTUAL_SOCIAL)
                        criteria.Add(Restrictions.And(Restrictions.Ge("Slot", 47), Restrictions.Lt("Slot", 64)));

                    // It's actually stored under inventory
                    containerId = 2;
                }
                // Don't list containers as "Items" under bank
                else if (containerId == (uint)InventoryIds.INV_BANK) {
                    var subquery = Subqueries.PropertyNotIn("LowKey", DetachedCriteria.For<BackpackIdentifier>().SetProjection(Projections.Property("Id")));
                    criteria.Add(subquery);
                }

                // Do this last, since "containerId" may get re-mapped
                if (containerId != 0)
                    criteria.Add(Restrictions.Eq("ParentContainerId", containerId));

                return IMapNamesToItems(criteria.List<Item>());
            }
        }


        /// <summary>
        /// Get every item ID which exists in the database
        /// This narrows down the search space when doing wildcard searches on names
        /// </summary>
        /// <returns></returns>
        public ICollection<long> GetAllItemIds() {
            using (ISession session = SessionFactory.OpenSession()) {
                List<long> itemIds = new List<long>();
                itemIds.AddRange(session.CreateQuery("SELECT LowKey FROM Item").List<long>());
                itemIds.AddRange(session.CreateQuery("SELECT HighKey FROM Item").List<long>());
                return new HashSet<long>(itemIds);
            }
        }



        /// <summary>
        /// List all items owned, missing from the central item database
        /// </summary>
        /// <returns></returns>
        public IList<Int64> ListAllItemsMissingFromDB() {
            const String hql = "SELECT LowKey FROM Item i WHERE NOT EXISTS (SELECT Id FROM DatabaseItem db WHERE db.Id = i.LowKey)";

            using (ISession session = SessionFactory.OpenSession()) {
                return session.CreateQuery(hql).List<Int64>();
            }
        }


        /// <summary>
        /// Items given away, container here is most likely the trade window.
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="containerId"></param> 
        public void DeleteFromContainer(long characterId, long containerId) {
            String query = "DELETE FROM Item WHERE Owner.Id = :owner AND ParentContainerId = :container";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("container", containerId) 
            };
            SingleQuery(query, parameters);
        }

        public void DeleteFromContainer(long containerId) {
            String query = "DELETE FROM Item WHERE ParentContainerId = :container";
            var parameters = new Parameter[] { 
                new Parameter("container", containerId) 
            };
            SingleQuery(query, parameters);
        }


        /// <summary>
        /// Delete from trade container, two possible parents.
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="containerId1"></param>
        /// <param name="containerId2"></param>
        public void DeleteItemsInTradeContainer(long characterId, long containerId1, long containerId2) {
            String query = "DELETE FROM Item WHERE Owner.Id = :owner AND (ParentContainerId = :containerA OR ParentContainerId = :containerB)";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("containerA", containerId1),
                new Parameter("containerB", containerId2) 
            };
            SingleQuery(query, parameters);
        }




        public static Item CreateItem(long keylow, long keyhigh, int ql, long flags, ushort stack, long parent, int slot, long children, long owner) {
            return new Item {
                LowKey = keylow,
                HighKey = keyhigh,
                QL = ql,
                Flags = flags,
                Stack = stack,
                ParentContainerId = parent,
                Slot = slot,
                Children = children,
                CharacterId = owner
            };
        }

        /// <summary>
        /// Map the internal database item for a collection of items
        /// 
        /// </summary>
        /// <param name="item"></param>
        public void MapInternals(IList<Item> items) {
            try {
                using (ISession session = SessionFactory.OpenSession()) {
                    for (int i = 0; i < items.Count; i++) {
                        items[i].InternalHigh = session.Get<DatabaseItem>(items[i].HighKey);
                        items[i].InternalLow = session.Get<DatabaseItem>(items[i].LowKey);
                    }                    
                }
            }
            catch (Exception ex) {
                logger.Error(ex.Message);
                logger.Error(ex.StackTrace);
                ExceptionReporter.ReportException(ex, "Map internals");
            }
        }

        /// <summary>
        /// Save a new item
        /// </summary>
        /// <param name="item"></param>
        public override void Save(Item item) {
            try {
                using (ISession session = SessionFactory.OpenSession()) {
                    using (ITransaction transaction = session.BeginTransaction()) {
                        session.Save(item);
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex) {
                logger.Error(ex.Message);
                logger.Error(ex.StackTrace);
                ExceptionReporter.ReportException(ex, "Save Item");
            }
        }

        /// <summary>
        /// Save a collection of new items
        /// </summary>
        /// <param name="items"></param>
        public void Save(ICollection<Item> items) {
            try {
                using (ISession session = SessionFactory.OpenSession()) {
                    var query = session.CreateSQLQuery("INSERT INTO items (lowkey, highkey, ql, Flags, Stack, Slot, parent, Children, owner) VALUES (:p0, :p1, :p2, :p3, :p4, :p5, :p6, :p7, :p8)");

                    using (ITransaction transaction = session.BeginTransaction()) {
                        foreach (Item item in items) {
                            var characterId = item.CharacterId;
                            if (characterId == 0 && item.Owner != null)
                                characterId = item.Owner.Id;
                            query.SetParameter("p0", item.LowKey);
                            query.SetParameter("p1", item.HighKey);
                            query.SetParameter("p2", item.QL);
                            query.SetParameter("p3", item.Flags);
                            query.SetParameter("p4", item.Stack);
                            query.SetParameter("p5", item.Slot);
                            query.SetParameter("p6", item.ParentContainerId);
                            query.SetParameter("p7", item.Children);
                            query.SetParameter("p8", characterId);
                            query.ExecuteUpdate();
                        }
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex) {
                logger.Error(ex.Message);
                logger.Error(ex.StackTrace);
                ExceptionReporter.ReportException(ex, "Save Items");
            }
        }




        /// <summary>
        /// Update the parent and slot for a given item
        /// </summary>
        /// <param name="newParent"></param>
        /// <param name="newSlot"></param>
        /// <param name="fromContainerId"></param>
        /// <param name="newslot"></param>
        /// <param name="characterId"></param>
        public void UpdateParentAndSlotForItem(long newParent, uint newSlot, long fromContainerId, uint slot, long characterId) {
            String query = "UPDATE Item SET ParentContainerId = :newParent, Slot = :newSlot WHERE ParentContainerId = :currentParent AND Slot = :currentSlot AND Owner.Id = :owner";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("newParent", newParent),
                new Parameter("newSlot", newSlot),
                new Parameter("currentParent", fromContainerId),
                new Parameter("currentSlot", slot)
            };
            SingleQuery(query, parameters);
        }

        public void RemoveOldContentForCharacter(long characterId) {
            String query = "DELETE FROM Item WHERE (ParentContainerId = 2 or ParentContainerId >= 4 and ParentContainerId <= 8) AND Owner.Id = :owner";
            SingleQuery(query, new Parameter("owner", characterId));
        }

        public void RemoveItemFromTrade(uint nextFreeInvSlot, long shopContainer, uint fromItemSlotId, long characterId) {
            String query = "UPDATE Item SET ParentContainerId = 2, Slot = :newSlot WHERE ParentContainerId = :container AND Slot = :slot AND Owner.Id = :owner";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("container", shopContainer),
                new Parameter("newSlot", nextFreeInvSlot),
                new Parameter("slot", fromItemSlotId)
            };    
            SingleQuery(query, parameters);
        }

        public void RemoveItemFromTemporaryShopTradePartner(long characterId, long shopContainer) {
            String query = "DELETE FROM Item WHERE Owner.Id = :owner AND ParentContainerId IN (SELECT Children FROM Item WHERE parent = :container AND Children > 0 AND Owner.Id = :owner)";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("container", shopContainer)
            };
            SingleQuery(query, parameters);
        }

        public void DeleteOrphanedContainersForCharacter(long characterId) {
            String query = "DELETE FROM Item WHERE Owner.Id = :owner AND ParentContainerId > 3 AND ParentContainerId NOT IN (SELECT DISTINCT Children FROM Item)";
            SingleQuery(query, new Parameter("owner", characterId));
        }

        public void RemoveBackpack(long characterId, long containerId) {
            String query = "DELETE FROM Item WHERE Owner.Id = :owner AND Children = :container";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("container", containerId)
            };
            SingleQuery(query, parameters);
        }

        public void RemoveBackpackFromTradeWindow(long characterId, long containerId, long otherTradeContainer) {
            String query = "DELETE FROM Item WHERE Owner.Id = :owner AND Children = :children AND ParentContainerId = :parent";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("parent", otherTradeContainer),
                new Parameter("children", containerId)
            };
            SingleQuery(query, parameters);
        }

        public void RemoveWhereParentSlotOwner(long characterId, uint slot, long otherTradeContainer) {
            String query = "DELETE FROM Item WHERE Owner.Id = :owner AND Slot = :slot AND ParentContainerId = :parent";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("parent", otherTradeContainer),
                new Parameter("slot", slot)
            };
            SingleQuery(query, parameters);
        }

        public void RemoveBankItems(long characterId) {
            String query = "DELETE FROM Item WHERE Owner.Id = :owner AND ParentContainerId = 1";
            SingleQuery(query, new Parameter("owner", characterId));
        }

        public void DeleteAtSlotInContainer(long characterId, long containerId, uint slot) {
            String query = "DELETE FROM Item WHERE Owner.Id = :owner AND ParentContainerId = :parent AND Slot = :slot";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("parent", containerId),
                new Parameter("slot", slot) 
            };
            SingleQuery(query, parameters);
        }

        public void SplitStacks(long characterId, uint val, long fromContainerId, uint fromItemSlotId) {
            String query = "UPDATE Item SET Stack = Stack - :count WHERE Owner.Id = :owner AND ParentContainerId = :parent AND Slot = :slot";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("parent", fromContainerId),
                new Parameter("slot", fromItemSlotId),
                new Parameter("count", val) 
            };
            SingleQuery(query, parameters);
        }

        /// <summary>
        /// Update the parent for a given item
        /// </summary>
        /// <param name="toContainer"></param>
        /// <param name="fromContainerId"></param>
        /// <param name="itemSlotId"></param>
        /// <param name="characterId"></param>
        public void UpdateParentForItem(long toContainer, long fromContainerId, uint itemSlotId, long characterId) {
            String query = "UPDATE Item SET ParentContainerId = :newContainerId WHERE ParentContainerId = :container and Slot = :slot and Owner.Id = :owner";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("container", fromContainerId),
                new Parameter("newContainerId", toContainer),
                new Parameter("slot", itemSlotId) 
            };
            SingleQuery(query, parameters);
        }





        public void DeleteSpecificItemInSlot(long characterId, long fromContainerId, long lowId, long highId, uint slot) {
            String query = "DELETE FROM Item WHERE Owner.Id = :owner AND ParentContainerId = :container AND LowKey = :low AND HighKey = :high AND Slot = :slot";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("container", fromContainerId),
                new Parameter("low", lowId),
                new Parameter("high", highId),
                new Parameter("slot", slot) 
            };
            SingleQuery(query, parameters);
        }


        public void ReduceStack(long characterId, long fromContainerId, uint fromItemSlotId, long keyhigh, long keylow) {
            String query = @"UPDATE Item SET Stack = Stack - 1 
                    WHERE Owner.Id = :owner 
                    AND ParentContainerId = :container 
                    AND Slot = :slot
                    AND HighKey = :high AND LowKey = :low
                    AND LowKey IN (SELECT Id FROM DatabaseItem WHERE Id = :low AND (flags & 4196368) != 0)";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("container", fromContainerId),
                new Parameter("slot", fromItemSlotId),
                new Parameter("high", keyhigh),
                new Parameter("low", keylow) 
            };
            SingleQuery(query, parameters);
        }





        public void MergeItemStacks(long characterId, long removedItemContainerId, uint removedItemSlotId, long fromContainerId, uint fromItemSlotId) {
            uint stack = 0;

            using (ISession session = SessionFactory.OpenSession()) {
                {
                    const string hql = "SELECT Stack FROM Item WHERE Owner.Id = :owner AND ParentContainerId = :container AND Slot = :slot";
                    var parameters = new Parameter[] { 
                                    new Parameter("owner", characterId), 
                                    new Parameter("container", removedItemContainerId),
                                    new Parameter("slot", removedItemSlotId) 
                                };
                    var query = session.CreateQuery(hql);
                    query.SetParameter("owner", characterId);
                    query.SetParameter("container", removedItemContainerId);
                    query.SetParameter("slot", removedItemSlotId);

                    stack = query.UniqueResult<uint>();
                }
                {
                    const string hql = "UPDATE Item SET Stack = (stack + :amount) WHERE Owner.Id = :owner AND ParentContainerId = :container AND Slot = :slot";

                    var query = session.CreateQuery(hql);
                    query.SetParameter("amount", stack);
                    query.SetParameter("owner", characterId);
                    query.SetParameter("container", fromContainerId);
                    query.SetParameter("slot", fromItemSlotId);

                    using (ITransaction transaction = session.BeginTransaction()) {
                        query.ExecuteUpdate();
                        transaction.Commit();
                    }

                }
            }


        }


        /// <summary>
        /// Find the next available slot in a given container
        /// </summary>
        /// <param name="characterId">CharacterID, in case the container is Bank or Inventory</param>
        /// <param name="containerId"></param>
        /// <returns></returns>
        public uint FindNextAvailableContainerSlot(long characterId, long containerId) {
            ushort posSlot = 0;
            if (containerId == 2)
                posSlot = 64; //start at slot 64 for inventory, first ones are reserved for equipment tabs!

            String hql = "SELECT Slot FROM Item WHERE ParentContainerId = :container AND Slot >= :slot AND Owner.Id = :owner ORDER BY Slot";

            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("container", containerId),
                new Parameter("slot", posSlot),
            };

            
            using (ISession session = SessionFactory.OpenSession()) {
                var query = session.CreateQuery(hql);
                foreach (var binding in parameters) {
                    query.SetParameter(binding.field, binding.value);
                }

                foreach (var slot in query.List<int>()) {
                    if (posSlot < slot) {
                        return posSlot; // We've found a free slot in-between items
                    }
                    posSlot++;
                }
            }

            return posSlot;
        }



        /// <summary>
        /// Get item properties, like stackability.
        /// </summary>
        /// <param name="charId"></param>
        /// <param name="containerId"></param>
        /// <param name="slot"></param>
        /// <returns></returns>
        public long GetItemProperties(long characterId, long containerId, int slot) {
            string hql = "SELECT PropertyFlags FROM DatabaseItem WHERE Id IN (SELECT LowKey FROM Item WHERE Owner.Id = :owner AND ParentContainerId = :container AND Slot = :slot)";
            
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("container", containerId),
                new Parameter("slot", slot)
            };

            using (ISession session = SessionFactory.OpenSession()) {
                var query = session.CreateQuery(hql);
                foreach (var binding in parameters) {
                    query.SetParameter(binding.field, binding.value);
                }

                foreach (var property in query.List<long>()) {
                    return property;
                }
            }

            return 0;
        }

        /// <summary>
        /// Update the amount of items in a stack by the amount in another stack
        /// (This is called as step 1/2 in a merge-stack operation)
        /// Neglecting to call step #2 will result in a duplication of the stack [in the local db]re
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="fromContainerId"></param>
        /// <param name="slot"></param>
        /// <param name="newParent"></param>
        /// <param name="newSlotId"></param>
        public void UpdateAmountInStack(long characterId, long fromContainerId, uint slot, long newParent, uint newSlotId) {
            string hqlSubquery = "SELECT Stack FROM Item WHERE Owner.Id = :owner AND ParentContainerId = :container AND Slot = :slot";
           

            using (ISession session = SessionFactory.OpenSession()) {
                var subquery = session.CreateQuery(hqlSubquery);
                subquery.SetParameter("owner", characterId);
                subquery.SetParameter("container", fromContainerId);
                subquery.SetParameter("slot", slot);
                uint subqueryResult = subquery.UniqueResult<uint>();

                
                string hql = "UPDATE Item SET Stack = (Stack + :amount) WHERE Owner.Id = :owner AND ParentContainerId = :container AND Slot = :slot";
                using (ITransaction transaction = session.BeginTransaction()) {
                    var query = session.CreateQuery(hql);

                    query.SetParameter("owner", characterId);
                    query.SetParameter("container", newParent);
                    query.SetParameter("slot", newSlotId);
                    query.SetParameter("amount", subqueryResult);
                    query.ExecuteUpdate();
                    transaction.Commit();
                }
            }
        }


        /// <summary>
        /// Delete an empty stack
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="fromContainerId"></param>
        /// <param name="fromItemSlotId"></param>
        /// <param name="keyhigh"></param>
        /// <param name="keylow"></param>
        public void DeleteEmptyStack(long characterId, long fromContainerId, uint fromItemSlotId, long keyhigh, long keylow) {
            String query = @"DELETE FROM Item
                                WHERE Stack = 0 AND ParentContainerId = :container AND Slot = :slot AND Owner.Id = :owner
                                AND HighKey = :high AND LowKey = :low
                                AND LowKey IN (SELECT Id FROM DatabaseItem WHERE Id = :low AND (flags & 4196368) != 0)";
            var parameters = new Parameter[] { 
                new Parameter("owner", characterId), 
                new Parameter("container", fromContainerId),
                new Parameter("slot", fromItemSlotId),
                new Parameter("high", keyhigh),
                new Parameter("low", keylow) 
            };
            SingleQuery(query, parameters);
        }


        /// <summary>
        /// Search for items based on their item ID
        /// This is the primary search used in IA
        /// </summary>
        /// <param name="itemKeys"></param>
        /// <param name="characterId"></param>
        /// <param name="dimension"></param>
        /// <param name="minQl"></param>
        /// <param name="maxQl"></param>
        /// <returns></returns>
        public List<Item> ListByID(long itemKey, long characterId, int dimension, int minQl, int maxQl) {
            using (IStatelessSession session = SessionFactory.OpenStatelessSession()) {
                var result = new List<Item>();


                var criteria = session.CreateCriteria<Item>();
                criteria.Add(Restrictions.Ge("QL", minQl));
                criteria.Add(Restrictions.Le("QL", maxQl));


                criteria.Add(Restrictions.Or(
                    Restrictions.Eq("LowKey", itemKey),
                    Restrictions.Eq("HighKey", itemKey)
                    ));

                if (characterId != 0)
                    criteria.Add(Restrictions.Eq("Owner.Id", characterId));

                if (dimension >= 0) {
                    criteria.CreateAlias("Owner", "o");
                    criteria.Add(Restrictions.Eq("o.Dimension", dimension));
                }

                result.AddRange(criteria.List<Item>());


                return result;
            }
        }

        /// <summary>
        /// List items by their ID
        /// OBS: NOT LOWKEY OR HIGHKEY, BUT ID!
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public IList<Item> ListByIDs(List<long> ids) {
            using (IStatelessSession session = SessionFactory.OpenStatelessSession()) {
                
                var criteria = session.CreateCriteria<Item>();
                criteria.Add(Restrictions.In("Id", ids));
                return criteria.List<Item>();
            }
        }

        /// <summary>
        /// Search for items based on their item ID
        /// This is the primary search used in IA
        /// </summary>
        /// <param name="itemKeys"></param>
        /// <param name="characterId"></param>
        /// <param name="dimension"></param>
        /// <param name="minQl"></param>
        /// <param name="maxQl"></param>
        /// <returns></returns>
        public IList<Item> ListByName(string searchParameters, long characterId, int dimension, int minQl, int maxQl) {
            using (ISession session = SessionFactory.OpenSession()) {

                var criteria = session.CreateCriteria<Item>();
                criteria.Add(Restrictions.Ge("QL", minQl));
                criteria.Add(Restrictions.Le("QL", maxQl));

                var searchString = string.Format("%{0}%", searchParameters).Replace(" ", "%").Replace("*", "%");

                var subqueryLowkey = Subqueries.PropertyIn("LowKey", DetachedCriteria.For<DatabaseItem>()
                    .SetProjection(Projections.Property("Id"))
                    .Add(Restrictions.InsensitiveLike("Name", searchString)));

                var subqueryHighkey = Subqueries.PropertyIn("HighKey", DetachedCriteria.For<DatabaseItem>()
                    .SetProjection(Projections.Property("Id"))
                    .Add(Restrictions.InsensitiveLike("Name", searchString)));


                criteria.Add(Restrictions.Or(subqueryLowkey, subqueryHighkey));

                if (characterId != 0)
                    criteria.Add(Restrictions.Eq("Owner.Id", characterId));

                else if (dimension >= 0) {
                    criteria.CreateAlias("Owner", "o");
                    criteria.Add(Restrictions.Eq("o.Dimension", dimension));
                }

                return criteria.List<Item>();
            }
        }


        /// <summary>
        /// This is used primarily to retrieve and map backpacks
        /// </summary>
        /// <param name="itemKeys"></param>
        /// <param name="characterId"></param>
        /// <param name="dimension"></param>
        /// <param name="minQl"></param>
        /// <param name="maxQl"></param>
        /// <returns></returns>
        public List<Item> ListByChildren(List<long> childrenIds) {
            List<Item> result = new List<Item>();
            using (ISession session = SessionFactory.OpenSession()) {
                
                int idx = 0;
                while (idx < childrenIds.Count) {
                    int diff = Math.Min(450, childrenIds.Count - idx);
                    var dataset = childrenIds.GetRange(idx, diff);
                    idx += diff;

                    var criteria = session.CreateCriteria<Item>();
                    criteria.Add(Restrictions.In("Children", dataset));
                    result.AddRange(criteria.List<Item>());
                }

            }
            return result;
        }



        public Item GetBackpack(long containerId) {
            using (ISession session = SessionFactory.OpenSession()) {
                var criteria = session.CreateCriteria<Item>();
                criteria.Add(Restrictions.Eq("Children", containerId));
                criteria.SetMaxResults(1);
                return criteria.UniqueResult<Item>();
            }
        }


        public void AddItemsReceivedFromTrade(List<NPCTradeRejectedItem> items, long characterId, long newParent, long shopContainer) {
            
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {

                    String hql = "UPDATE Item SET ParentContainerId = :newParent, Slot = :slot WHERE ParentContainerId = :oldParent AND LowKey = :low AND HighKey = :high AND QL = :ql AND Owner.Id = :owner";
                    var query = session.CreateQuery(hql);

                    foreach (NPCTradeRejectedItem item in items) {
                        // Definitely garbled data, had some cases of QL 0xFFFFFFFF
                        if (item.ql > int.MaxValue || item.ql < 0)
                            continue;

                        uint nextFreeInvSlot = FindNextAvailableContainerSlot(characterId, newParent);

                        // Item overflowed, handled in overflow callback
                        if (nextFreeInvSlot >= 94) {
                            transaction.Commit();
                            return;
                        }

                        query.SetParameter("slot", nextFreeInvSlot);
                        query.SetParameter("owner", characterId);
                        query.SetParameter("newParent", newParent);
                        query.SetParameter("oldParent", shopContainer);
                        query.SetParameter("low", (long)item.itemid.low);
                        query.SetParameter("high", (long)item.itemid.high);
                        query.SetParameter("ql", item.ql);

                        try {
                            query.ExecuteUpdate();
                        }
                        catch (OverflowException ex) {
                            string message = string.Format("AddItemsReceivedFromTrade, slot:{0}, owner:{1}, newParent:{2}, oldParent:{3}, low:{4}, high:{5}, ql:{6}", nextFreeInvSlot, characterId, newParent, shopContainer, item.itemid.low, item.itemid.high, item.ql);
                            ExceptionReporter.ReportException(ex, message);
                        }

                    }

                    transaction.Commit();
                }
            }
        }

        public IList<Item> GetBuffItems(HashSet<int> skills, int dimension) {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    var criteria = session.CreateCriteria(typeof(Item));

                    foreach (int skill in skills) {
                        var subCriteria = DetachedCriteria.For<AOItemEffect>()
                            .SetProjection(Projections.Property("Parent.Id"))
                            .Add(Expression.Eq("Type", skill))
                            .Add(Expression.Gt("Value", 0));

                        criteria.Add(Restrictions.Or(
                            Subqueries.PropertyIn("LowKey", subCriteria), 
                            Subqueries.PropertyIn("HighKey", subCriteria))
                            );
                    }

                    if (dimension >= 0) {
                        criteria.CreateAlias("Owner", "o");
                        criteria.Add(Restrictions.Eq("o.Dimension", dimension));
                    }

                    criteria.SetFetchMode("InternalLow", FetchMode.Eager);
                    criteria.SetFetchMode("InternalHigh", FetchMode.Eager);
                    criteria.SetFetchMode("InternalLow.Effects", FetchMode.Eager);
                    criteria.SetResultTransformer(new DistinctRootEntityResultTransformer());

                    var items = criteria.List<Item>();

                    foreach (var item in items) {
                        item.InitializeEffects();
                        /*
                            item.InternalHigh.Effects = new SortedSet<AOItemEffect>(session.CreateCriteria<AOItemEffect>().Add(Restrictions.Eq("Parent.Id", item.InternalHigh.Id)).List<AOItemEffect>());
                            item.InternalLow.Effects = new SortedSet<AOItemEffect>(session.CreateCriteria<AOItemEffect>().Add(Restrictions.Eq("Parent.Id", item.InternalLow.Id)).List<AOItemEffect>());
                        }*/
                    }

                    return items;
                }
            }
        }


        public void AddItemsReceivedFromShop(uint shopCapacity, long characterId, long otherTradeContainer) {
            
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    for (uint i = 0; i <= shopCapacity; i++) {
                        uint nextFreeInvSlot = FindNextAvailableContainerSlot(characterId, (long)InventoryIds.INV_TOONINV);
                        String hql = "UPDATE Item SET ParentContainerId = 2, Slot = :newslot WHERE ParentContainerId = :parent AND Slot = :currentslot AND Owner.Id = :owner";

                        var query = session.CreateQuery(hql);
                        query.SetParameter("newslot", nextFreeInvSlot);
                        query.SetParameter("parent", otherTradeContainer);
                        query.SetParameter("currentslot", i);
                        query.SetParameter("owner", characterId);
                        query.ExecuteUpdate();
                    }

                    transaction.Commit();
                }
            
            }
        }


    }
}
