﻿using ItemsLight.AOIA;
using ItemsLight.Model;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Dao {
    class SymbiantDao : BaseDao<Symbiant> {

        public IList<Symbiant> GetLootForBoss(string pattern) {
            using (ISession session = SessionFactory.OpenSession()) {
                var criteria = session.CreateCriteria<Symbiant>();
                criteria.Add(Restrictions.Eq("Origin", pattern));
                criteria.AddOrder(Order.Asc("QL"));
                return criteria.List<Symbiant>();
            }
        }
        // Every +10 QL is ~7 levels
        // Sometimes its 8
        private int GetMaxQLForLevel(int level) {
            if (level >= 214)
                return 400;
            else if (level >= 154 + 8 * 7)
                return 280;
            else if (level >= 154 + 8 * 6)
                return 270;
            else if (level >= 154 + 8 * 5)
                return 260;
            else if (level >= 154 + 8 * 4)
                return 250;
            else if (level >= 154 + 8 * 3)
                return 240;
            else if (level >= 154 + 8 * 2)
                return 230;
            else if (level >= 154 + 8 * 1)
                return 220;
            else if (level >= 154)
                return 210;
            else if (level >= 146)
                return 200;
            else if (level >= 139)
                return 190;
            else if (level >= 132)
                return 180;
            else if (level >= 125)
                return 170;
            else if (level >= 117)
                return 160;
            else if (level >= 110)
                return 150;
            else if (level >= 103)
                return 140;
            else if (level >= 95)
                return 130;
            else if (level >= 88)
                return 120;
            else if (level >= 80)
                return 110;
            else if (level >= 73)
                return 100;
            else if (level >= 66)
                return 90;
            else if (level >= 59)
                return 80;
            else if (level >= 52)
                return 70;
            else if (level >= 44)
                return 60;
            else if (level >= 37)
                return 50;
            else if (level >= 30)
                return 40;
            else if (level >= 22)
                return 30;
            else if (level >= 19)
                return 25;
            else
                return 20;
        }
        private string[] GetTypesForProfession(long profession) {
            switch (profession) {
                case 6: // ADV
                    return new string[] { "Infantry", "Support", "Artillery" };
                case 5: // Agent
                case 1: // Soldier
                    return new string[] { "Artillery" };
                case 8: // Crat
                    return new string[] { "Extermination", "Control" };
                case 12: // Meta
                    return new string[] { "Extermination", "Control", "Support" };
                case 11: // NT
                    return new string[] { "Extermination" };
                case 10: // Doc
                    return new string[] { "Support" };
                case 9: // Enf
                    return new string[] { "Infantry" };
                case 2: // MA
                    return new string[] { "Infantry", "Support" };
                case 3: // Eng
                    return new string[] { "Control" };
                case 4: // Fixer
                    return new string[] { "Support", "Artillery" };
                case 14: // Keeper
                    return new string[] { "Infantry", "Support" };
                case 7: // Trader
                    return new string[] { "Control", "Artillery", "Support" };
                default:
                    return new string[] { "" }; // Shade?
            }
        }

        public IList<Item> SymbiantsForCharacter(long characterId, string slot, bool ownedOnly, bool availableOnly) {
            using (ISession session = SessionFactory.OpenSession()) {
                var character = session.CreateCriteria<Character>().Add(Restrictions.Eq("Id", characterId)).UniqueResult<Character>();
                if (character == null)
                    return new List<Item>();


                // Only look up syndicate symbiants for head slot
                Item syndicate = null;
                if (slot == "head") {
                    List<long> syndicateSymbiants = new List<long>();
                    for (int i = 268274; i <= 268288; i++)
                        syndicateSymbiants.Add(i);

                    // Grab the syndicate item if available
                    foreach (var item in session.CreateCriteria<Item>()
                        .Add(Restrictions.Eq("Owner.Id", characterId))
                        .Add(Restrictions.In("LowKey", syndicateSymbiants)).List<Item>()) {
                        syndicate = item;
                    }
                }


                var subquery = Subqueries.PropertyIn("LowKey", DetachedCriteria.For<Symbiant>()
                    .SetProjection(Projections.Property("AOID"))
                    .Add(Restrictions.Eq("Slot", slot))
                    .Add(Restrictions.In("Type", GetTypesForProfession(character.ProfessionId)))
                    );
                
                var criteria = session.CreateCriteria<Item>();
                criteria.Add(subquery);
                criteria.Add(Restrictions.Le("QL", GetMaxQLForLevel(character.Level)));

                // This should remove equipped items from the result
                if (availableOnly && ownedOnly)
                    criteria.Add(Restrictions.Or(
                        Restrictions.Gt("Slot", 64),
                        !Restrictions.Eq("ParentContainerId", (long)InventoryIds.INV_TOONINV)
                        ));
                
                var result = criteria.List<Item>();
                if (syndicate != null)
                    result.Add(syndicate);

                return result;
                
            }

        }
    }
}
