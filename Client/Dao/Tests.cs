﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ItemsLight.AOIA;
using ItemsLight.Dao;
using ItemsLight.Model;
using ItemsLight.AOIA.Parsers;
using NHibernate;
using ItemsLight.AOIA.HelperStructs;

[TestClass]
public class Tests {

    
    ItemDao itemDao = new ItemDao();
    DatabaseItemDao databaseItemDao = new DatabaseItemDao();
    MarketItemDao GMIDao = new MarketItemDao();
    CharacterDao characterDao = new CharacterDao();
    SymbiantDao symbiantDao = new SymbiantDao();
    private PatternDao patternDao = new PatternDao();

    
    [TestMethod]
    public void ttretrter() {

        using (ISession session = SessionFactory.OpenSession()) {
            session.CreateCriteria<IdentifyItem>().List();
        }
    }

    
    [TestMethod]
    public void TestSymbiantsForCharacter() {
        Assert.AreNotEqual(symbiantDao.SymbiantsForCharacter(265858, "head", true, true).Count, 0);
    }

    [TestMethod]
    public void TestRENAME_ME_PLEASE() {
        LegacySQL.RENAME_ME_PLEASE(555, 567, 123, 0, 1);
    }
    /*
    [TestMethod]
    public void TestGetBuffItems() {
        {
            RequestBuffItems query = new RequestBuffItems {
                Ability = "Agility",
                Breed = "Any",
                Dimension = -1,
                Profession = "All",
                showAvailableOnly = false,
                Slot = "Legs"
            };
            SqlCE.GetBuffItems(query);
        }

        {
            RequestBuffItems query = new RequestBuffItems {
                Ability = "Agility",
                Breed = "Any",
                Dimension = -1,
                Profession = "All",
                showAvailableOnly = true,
                Slot = "Legs"
            };
            SqlCE.GetBuffItems(query);
        }

        {
            RequestBuffItems query = new RequestBuffItems {
                Ability = "Agility",
                Breed = "Any",
                Dimension = 13,
                Profession = "All",
                showAvailableOnly = true,
                Slot = "Legs"
            };
            SqlCE.GetBuffItems(query);
        }

        {
            RequestBuffItems query = new RequestBuffItems {
                Ability = "Agility",
                Breed = "Opifex",
                Dimension = 13,
                Profession = "All",
                showAvailableOnly = true,
                Slot = "Legs"
            };
            SqlCE.GetBuffItems(query);
        }

        {
            RequestBuffItems query = new RequestBuffItems {
                Ability = "Agility",
                Breed = "Any",
                Dimension = 13,
                Profession = "Engineer",
                showAvailableOnly = true,
                Slot = "Legs"
            };
            SqlCE.GetBuffItems(query);
        }

        {
            RequestBuffItems query = new RequestBuffItems {
                Ability = "Agility",
                Breed = "Any",
                Dimension = 13,
                Profession = "All",
                showAvailableOnly = true,
                Slot = "Head"
            };
            SqlCE.GetBuffItems(query);
        }
    }*/
    
    [TestMethod]
    public void TestIdentify() {
        LegacySQL.CListItemsForIdentify(IdentifyType.AI_WEAPONS, -1, 0);
        LegacySQL.CListItemsForIdentify(IdentifyType.ARULSABA, 13, 0);
        LegacySQL.CListItemsForIdentify(IdentifyType.AI_WEAPONS, 13, 555);
        LegacySQL.CListItemsForIdentify(IdentifyType.AI_WEAPONS, -1, 555);
    }
        //public static List<Item> CListItemsForIdentify(IdentifyType type, int dimension, long characterId = 0) {
    

    [TestMethod]
    public void GetNumContainers() {
        itemDao.GetNumContainers();
    }
    [TestMethod]
    public void GetPatternsForCharacterPattern() {
        var n1 = patternDao.GetPiecesForPattern("The Proprietrix", -1, 0);
        var n2 = patternDao.GetPiecesForPattern("The Proprietrix", -1, 313000);
        var n3 = patternDao.GetPiecesForPattern("The Proprietrix", 13, 0);
        var n4 = patternDao.GetPiecesForPattern("Skulky", -1, 0);
    }
    [TestMethod]
    public void FindFirstItemOfSameType() {
        itemDao.FindFirstItemOfSameType(123, 123, 0, 124);
    }
    
    /*[TestMethod]
    public void GetPatternsForCharacterPatternCmp() {
        var n1 = patternDao.GetPatternsForCharacterPattern("The Proprietrix", -1, 0);
        var n2 = patternDao.GetPatternsForCharacterPattern("The Proprietrix", -1, 313000);
        var n3 = patternDao.GetPatternsForCharacterPattern("The Proprietrix", 13, 0);
        var n4 = patternDao.GetPatternsForCharacterPattern("Skulky", -1, 0);


        var m1 = SqlCE.Instance.GetPatternsForCharacterPattern("The Proprietrix", -1, 0);
        var m2 = SqlCE.Instance.GetPatternsForCharacterPattern("The Proprietrix", -1, 313000);
        var m3 = SqlCE.Instance.GetPatternsForCharacterPattern("The Proprietrix", 13, 0);
        var m4 = SqlCE.Instance.GetPatternsForCharacterPattern("Skulky", -1, 0);

        Assert.AreEqual(n1.Count, m1.Count);
        Assert.AreEqual(n2.Count, m2.Count);
        Assert.AreEqual(n3.Count, m3.Count);
        Assert.AreEqual(n4.Count, m4.Count);
    }*/
/*
    [TestMethod]
    public void TestGetItemByIDNoDimension() {
        List<long> ids = new List<long>(new long[] { 46077, 46050, 46077, 46036, 158227, 155721, 158229, 158228, 164561, 164553, 164557, 150922, 291082, 23314, 112324, 273381, 95577, 247106 });
        var a = itemDao.ListByID(ids, 0, -1, 0, 300);
        var b = SqlCE.Instance.CListItemByID(ids, 0, -1, 0, 300);
        Assert.AreEqual(a.Count, b.Count);
    }*/

/*
    [TestMethod]
    public void TestGetItemByIDDimension13() {
        List<long> ids = new List<long>(new long[] { 46077, 46050, 46077, 46036, 158227, 155721, 158229, 158228, 164561, 164553, 164557, 150922, 291082, 23314, 112324, 273381, 95577, 247106 });
        var a = itemDao.ListByID(ids, 0, 13, 0, 300);
        var b = SqlCE.Instance.CListItemByID(ids, 0, 13, 0, 300);
        Assert.AreEqual(a.Count, b.Count);
    }*/

/*
    [TestMethod]
    public void TestCanListGMI() {
        {
            var a = GMIDao.ListGMI(1393919);
            var b = SqlCE.Instance.ListGMI(1393919);
            Assert.AreEqual(a.Count, b.Count);
        }
        {
            var a = GMIDao.ListGMI(1393920);
            var b = SqlCE.Instance.ListGMI(1393920);
            Assert.AreEqual(a.Count, b.Count);
        }
    }*/

    [TestMethod]
    public void CanListContainerData() {
        var n1 = itemDao.ListAllContainers(-1).Count;
        var n2 = itemDao.ListAllContainers(13).Count;

    }
    [TestMethod]
    public void ItemLoadTestWithCharacter() {
        var n1 = itemDao.ListAllContainers(-1).Count;
        var n2 = itemDao.ListAllContainers(13).Count;

    }

/*
    [TestMethod]
    public void CanGetCharacterName() {
        Assert.AreEqual(SqlCE.Instance.CGetNameForCharacter(312995), characterDao.GetName(312995));
        Assert.AreEqual(SqlCE.Instance.CGetNameForCharacter(1393919), characterDao.GetName(1393919));
    }*/


    /// <summary>
    /// This test fails, but that is OK.
    /// Has no adverse effects
    /// </summary>
/*
    [TestMethod]
    public void CanGetCharacterIndex() {
        {
            var n1 = characterDao.ListAll(0);
            var n2 = SqlCE.Instance.CListAllCharacters(0);
            Assert.AreEqual(n1.Count, n2.Count);
            foreach (var key in n1.Keys) {
                Assert.AreEqual(n1[key], n2[key]);
            }
        }


        {
            var n1 = characterDao.ListAll(13);
            var n2 = SqlCE.Instance.CListAllCharacters(13);
            Assert.AreEqual(n1.Count, n2.Count);
            foreach (var key in n1.Keys) {
                Assert.AreEqual(n1[key], n2[key]);
            }
        }

    }*/

/*
    [TestMethod]
    public void TestCanSearchGMI() {
        {
            var a = GMIDao.SearchForItems("Hot Pants", -1);
            var b = SqlCE.Instance.SearchForGMIITems("Hot Pants", -1);
            Assert.AreEqual(a.Count, b.Count);
            Assert.AreNotEqual(a.Count, 0);
        }
        {
            var a = GMIDao.SearchForItems("Instr Disc", -1);
            var b = SqlCE.Instance.SearchForGMIITems("Instr Disc", -1);
            Assert.AreEqual(a.Count, b.Count);
            Assert.AreNotEqual(a.Count, 0);
        }
    }*/
    
/*
    [TestMethod]
    public void GetAllPBPatterns() {
        var n1 = patternDao.GetAllPBPatternsOmni();
        var m1 = SqlCE.Instance.GetAllPBPatterns(true);
        Assert.AreEqual(n1.Count, m1.Count);

        var n2 = patternDao.GetAllPBPatternsClan();
        var m2 = SqlCE.Instance.GetAllPBPatterns(false);
        Assert.AreEqual(n2.Count, m2.Count);

    }*/

/*
    [TestMethod]
    public void GetAllPBPatterns() {
        var n2 = patternDao.GetAllPatterns();
        var m2 = SqlCE.Instance.GetAllPatterns();
        Assert.AreEqual(n2.Count, m2.Count);
    }*/

    [TestMethod]
    public void TestMergeItemStacks() {
        itemDao.MergeItemStacks(123, 0, 13, 0, 300);

    }
/*
    [TestMethod]
    public void TestPocketBossLocation() {
        var n1 = patternDao.GetPatternDropLocations("Adobe Suzerain");
        var n2 = SqlCE.Instance.GetPatternDropLocations("Adobe Suzerain");
        Assert.AreEqual(n1.Count, n2.Count);
        Assert.AreEqual(n1[0].Location, n2[0].Location);
        Assert.AreEqual(patternDao.GetPatternDropLocations("Circumbendibum").Count, SqlCE.Instance.GetPatternDropLocations("Circumbendibum").Count);
        Assert.AreEqual(patternDao.GetPatternDropLocations("Circumbendibum")[0].Location, SqlCE.Instance.GetPatternDropLocations("Circumbendibum")[0].Location);
        Assert.AreEqual(patternDao.GetPatternDropLocations("Agent of Decay").Count, SqlCE.Instance.GetPatternDropLocations("Agent of Decay").Count);
        Assert.AreEqual(patternDao.GetPatternDropLocations("Agent of Decay")[0].Location, SqlCE.Instance.GetPatternDropLocations("Agent of Decay")[0].Location);
        
        
    }*/

    //AddItemsReceivedFromTrade
    //
    [TestMethod]
    public void AddItemsReceivedFromShop() {
        itemDao.AddItemsReceivedFromShop(123, 123, 123);
    }

    [TestMethod]
    public void AddItemsReceivedFromTrade() {
        List<NPCTradeRejectedItem> items = new List<NPCTradeRejectedItem>();
        var id = new AoObjectId();
        id.high = 124;
        id.low = 123;
        items.Add(new NPCTradeRejectedItem {
            itemid = id,
            ql = 0
        });
        itemDao.AddItemsReceivedFromTrade(items, 123, 124, 125);
    }

    [TestMethod]
    public void GetPatternsForCharacter() {
        patternDao.GetPatternsForCharacter(0);

        patternDao.GetPatternsForCharacter(-1);
        patternDao.GetPatternsForCharacter(13);

        patternDao.GetPatternsForCharacter(0, 451532168);
        patternDao.GetPatternsForCharacter(-1, 451532168);
    }
/*

    [TestMethod]
    public void GetPatternsForCharacterCompare() {
        {
                var a = patternDao.GetPatternsForCharacter(0);
                var b = SqlCE.Instance.GetPatternsForCharacter(0);
                Assert.AreEqual(a.Count, b.Count);
                foreach (var key in a.Keys) {
                    Assert.AreEqual(a[key].Total, b[key].Total);
                }
        }
        {
                var a = patternDao.GetPatternsForCharacter(-1);
                var b = SqlCE.Instance.GetPatternsForCharacter(-1);
                Assert.AreEqual(a.Count, b.Count);
                foreach (var key in a.Keys) {
                    Assert.AreEqual(a[key].Total, b[key].Total);
                }
        }
        {
                var a = patternDao.GetPatternsForCharacter(13);
                var b = SqlCE.Instance.GetPatternsForCharacter(13);
                Assert.AreEqual(a.Count, b.Count);
                foreach (var key in a.Keys) {
                    Assert.AreEqual(a[key].Total, b[key].Total);
                }
        }

        {
            var a = patternDao.GetPatternsForCharacter(0, 451532168);
            var b = SqlCE.Instance.GetPatternsForCharacter(0, 451532168);
            Assert.AreEqual(a.Count, b.Count);
            foreach (var key in a.Keys) {
                Assert.AreEqual(a[key].Total, b[key].Total);
            }
        }

        {
            var a = patternDao.GetPatternsForCharacter(-1, 451532168);
            var b = SqlCE.Instance.GetPatternsForCharacter(-1, 451532168);
            Assert.AreEqual(a.Count, b.Count);
            foreach (var key in a.Keys) {
                Assert.AreEqual(a[key].Total, b[key].Total);
            }
        }
    }*/
    
/*
    [TestMethod]
    public void TestGetItemByIDDimension1() {
        List<long> ids = new List<long>(new long[] { 46077, 46050, 46077, 46036, 158227, 155721, 158229, 158228, 164561, 164553, 164557, 150922, 291082, 23314, 112324, 273381, 95577, 247106 });
        var a = itemDao.ListByID(ids, 0, 1, 0, 300);
        var b = SqlCE.Instance.CListItemByID(ids, 0, 1, 0, 300);
        Assert.AreEqual(a.Count, b.Count);
    }*/


/*
    [TestMethod]
    public void CanListEveryItemInDatabase() {
        var a = databaseItemDao.ListAllItems();
        var b = SqlCE.Instance.ListAllItems();
        Assert.AreEqual(a.Count, b.Count);
        int i = 0;
        foreach (var elem in a) {
            Assert.IsFalse(string.IsNullOrEmpty(elem.Name));
            if (i++ > 10)
                break;
        }
    }*/
/*
    [TestMethod]
    public void CanEnumerateDimensions() {
        var a = characterDao.ListAllDimensions();
        var b = SqlCE.Instance.ListAllDimensions();
        Assert.AreEqual(a.Count, b.Count);
    }*/
    
/*
    [TestMethod]
    public void CanGetMissingItemNamesFromGMI() {
        var a = GMIDao.GetItemsMissingName();
        var b = SqlCE.Instance.GetMissingGMIItemNames();
        Assert.AreEqual(a.Count, b.Count);
        Assert.AreEqual(a.Count, 1);
    }*/
    [TestMethod]
    public void CanUpdateItemNameForGMI() {
        GMIDao.UpdateName(1, "Test");
        GMIDao.UpdateName(1, "Test2");
    }
    [TestMethod]
    public void CanSaveGMIItems() {
        List<GMIItem> items = new List<GMIItem>();
        items.Add(new GMIItem {
            ItemId = 1,
            CharacterId = 66586,
            QL = 200
        });
        items.Add(new GMIItem {
            ItemId = 1,
            CharacterId = 66586,
            QL = 200
        });
        items.Add(new GMIItem {
            ItemId = 2,
            CharacterId = 66586,
            QL = 200
        });
        MarketItemDao dao = new MarketItemDao();
        dao.SaveGMIItems(items, 66586);
    }
/*
    [TestMethod]
    public void CanListConentsOfContainer() {
        {
            var n1 = itemDao.ListContainerContent(654217, 312998);
            var n2 = SqlCE.Instance.CListContainerContent(654217, 312998);
            Assert.AreEqual(n1.Count, n2.Count);
        }
        {
            var n1 = itemDao.ListContainerContent(1, 312998);
            var n2 = SqlCE.Instance.CListContainerContent(1, 312998);
            Assert.AreEqual(n1.Count, n2.Count);
        }
        {
            var n1 = itemDao.ListContainerContent(2, 312998);
            var n2 = SqlCE.Instance.CListContainerContent(2, 312998);
            Assert.AreEqual(n1.Count, n2.Count);
        }
    }*/



    [TestMethod]
    public void CanListCharactersForUI() {
        characterDao.ListAllAsComboBox(0);
        characterDao.ListAllAsComboBox(13);
        characterDao.ListAllAsComboBox(-1);
    }
    /*
    [TestMethod]
    public void CanListAllSymbiantNamesForPB() {
        Assert.AreEqual(SqlCE.Instance.GetLootForPocketboss("Adobe Suzerain").Count, symbiantDao.GetLootForBoss("Adobe Suzerain").Count);
        Assert.AreEqual(SqlCE.Instance.GetLootForPocketboss("Anya").Count, symbiantDao.GetLootForBoss("Anya").Count);
        Assert.AreEqual(SqlCE.Instance.GetLootForPocketboss("White").Count, symbiantDao.GetLootForBoss("White").Count);
        Assert.AreEqual(SqlCE.Instance.GetLootForPocketboss("The Proprietrix").Count, symbiantDao.GetLootForBoss("The Proprietrix").Count);
    }*/
    [TestMethod]
    public void CanDeleteCharacter() {
        characterDao.Delete(123);
    }
    [TestMethod]
    public void CanArchieveCharacter() {
        characterDao.AchieveCharacter(123);
    }
    [TestMethod]
    public void CanDeleteFromContainer() {
        itemDao.DeleteFromContainer(123, 123);
        itemDao.DeleteItemsInTradeContainer(123, 123, 124);
    }
    [TestMethod]
    public void CanSaveNewItem() {
        Item item = ItemDao.CreateItem(0, 21793, 500, 0, 0, 123, 0, 0, 123);
        itemDao.Save(item);

    }
    [TestMethod]
    public void ItemDeleteAtSlotInContainer() {        
        itemDao.DeleteAtSlotInContainer(123, 123, 123);
    }
    [TestMethod]
    public void ItemSplitItemStacks() {
        itemDao.SplitStacks(123, 123, 123, 4);
    }
    [TestMethod]
    public void ItemReduceItemStack() {
        itemDao.ReduceStack(123, 123, 123, 23314, 23315);
    }
    [TestMethod]
    public void ItemDeleteEmptyStack() {
        itemDao.DeleteEmptyStack(123, 123, 123, 23314, 23315);
    }
    [TestMethod]
    public void ItemDeleteFromContainer() {
        itemDao.DeleteFromContainer(123);
    }
    [TestMethod]
    public void CharUpdateNameAndDimension() {
        characterDao.UpdateNameAndDimension(123, "Mr Test", 13);
        characterDao.UpdateNameAndDimension(123, "Mr Test!", 1);
    }
/*
    [TestMethod]
    public void TestFindNextAvailableContainerSlot() {
        Assert.AreEqual(itemDao.FindNextAvailableContainerSlot(312998, 654217),
            SqlCE.Instance.FindNextAvailableContainerSlot(312998, 654217));

        Assert.AreEqual(itemDao.FindNextAvailableContainerSlot(312998, 2),
            SqlCE.Instance.FindNextAvailableContainerSlot(312998, 2));

        Assert.AreEqual(itemDao.FindNextAvailableContainerSlot(312998, 1),
            SqlCE.Instance.FindNextAvailableContainerSlot(312998, 1));

        Assert.AreEqual(itemDao.FindNextAvailableContainerSlot(266505, 442913),
            SqlCE.Instance.FindNextAvailableContainerSlot(266505, 442913));

        Assert.AreEqual(itemDao.FindNextAvailableContainerSlot(266505, 442938),
            SqlCE.Instance.FindNextAvailableContainerSlot(266505, 442938));
    }*/
    

/*
    [TestMethod]
    public void ItemDbItemProperties() {
        Assert.AreEqual(itemDao.GetItemProperties(312998, 654217, 12), SqlCE.Instance.getItemProperties(312998, 654217, 12));
        Assert.AreEqual(itemDao.GetItemProperties(312998, 654217, 13), SqlCE.Instance.getItemProperties(312998, 654217, 13));
        Assert.AreEqual(itemDao.GetItemProperties(312998, 654217, 14), SqlCE.Instance.getItemProperties(312998, 654217, 14));
        Assert.AreEqual(itemDao.GetItemProperties(312998, 654217, 15), SqlCE.Instance.getItemProperties(312998, 654217, 15));
        Assert.AreEqual(itemDao.GetItemProperties(266505, 442913, 7), SqlCE.Instance.getItemProperties(266505, 442913, 7));
    }*/


    [TestMethod]
    public void UpdateAmountInStack() {
        itemDao.UpdateAmountInStack(123, 123, 100, 123, 101);
    }

    [TestMethod]
    public void UpdateCharacterValues() {
        characterDao.UpdateCredits(123, 123);
        characterDao.UpdateGMICredits(123, 123);
        characterDao.UpdateLevel(123, 123);
        characterDao.UpdateVP(123, 123);
        characterDao.UpdateProfession(123, 123);
    }


    [TestMethod]
    public void TestItemUpdateOperations() {
        itemDao.UpdateParentAndSlotForItem(123, 123, 123, 123, 123);
        itemDao.RemoveOldContentForCharacter(123);
        itemDao.RemoveItemFromTrade(123, 123, 123, 123);
        itemDao.RemoveItemFromTemporaryShopTradePartner(123, 123);
        itemDao.DeleteOrphanedContainersForCharacter(123);
        itemDao.RemoveBackpack(123, 123);
        itemDao.RemoveBackpackFromTradeWindow(123, 123, 123);
        itemDao.RemoveWhereParentSlotOwner(123, 123, 123);
        itemDao.RemoveBankItems(123);
    }

    BackpackIdentifierDao backpackIdentifierDao = new BackpackIdentifierDao();
    [TestMethod]
    public void TestBackpackIdentifierDao() {
        backpackIdentifierDao.UpdateBackpackDatabase();
    }

    [TestMethod]
    public void TestItemUpdateOperations2() {
        itemDao.UpdateParentForItem(123, 123, 123, 123);
        itemDao.DeleteSpecificItemInSlot(123, 123, 123, 123, 123);
    }

    [TestMethod]
    public void UpdateCharacterPositionsFromPositionTracker() {
        characterDao.UpdateCharacterPositionsFromPositionTracker();
    }

/*
    [TestMethod]
    public void ListAllItemsMissingFromDB() {
        Assert.AreEqual(itemDao.ListAllItemsMissingFromDB().Count, SqlCE.Instance.ListAllItemsMissingFromDB().Count);
    }*/
    
    
/*
    [TestMethod]
    public void TestGetCharacterSummary() {
        {
            var a = characterDao.GetCharacterSummary(-1);
            var b = SqlCE.Instance.GetCharacterSummary(-1);
            Assert.AreEqual(a.Count, b.Count);
            for (int i = 0; i < a.Count; i++) {
                Assert.AreEqual(a[i].NumItems, b[i].NumItems);
                Assert.AreEqual(a[i].NumContainers, b[i].NumContainers);
                Assert.AreEqual(a[i].Id, b[i].Id);
            }
        }
        {
            var a = characterDao.GetCharacterSummary(13);
            var b = SqlCE.Instance.GetCharacterSummary(13);
            Assert.AreEqual(a.Count, b.Count);
            for (int i = 0; i < a.Count; i++) {
                Assert.AreEqual(a[i].NumItems, b[i].NumItems);
                Assert.AreEqual(a[i].NumContainers, b[i].NumContainers);
                Assert.AreEqual(a[i].Id, b[i].Id);
            }
        }
    }*/
    
    
}

