﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using log4net;
using Ionic.Zip;
using ItemsLight.Dao;
using ItemsLight.AOIA;
using EvilsoftCommons.Exceptions;

namespace ItemsLight.Cloud {
    class CloudBackup {
        static ILog logger = LogManager.GetLogger(typeof(CloudBackup));
        private Stopwatch timer;
        private CloudWatcher provider = new CloudWatcher();

        public void Update() {
            if (timer == null) {
                timer = new Stopwatch();
                timer.Start();
                Backup();
            }
            else if (timer.ElapsedMilliseconds > 1000 * 60 * 30) {
                timer.Restart();
                Backup();
            }
        }

        public void Backup() {
            try {
                CloudProviderEnum currentProvider = (CloudProviderEnum)Properties.Settings.Default["cloud"];
                if (currentProvider != CloudProviderEnum.NONE && currentProvider != CloudProviderEnum.UNINITIALIZED) {
                    
                    if (!provider.Providers.Any(m => m.Provider == currentProvider)) {
                        Properties.Settings.Default["cloud"] = (int)CloudProviderEnum.UNINITIALIZED;
                        logger.WarnFormat("Could not find cloud provider {0}, resetting provider to UNINITIALIZED", currentProvider);
                    }
                    else {
                        string path = provider.Providers.Where(m => m.Provider == currentProvider).First<CloudProvider>().Location;
                        Backup(Path.Combine(path, "EvilSoft", "IALight"));
                    }
                }
            }
            catch (IOException ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
            }
            catch (Exception ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                ExceptionReporter.ReportException(ex, "Creating a backup");
            }
        }
        private void Backup(string destination) {
            if (!Directory.Exists(destination))
                Directory.CreateDirectory(destination);

            string target = Path.Combine(destination, string.Format("{0}.bak", DateTime.Now.DayOfWeek));

            // If the file already exists and is newer than 3 days ('not written today'), just skip it.
            if (File.Exists(target)) {
                DateTime lastModified = File.GetLastWriteTime(target);
                if ((DateTime.Now - lastModified).TotalDays < 3)
                    return;

            }

            using (ZipFile zip = new ZipFile()) {
                zip.AddFile(SessionFactory.UserdataLocation);
                zip.Comment = "This backup was created at " + System.DateTime.Now.ToString("G");
                zip.Save(target);
                logger.Info("Created a new backup of the database");
            }
        }


    }
}
