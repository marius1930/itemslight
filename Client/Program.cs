﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

using log4net;
using System.Runtime.InteropServices;
using ItemsLight.AOIA;
using System.Windows.Forms;
using System.Collections.Concurrent;


using ItemsLight.Dao;
using ItemsLight.Cloud;
using NHibernate;
using ItemsLight.Model;
using EvilsoftCommons.SingleInstance;
using EvilsoftCommons.Exceptions;
namespace ItemsLight {

    public class Program {
        static ILog logger = LogManager.GetLogger(typeof(Program));
        private static MainWindow mw;



        static void UpgradeSettings() {
            try {
                if (Properties.Settings.Default.CallUpgrade) {
                    Properties.Settings.Default.Upgrade();
                    Properties.Settings.Default.CallUpgrade = false;
                }
            }
            catch (Exception ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                ExceptionReporter.ReportException(ex);
            }
        }

        static void run() {
            // Remove the nag screen from "ListDlls"
            UpgradeSettings();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            mw = new MainWindow();

            mw.Visible = false;
            Application.Run(mw);
        }

        static bool IsWinXP() {
            OperatingSystem OS = Environment.OSVersion;
            return (OS.Version.Major == 5) && (OS.Version.Minor >= 1);
        }


        [STAThread]
        static void Main(string[] args) {
            ExceptionReporter.URL_HOST = "http://ribbs.dreamcrash.org/ialight";
            ExceptionReporter.EnableLogUnhandledOnThread();
            new DisableBrowserClickSound();



            logger.Info("Starting program");

            foreach (string file in new string[] { "ItemAssistantHook.dll" }) {
                if (!File.Exists(file)) {
                    logger.Fatal(string.Format("Could not find \"{0}\"", file));
                    MessageBox.Show(string.Format("Could not find \"{0}\"", file));
                    return;
                }
            }
            




            Guid guid = new Guid("{E9D8E68A-D997-467E-A1B6-76EBE954AD7C}");
            using (SingleInstance singleInstance = new SingleInstance(guid)) {
                if (singleInstance.IsFirstInstance) {
                    logger.Info("Calling run..");
                    run();
                }
            }
        }




        static void singleInstance_ArgumentsReceived(object sender, ArgumentsReceivedEventArgs e) {
            try {
                if (mw != null) {
                    Action<String[]> RestoreWindow = arguments => {
                        mw.WindowState = FormWindowState.Normal;
                        mw.Activate();
                    };

                    mw.Invoke(RestoreWindow);
                }
            }
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex, "singleInstance_ArgumentsReceived");
            }
        }



    }
}
