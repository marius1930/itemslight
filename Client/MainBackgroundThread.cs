﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

using log4net;
using System.Runtime.InteropServices;
using ItemsLight.AOIA;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Concurrent;
using ItemsLight.AOIA.Parsers;
using ItemsLight.AOIA.Misc;
using System.Diagnostics;
using ItemsLight.Model;
using ItemsLight.Dao;
using ItemsLight.Cloud;
using ItemsLight.AOIA.HelperStructs;
using ItemsLight.AOIA.Utility;
using ItemsLight.AORipper;
using System.Threading;
using AORipper.Structures;
using EvilsoftCommons.DllInjector;
using EvilsoftCommons.Exceptions;

namespace ItemsLight {
    public class MainBackgroundThread : IDisposable {
        static ILog logger = LogManager.GetLogger(typeof(MainBackgroundThread));
        private Dictionary<long, String> accountMap_ = new Dictionary<long, String>();
        private ConcurrentQueue<MessageData> resultQueue;
        private ConcurrentQueue<MessageDataRequest> requestQueue;

        // Used to periodically refresh the bag view
        private long numberOfContainersLastCheck = 0;

        private ContainerMapper containerMapper;
        private ItemDao itemDao = new ItemDao();
        private CharacterDao characterDao = new CharacterDao();
        private SymbiantDao symbiantDao = new SymbiantDao();
        private MarketItemDao GMIDao = new MarketItemDao();
        private PatternDao patternDao = new PatternDao();
        private DatabaseItemDao databaseItemDao = new DatabaseItemDao();
        private BackpackIdentifierDao backpackIdentifierDao = new BackpackIdentifierDao(); 
        private BackgroundRipper backgroundRipper = null;


        private CloudBackup cloudBackup = new CloudBackup();
        

        private int numberOfDimensions = -1;

        private TreeviewUpdater treeviewUpdater = new TreeviewUpdater();


        private int mostRecentDimension = 0;

        private Dictionary<long, long> lastKnownGmiCreditCache = new Dictionary<long, long>();
        private Dictionary<long, long> lastKnownGmiSlotCache = new Dictionary<long, long>();
        private void TimerUpdateGMIMonies() {
            try {

                foreach (var characterId in GMITracker.CharacterIds) {
                    string cookie;
                    if (GMITracker.TryGet(characterId, out cookie)) {
                        var gmiResult = GetGMIData.DownloadItemdata(characterId, cookie);
                        if (gmiResult != null) {
                            string monies = gmiResult.credits.Replace(" ", "");
                            long credits;
                            if (long.TryParse(monies, out credits)) {
                                characterDao.UpdateGMICredits(characterId, credits);

                                if (!lastKnownGmiCreditCache.ContainsKey(characterId)) {
                                    lastKnownGmiCreditCache[characterId] = credits;
                                }
                                if (!lastKnownGmiSlotCache.ContainsKey(characterId)) {
                                    lastKnownGmiSlotCache[characterId] = gmiResult.availableSlots;
                                }

                                // If we now have more credits, and more available slots.. We got a sale!
                                if (lastKnownGmiCreditCache[characterId] < credits && lastKnownGmiSlotCache[characterId] < gmiResult.availableSlots) {
                                    var charname = characterDao.GetName(characterId);
                                    if (charname == null)
                                        charname = "Char" + characterId;

                                    // Notify the user of a sale
                                    resultQueue.Enqueue(new MessageData {
                                        Type = MessageDataType.RES_GMI_SALE,
                                        Data = new GMISale {
                                            characterId = characterId,
                                            name = charname,
                                            amount = credits - lastKnownGmiCreditCache[characterId]
                                        }
                                    });
                                }

                                // Either added or removed credits, update the summary view
                                if (lastKnownGmiCreditCache[characterId] != credits) {
                                    var results = characterDao.GetCharacterSummary(mostRecentDimension);
                                    resultQueue.Enqueue(new MessageData { Type = MessageDataType.RES_UPDATE_SUMMARY, Data = results });
                                }

                                lastKnownGmiCreditCache[characterId] = credits;
                            }
                            else
                                logger.Debug("Could not parse GMI credits " + monies);
                        }
                    }
                }

                foreach (var characterId in GMITracker.CharacterIds) {
                    string cookie;
                    if (GMITracker.TryGet(characterId, out cookie)) {
                        GetGMIData.DownloadMissingItemNames(characterId, cookie);
                        break;
                    }
                }
            }         
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex, "GmiCredits");
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
            }
        }




        private void TimerUpdateInternalDimensions() {
            ICollection<int> dimensionIds = characterDao.ListAllDimensions();
            List<Dimension> dimensions = new List<Dimension>();
            dimensions.Add(new Dimension { Id = -1, name = "Any" });

            if (numberOfDimensions != dimensionIds.Count) {
                foreach (var id in dimensionIds) {
                    if (id == 500)
                        dimensions.Add(new Dimension { Id = id, name = "Archived characters" });
                    else if (id == 13)
                        dimensions.Add(new Dimension { Id = id, name = "Rubi-Ka (Live)" });
                    else if (id == 15)
                        dimensions.Add(new Dimension { Id = id, name = "Test" });
                    else
                        dimensions.Add(new Dimension { Id = id, name = "Dimension #" + id });
                }


                resultQueue.Enqueue(new MessageData { Type = MessageDataType.RES_SET_DIMENSIONS, Data = dimensions });
                numberOfDimensions = dimensionIds.Count;
            }
            
        }



        /// <summary>
        /// In case there's been an update to the AO DB format, as a fallback, download itemdata online.
        /// </summary>
        /*private void UpdateInternalDatabaseForMissingItems() {
            if (timerUpdateInternalItemdb == null) {
                timerUpdateInternalItemdb = new Stopwatch();
                timerUpdateInternalItemdb.Start();
            }
            if (timerUpdateInternalItemdb.ElapsedMilliseconds > 1000 * 60 * 5) {
                IList<Int64> items = itemDao.ListAllItemsMissingFromDB();
                if (items.Count > 0) {
                    List<DownloadedItemdata> newItems = SyncMissingItems.DownloadItemdata(items);
                    databaseItemDao.UpdateGameItemDatabase(newItems);
                }
                timerUpdateInternalItemdb.Restart();
            }
        }*/

        private void SendResponse(MessageDataType type, Object data = null) {
            resultQueue.Enqueue(new MessageData {
                Type = type,
                Data = data
            });
        }



        /// <summary>
        /// Regular updates to the container tree-view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimerHandleTreeviewUpdates() {
            try {
                // Update the stopwatch, 30 sec intervals
                long numContainers = numContainers = itemDao.GetNumContainers();
                if (numberOfContainersLastCheck != numContainers && numberOfContainersLastCheck != 0) {
                    UpdateTreeViewGUI(mostRecentDimension);
                }

                numberOfContainersLastCheck = numContainers;                
            }
            catch (Exception ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                ExceptionReporter.ReportException(ex, "TimerHandleTreeviewUpdates");
            }
        }


        private void UpdateTreeViewGUI(int dimension) {
            foreach (var elem in treeviewUpdater.UpdateTreeViewGUI(dimension))
                resultQueue.Enqueue(elem);
        }

        BackgroundWorker bw = new BackgroundWorker();

        public MainBackgroundThread(QueueGUI arguments, RunWorkerCompletedEventHandler completionCallback, ProgressChangedEventHandler progress) {
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.ProgressChanged += progress;
            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;
            bw.RunWorkerCompleted += completionCallback;
            bw.RunWorkerAsync(arguments);
        }



        private string GetAccount(long characterId) {
            if (accountMap_.ContainsKey(characterId))
                return accountMap_[characterId];
            return string.Empty;
        }




        private IList<Item> REQ_OPEN_CONTAINER(long containerId, long characterId) {
            if (containerId == TreeviewUpdater.VIRTUAL_GMI_ID) {
                return GMIDao.ListGMI(characterId);
            }
            else {
                List<Item> results = new List<Item>(itemDao.ListContainerContent(containerId, characterId));
                containerMapper.MapContainerNamesForBackpackContents(ref results);
                return results;
            }
        }


        private BackgroundWorker _worker;
        private void UpdateLoadingScreenImmediately(string message) {
            if (!_worker.CancellationPending) {
                resultQueue.Enqueue(new MessageData { Type = MessageDataType.RES_SHOW_PROGRESSBAR, Data = message });
                logger.Debug("LoadingMessage: " + message);
                //GUI.processMessagesInQueueDELEGATED();
                this._worker.ReportProgress(0);
                
            }
        }

        /// <summary>
        /// Just fire off another event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void backpackRenameCompleted(object sender, RunWorkerCompletedEventArgs e) {
            requestQueue.Enqueue(new MessageDataRequest { Type = RequestMessage.RESTART_BACKPACK_RENAMER });
        }

        void injectorCallback(object sender, ProgressChangedEventArgs e) {
            if (e.ProgressPercentage == InjectionHelper.INJECTION_ERROR && e.UserState != null) {
                resultQueue.Enqueue(new MessageData {
                    Type = MessageDataType.RES_SET_STATUS_MESSAGE,
                    Data = e.UserState.ToString()
                });
            }
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e) {
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "MainBackgroundThread";

            BackgroundWorker worker = sender as BackgroundWorker;
            this._worker = worker;
            QueueGUI arguments = (QueueGUI)e.Argument;
            
            
            ExceptionReporter.EnableLogUnhandledOnThread();
            
            this.requestQueue = arguments.requestQueue;
            resultQueue = arguments.resultQueue;


            UpdateLoadingScreenImmediately("Initializing database..");
            ProgressChangedEventHandler injectorCallbackDelegate = injectorCallback;
            InjectionHelper injector = new InjectionHelper(new BackgroundWorker(), injectorCallbackDelegate, false, "Anarchy client", string.Empty, "ItemAssistantHook.dll");
            // resultQueue

            StopwatchManager stopwatchManager = new StopwatchManager();
            stopwatchManager.Add(TimerUpdateGMIMonies, 1000 * 120, false);
            stopwatchManager.Add(characterDao.UpdateCharacterPositionsFromPositionTracker, 1000 * 3, false);

            stopwatchManager.Add(() => {   
                if ((bool)Properties.Settings.Default["summaryscript"]) {
                    var results = characterDao.GetCharacterSummary(mostRecentDimension);
                    resultQueue.Enqueue(new MessageData { Type = MessageDataType.RES_UPDATE_SUMMARY, Data = results });                
                }
            }, 1000 * 60, false);
            
            stopwatchManager.Add(TimerUpdateInternalDimensions, 1000 * 33, true);            
            stopwatchManager.Add(TimerHandleTreeviewUpdates, 30000, true);
            
            Stopwatch requireItemUpdate = null;
            {
                UpdateLoadingScreenImmediately("Checking for changes to item database...");
                if (AppdataPathFinder.HasValidAnarchyPath() && backgroundRipper == null) {
                    backgroundRipper = new BackgroundRipper(AppdataPathFinder.AnyAnarchyDirectory());
                }
                else {
                    requireItemUpdate = new Stopwatch();
                    requireItemUpdate.Start();
                }
            }

                       
            // Write the /aoia script
            ScriptUtilities.CreateAOIAScriptIfRequired();


     
            // Load account:id map
            try {                
                UpdateLoadingScreenImmediately("Mapping account names..");
                foreach (String folder in AppdataPathFinder.ListAnarchyDirectories()) {
                    BackpackRenamer.MapAccountCharid(folder, ref accountMap_);
                }

                BackpackRenamer.RenameBackpacksAsync(itemDao.ListAllContainers(), backpackRenameCompleted);
            }
            catch (InvalidOperationException) {
                // Happens..
            }
            catch (Exception ex) {
                logger.Warn(ex.Message + ex.StackTrace);
                ExceptionReporter.ReportException(ex);
            }

            // OBS: Make sure the account map has been initialized
            containerMapper = new ContainerMapper(accountMap_);


            // Load data into the position tracker
            {
                var summary = characterDao.GetCharacterSummary(0);
                foreach (var t in summary) {
                    PositionTracker.Initialize(t.Id, t.PositionX, t.PositionZ, (uint)t.ZoneId);
                }
            }
            

            logger.Debug("Signaling program ready..");
            resultQueue.Enqueue(new MessageData { Type = MessageDataType.RES_READY_TO_BEGIN });
            if (!worker.CancellationPending)
                worker.ReportProgress(0);

            while (!worker.CancellationPending) {
                System.Threading.Thread.Sleep(1);

                // Process messages from AO
                while (!arguments.messageQueue.IsEmpty) {
                    ByteAndType data;
                    if (arguments.messageQueue.TryDequeue(out data)) {
                        ClassifyMessage.CustomWndProc(data.Data, data.Type);
                    }
                    else {
                        logger.Fatal("Dequeue failed!");
                    }
                }


                // Handle only the latest request
                if (!arguments.requestQueue.IsEmpty) {
                    MessageDataRequest search_item_message = new MessageDataRequest() { Type = RequestMessage.ERROR };
                    MessageDataRequest identify_item_message = new MessageDataRequest() { Type = RequestMessage.ERROR };
                    MessageDataRequest message = new MessageDataRequest() { Type = RequestMessage.ERROR };

                    while (!arguments.requestQueue.IsEmpty) {
                        if (arguments.requestQueue.TryDequeue(out message)) {

                            switch (message.Type) {
                                case RequestMessage.WRITE_ITEMS_SCRIPT: 
                                    {
                                        var items = itemDao.ListByIDs((List<long>)message.Data);
                                        containerMapper.MapContainerNames(items);
                                        ScriptUtilities.WriteItemsScript(items);
                                    }
                                    break;

                                case RequestMessage.SEARCH_ITEM_ID: 
                                    {
                                        SearchRequestById request = (SearchRequestById)message.Data;

                                        var results = new List<Item>(itemDao.ListByID(request.Search, 0, request.Dimension, 0, 5000));
                                        containerMapper.MapContainerNames(results);
                                        resultQueue.Enqueue(
                                            new MessageData { 
                                            Type = MessageDataType.RES_SEARCH_RESULT, 
                                            Data = results 
                                        });

                                    }
                                    break;

                                case RequestMessage.SEARCH_ITEM:
                                    search_item_message = message;
                                    break;

                                case RequestMessage.SHOW_SYMBIANTS:
                                    {
                                        ShowSymbiantsRequest request = (ShowSymbiantsRequest)message.Data;
                                        IList<Item> result = symbiantDao.SymbiantsForCharacter(request.CharacterId, request.Slot, request.ShowOwnedOnly, request.ShowAvailableOnly);
                                        containerMapper.MapContainerNames(result);

                                        resultQueue.Enqueue(
                                            new MessageData {
                                                Type = MessageDataType.RES_SHOW_SYMBIANTS,
                                                Data = result
                                            });
                                    }
                                    
                                    break;

                                case RequestMessage.UPDATE_BACKPACK_DATABASE:
                                    // Before creating a tree, update the backpack database to detect any new backpack types
                                    try {
                                        backpackIdentifierDao.UpdateBackpackDatabase();
                                    }
                                    catch (NHibernate.NonUniqueObjectException ex) {
                                        logger.Warn(ex.Message);
                                        logger.Warn(ex.StackTrace);
                                        ExceptionReporter.ReportException(ex, "Exception handled, but needs to be fixed.");
                                    }
                                    break;

                                case RequestMessage.UPDATE_SUMMARY: 
                                    {
                                        mostRecentDimension = (int)message.Data;
                                        var results = characterDao.GetCharacterSummary((int)message.Data);
                                        resultQueue.Enqueue(new MessageData { Type = MessageDataType.RES_UPDATE_SUMMARY, Data = results });
                                    }
                                    break;

                                case RequestMessage.OPEN_CONTAINER: 
                                    {
                                        CharacterAndContainer data = (CharacterAndContainer)message.Data;
                                        var results = REQ_OPEN_CONTAINER(data.ContainerId, data.CharacterId);
                                        resultQueue.Enqueue(new MessageData { Type = MessageDataType.RES_CONTAINER_DATA, Data = results });
                                    } 
                                    break;


                                case RequestMessage.REFRESH_TREEVIEW:
                                    {
                                        if (message.Data != null) {
                                            int dimension = (int)message.Data;
                                            mostRecentDimension = dimension;
                                            UpdateTreeViewGUI(dimension);
                                            
                                        }
                                        else {
                                            UpdateTreeViewGUI(mostRecentDimension);
                                        }
                                    }
                                    break;

                                case RequestMessage.IMPORT_AOIA: 
                                    {
                                        try {
                                            AOIAImportRequest request = (AOIAImportRequest)message.Data;
                                            ItemDBConverter.ImportAOIA(request.Path, UpdateLoadingScreenImmediately, request.ClearExistingDatabase);
                                        }
                                        catch (Exception ex) {
                                            logger.Fatal(ex.Message + ex.StackTrace);
                                            ExceptionReporter.ReportException(ex);
                                            /*resultQueue.Enqueue(
                                                new MessageData { 
                                                    type = MessageDataType.RES_EXCEPTION,
                                                    data = "Something went horribly wrong!\nCould not import the AOIA database.\n" + ex.Message
                                                });*/
                                            MessageBox.Show("Something went horribly wrong!\nCould not import the AOIA database.\n" + ex.Message, "Oh Noes!");
                                        }
                                        UpdateTreeViewGUI(mostRecentDimension);
                                        resultQueue.Enqueue(new MessageData { Type = MessageDataType.RES_READY_TO_BEGIN });                                        
                                    }
                                    break;


                                // Load buff items
                                case RequestMessage.BUFF_ITEMS: 
                                    {
                                        RequestBuffItems request = (RequestBuffItems)message.Data;
                                        //var results = SqlCE.GetBuffItems(request);
                                        IList<Item> results = itemDao.GetBuffItems(request.Skills, request.Dimension);
                                        containerMapper.MapContainerNames(results);
                                        
                                        resultQueue.Enqueue(new MessageData {
                                            Type = MessageDataType.RES_BUFF_ITEMS,
                                            Data = results
                                        });

                                    }
                                    break;

                                case RequestMessage.RENAME_CONTAINER: 
                                    {
                                        BackpackRenameRequest data = message.Data as BackpackRenameRequest;
                                        long charid = data.CharacterId;
                                        if (accountMap_.ContainsKey(charid)) {
                                            BackpackRenamer.RenameBackpack(accountMap_[charid], data.Name, data.ContainerId, charid, true);
                                            UpdateTreeViewGUI(mostRecentDimension);
                                        }
                                        else {
                                            logger.Warn("Backpack rename canceled, no account name for character " + charid);
                                        }
                                    }
                                    break;
                                    /*
                                case RequestMessage.UPDATE_AO_ITEM_DATABASE:
                                    if (backgroundRipper != null) {
                                        backgroundRipper.SaveProcessedItems((List<AOItem>)message.Data);
                                    }
                                    
                                    break;*/

                                case RequestMessage.DELETE_CHARACTER:
                                    characterDao.Delete((long)message.Data);
                                    UpdateTreeViewGUI(mostRecentDimension);
                                    break;

                                case RequestMessage.ARCHIVE_CHARACTER:
                                    characterDao.AchieveCharacter((long)message.Data);                                    
                                    UpdateTreeViewGUI(mostRecentDimension);
                                    break;

                                // Immediate backup requested
                                case RequestMessage.CREATE_BACKUP_IMMEDIATELY:
                                    cloudBackup.Backup();
                                    break;


                                // Update the "container" for item identification
                                case RequestMessage.IDENTIFY_ITEMS: 
                                    {
                                        identify_item_message = message;
                                    }
                                    break;

                                case RequestMessage.GET_TREEVIEW_CHILDREN: {
                                        CharacterAndContainer request = (CharacterAndContainer)message.Data;

                                        SendResponse(MessageDataType.RES_SET_TREEVIEW_CHILDREN,
                                            treeviewUpdater.UpdateTreeViewNode(request.CharacterId, request.ContainerId)
                                        );
                                    }
                                    break;
                                case RequestMessage.GET_TREEVIEW_CHILDREN_AND_SELECT: 
                                    {
                                        UpdateTreeviewNodeRequest request = (UpdateTreeviewNodeRequest)message.Data;

                                        SendResponse(MessageDataType.RES_SET_TREEVIEW_CHILDREN,
                                            treeviewUpdater.UpdateTreeViewNode(request.CharacterId, request.ContainerId)
                                        );

                                        if (request.ContainerToSelect != 0)
                                            SendResponse(MessageDataType.RES_SET_TREEVIEW_SELECTION, new CharacterAndContainer {
                                                ContainerId = request.ContainerToSelect,
                                                CharacterId = request.CharacterId
                                            });
                                    }
                                    break;

                                // Load patterns to display in web view
                                case RequestMessage.LOAD_SINGLE_PATTERN: 
                                    {
                                        PatternRequest request = (PatternRequest)message.Data;

                                        List<PatternPiece> result = patternDao.GetPiecesForPattern(request.Name, request.Dimension, request.CharacterId);
                                        containerMapper.MapContainerNamesForPatterns(ref result);
                                        var drops = symbiantDao.GetLootForBoss(request.Name);
                                        var locations = patternDao.GetPatternDropLocations(request.Name);

                                        resultQueue.Enqueue(new MessageData {
                                            Type = MessageDataType.RES_LOAD_SINGLE_PATTERN,
                                            Data = new PatternLookupResult { Pieces = result, Name = request.Name, Drops = drops, Locations = locations }
                                        });
                                    }
                                    break;
                                    
                                case RequestMessage.REQ_CHARACTER:
                                    resultQueue.Enqueue(new MessageData {
                                        Type = MessageDataType.RES_CHARACTER,
                                        Data = characterDao.GetById((long)message.Data)
                                    });
                                    break;

                                case RequestMessage.SHOW_EQUIPPED_ITEM: 
                                    {
                                        var cad = (CharacterAndContainer)message.Data;

                                        resultQueue.Enqueue(new MessageData {
                                            Type = MessageDataType.RES_SHOW_EQUIPPED_ITEM,
                                            Data = itemDao.GetByOwnerAndSlot(cad.CharacterId, (int)cad.ContainerId)
                                        });
                                    }
                                    break;

                                case RequestMessage.RESTART_BACKPACK_RENAMER:
                                    BackpackRenamer.RenameBackpacksAsync(itemDao.ListAllContainers(), backpackRenameCompleted);
                                    break;
                                    
                                case RequestMessage.UPDATE_PATTERNS: 
                                    {
                                        PatternRequest request = (PatternRequest)message.Data;
                                        logger.DebugFormat("Updating patterns.. Set: {0}", request.set);

                                        Dictionary<string, Pattern> result = new Dictionary<string, Pattern>();
                                        if (request.set == PatternRequest.PatternSet.SHOW_ALL) {
                                            result = patternDao.GetAllPatterns();

                                            // Send off a preliminary result
                                            // OBS: Since this data is still being modified, send off a fresh copy!
                                            resultQueue.Enqueue(new MessageData {
                                                Type = MessageDataType.RES_UPDATE_PATTERNS,
                                                Data = patternDao.GetAllPatterns()
                                            });
                                            logger.Debug("Updating patterns.. Preliminary results sent to GUI");

                                            var filledPatterns = patternDao.GetPatternsForCharacter(request.Dimension, request.CharacterId);
                                            foreach (var key in filledPatterns.Keys) {
                                                result[key] = filledPatterns[key];
                                            }
                                        }
                                        else if (request.set == PatternRequest.PatternSet.SHOW_PARTIAL) {
                                            result = patternDao.GetPatternsForCharacter(request.Dimension, request.CharacterId);
                                        }
                                        else if (request.set == PatternRequest.PatternSet.SHOW_COMPLETEABLE) {
                                            var filledPatterns = patternDao.GetPatternsForCharacter(request.Dimension, request.CharacterId);

                                            result = filledPatterns.Where(pair => pair.Value.Total >= 1.0f)
                                                .ToDictionary(pair => pair.Key, pair => pair.Value);
                                        }

                                        else if (request.set == PatternRequest.PatternSet.SHOW_OMNI || request.set == PatternRequest.PatternSet.SHOW_CLAN) {
                                            if (request.set == PatternRequest.PatternSet.SHOW_OMNI)
                                                result = patternDao.GetAllPBPatternsOmni();
                                            else
                                                result = patternDao.GetAllPBPatternsClan();

                                            // Send off a preliminary result
                                            resultQueue.Enqueue(new MessageData {
                                                Type = MessageDataType.RES_UPDATE_PATTERNS,
                                                Data = result
                                            });
                                            logger.Debug("Updating patterns.. Preliminary results sent to GUI");

                                            var filledPatterns = patternDao.GetPatternsForCharacter(request.Dimension, request.CharacterId);
                                            foreach (var key in filledPatterns.Keys) {
                                                if (result.ContainsKey(key))
                                                    result[key] = filledPatterns[key];
                                            }
                                        }

                                        resultQueue.Enqueue(new MessageData {
                                            Type = MessageDataType.RES_UPDATE_PATTERNS,
                                            Data = result
                                        });
                                        logger.Debug("Updating patterns.. Final result sent to GUI");

                                    }
                                    break;

                            }

                        }
                    }

                    // We might have gotten several identify messages, only process the last one.
                    if (identify_item_message.Type != RequestMessage.ERROR) {
                        UpdateIdentifyRequest request = (UpdateIdentifyRequest)identify_item_message.Data;
                        List<Item> results = LegacySQL.CListItemsForIdentify(request.Type, request.Dimension);
                        containerMapper.MapContainerNames(results);
                        resultQueue.Enqueue(new MessageData {
                            Type = MessageDataType.RES_IDENTIFY_DATA,
                            Data = results
                        });
                    }


                    // We might have gotten several search messages, only process the last one.
                    if (search_item_message.Type != RequestMessage.ERROR) {
                        SearchRequest request = (SearchRequest)search_item_message.Data;

                        var GMIHits = GMIDao.SearchForItems(request.Search, request.Dimension);
                        List<Item> results = new List<Item>(itemDao.ListByName(request.Search, request.CharacterId, request.Dimension, request.MinQL, request.MaxQL));

                        if (results.Count > 0) {
                            containerMapper.MapContainerNames(results);
                            results.AddRange(GMIHits);
                            resultQueue.Enqueue(new MessageData { Type = MessageDataType.RES_SEARCH_RESULT, Data = results });
                            logger.Debug(string.Format("Search for {0} on dimension {1} resulted in {2} items + {3} gmi items", 
                                request.Search, request.Dimension, results.Count, GMIHits.Count));
                        }
                        else {
                            resultQueue.Enqueue(new MessageData { Type = MessageDataType.RES_SEARCH_RESULT, Data = GMIHits });
                            logger.Debug(string.Format("Search for {0} on dimension {1} resulted in 0 DB hits and {2} gmi hits", request.Search, request.Dimension, GMIHits.Count));
                        }
                        
                    }

                }

                // Store any items in the queue
                if (backgroundRipper != null)
                    backgroundRipper.Process();


                if ((bool)Properties.Settings.Default["waypoints"]) {
                    ScriptUtilities.UpdateWaypointsTimed();
                }

                stopwatchManager.Execute();
                
                
                treeviewUpdater.Update(resultQueue);
                
                cloudBackup.Update();

                
                // Check if we require an update to the item DB 
                // This happens if AO:Light was started before AO, on the first-run-ever.
                if (requireItemUpdate != null && requireItemUpdate.ElapsedMilliseconds > 1000 * 30 && backgroundRipper == null) {
                    logger.Info("Attempting to detect anarchy online..");
                    requireItemUpdate.Restart();

                    if (AppdataPathFinder.HasValidAnarchyPath() && backgroundRipper == null) {
                        backgroundRipper = new BackgroundRipper(AppdataPathFinder.AnyAnarchyDirectory());
                    }
                }

                if (!worker.CancellationPending)
                    worker.ReportProgress(0);
            }

            // Save positions before shutting down
            characterDao.UpdateCharacterPositionsFromPositionTracker();
            injector.Cancel();
            injector.Dispose();
            if (backgroundRipper != null) {
                backgroundRipper.Dispose();
                backgroundRipper = null;
            }
        }

        public void Dispose() {
            bw.CancelAsync();
        }


    }
}


