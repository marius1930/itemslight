﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using ItemsLight.AOIA.Parsers;
using log4net;
using ItemsLight.AOIA.Misc;
using ItemsLight.Dao;
using ItemsLight.Model;
using EvilsoftCommons.Exceptions;

namespace ItemsLight.AOIA {
    public enum MessageId {
        MSG_WITH_AWESOMIUM_COOKIES = 0x1B,
        MSG_SKILLSYNC = 0x3E205660,
        MSG_CONTAINER = 0x4E536976,
        MSG_BACKPACK = 0x465A5D73,
        MSG_CHAT = 0x5F4B442A,
        MSG_MISSIONS = 0x5C436609,
        MSG_BANK = 0x343c287f,   // size 2285.. dynamic most likely
        // MSG_POS_SYNC     = 0x5E477770,   // size = 55 bytes Thats wrong?
        MSG_UNKNOWN_1 = 0x25314D6D,   // size = 53 bytes
        MSG_UNKNOWN_2 = 0x00000001,   // size = 40 bytes
        MSG_UNKNOWN_3 = 0x0000000b,   // size = 548 bytes
        MSG_FULLSYNC = 0x29304349,   // size 4252.. dynamic?  Contains equip and inv + atribs (?) +++
        MSG_UNKNOWN_4 = 0x54111123,
        MSG_MOB_SYNC = 0x271B3A6B,
        MSG_SHOP_ITEMS = 0x353F4F52,   // Content of a player shop
        MSG_SHOP_INFO = 0x2e072a78,   // Information about a playershop. Received on zone into a playershop.
        MSG_ITEM_MOVE = 0x47537a24,   //Client or server msg move item between inv/bank/wear/backpack
        MSG_ITEM_BOUGHT = 0x052e2f0c,   //Sent from server when buying stuff, 1 message pr item.
        MSG_OPENBACKPACK = 0x52526858,   //1196653092: from client when opening backpack
        MSG_CHAR_OPERATION = 0x5e477770,   //was MSG_ITEM_OPERATION //from client when deleting an item, splitting or joining, nano, perk, tradeskill+++
        MSG_SHOP_TRANSACTION = 0x36284f6e,   //from server and client when adding/removing/accepting shop stuff
        MSG_PARTNER_TRADE = 0x35505644,   //from server when trade partner adds/removes items.
        MSG_SHOW_TEXT = 0x206b4b73,   //from server when showing yellow text (like "You succesfully combined.../XXX is already fully loaded")
        MSG_SPAWN_REWARD = 0x3b11256f,
        //MSG_DELETE_MISH??= 0x212c487a 
        //MSG_NPC_CHATLINE = 0x5d70532a
        //MSG_NPC_CHATCHOICES = 0x55704d31
        //MSG_NPC_TRACE_CANCEL = 0x2d212407
        MSG_GIVE_TO_NPC = 0x3a1b2c0c,   //give or remove from npc trade win
        MSG_ACCEPT_NPC_TRADE = 0x2d212407,   //From server when accepting trade, contains items npc didnt want..
        MSG_END_NPC_TRADE = 0x55682b24,   //From client when ending trade, contains accept or cancel

        
        SMSG_MOVED = 1410404643,
        SMSG_UNKNOWN_WITH_ZONEID0 = 0x43197D22,	// [2+8*4], [2+12*4]
        SMSG_UNKNOWN_WITH_ZONEID1 = 0x5F4B1A39,	// [2+5*4], [2+9*4]
        SMSG_UNKNOWN_WITH_ZONEID2 = 0x3B1D2268,	// [1 + 3*4]
        SMSG_CORPSESPAWN = 0x4F474E05, // it triggers on a mob death, contains the amount of credits he has
        SMSG_RELATED_TO_CORPSESPAWN = 0x50544D19, // triggers on mob death

        SMSG_OPEN_MAIL = 859514983,
        //SMSG_GET_ITEM_FROM_MAIL = 894457412, // trade partner message
        /*
        followed by a move slotId0 from overflow each!
        spiked food sack: (id 0187C4)
        DFDF000A 00010077 00000BF9 991FC4E6 3B11256F
        0000C73D 498B058E 00000000 0B0000C3 50991FC4
        E6000002 1C000F42 4F000000 00716F00 001F8800
        00000080 00000300 00001700 0187C400 0002BD00
        00000100 0002BE00 0187C400 0002BF00 0187C400
        00019C00 00000100 0001EB00 00000100 000000FD

        mission key:
        DFDF000A 00010089 00000BF9 991FC4E6 3B11256F
        0000C76D 0907C318 00000000 0B0000C3 50991FC4
        E6000002 1C000F42 4F000000 00716F00 001B9700
        00000080 00020500 00001700 006FA100 0002BD00
        00000100 0002BE00 006FA100 0002BF00 006FA100
        00019C00 00000100 00001A4D 69737369 6F6E206B
        65792074 6F206120 6275696C 64696E67 00FDFDFD

        [5500] operationId  0xe 
        [5500] operationId  0x71 

        //Also TODO: mish items deleted when mish deleted not auto
        */

        MSG_CHAR_INFO = 0x4d38242e,
        MSG_ORG_CONTRACT = 0x64582a07,
        MSG_ORG_CITY_P1 = 0x365e555b,
        MSG_ORG_CITY_P2 = 0x5f4b442a,

        /*
        Sent to client when a nano program terminated.
        Ex: Body Boost (29091 / 0x71A3) terminated:
        2AB4000A 00010027 00000BDE AB106B4D 39343C68
        0000C350 AB106B4D 00000000 00CF1B00 0071A3FD <-- ID almost at the end.
        */
        MSG_PROGRAM_TERMINATED = 0x39343c68,
    };


    class ParseServerMessage {
        static ILog logger = LogManager.GetLogger(typeof(ParseServerMessage));
        CharacterDao characterDao = new CharacterDao();
        ItemDao itemDao = new ItemDao();

        public bool MSG_MOB_SYNC(byte[] bdata) {
            MobInfo msg = new MobInfo(bdata);
            if (msg.CharacterId == msg.EntityId) {

                // Thanks for all the help!
                string[] b = {"demoder", "cratine", "savagedheals", "enfine", "zoewrangle", "savagedlight"};
                foreach (string cc in b) {
                    if (cc.Equals(msg.charname.ToLower())) {
                        logger.Warn("Attempting to use a toolchain which isn't currently available");
                        return true;
                    }
                }

                characterDao.UpdateNameAndDimension(msg.CharacterId, msg.charname.Replace("\0", "").Trim(), (int)msg.dimensionid);

            }
            MonsterNameTracker.Instance.MapNameToMonster(msg.EntityId, msg.charname.Replace("\0", "").Trim());

            return true;
        }

        public bool MSG_GIVE_TO_NPC(byte[] bdata) {
            AOGiveToNPC give = new AOGiveToNPC(bdata);

            uint fromContainerId = Inventory.GetFromContainerId(give.CharacterId, give.invItemGiven.type, give.invItemGiven.containerTempId); 
            uint toContainer = (uint)InventoryIds.INV_TRADE;
            
#if DEBUG
            logger.DebugFormat("GIVE_TO_NPC: {0} gave item {1} into container {2}, (direction {3})",
                give.CharacterId, give.invItemGiven.type, give.invItemGiven.containerTempId);
            if (give.direction == 1 && fromContainerId != (uint)InventoryIds.INV_TRADE)
                logger.Debug("GIVE_TO_NPC message was discarded, source is not trade container");
#endif

            if (give.direction == 1) {
                toContainer = (uint)InventoryIds.INV_TOONINV;
                if (fromContainerId != (uint)InventoryIds.INV_TRADE)
                    return false;
            }

            itemDao.UpdateParentForItem(toContainer, fromContainerId, give.invItemGiven.itemSlotId, give.CharacterId);



            return true;            
        }

        enum ShopTransactionOperationId {
            START_TRADE = 0x00,
            CLIENT_ACCEPT = 1,
            DECLINE = 2,
            TEMP_ACCEPT = 3,
            COMMIT = 4,
            ADD_ITEM = 5,
            REMOVE_ITEM = 6,
            UNKNOWN = 7,
            ADD_BACKPACK = 8,
            REMOVE_BACKPACK = 9
        };

        

        public bool MSG_SHOP_TRANSACTION(byte[] bdata) {
            AOTradeTransaction item = new AOTradeTransaction(bdata);

#if DEBUG
            logger.DebugFormat("SHOP_TRANSACTION.. {0}", (ShopTransactionOperationId)item.operationId);
#endif

            ShopTransactionOperationId operation = (ShopTransactionOperationId)item.operationId;
            uint otherTradeContainer = (uint)InventoryIds.INV_TRADEPARTNER; //trade partner
            const uint shopContainer = (uint)InventoryIds.INV_TRADE;

            if (item.EntityId != item.CharacterId) {

                switch (operation) {
                    case ShopTransactionOperationId.ADD_ITEM: 
                        {
                            uint fromContainerId = Inventory.GetFromContainerId(item.CharacterId, item.itemToTrade.type, item.itemToTrade.containerTempId);
                            uint nextFreeInvSlot = itemDao.FindNextAvailableContainerSlot(item.CharacterId, shopContainer);
                            itemDao.UpdateParentAndSlotForItem(shopContainer, nextFreeInvSlot, fromContainerId, item.itemToTrade.itemSlotId, item.CharacterId);
#if DEBUG
                            logger.Debug(string.Format("SHOP_TRANSACTION: Player {0} added item {1} to a trade.. fromid: {2}, char {3}", item.m_target, item.itemToTrade.type, item.fromId.low, item.CharacterId));
#endif
                        }
                        break;
                }

                return true;
            }


            switch (operation) {
                case ShopTransactionOperationId.START_TRADE:
                    itemDao.DeleteItemsInTradeContainer(item.CharacterId, shopContainer, otherTradeContainer);
                    
                    break;

                case ShopTransactionOperationId.DECLINE:                    
                    {
                        uint shopCapacity = 35;
                        uint newParent = (uint)InventoryIds.INV_TOONINV;
                    
                        for (uint i = 0; i <= shopCapacity; i++) {
                            uint nextFreeInvSlot = itemDao.FindNextAvailableContainerSlot(item.CharacterId, newParent);
                            if (nextFreeInvSlot >= 94) {
                                // If full, move to overflow. 
                                // This seems to crash with a MSG_CONTAINER on overflow before this, even if overflow already open.
                                // if we dont break here, we get duplicate items in IA.
                                break;
                            }
                    
                            // Move stuff back
                            itemDao.UpdateParentAndSlotForItem(newParent, nextFreeInvSlot, shopContainer, i, item.CharacterId);
                        }
                                        
                        // Delete items our trade partner had.
                        itemDao.DeleteFromContainer(item.CharacterId, otherTradeContainer);
                    } 
                    break;

                case ShopTransactionOperationId.COMMIT:
                    if (item.fromId.low == 0xC790) {
                        // Playershops are removed from the game, ignore.
                    }
                    else {
                        //[13464] AOTradeTransaction:
                        //[13464] unknown2 1  operationId 4 
                        //[13464] fromType c75b 
                        //[13464] fromContainerTempId 2018  fromItemSlotId c404 
                        //[13464] fromId [51035|538493956] 
                        
                        //[13464] AOTradeTransaction:
                        //[13464] unknown2 1  operationId 4 
                        //[13464] fromType c75b 
                        //[13464] fromContainerTempId 2018  fromItemSlotId c404 
                        //[13464] fromId [51035|538493956] 

                        // Commit/server accept: (bye-bye stuff!)
                        itemDao.RemoveItemFromTemporaryShopTradePartner(item.CharacterId, shopContainer);
                        itemDao.DeleteFromContainer(item.CharacterId, shopContainer);
                        

                        //grab the new stuff!
                        uint shopCapacity = 35;
                        itemDao.AddItemsReceivedFromShop(shopCapacity, item.CharacterId, otherTradeContainer);
                    }
                    break;

                case ShopTransactionOperationId.ADD_ITEM:
                    if (item.fromId.high == item.CharacterId) {
                        uint fromContainerId = Inventory.GetFromContainerId(item.CharacterId, item.itemToTrade.type, item.itemToTrade.containerTempId);
                        uint nextFreeInvSlot = itemDao.FindNextAvailableContainerSlot(item.CharacterId, shopContainer);
                        itemDao.UpdateParentAndSlotForItem(shopContainer, nextFreeInvSlot, fromContainerId, item.itemToTrade.itemSlotId, item.fromId.high);
                    }
                    else if (item.itemToTrade.type == 0x6b && item.fromId.low == 0xC790) {
                        // Deprecated: Player shop
                    }
                    break;

                case ShopTransactionOperationId.REMOVE_ITEM:
                    if (item.fromId.high == item.CharacterId) {
                        uint nextFreeInvSlot = itemDao.FindNextAvailableContainerSlot(item.CharacterId, (uint)InventoryIds.INV_TOONINV);
                        itemDao.RemoveItemFromTrade(nextFreeInvSlot, shopContainer, item.itemToTrade.itemSlotId, item.CharacterId);
                    }
                    else if (item.itemToTrade.type == 0x6b && item.fromId.low == 0xC790) {
                        // Deprecated: Player shop
                    }

                    break;

                case ShopTransactionOperationId.ADD_BACKPACK: 
                    {
                        //uint otherTradeContainer = (uint)InventoryIds.INV_TRADEPARTNER;
                        uint containerId = (uint)((item.itemToTrade.containerTempId << 16) + item.itemToTrade.itemSlotId);

                        // Insert it as a small backpack
                        uint nextFreeInvSlot = itemDao.FindNextAvailableContainerSlot(item.CharacterId, otherTradeContainer);
                        var item_ = ItemDao.CreateItem(
                                99228, 99228,
                                1, 0, 1,
                                otherTradeContainer, (int)nextFreeInvSlot, containerId, item.CharacterId);
                        itemDao.Save(item_);

                    }
                    break;

                case ShopTransactionOperationId.REMOVE_BACKPACK: 
                    {
                        //uint otherTradeContainer = (uint)InventoryIds.INV_TRADEPARTNER;
                        uint containerId = (uint)((item.itemToTrade.containerTempId << 16) + item.itemToTrade.itemSlotId);
                        itemDao.RemoveBackpackFromTradeWindow(item.CharacterId, containerId, otherTradeContainer);
                    }
                    break;
                    
            }

            return true;
        }


        public bool MSG_CHAR_INFO(byte[] bdata) {
            AOMessageBase msg = new AOMessageBase(bdata);
	        msg.popInteger();
	        int level = msg.popChar();
	        if (level == 200) 
                return false;		// Can't detect shadowlevels yet, ignore 200.
	        int TL = msg.popChar();
	        int prof = msg.popChar();
            if (msg.EntityId == msg.CharacterId) {
                //logger.Debug(string.Format("Character {0} is a level {1} profession {2}", msg.characterId, level, prof));
                characterDao.UpdateProfession(msg.CharacterId, prof);
            }
	        //out_id = msg.entityId();
	        //out_charid_self = msg.characterId();

            return true;
        }
     

        
        public bool MSG_ITEM_MOVE(byte[] bdata) {
            AOMessageBase msg = new AOMessageBase(bdata);
            
            //The ItemMoved is identical to the client send version except the header is server type.
            AOItemMoved moveOp = new AOItemMoved(bdata);

            uint fromContainerId = Inventory.GetFromContainerId(moveOp.CharacterId, moveOp.fromItem.type, moveOp.fromItem.containerTempId);
            uint newParent;
            uint toType = moveOp.toContainer.low;

            if (fromContainerId == (uint)InventoryIds.INV_OVERFLOW) {
                newParent = (uint)InventoryIds.INV_TOONINV; //toType seems to be 0x006e sometimes (tradeskill) when moving from overflow
            }
            else if (toType == 0xc749) //to backpack
                newParent = moveOp.toContainer.high;
            else if (toType == 0xdead)
                newParent = (uint)InventoryIds.INV_BANK;
            else if (toType == 0xc350)
                newParent = (uint)InventoryIds.INV_TOONINV;
            else if (toType == 0x006e) //to overflow
                newParent = (uint)InventoryIds.INV_OVERFLOW;
            else {
                logger.Warn("TO-DO: MOVE/UNKNOWN TO TYPE " + toType);
                return false;
            }

            if (fromContainerId == 0) {
                logger.Warn("FromContainerId not found in cache ");
                return false; //we don't have the value cached, either a bug or ia was started after the bp was opened.
            }

            ushort newSlot = moveOp.targetSlot;

            if (newParent == (uint)InventoryIds.INV_TOONINV) {
                if (newSlot < 64) { // 64 = first inv slot!
                    //we are moving to an equip panel!

                    //if anything there, we must hotswap!
                    //we move anything that was there to a temp hotswap inv, move the new item, then move the other back.

                    itemDao.UpdateParentAndSlotForItem((uint)InventoryIds.INV_HOTSWAPTEMP, moveOp.fromItem.itemSlotId,
                        newParent, newSlot, moveOp.CharacterId);

                    itemDao.UpdateParentAndSlotForItem(newParent, newSlot, fromContainerId, moveOp.fromItem.itemSlotId, moveOp.CharacterId);

                    itemDao.UpdateParentForItem(fromContainerId, (uint)InventoryIds.INV_HOTSWAPTEMP, moveOp.fromItem.itemSlotId, moveOp.CharacterId);

                    return true;
                }
                else if (newSlot >= 0x6f) {
                    //Check if item is stackable, splittable, we are moving to inventory 
                    //and there is an item with the same itemkey there.
                    //If so join with the one with the lowest slotId (I think).
                    uint PROP_NO_SPLIT           = 0x01000000;
                    uint PROP_STACKABLE          = 0x00000200;
                    long itemProp = itemDao.GetItemProperties(moveOp.CharacterId, fromContainerId,  moveOp.fromItem.itemSlotId);

                    //DID during downtime, check this with extruder bars and ammo at least
                    if ((itemProp & PROP_STACKABLE) != 0 && ((itemProp & PROP_NO_SPLIT) == 0)) {
                        //OutputDebugString(_T("Its stackable"));
                        long newSlotId = itemDao.FindFirstItemOfSameType(moveOp.CharacterId, fromContainerId,
                                                                            moveOp.fromItem.itemSlotId, newParent);

                        if (newSlotId > 0) {
                            // Stack join from move operation
                            //join them and delete the old one.
                            itemDao.UpdateAmountInStack(msg.CharacterId, fromContainerId, moveOp.fromItem.itemSlotId, newParent, newSlot);
                            itemDao.DeleteAtSlotInContainer(msg.CharacterId, fromContainerId, moveOp.fromItem.itemSlotId);

                            return true;
                        }
                    }
                }
            }


            if (newSlot >= 0x6f || newSlot == 0x00)  {
                newSlot = (ushort)itemDao.FindNextAvailableContainerSlot(moveOp.CharacterId, newParent);                 // why make this a short?
            }

            itemDao.UpdateParentAndSlotForItem(newParent, newSlot, fromContainerId, moveOp.fromItem.itemSlotId, moveOp.CharacterId);         
            return true;
        }




        public bool MSG_SPAWN_REWARD(byte[] bdata) {
            OpenBackpack bp = new OpenBackpack(bdata);

            if (bp.ownerId.high == bp.CharacterId) {

                if (bp.operationId == 0x01) {
                    //TO-DO:
                    //this sets an item's temp id on zone!
                    //stored in bp.target().High() in this msg.
                    //also contains inv slot id. (plus keylow/ql/stack/flags)
                    //when mission deleted, this id is sent in a MSG_CHAR_OPERATION/op 0x2f
                    TempContainerCache.Instance.UpdateTempItemId(bp.CharacterId, bp.m_target, bp.invSlot); 

                }
                else if (bp.operationId == 0x71) { //I got an mission item
                    uint containerId = bp.m_target;
                    uint newParent = (uint)InventoryIds.INV_OVERFLOW;

                    //find a free spot:
                    uint slotId = bp.invSlot;
                    if (slotId >= 0x6f) {
                        slotId = itemDao.FindNextAvailableContainerSlot(bp.CharacterId, newParent);
                    }

                    // Add item
                    itemDao.Save(ItemDao.CreateItem(bp.itemKeyLow, bp.itemKeyHigh, (int)bp.ql, bp.flags, 1, newParent, (int)slotId, 0, bp.CharacterId));

                    //this will be moved to the next inv slot in the next msg.
                    uint invSlotId = bp.invSlot;
                    if (invSlotId >= 0x6f)
                        invSlotId = itemDao.FindNextAvailableContainerSlot(bp.CharacterId, (uint)InventoryIds.INV_TOONINV);

                    TempContainerCache.Instance.UpdateTempItemId(bp.CharacterId, bp.m_target, invSlotId);


                }
                else {
                    logger.Warn("UNKNOWN MSG_SPAWN_REWARD operation Id: " + bp.operationId);
                    return false;
                }
            }
        

            return true;
        }

        
        public bool MSG_ITEM_BOUGHT(byte[] bdata) {
            BoughtItemFromShop item = new BoughtItemFromShop(bdata);

            uint nextFreeInvSlot = itemDao.FindNextAvailableContainerSlot(item.CharacterId, (uint)InventoryIds.INV_TOONINV);
            itemDao.Save(ItemDao.CreateItem(item.itemid.low, item.itemid.high, (int)item.ql, item.flags, item.stack,
                (uint)InventoryIds.INV_TOONINV, // parent 2 = equip
                (int)nextFreeInvSlot, 0, item.CharacterId));

            return true;
        }

        private enum PartnerTradeOperation {
            OTHER_ADD_ITEM = 0x55,
            OTHER_REMOVE_ITEM = 0x56,
            TRADESKILL_OVERFLOWED = 0x57,
            USE_DEPLETEABLE_ITEM = 0x03, // Use a stim/healthkit etc, reduce stack.
        }

    
        public bool MSG_PARTNER_TRADE(byte[] bdata) {
            AOPartnerTradeItem msg = new AOPartnerTradeItem(bdata);


            // Make sure were not spying on others
            if (msg.EntityId != msg.CharacterId) {
#if DEBUG
                if (msg.operationId == (uint)PartnerTradeOperation.OTHER_REMOVE_ITEM)
                    logger.DebugFormat("PARTNER_TRADE IGNORED: {0} removed item {1}/{2} from trade container..", msg.targetId, msg.itemid.low, msg.itemid.high  );
                else if (msg.operationId == (uint)PartnerTradeOperation.OTHER_ADD_ITEM)
                    logger.DebugFormat("PARTNER_TRADE IGNORED: {0} added item {1}/{2} to trade container..", msg.targetId, msg.itemid.low, msg.itemid.high  );
#endif
                return true;
            }


            uint opId = msg.operationId;
            switch (opId) {
                case (uint)PartnerTradeOperation.OTHER_ADD_ITEM: {
                        uint otherTradeContainer = (uint)InventoryIds.INV_TRADEPARTNER;
                        if (msg.partnerInvItem.type == 0x6e || msg.partnerInvItem.type == 0x68) {
                            uint slotId = msg.partnerInvItem.itemSlotId;

                            if (slotId >= 0x6f)
                                slotId = itemDao.FindNextAvailableContainerSlot(msg.CharacterId, otherTradeContainer);

                            itemDao.Save(ItemDao.CreateItem(msg.itemid.low,
                                            msg.itemid.high,
                                            (int)msg.ql,
                                            msg.flags,
                                            msg.stack,
                                            otherTradeContainer,  // 2 = inventory
                                            (int)slotId,
                                            0,
                                            msg.CharacterId));

#if DEBUG
                            logger.DebugFormat("PARTNER_TRADE(OTHER_ADD): Got item {0}:{1}.. stored to character {2}", msg.itemid.low, msg.ql, msg.CharacterId);
#endif
                        }
                    }
                    break;

                case (uint)PartnerTradeOperation.OTHER_REMOVE_ITEM: 
                    {
                        if (msg.partnerInvItem.type == 0x6c) { //remove from trade window

                            uint otherTradeContainer = (uint)InventoryIds.INV_TRADEPARTNER;
                            itemDao.RemoveWhereParentSlotOwner(msg.CharacterId, msg.partnerInvItem.itemSlotId, otherTradeContainer);
#if DEBUG
                            logger.DebugFormat("PARTNER_TRADE(REMOVE_FROM_TRADE): An item was removed..");
#endif
                        }
                    }
                    break;

                case (uint)PartnerTradeOperation.TRADESKILL_OVERFLOWED: 
                    {
                        if (msg.partnerInvItem.type == 0x6e) {
                            uint slotId = msg.partnerInvItem.itemSlotId;
                            uint otherTradeContainer = (uint)InventoryIds.INV_OVERFLOW;
                            if (slotId >= 0x6f)
                                slotId = itemDao.FindNextAvailableContainerSlot(msg.CharacterId, otherTradeContainer);

                            itemDao.Save(ItemDao.CreateItem(msg.itemid.low,
                                                   msg.itemid.high,
                                                   (int)msg.ql,
                                                   msg.flags,
                                                   msg.stack,
                                                   otherTradeContainer,  // overflow
                                                   (int)slotId,
                                                   0,
                                                   msg.CharacterId));
                        }
                    }
                    break;

                case (uint)PartnerTradeOperation.USE_DEPLETEABLE_ITEM: 
                    {
                        uint fromContainerId = Inventory.GetFromContainerId(msg.CharacterId,
                                                                      msg.partnerInvItem.type,
                                                                      msg.partnerInvItem.containerTempId);
                        
                        itemDao.ReduceStack(msg.CharacterId, fromContainerId, msg.partnerInvItem.itemSlotId, 
                            msg.itemid.high, msg.itemid.low);

                        itemDao.DeleteEmptyStack(msg.CharacterId, fromContainerId, msg.partnerInvItem.itemSlotId,
                            msg.itemid.high, msg.itemid.low);

                        
                    }
                    break;
            }
            
            return true;
        }

        // CharId => Name
        // CharId => X

        /// <summary>
        /// A corpse has spawned, this contains the container ID of his loot, and characterId.
        /// </summary>
        /// <param name="bdata"></param>
        /// <returns></returns>
        public bool SMSG_CORPSESPAWN(byte[] bdata) {
            try {
                CorpseSpawn msg = new CorpseSpawn(bdata);
                MonsterNameTracker.Instance.MapMonsterToContainer(msg.MonsterId, msg.EntityId);
            }
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex, string.Format("SMSG_CORPSESPAWN Length of message is {0}", bdata.Length));
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                return false;
            }
            return true;
        }
        
        public bool MSG_CHAR_OPERATION(byte[] bdata) {
            CharacterAction msg = new CharacterAction(bdata);

            // Ignore messages regarding others
            if (msg.EntityId != msg.CharacterId)
                return true;

            uint opId = msg.operationId;
            if (opId == 112) { // Delete (user or tradeskill)
                uint fromContainerId = Inventory.GetFromContainerId(msg.CharacterId, msg.itemToDelete.type, msg.itemToDelete.containerTempId);
                if (fromContainerId == 0)
                    return false; // No container found

                itemDao.DeleteAtSlotInContainer(msg.CharacterId, fromContainerId, msg.itemToDelete.itemSlotId);
            }
            else if (opId == 47) { // Delete temporary item

                
                uint fromContainerId = (uint)InventoryIds.INV_TOONINV;
                uint charId = msg.CharacterId;
                uint itemTempId = (uint)(msg.itemToDelete.containerTempId << 16) + msg.itemToDelete.itemSlotId;
                uint realSlotId = TempContainerCache.Instance.GetItemSlotId(charId, itemTempId); // Need to calculate the real slot ID from the temporary one

                itemDao.DeleteAtSlotInContainer(msg.CharacterId, fromContainerId, msg.itemToDelete.itemSlotId);
                if (realSlotId == 0)
                    return false;

                itemDao.DeleteAtSlotInContainer(msg.CharacterId, fromContainerId, realSlotId);
            }
            else if (opId == 53) { // MERGE STACKS
                /*
                [9352] from Server: CHAR_ACTION_JOINSTACKS
                [9352] AOCharacterAction:MsgId 5e477770 CharId 2568996070 Target 2568996070 
                [9352] operationId  0x35 
                [9352] unknown3  0x0 
                [9352] fromType  0x68 
                [9352] fromContainerTempId 0 
                [9352] fromItemSlotId 58 
                [9352] itemId [104|90]
                */
                
                ushort removedItemContType = (ushort)(msg.itemId.low & 0xff);
                ushort removedItemContTempId = (ushort)(msg.itemId.high >> 16);
                ushort removedItemSlotId = (ushort)(msg.itemId.high & 0xff);
                uint removedItemContainerId = Inventory.GetFromContainerId(msg.CharacterId, removedItemContType,
                                                                         removedItemContTempId);
                uint fromContainerId = Inventory.GetFromContainerId(msg.CharacterId, msg.itemToDelete.type,
                                                                  msg.itemToDelete.containerTempId);
                if (fromContainerId == 0)
                    return false;

                // Merge / Add to stack
                itemDao.MergeItemStacks(msg.CharacterId, removedItemContainerId, removedItemSlotId, fromContainerId, msg.itemToDelete.itemSlotId);

                // Remove old
                itemDao.DeleteAtSlotInContainer(msg.CharacterId, removedItemContainerId, removedItemSlotId);
            }

            return true;
        }


        
        public bool MSG_ACCEPT_NPC_TRADE(byte[] bdata) {
            AONPCTradeAccept msg = new AONPCTradeAccept(bdata);
            
            // No need to log trades to others
            if (msg.EntityId != msg.CharacterId) { // "entityId", is this right?
                return true;
            }

            
            
            
            uint newParent = (uint)InventoryIds.INV_TOONINV;
            uint shopContainer = (uint)InventoryIds.INV_TRADE;

            // Add items received
            itemDao.AddItemsReceivedFromTrade(msg.items, msg.CharacterId, newParent, shopContainer);

            // Delete items given away
            itemDao.DeleteFromContainer(msg.CharacterId, shopContainer);

#if DEBUG
            foreach (var item in msg.items) {
                logger.DebugFormat("Character {0} received item {1}:{2}, stored in container {3} (from shop {4})", msg.CharacterId, item.itemid, item.ql, newParent, shopContainer);
                    
            }
            logger.DebugFormat("ACCEPT_NPC_TRADE: char {0} deleted all items in trade-container {1}", msg.CharacterId, shopContainer);
#endif
            

            return true;
        }


        Dictionary<uint, uint> MailCreditsMap = new Dictionary<uint, uint>();

        public bool MSG_OPEN_MAIL(byte[] bdata) {
            try {
                ReceivedMail msg = new ReceivedMail(bdata);
                if (msg.Type2 == ReceivedMail.MESSAGE_WITH_CREDITS) {
                    logger.DebugFormat("Got {1} credits in mail {0}, flag:{2}", msg.MailId, msg.Credits, msg.Flag);
                    MailCreditsMap[msg.MailId] = msg.Credits;
                }
                else if (msg.Type2 == ReceivedMail.MESSAGE_LOOT) {
                    logger.DebugFormat("Looted credits from mail {0}, flag:{1}", msg.MailId, msg.Flag);
                    if (MailCreditsMap.ContainsKey(msg.MailId))
                        characterDao.UpdateCredits(msg.CharacterId, MailCreditsMap[msg.MailId]);
                    else
                        logger.WarnFormat("Looted credits from inbox {0}, but credit value is not in cache!", msg.MailId);
                }
                else if (msg.Type2 == ReceivedMail.MESSAGE_WITH_CREDITS_PAD) {
                    logger.DebugFormat("Got {1} credits in mail {0}, flag:{2} (padded)", msg.MailId, msg.Credits, msg.Flag);
                    MailCreditsMap[msg.MailId] = msg.Credits;
                }
                else if (msg.Type2 == ReceivedMail.MESSAGE_READ) {
                    logger.DebugFormat("Mail {0} has been marked as read, flag:{1}", msg.MailId, msg.Flag);
                }
                // We just opened the mail terminal, should(tm) be safe to clear cache!
                else if (msg.Type2 == ReceivedMail.LIST_MESSAGES) {
                    MailCreditsMap.Clear();
                }
                return true;
            }
            catch (IndexOutOfRangeException ex) {
                ExceptionReporter.ReportException(ex, string.Format("Length of message is {0}", bdata.Length));
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
            }
            return false;
        }


        public bool MSG_FULLSYNC(byte[] bdata) {
            AOFullCharacterMessage parser = new AOFullCharacterMessage(bdata);
            
            // Remove old contents from DB (inventory, trade, remote trade and overflow win.
            itemDao.RemoveOldContentForCharacter(parser.CharacterId);


            TempContainerCache.Instance.ClearTempContainerIdCache(parser.CharacterId);

            // Register items
            List<AOContainerItem> items = parser.m_inventory;
            List<Item> items_ = new List<Item>();
            foreach (AOContainerItem item in items) {
                items_.Add(ItemDao.CreateItem(item.itemid.low, item.itemid.high, (int)item.ql, item.flags, item.stack,
                    (uint)InventoryIds.INV_TOONINV, // parent 2 = equip
                    (int)item.index, item.containerid.high, parser.CharacterId));
            }
            itemDao.Save(items_);

            if (parser.stats.ContainsKey(61)) {
                characterDao.UpdateCredits(parser.CharacterId, parser.stats[61]);
                logger.DebugFormat("Stored {0} credits for character {1}", parser.stats[61], parser.CharacterId);
            }
            if (parser.stats.ContainsKey(54)) {
                characterDao.UpdateLevel(parser.CharacterId, (int)parser.stats[54]);
            }
            if (parser.stats.ContainsKey(669)) {
                characterDao.UpdateVP(parser.CharacterId, (int)parser.stats[669]);
            }
            if (parser.stats.ContainsKey(60)) {
                characterDao.UpdateProfession(parser.CharacterId, (int)parser.stats[60]);
            }
            else if (parser.stats.ContainsKey(368)) {
                characterDao.UpdateProfession(parser.CharacterId, (int)parser.stats[368]);
            }
            itemDao.DeleteOrphanedContainersForCharacter(parser.CharacterId);

            // Since we probably zoned, lets clear our monster cache
            MonsterNameTracker.Instance.Clear();

            return true;
        }




        /// <summary>
        /// Happens when overflow opens, loot container opens or you get a new container from a trade.
        /// </summary>
        /// <param name="bdata"></param>
        /// <returns></returns>
        public bool MSG_CONTAINER(byte[] bdata) {
            Container bp = new Container(bdata);

            uint containerId = bp.contEnd.containerId.high;
            if (bp.contEnd.tempContainerId == 0x006e && bp.contEnd.containerId.low == 0xc350) {
                containerId = (uint)InventoryIds.INV_OVERFLOW;
            }


            // Remove old contents from BP
            itemDao.DeleteFromContainer(containerId);


            // Register BP contents
            List<Item> items_ = new List<Item>();
            foreach (ContItem item in bp.items) {
                items_.Add(ItemDao.CreateItem(item.itemid.low, item.itemid.high, (int)item.ql, item.flags, item.stack,
                    containerId, (int)item.index, 0, bp.CharacterId));
            }
            itemDao.Save(items_);

            //don't store INV_OVERFLOW as contId 110, this id can be used by a backpack!
            if (containerId != (uint)InventoryIds.INV_OVERFLOW) {
                TempContainerCache.Instance.UpdateTempContainerId(bp.CharacterId, bp.contEnd.tempContainerId, containerId);
            }
            

            // Update /loot
            if (bp.unknown2 == 0x00000002 && bp.contEnd.containerId.low == 51050 && items_.Count > 0) {
                itemDao.MapInternals(items_);
                ScriptUtilities.UpdateLootScript(items_, MonsterNameTracker.Instance.MapContainerToMonster(containerId));
                ScriptUtilities.WriteBotLootCommand(items_);
                logger.DebugFormat("Updated /loot and /eloot with {0} items", items_.Count);
            }

#if DEBUG
            if (bp.unknown2 == 0x00000002) {
                if (bp.contEnd.containerId.low == 51050) {
                    logger.Debug("Loot container!");
                }
                else {
                    logger.DebugFormat("BP unknown == 2 but bp.contEnd.containerId.low == {0}", bp.contEnd.containerId.low);
                }
            }
            else if (bp.unknown2 != 0x00000003) {
                logger.DebugFormat("BP unknown == {0}", bp.unknown2);
            }
#endif
            return true;
        }



        public bool MSG_BACKPACK(byte[] bdata) {
            OpenBackpack msg = new OpenBackpack(bdata);
            if (msg.ownerId.high == msg.CharacterId) {

                if (msg.operationId == 0x65) { //I bought a backpack (I think)
                    uint containerId = msg.EntityId; // BUG: This might be wrong.. HIGH/LOW issue
                    uint newParent = (uint)InventoryIds.INV_TOONINV;


                    //find a free spot:
                    uint slotId = msg.invSlot;
                    if (slotId >= 0x6f)
                        slotId = itemDao.FindNextAvailableContainerSlot(msg.CharacterId, newParent);

                    // Remove old ref to backpack:
                    itemDao.RemoveBackpack(msg.CharacterId, containerId);


                    // Add backpack:
                    itemDao.Save(ItemDao.CreateItem(msg.itemKeyLow, msg.itemKeyHigh, (int)msg.ql, msg.flags, 1,
                        newParent, (int)slotId, containerId, msg.CharacterId));
                }
            }
            
            return true;
        }


        public bool MSG_BANK(byte[] bdata) {
            AOMessageBase msg = new AOMessageBase(bdata);
            itemDao.RemoveBankItems(msg.CharacterId);

            OpenBank bank = new OpenBank(bdata);
            List<Item> items = new List<Item>();
            foreach (ContItem item in bank.items) {
                items.Add(ItemDao.CreateItem(item.itemid.low, item.itemid.high, (int)item.ql, item.flags, item.stack,
                    (uint)InventoryIds.INV_BANK,
                    (int)item.index, item.containerid.high, msg.CharacterId));

            }
            itemDao.Save(items);
            return true;
        }

        
        	// [2+8*4], [2+12*4]
        public bool SMSG_UNKNOWN_WITH_ZONEID0(byte[] bdata) {
            /*AOMessageBase msg = new AOMessageBase(bdata);
            msg.skip(2 + 8 * 4);
            uint zoneid = msg.popInteger();

            msg.skip(2 + 12 * 4 - 4); // -4 since we popped an int
            uint zoneid2 = msg.popInteger();
            */

            // PositionTracker.setZone(msg.characterId, zoneid);
            return true;
        }


        // [2+5*4], [2+9*4]
        public bool SMSG_UNKNOWN_WITH_ZONEID1(byte[] bdata) {
            try {
                AOMessageBase msg = new AOMessageBase(bdata);
                msg.skip(2 + 5 * 4);
                uint zoneid = msg.popInteger();
                //logger.Debug("zone? " + zoneid);
                //if (msg.characterId == msg.entityId())
                    PositionTracker.setZone(msg.CharacterId, zoneid);
                return true;
            }
            catch (Exception e) {
                logger.Warn("Exception " + e.Message + " parsing zoneid1");
                return false;
            }
        }


        /// <summary>
        /// Called when zoning into an instanced store.. not when zoning into Omni:trade...
        /// Odd!
        /// </summary>
        /// <param name="bdata"></param>
        /// <returns></returns>
        public bool SMSG_UNKNOWN_WITH_ZONEID2(byte[] bdata) {
            AOMessageBase msg = new AOMessageBase(bdata);
            msg.skip(13);
            uint zoneid = msg.popInteger();
            if (msg.CharacterId == msg.EntityId)
                PositionTracker.setZone(msg.CharacterId, zoneid);
            //WriteWaypointsScript();
            return true;
        }

        public bool SMSG_MOVED(byte[] bdata) {
			SMSG_MOVED_DATA msg = new SMSG_MOVED_DATA(bdata);
            if (msg.CharacterId == msg.EntityId)
                PositionTracker.setPosition(msg.CharacterId, msg.x, msg.y);
			//WriteWaypointsScript();

            return true;
        }
    }
}
