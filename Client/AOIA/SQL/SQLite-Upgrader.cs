﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ItemsLight.Model;
using ItemsLight.Dao;

namespace ItemsLight.AOIA.SQL {
    class SQLite_Upgrader {

        /// <summary>
        /// Get the current version of the database
        /// </summary>
        /// <returns></returns>
        private static long GetDatabaseVersion() {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    DatabaseSetting setting = session.CreateQuery("FROM DatabaseSetting WHERE Id = 'dbversion'").UniqueResult<DatabaseSetting>();
                    if (setting == null) {
                        DatabaseSetting initialize = new DatabaseSetting { Id = "dbversion", V2 = 1 };
                        session.Save(initialize);
                        transaction.Commit();
                    }
                    else {
                        return setting.V2;
                    }

                }
            }


            return 0;
        }
        /*
        public static void PerformUpgrades() {
            do {
                // Nothing
            } while (PerformUpgrades_(GetDatabaseVersion()));
        }

        private static bool PerformUpgrades_(long version) {
            if (version == 1) {
                using (ISession session = SessionFactory.OpenSession()) {
                    using (ITransaction transaction = session.BeginTransaction()) {
                        Symbiant s = new Symbiant {
                            AOID = 268287,
                            Name = "Syndicate Brain",
                            QL = 230,
                            Slot = "head",
                            Type = "Infantry",
                            Origin = "Adonis Xan Quest"
                        }

                        session.CreateQuery("UPDATE DatabaseSetting SET V2 = V2 + 1 WHERE Id = 'dbversion'");
                        transaction.Commit();
                    }
                }
                
            }

            return false;
        }
        */
    }
}
