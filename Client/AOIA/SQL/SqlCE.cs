﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Management;
using System.IO;
using log4net;
using ItemsLight.AOIA.Parsers;
using System.Data.SqlTypes;
using ItemsLight.Dao;
using NHibernate;
using ItemsLight.Model;
using ItemsLight.AOIA.HelperStructs;
using ItemsLight.AOIA.Misc;

namespace ItemsLight.AOIA {
    class LegacySQL {
        static ILog logger = LogManager.GetLogger(typeof(LegacySQL));


        private static List<string> CListItemsForIdentifyPrepareQuery(int index, int dimension, long characterId) {
            List<string> queries = new List<string>();

            foreach (string field in new string[] { /*"highkey",*/ "lowkey" }) {
                queries.Add(@"
                    SELECT i.ql, c.name as charname, parent, children, owner, i.lowkey, i.slot, id.description, db.name FROM items i
                        JOIN characters c on characterId = owner 
                        JOIN aodb db on i.lowkey = db.aoid
                        LEFT OUTER JOIN identify id ON id.lowkey = i.lowkey
                        WHERE (i.lowkey IN (select lowkey from (select lowkey, sortIdx from identify union select highkey, sortIdx from identify) x  where sortIdx = :index) )");
            }
            

            for (int i = 0; i < queries.Count; i++) {
                if (characterId != 0)
                    queries[i] += " AND (i.owner = :characterId)";
            }

            return queries;
        }


        public static List<Item> CListItemsForIdentify(IdentifyType type, int dimension, long characterId = 0) {
            List<string> queries = new List<string>();
            int index = (int)type;

            queries = CListItemsForIdentifyPrepareQuery(index, dimension, characterId);
            List<Item> result = new List<Item>();


            using (IStatelessSession session = SessionFactory.OpenStatelessSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    HashSet<long> validCharacterIds;
                    if (dimension >= 0)
                        validCharacterIds = new HashSet<long>(session.CreateQuery("SELECT Id FROM Character WHERE Dimension = :dimension")
                            .SetParameter("dimension", dimension)
                            .List<long>());
                    else
                        validCharacterIds = new HashSet<long>(session.CreateQuery("SELECT Id FROM Character")
                            .List<long>());
                    

                    foreach (var query in queries) {
                        var sql = session.CreateSQLQuery(query);

                        if (characterId != 0)
                            sql.SetParameter("characterId", characterId);
                        sql.SetParameter("index", index);

                        foreach (object[] array in sql.List<object[]>()) {
                            string extras = (array[7] == null) ? string.Empty : (string)array[7];
                            if (extras.Equals(string.Empty) || extras.Equals("Null")) {
                                extras = "Bug " + (long)array[5];
                            }
                            long charId = (long)array[4];
                            if (validCharacterIds.Contains(charId)) {
                                result.Add(
                                    new Item {
                                        //Name = MemoryDB.Instance.GetNameForItem((long)array[5]),
                                        Name = (string)array[8],
                                        QL = (int)(long)array[0],
                                        ParentContainerId = (long)array[2],
                                        Children = (long)array[3],
                                        Owner = new Character { Id = (long)array[4], Name = (string)array[1] },
                                        LowKey = (long)array[5],
                                        Slot = (int)(long)array[6],
                                        Extras = extras
                                    });
                            }
                        }
                        
                    }
                }
            }

            return result;
        }


        
        public static void RENAME_ME_PLEASE(long characterId, long fromContainerId, long keyhigh, uint nextFreeInvSlot, uint fromItemSlotId) {
            String query = string.Format("INSERT INTO items (lowkey, highkey, ql, stack, parent, slot, children, owner) SELECT lowkey, highkey, ql, {0}, parent, {1}, children, owner FROM items WHERE owner = {2} AND parent = {3} AND slot = {4}", keyhigh, nextFreeInvSlot, characterId, fromContainerId, fromItemSlotId);
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    session.CreateSQLQuery(query).ExecuteUpdate();

                    transaction.Commit();
                }
            }
        }
   
    }
}
