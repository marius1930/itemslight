﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.HelperStructs {
    class ShowSymbiantsRequest {
        public string Slot { get; set; }
        public long CharacterId { get; set; }
        public bool ShowAvailableOnly { get; set; }
        public bool ShowOwnedOnly { get; set; }
    }
}
