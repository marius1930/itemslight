﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA {
    public struct RequestBuffItems {
        public int Dimension { get; set; }
        
        public string Profession { get; set; }
        public string Breed { get; set; }
        public string Slot { get; set; }
        
        public HashSet<int> Skills { get; set; }
    }
}
