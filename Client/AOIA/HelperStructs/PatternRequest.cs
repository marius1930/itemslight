﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA {
    public struct PatternRequest {
        public int Dimension { get; set; }
        public long CharacterId { get; set; }

        // Used for looking up pattern lists
        public PatternSet set { get; set; }

        // Used when looking up a specific pattern
        public string Name { get; set; }

        public enum PatternSet {
            SHOW_ALL,
            SHOW_PARTIAL,
            SHOW_COMPLETEABLE,
            SHOW_OMNI,
            SHOW_CLAN
        }
    }
}
