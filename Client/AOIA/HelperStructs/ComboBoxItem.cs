﻿using ItemsLight.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.HelperStructs {
    public class ComboBoxItem {
        public ComboBoxItem(Character character) {
            Value = character.Id;
            Text = character.Name;
        }
        public ComboBoxItem(int dimensionId, string dimensionName) {
            Value = dimensionId;
            Text = dimensionName;
        }
        public long Value {
            get;
            private set;
        }
        public string Text {
            get;
            private set;
        }

        public override string ToString() {
            return Text;
        }
    }
}
