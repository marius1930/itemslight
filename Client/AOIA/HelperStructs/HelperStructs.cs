﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ItemsLight.Model;
using ItemsLight.AOIA.HelperStructs;

namespace ItemsLight.AOIA {
    struct Dimension {
        public int Id { get; set; }
        private String _name;
        public string name {
            get {
                if (_name == null || _name.Length > 0)
                    return _name;
                else
                    return "" + Id;
            }
            set {
                _name = value;
            }
        }
    }




    struct SearchRequest {
        public string Search { get; set; }
        public int Dimension;
        public int MinQL { get; set; }
        public int MaxQL { get; set; }
        public long CharacterId { get; set; }
    }
    struct SearchRequestById {
        public long Search { get; set; }
        public int Dimension { get; set; }
    }

    class UpdateIdentifyRequest {
        public int Dimension { get; set; }
        public IdentifyType Type { get; set; }
    }









    struct PatternLookupResult {
        public string Name;
        public IList<PatternPiece> Pieces { get; set; }
        public ICollection<Symbiant> Drops { get; set; }
        public IList<PocketBossLocation> Locations { get; set; }
        public float Total {
            get {
                Pattern p = new Pattern { a = 0, b = 0, c = 0, d = 0 };
                foreach (var piece in Pieces) {
                    p.Add(piece.Type);                    
                }
                return p.Total;
            }
        }
    }


    class PatternPiece {
        public Item item { protected get; set; }
        public Pattern pattern { protected get; set; }
        public PocketBoss pocketboss { protected get; set; }

        public string Type {
            get {
                if (pattern != null)
                    return pattern.Piece;
                else
                    return "ABCD";
            }
        }
        public Character Owner {
            get {
                return item.Owner;
            }
        }

        public long ContainerId {
            get {
                return item.ParentContainerId;
            }
        }

        public long ParentContainerId { get; set; }
        public string Container { get; set; }
        public string ParentContainer { 
            get {
                if (ParentContainerId == 1)
                    return "Bank";
                else if (ParentContainerId == 2)
                    return "Inventory";
                else
                    return "Unknown";
            }
        }
    }




    class BuffItem : Item {
        public string Ability { get; set; }
        public int Modifier { get; set; }
        public string EquippedSlot { get; set; }
    }
}
