﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using ItemsLight.AOIA;

namespace ItemsLight {
    public struct QueueGUI {
        public ConcurrentQueue<MessageDataRequest> requestQueue;
        public ConcurrentQueue<MessageData> resultQueue;
        public ConcurrentQueue<ByteAndType> messageQueue;
    }
}
