﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.HelperStructs {
    class ContainerUpdateNode {
        public long CharacterId { get; set; }
        public long ContainerId { get; set; }
        public List<ContainerNode> nodes { get; set; }
    }
}
