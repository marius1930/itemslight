﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA {
    public struct DownloadedItemdata {
        public string name;
        public long aoid;
        public uint properties;
    }
}
