﻿using ItemsLight.AOIA.Misc;
using ItemsLight.Dao;
using ItemsLight.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using EvilsoftCommons.Exceptions;

namespace ItemsLight.AOIA.Utility {
    class ContainerMapper {
        private static ILog logger = LogManager.GetLogger(typeof(ContainerMapper));
        private ItemDao itemDao = new ItemDao();
        private CharacterDao characterDao = new CharacterDao();
        private Dictionary<long, String> accountMap_ = new Dictionary<long, String>();

        public ContainerMapper(Dictionary<long, String> accountMap) {
            accountMap_ = accountMap;
        }

        public void MapContainerNamesForPatterns(ref List<PatternPiece> data) {
            List<long> containerIds = new List<long>(data.Select(m => m.ContainerId));
            var containers = itemDao.ListByChildren(containerIds);
            Dictionary<long, Item> containerMapping = new Dictionary<long, Item>();
            foreach (var container in containers)
                containerMapping[container.Children] = container;

            // Map the container names and parent inventory
            // This is done separately as it would require a rather complex query
            for (int i = 0; i < data.Count; i++) {
                PatternPiece item = data[i];

                data[i].Container = BackpackRenamer.GetBackpackName(GetAccount(item.Owner.Id), item.Owner.Id, item.ContainerId);

                if (containerMapping.ContainsKey(item.ContainerId)) {
                    if (string.IsNullOrEmpty(data[i].Container))
                        data[i].Container = containerMapping[data[i].ContainerId].Name;

                    data[i].ParentContainerId = containerMapping[data[i].ContainerId].ParentContainerId;
                }
                // No parent, placed in inventory/bank
                else if (item.ContainerId <= 2)
                    data[i].ParentContainerId = item.ContainerId;
            }

        }



        public void MapContainerNames(IList<Item> data) {
            if (data == null || data.Count == 0)
                return;
            try {
                List<long> containerIds = new List<long>( new HashSet<long>(data.Select(m => m.ParentContainerId)) );
                var containers = itemDao.ListByChildren(containerIds);
                Dictionary<long, Item> containerMapping = new Dictionary<long, Item>();
                foreach (var container in containers)
                    containerMapping[container.Children] = container;

                Dictionary<long, string> containerNames = new Dictionary<long, string>();
                foreach (var container in containers) {
                    if (container.Owner == null)
                        ExceptionReporter.ReportIssue("container.Owner == null in MapContainerNames");
                    if (accountMap_.ContainsKey(container.Owner.Id)) {
                        containerNames[container.Children] = BackpackRenamer.GetBackpackName(accountMap_[container.Owner.Id], container.Owner.Id, container.Children);
                        if (string.IsNullOrEmpty(containerNames[container.Children]))
                            containerNames[container.Children] = container.Name;
                    }
                    else {
                        containerNames[container.Children] = container.Name;
                    }
                }

                // Map the container names and parent inventory
                // This is done separately as it would require a rather complex query
                for (int i = 0; i < data.Count; i++) {
                    if (data[i].ParentContainerId > 64) {
                        Item item = data[i];
                        if (containerNames.ContainsKey(item.ParentContainerId)) {
                            item.ParentContainerName = containerNames[item.ParentContainerId];
                            item.ParentParentContainerId = containerMapping[item.ParentContainerId].ParentContainerId;
                            data[i] = item;
                        }
                        else {
                            logger.WarnFormat("Container name mapping does not contain Id {0}", item.ParentContainerId);
                        }

                    }
                }
            }
            catch (NullReferenceException ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                ExceptionReporter.ReportException(ex, "MapContainerNames");
            }
        }



        /// <summary>
        /// Map the "parent container" for backpack contents
        /// Since all the items are in the same backpack, we only need a single lookup.
        /// </summary>
        /// <param name="data"></param>
        public void MapContainerNamesForBackpackContents(ref List<Item> data) {
            try {
                if (data.Count > 0 && data[0].ParentContainerId > 2) {
                    Item mainItem = data[0];

                    // Map container names from file
                    long ownerId = mainItem.Owner.Id;
                    var container = itemDao.GetBackpack(mainItem.ParentContainerId);
                    string inventory = (container.ParentContainerId == (uint)InventoryIds.INV_BANK) ? "Bank" : "Inventory";

                    string containerName = BackpackRenamer.GetBackpackName(GetAccount(ownerId), ownerId, mainItem.ParentContainerId);
                    if (string.IsNullOrEmpty(containerName)) {
                        containerName = container.Name;
                    }

                    for (int i = 0; i < data.Count; i++) {
                        data[i].ParentContainerName = containerName;
                        data[i].ParentInventory = inventory;
                    }

                }
            }
            catch (NullReferenceException ex) {
                string detailedMessage = string.Format("owner: {0}, containerid: {1}", data[0].Owner, data[0].ParentContainerId);
                ExceptionReporter.ReportException(ex, detailedMessage);
            }
        }




        private string GetAccount(long characterId) {
            if (accountMap_.ContainsKey(characterId))
                return accountMap_[characterId];
            return string.Empty;
        }

    }
}
