﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.Utility {
    class StopwatchManager {
        class WatchAndFunc {
            public Stopwatch stopwatch { get; set; }
            public Action func { get; set; }
            public long Timeout { get; set; }
        }

        List<WatchAndFunc> watches = new List<WatchAndFunc>();


        /// <summary>
        /// 
        /// </summary>
        /// <param name="func">The method to execute</param>
        /// <param name="timeout">Timeout in milliseconds</param>
        /// <param name="firstrun"></param>
        public void Add(Action func, long timeout, bool firstrun) {
            Stopwatch sw = null;
            if (!firstrun) {
                sw = new Stopwatch();
                sw.Start();
            }

            var s = new WatchAndFunc {
                func = func,
                Timeout = timeout,
                stopwatch = sw
            };

            watches.Add(s);
        }

        public void Execute() {
            foreach (WatchAndFunc wf in watches) {
                if (wf.stopwatch == null) {
                    wf.stopwatch = new Stopwatch();
                    wf.stopwatch.Start();
                    wf.func();
                }
                else if (wf.stopwatch.ElapsedMilliseconds >= wf.Timeout) {
                    wf.func();
                    wf.stopwatch.Restart();
                }
            }
        }
    }

}
