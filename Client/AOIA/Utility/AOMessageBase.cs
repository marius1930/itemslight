﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA {

    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
    public class AOMessageBase : Parser {
        public enum HeaderType {
            UNKNOWN_MESSAGE,
            MSG_TYPE_1 = 0x0001,
            MSG_TYPE_A = 0x000A,
            MSG_TYPE_B = 0x000B,
            MSG_TYPE_D = 0x000D,
            MSG_TYPE_E = 0x000E,
        };

        public AOMessageBase(IntPtr pRaw, uint size)
            : base(pRaw, size) {
            initialize();
        }

        public AOMessageBase(byte[] data)
            : base(data) {
            initialize();
        }

        public List<int> FindShort(ushort n) {
            int cpos = 0;
            List<int> result = new List<int>();
            while (cpos < memory.Length - 2) {
                byte[] bytes = new byte[] { (byte)memory[cpos], (byte)memory[cpos + 1] };
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(bytes);
                UInt16 s = BitConverter.ToUInt16(bytes, 0);
                if (s == n)
                    result.Add(cpos);

                cpos++;
            }

            return result;
        }
        public List<int> FindLong(ulong n) {
            int cpos = 0;
            List<int> result = new List<int>();
            while (cpos < memory.Length - 8) {
                byte[] bytes = new byte[] { (byte)memory[cpos], (byte)memory[cpos + 1], (byte)memory[cpos + 2], (byte)memory[cpos + 3], (byte)memory[cpos + 4], (byte)memory[cpos + 5], (byte)memory[cpos + 6], (byte)memory[cpos + 7] };
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(bytes);
                UInt64 s = BitConverter.ToUInt64(bytes, 0);
                if (s == n)
                    result.Add(cpos);

                cpos++;
            }

            return result;
        }
        public List<int> FindInteger(int n) {
            int cpos = 0;
            List<int> result = new List<int>();
            while (cpos < memory.Length - 4) {
                byte[] bytes = new byte[] { (byte)memory[cpos], (byte)memory[cpos + 1], (byte)memory[cpos + 2], (byte)memory[cpos + 3] };
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(bytes);
                UInt32 s = BitConverter.ToUInt32(bytes, 0);
                if (s == n)
                    result.Add(cpos);

                cpos++;
            }

            return result;
        }


        /// <summary>
        /// Get the string at the current location
        /// Does not increment the read pointer
        /// </summary>
        /// <param name="idx"></param>
        /// <returns></returns>
        public string PeekString(int idx) {
            ushort len = Convert.ToChar((char)memory[idx]);
            char[] sub = new char[len];
            for (int i = 0; i < len - 1; i++) { // skip \0
                sub[i] = Convert.ToChar(memory[idx + i]);
            }

            string retval = new String(sub);
            return retval;
        }


        public static byte[] StringToByteArray(string hex) {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }


        public IList<int> FindByteArray(byte[] bytes) {
            List<int> result = new List<int>();
            int idx = 0;
            while (idx < (memory.Length - bytes.Length)) {
                int tmp = 0;
                while (memory[idx + tmp] == bytes[tmp]) {
                    tmp++;
                }

                if (tmp >= bytes.Length - 2) {
                    result.Add(idx);
                }

                idx++;
            }
            return result;
        }

        public List<int> FindString(string s) {
            List<int> result = new List<int>();

            // Attempt to find possible start locations of the string
            int idx = 0;
            while (idx < memory.Length) {
                if (memory[idx] == s.Length) {
                    int remainingLength = memory.Length - idx;
                    if (remainingLength >= s.Length) {
                        string test = PeekString(idx);
                        if (s.Equals(test)) {
                            result.Add(idx);
                        }
                    }
                }

                idx++;
            }

            return result;
        }

        private void initialize() {
            m_entityid = 0;
            m_messageid = 0;
            m_characterid = 0;
            m_size = 0;
            m_type = HeaderType.UNKNOWN_MESSAGE;

            skip(2);    // Skip the sequence number (short). introduced in v18.1 and replaces the old 0xDFDF sequence.

            // Parse and validate header
            ushort t = popShort();
            switch (t) {
                case 0x000A:
                    m_type = HeaderType.MSG_TYPE_A;
                    break;
                case 0x000D:
                    m_type = HeaderType.MSG_TYPE_D;
                    char c = popChar();
                    break;
                case 0x000B:
                    m_type = HeaderType.MSG_TYPE_B;
                    break;
                case 0x000E:
                    m_type = HeaderType.MSG_TYPE_E;
                    break;
                case 0x0001:
                    m_type = HeaderType.MSG_TYPE_1;
                    break;
                default:
                    return;

            }

            skip(2);    // 0x0001		protocol version?
            m_size = popShort();
            m_serverid = popInteger(); // skip(4);    // Receiver: Instance ID? Dimension? Subsystem?
            m_characterid = popInteger();
            m_messageid = popInteger();
            m_target = popInteger(); // skip(4);    // Target Entity: Instance ID? Dimension? Subsystem?
            if (Remaining >= 4)
                m_entityid = popInteger();
        }

        public HeaderType headerType() { return m_type; }
        public uint size() { return m_size; }
        public uint CharacterId { get { return m_characterid; } }
        public uint messageId() { return m_messageid; }
        public uint EntityId {
            get {
                return m_entityid;
            }
        }
        public uint dimensionid {
            get { return (m_serverid & 0x0000FF00) >> 8; }
        }
        public uint targetId {
            get {
                return m_target;
            }
        }

        public uint m_target;
        private HeaderType m_type;
        private uint m_size;
        private uint m_characterid;
        private uint m_messageid;
        private uint m_entityid;
        public uint m_serverid;
    };



}
