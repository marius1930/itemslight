﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA {
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
    public class Parser {
        public Parser(IntPtr memory, uint size) {
            this.memory = new byte[size];
            System.Runtime.InteropServices.Marshal.Copy(memory, this.memory, 0, (int)size);

            m_start = 0;
            m_pos = 0;
            m_end = size;
        }
        public Parser(byte[] data) {
            this.memory = data;
            m_start = 0;
            m_pos = 0;
            m_end = (uint)data.Length;
        }


        // Number of bytes remaining to be parsed
        public uint Remaining {
            get {
                if (m_end > m_start) {
                    return m_end - m_pos;
                }
                return 0;
            }
        }
        public bool Empty() {            
            return m_end > m_start;            
        }

        // Moves the current position the number of bytes ahead
        public void skip(uint count) {
            if (count <= Remaining) {
                m_pos += count;
            }
        }

        // Get the next byte
        public char popChar() {
            char c = Convert.ToChar((char)memory[m_pos]);
            m_pos += 1;
            return c;
        }

        // Get the next 2 bytes 
        public ushort popShort() {
            byte[] bytes = new byte[] { (byte)memory[m_pos], (byte)memory[m_pos + 1] };
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            ushort s = BitConverter.ToUInt16(bytes, 0);
            m_pos += 2;

            return s;
        }

        // Get the next 4 bytes
        public uint popInteger() {
            byte[] bytes = new byte[] { (byte)memory[m_pos], (byte)memory[m_pos + 1], (byte)memory[m_pos + 2], (byte)memory[m_pos + 3] };
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            UInt32 s = BitConverter.ToUInt32(bytes, 0);
            m_pos += 4;

            return s;
        }

        // Get the next 4 bytes
        public float popFloat() {
            byte[] bytes = new byte[] { (byte)memory[m_pos], (byte)memory[m_pos + 1], (byte)memory[m_pos + 2], (byte)memory[m_pos + 3] };
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            double s = BitConverter.ToSingle(bytes, 0);
            m_pos += 4;

            return (float)s;
        }

        // Get the string at current position. <len><char[]><NULL>
        public string popString() {            
            ushort len = popChar();
            char[] sub = new char[len];
            for (int i = 0; i < len-1; i++) { // skip \0
                sub[i] = Convert.ToChar(memory[m_pos + i]);
            }
            //Array.Copy(memory, m_pos, sub, 0, len);
            string retval = new String(sub);
            m_pos += (uint)len + 1;
            return retval;
        }

        // Get the 3F1 encoded counter value 
        // counter = (x/1009)-1
        public uint pop3F1Count() {            
            uint val = popInteger();
            return (val / 1009) - 1;
        }

        public uint start() { return m_start; }
        /*public char* end() { return m_end; }
        public char* pos() { return m_pos; }*/
        public uint pos() { return m_pos; }

        protected byte[] memory;
        private uint m_start;
        private uint m_end;
        private uint m_pos;
    };




    

    class MobInfo : AOMessageBase {
        public MobInfo(IntPtr pRaw, uint size) : base(pRaw, size) {
            skip(42);
            charname = popString();
        }
        public MobInfo(byte[] data) : base(data) {
            skip(42);
            charname = popString();
        }
        public String charname;
    }



    
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
    class AOClientMessageBase : Parser {
        public enum HeaderType {
            UNKNOWN_MESSAGE,
            MSG_TYPE_1 = 0x00000001,
            MSG_TYPE_A = 0x0000000A,
            MSG_TYPE_B = 0x0000000B,
            MSG_TYPE_D = 0x0000000D,
            MSG_TYPE_E = 0x0000000E,
        };

        public AOClientMessageBase(IntPtr pRaw, uint size) : base(pRaw, size) {
            AOClientMessageBase_Init();
        }



        public AOClientMessageBase(byte[] data) : base(data) {
            AOClientMessageBase_Init();
        }

        protected void AOClientMessageBase_Init() {
            m_type = HeaderType.UNKNOWN_MESSAGE;
            m_characterid = 0;
            m_messageid = 0;

            //H3 = I4 msgType  I4 Zero     I4  someId  I4   02     I4 msgId I4+I4 charId
            // Parse and validate header
            uint t = popInteger();
            if (t == 0x0000000A) {
                m_type = HeaderType.MSG_TYPE_A;
            }
            else if (t == 0x0000000B) {
                m_type = HeaderType.MSG_TYPE_B;
            }
            else {
                //Logger::instance().log(STREAM2STR(_T("Error unknown client message header: " + t));
                return;
            }

            skip(4);    // 0x00 00 00 00
            skip(4);    // Receiver: Instance ID? Dimension? Subsystem?
            skip(4); //??ex 00 00 00 02 or 00 00 0b ed  on logoff

            m_messageid = popInteger();

            m_target = popInteger(); // skip(4);    // Target Entity: Instance ID? Dimension? Subsystem? 50000
            //m_entityid = popInteger();

            m_characterid = popInteger();
        }

        public HeaderType headerType() { return m_type; }
        public uint characterId() { return m_characterid; }
        public uint messageId() { return m_messageid; }

        private HeaderType m_type;
        public uint m_target;
        private uint m_characterid;
        private uint m_messageid;
    };

    
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
    public struct AoObjectId {
        public UInt32 low;
        public UInt32 high;
        public static AoObjectId spawn(Parser p) {
            AoObjectId obj = new AoObjectId();
            obj.low = p.popInteger();
            obj.high = p.popInteger();
            return obj;
        }
    };
    
    /*
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
    public struct AoString {
        public byte strLen;         // Number of bytes allocated to 'str'
        public IntPtr str;
        //char str;            // array of strLen number of bytes.

        public string value {
            get {
                uint len = Convert.ToUInt32(strLen);
                char[] charname = new char[len];
                len = 5;

                System.Runtime.InteropServices.Marshal.Copy(str, charname, 0, (int)len - 1);
                return new String(charname);
            }
        }
    };*/
    /*
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
    public struct Header {
        UInt32	headerid;	// 0xDFDF000A
        UInt16 unknown1;	// 0x0001		protocol version?
        UInt16 msgsize;	//				size incl header
        UInt32 serverid;	// 0x00000C20	ServerID
        UInt32 charid;		// 0x1F26E77B	== Adjuster
        UInt32 msgid;		//
        AoObjectId		target;		// 0x0000C350	dimension? (50000:<charid>)
    };*/
    
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
    public struct InvItemId {
		public ushort	unknown4;//00 00 (part of fromType probably)
		public ushort	type;//00 68 for inventory etc..
		public ushort	containerTempId;//the temporary id of the container
		public ushort	itemSlotId;//The slot id of the item in its container/inventory.
        public InvItemId(Parser p) {
            unknown4 = p.popShort();
            type = p.popShort();
            containerTempId = p.popShort();
            itemSlotId = p.popShort();
        }
	};
    

}
