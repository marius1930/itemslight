﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using ItemsLight.AOIA.Misc;

namespace ItemsLight.AOIA {
    enum InventoryIds {
        INV_BANK = 1,    // Bank container
        INV_TOONINV = 2,    // Toon inventory, includes equipment etc.
        INV_PLAYERSHOP = 3,    // Stuff in your shop
        INV_TRADE = 4,    // The stuff I put on the table in a trade
        INV_TRADEPARTNER = 5,    // The stuff a tradepartner adds to a trade.
        INV_OVERFLOW = 6,    // Overflow window. Clear on zone!
        INV_HOTSWAPTEMP = 7,    //  used by IA when hotswapping as temp storage.
        INV_OTHER_PLAYERSHOP = 8,    //  Current playershop in trade.

        // Virtual containers, these IDs exists only inside this program, not in AO.
        // All of these are 'children' of INV_TOONINV depending on SLOT
        VIRTUAL_WEAPONS = 10,
        VIRTUAL_ARMOR = 11,
        VIRTUAL_IMPLANTS = 12,
        VIRTUAL_SOCIAL = 13,
    };

    class Inventory {
        static ILog logger = LogManager.GetLogger(typeof(Inventory));

        private static Dictionary<int, string> _v;
        public static Dictionary<int, string> SlotMap {
            get {
                if (_v != null)
                    return _v;

                _v = new Dictionary<int, string>();
                _v[1] = "Weapons (Hud 1)";
                _v[15] = "Weapons (Hud 2)";
                _v[2] = "Weapons (Hud 3)";
                _v[3] = "Weapons (Utils 1)";
                _v[4] = "Weapons (Utils 2)";
                _v[5] = "Weapons (Utils 3)";

                _v[0x0100] = "Left Hand";
                _v[0x0040] = "Right Hand";
                _v[0x0080] = "Belt";

                _v[5120] = "Wrists";
                _v[640] = "Arms";
                _v[33] = "Body";
                _v[256] = "Hands";
                


                _v[0x0200] = "NCU (1)";
                _v[0x0400] = "NCU (2)";
                _v[0x0800] = "NCU (3)";
                _v[0x1000] = "NCU (4)";
                _v[0x2000] = "Rfinger/Feet";
                _v[0x4000] = "NCU (6)";
                _v[32256] = "NCU";

                _v[29] = "Right finger";
                _v[31] = "Left finger";

                _v[80] = "Shoulders";
                _v[40961] = "Fingers";
                _v[40960] = "Fingers";
                _v[320] = "Weapon";
                _v[2049] = "Legs";
                _v[16385] = "Feet";

                _v[0x0002] = "Neck";
                _v[0x0004] = "Head";
                _v[0x0008] = "Back";
                _v[0x0010] = "Right Shoulder";
                _v[0x0020] = "Body";
                _v[0x0040] = "Weapon";
                _v[0x0080] = "Belt";
                _v[0x0100] = "Hands";
                _v[0x0200] = "Left arm";
                _v[0x0400] = "Right Wrist";
                _v[0x0800] = "Legs";
                _v[0x1000] = "Left Wrist";
                _v[0x4000] = "Feet";

                _v[0x0002] = "Eye (imp)";
                _v[0x0008] = "Back/Ear(imp)";
                /*
                _v[0x0002] = "Eye (imp)";
                _v[0x0004] = "Head (imp)";
                _v[0x0008] = "Ear (imp)";
                _v[0x0010] = "Right Arm (imp)";
                _v[0x0020] = "Body (imp)";
                _v[0x0040] = "Left Arm (imp)";
                _v[0x0080] = "Right Wrist (imp)";
                _v[0x0100] = "Waist (imp)";
                _v[0x0200] = "Left Wrist (imp)";
                _v[0x0400] = "Right Hand (imp)";
                _v[0x0800] = "Legs (imp)";
                _v[0x1000] = "Left Hand (imp)";
                _v[0x4000] = "Feet (imp)";*/

                return _v;
            }
        }
        public static string MapLocation(InventoryIds inventory, int Slot) {
            if (inventory == InventoryIds.INV_BANK)
                return "Bank";

            else if (inventory == InventoryIds.INV_TOONINV) {
                if (Slot == 1)
                    return "Weapons (Hud 1)";
                else if (Slot == 15)
                    return "Weapons (Hud 3)";
                else if (Slot == 2)
                    return "Weapons (Hud 3)";
                else if (Slot == 3)
                    return "Weapons (Utils 1)";
                else if (Slot == 4)
                    return "Weapons (Utils 2)";
                else if (Slot == 5)
                    return "Weapons (Utils 3)";

                else if (Slot == 7 || (Slot >= 9 && Slot <= 14))
                    return "Weapons (Belt)";


                else if (Slot == 22 || Slot == 20)
                    return "Equipped (Shoulder)";
                else if (Slot == 29)
                    return "Equipped (Right finger)";
                else if (Slot == 31)
                    return "Equipped (Left finger)";
                else if (Slot == 17)
                    return "Equipped (Neck)";
                else if (Slot < 16)
                    return "Weapons";
                else if (Slot < 32)
                    return "Equipped";
                else if (Slot < 47)
                    return "Implants";
                else if (Slot < 64)
                    return "Social";
                else
                    return "Inventory";
            }

            return string.Empty;
        }
        public static string MapLocationSimple(InventoryIds inventory, int Slot) {
            if (inventory == InventoryIds.INV_BANK)
                return "Bank";

            else if (inventory == InventoryIds.INV_TOONINV) {
                return "Inventory";
            }

            return string.Empty;
        }

        public static uint GetFromContainerId(uint charId, ushort fromType, ushort fromSlotId) {
            if (fromType == 0x006b) { //backpack. fromSlotId contains a temp container id. 
                return TempContainerCache.Instance.GetContainerId(charId, fromSlotId);
            }

            else if (fromType == 0x0068 //inv
                    || fromType == 0x0065  //utils, hud, ncu, weap pane
                    || fromType == 0x0073  //social pande
                    || fromType == 0x0067  //implants
                    || fromType == 0x0066) { //wear neck,sleeve
                return (uint)InventoryIds.INV_TOONINV;
            }
            else if (fromType == 0x0069)//bank
                return (uint)InventoryIds.INV_BANK;

            else if (fromType == 0x006f) //trade window
                return 0;

            else if (fromType == 0xc767)//shop inventory
                return 0;

            else if (fromType == 0xc790)//51088 player shop inventory
                return 0;

            else if (fromType == 0x006c)//shop/trade window.. could be remote or local. If needed, add an owner charId to compare with to func.
                return (uint)InventoryIds.INV_TRADE;

            else if (fromType == 0x006e)//overflow or tradeskill temp window
                return (uint)InventoryIds.INV_OVERFLOW;

            else
                return 2; //we assume inventory o.O
    
        }

    }
}
