﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using ItemsLight.Dao;
using ItemsLight.Model;
using ItemsLight.AOIA.Misc;
using System.IO;
using ItemsLight.AOIA.HelperStructs;
using System.Collections.Concurrent;
using System.Diagnostics;
using EvilsoftCommons.Exceptions;

namespace ItemsLight.AOIA {
    class TreeviewUpdater {
        private BackpackIdentifierDao backpackIdentifierDao = new BackpackIdentifierDao();
        private ItemDao itemDao = new ItemDao();
        static ILog logger = LogManager.GetLogger(typeof(TreeviewUpdater));
        public const long VIRTUAL_GMI_ID = -0x100;
        private CharacterDao characterDao = new CharacterDao();
        private HashSet<long> charactersInLoadingQueue = new HashSet<long>();
        private Stopwatch cooldown;
        
        private Dictionary<long, String> accountMap = new Dictionary<long, String>();
        public TreeviewUpdater() {            
            // Load account:id map
            try {
                foreach (String folder in AppdataPathFinder.ListAnarchyDirectories()) {
                    BackpackRenamer.MapAccountCharid(folder, ref accountMap);
                }
            }
            catch (InvalidOperationException) {
                // Happens..
            }
            catch (Exception ex) {
                logger.Warn(ex.Message + ex.StackTrace);
            }
        }

        // Creates the basic minimal tree
        private ContainerTreeNode AddBasicTree(Character character) {
            string label = string.Format("{1} :: {0}", ScriptUtilities.FormatAOCurrency(character.Credits), character.Name);
            long ownerId = character.Id;

            ContainerTreeNode tree = new ContainerTreeNode { label = label, children = new List<ContainerTreeNode>(), containerId = 0, characterId = ownerId };
            tree.children.Add(new ContainerTreeNode { label = "Inventory", children = new List<ContainerTreeNode>(), containerId = 2, characterId = ownerId });
            tree.children.Add(new ContainerTreeNode { label = "Bank", children = new List<ContainerTreeNode>(), containerId = 1, characterId = ownerId });
            tree.children.Add(new ContainerTreeNode { label = "Weapons", children = new List<ContainerTreeNode>(), containerId = 10, characterId = ownerId });
            tree.children.Add(new ContainerTreeNode { label = "Equipped", children = new List<ContainerTreeNode>(), containerId = 11, characterId = ownerId });
            tree.children.Add(new ContainerTreeNode { label = "Implants", children = new List<ContainerTreeNode>(), containerId = 12, characterId = ownerId });
            tree.children.Add(new ContainerTreeNode { label = "Social", children = new List<ContainerTreeNode>(), containerId = 13, characterId = ownerId });
            tree.children.Add(new ContainerTreeNode { label = "GMI", children = new List<ContainerTreeNode>(), containerId = VIRTUAL_GMI_ID, characterId = ownerId });
            return tree;
        }

        string MapIdAccount(long characterId) {
            if (accountMap.ContainsKey(characterId)) {
                return accountMap[characterId];
            }
            else {
                return "Unknown";
            }
        }

        string MapIdBackpack(long characterId, long containerId) {
            if (accountMap.ContainsKey(characterId)) {
                return BackpackRenamer.GetBackpackName(accountMap[characterId], characterId, containerId);
            }
            return string.Empty;
        }


        /// <summary>
        /// Load the containers for a specific inventory (bank/weapons/inventory/etc..)
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="containerId"></param>
        /// <returns></returns>
        public ContainerUpdateNode UpdateTreeViewNode(long characterId, long containerId) {
            IList<Item> containers = itemDao.ListContainersFor(characterId, containerId);
            charactersInLoadingQueue.Remove(characterId);

            //logger.DebugFormat("Updating treeview for character {1}.. {0} containers in parent {2}", containers.Count, characterId, containerId);
            List<ContainerNode> nodes = new List<ContainerNode>();
            foreach (var container in containers) {
                try {
                    // Will be null if logging in from a new account/new toon
                    string label = MapIdBackpack(container.Owner.Id, container.Children);
                    if (string.IsNullOrEmpty(label))
                        label = container.Name;


                    nodes.Add(new ContainerNode { Text = label, ContainerId = container.Children, CharacterId = container.Owner.Id });
                }
                catch (NullReferenceException ex) {
                    logger.Warn(ex.Message);
                    logger.Warn(ex.StackTrace);
                    ExceptionReporter.ReportException(ex, "UpdateTreeViewNode() - THIS IS BAD");
                }
            }

            // Sort containers alphabetically
            if ((bool)Properties.Settings.Default["postfixbags"]) {
                nodes.Sort(
                    delegate(ContainerNode a, ContainerNode b) {
                        return a.Text.CompareTo(b.Text);
                    });
            }
            else {
                nodes.Sort(
                    delegate(ContainerNode a, ContainerNode b) {
                        try {
                            var sa = a.Text.StartsWith("#") ? a.Text.Substring(1 + a.Text.IndexOf(" ")) : a.Text;
                            var sb = b.Text.StartsWith("#") ? b.Text.Substring(1 + b.Text.IndexOf(" ")) : b.Text;
                            return sa.CompareTo(sb);
                        }
                        catch (Exception ex) {
                            ExceptionReporter.ReportException(ex, a.Text + " + " + b.Text);
                            return 0;
                        }
                    });

            }

            return new ContainerUpdateNode {
                CharacterId = characterId,
                ContainerId = containerId,
                nodes = nodes
            };
        }


        /// <summary>
        /// Update the tree view on the GUI
        /// Containers/characters/etc
        /// </summary>
        /// <param name="resultQueue"></param>
        public List<MessageData> UpdateTreeViewGUI(int dimension) {
            List<ContainerTreeNode> nodes = new List<ContainerTreeNode>();
            charactersInLoadingQueue.Clear();
            
            Dictionary<string, ContainerTreeNode> rootnodes = new Dictionary<string, ContainerTreeNode>();
            // TODO: The actual looping here could be run in a separate thread, to prevent the primary processing thread from being occupied

            // Generate all the character root nodes
            IList<Character> characters = characterDao.ListByDimensionOrderByLastSeen(dimension);
            Dictionary<long, ContainerTreeNode> NodesPerCharacter = new Dictionary<long, ContainerTreeNode>();
            logger.DebugFormat("Updating treeview.. {0} characters without containers..", characters.Count);
            foreach (Character character in characters) {
                NodesPerCharacter[character.Id] = AddBasicTree(character);


                // Map to account
                string account = MapIdAccount(character.Id);
                if (!rootnodes.ContainsKey(account)) {
                    rootnodes[account] = new ContainerTreeNode { label = account, children = new List<ContainerTreeNode>(), containerId = 0, characterId = 0 };
                    nodes.Add(rootnodes[account]);
                }

                charactersInLoadingQueue.Add(character.Id);

                // Set character node as child of account
                rootnodes[account].children.Add(NodesPerCharacter[character.Id]);
            }


            logger.Debug("Counting credits for treeview..");

            // Count credits per account
            Dictionary<string, long> CreditsPerAccount = new Dictionary<string, long>();
            foreach (var character in characters) {
                string account = MapIdAccount(character.Id);
                if (!CreditsPerAccount.ContainsKey(account))
                    CreditsPerAccount[account] = 0;
                CreditsPerAccount[account] += character.Credits;
            }

            // Map credits to account name
            for (int i = 0; i < nodes.Count; i++) {
                ContainerTreeNode accountNode = nodes[i];
                string account = accountNode.label;
                if (CreditsPerAccount.ContainsKey(account))
                    accountNode.label += string.Format(" :: {0}", ScriptUtilities.FormatAOCurrency(CreditsPerAccount[account]));
                else
                    accountNode.label += " :: 0m";

                nodes[i] = accountNode;
            }

            if (nodes.Count == 0) {
                nodes.Add(new ContainerTreeNode { label = "No toons!?", containerId = 0, characterId = 0 });
            }

            logger.Debug("Finished generating treeview..");

            List<MessageData> result = new List<MessageData>();
            result.Add(new MessageData { Type = MessageDataType.RES_CHARACTER_TREE, Data = nodes });

            result.Add(new MessageData {
                Type = MessageDataType.RES_SET_CHARACTER_LIST,
                Data = characterDao.ListAllAsComboBox(dimension)
            });

            return result;
        }

        /// <summary>
        /// Update the tree view on the GUI
        /// Containers/characters/etc
        /// </summary>
        /// <param name="resultQueue"></param>
        public List<MessageData> UpdateTreeViewGUI_old(int dimension) {
            List<ContainerTreeNode> nodes = new List<ContainerTreeNode>();

            IList<Item> containers = itemDao.ListAllContainers(dimension);
            Dictionary<string, ContainerTreeNode> rootnodes = new Dictionary<string, ContainerTreeNode>();
            // TODO: The actual looping here could be run in a separate thread, to prevent the primary processing thread from being occupied

            // Generate all the character root nodes
            IList<Character> characters = characterDao.ListByDimensionOrderByLastSeen(dimension);
            Dictionary<long, ContainerTreeNode> NodesPerCharacter = new Dictionary<long, ContainerTreeNode>();
            logger.DebugFormat("Updating treeview.. {0} characters without containers..", characters.Count);
            foreach (Character character in characters) {
                NodesPerCharacter[character.Id] = AddBasicTree(character);


                // Map to account
                string account = MapIdAccount(character.Id);
                if (!rootnodes.ContainsKey(account)) {
                    rootnodes[account] = new ContainerTreeNode { label = account, children = new List<ContainerTreeNode>(), containerId = 0, characterId = 0 };
                    nodes.Add(rootnodes[account]);
                }

                // Set character node as child of account
                rootnodes[account].children.Add(NodesPerCharacter[character.Id]);
            }
            

            // Place containers under each character
            logger.DebugFormat("Updating treeview.. {0} containers..", containers.Count);
            foreach (var container in containers) {
                if (!NodesPerCharacter.ContainsKey(container.Owner.Id)) {
                    NodesPerCharacter[container.Owner.Id] = AddBasicTree(container.Owner);

                    // TODO: DRY issue
                    // Map to account
                    string account = MapIdAccount(container.Owner.Id);
                    if (!rootnodes.ContainsKey(account)) {
                        rootnodes[account] = new ContainerTreeNode { label = account, children = new List<ContainerTreeNode>(), containerId = 0, characterId = 0 };
                        nodes.Add(rootnodes[account]);
                    }

                    // Set character node as child of account
                    rootnodes[account].children.Add(NodesPerCharacter[container.Owner.Id]);
                }

                ContainerTreeNode currentNode;
                currentNode = NodesPerCharacter[container.Owner.Id];


                // Will be null if logging in from a new account/new toon
                string label = MapIdBackpack(container.Owner.Id, container.Children);
                if (string.IsNullOrEmpty(label))
                    label = container.Name;


                // Place this container under "Bank" or "Inventory"
                if (container.ParentContainerId == (uint)InventoryIds.INV_BANK) {
                    currentNode.children[1].children.Add(new ContainerTreeNode { label = label, containerId = container.Children, characterId = container.Owner.Id });
                }
                else if (container.ParentContainerId == (uint)InventoryIds.INV_TOONINV) {
                    currentNode.children[0].children.Add(new ContainerTreeNode { label = label, containerId = container.Children, characterId = container.Owner.Id });
                }
                NodesPerCharacter[container.Owner.Id] = currentNode;
            }

            logger.Debug("Counting credits for treeview..");

            // Count credits per account
            Dictionary<string, long> CreditsPerAccount = new Dictionary<string, long>();
            foreach (var character in characters) {
                string account = MapIdAccount(character.Id);
                if (!CreditsPerAccount.ContainsKey(account))
                    CreditsPerAccount[account] = 0;
                CreditsPerAccount[account] += character.Credits;
            }

            // Map credits to account name
            for (int i = 0; i < nodes.Count; i++) {
                ContainerTreeNode accountNode = nodes[i];
                string account = accountNode.label;
                if (CreditsPerAccount.ContainsKey(account))
                    accountNode.label += string.Format(" :: {0}", ScriptUtilities.FormatAOCurrency(CreditsPerAccount[account]));
                else
                    accountNode.label += " :: 0m";

                nodes[i] = accountNode;                
            }

            if (nodes.Count == 0) {
                nodes.Add(new ContainerTreeNode { label = "No toons!?", containerId = 0, characterId = 0 });
            }

            logger.Debug("Finished generating treeview..");

            List<MessageData> result = new List<MessageData>();
            result.Add(new MessageData { Type = MessageDataType.RES_CHARACTER_TREE, Data = nodes });

            result.Add(new MessageData {
                Type = MessageDataType.RES_SET_CHARACTER_LIST,
                Data = characterDao.ListAllAsComboBox(dimension)
            });

            return result;
        }


        /// <summary>
        /// Progressively update the treeview
        /// Updating at two second intervals as not to disturb other activity too much
        /// </summary>
        /// <param name="queue"></param>
        public void Update(ConcurrentQueue<MessageData> queue) {
            try {
                if (charactersInLoadingQueue.Count > 0) {
                    if (cooldown == null) {
                        cooldown = new Stopwatch();
                        cooldown.Start();
                    }

                    if (cooldown.ElapsedMilliseconds > 1000 * 2) {
                        var id = charactersInLoadingQueue.First();
                        charactersInLoadingQueue.Remove(id);
                        queue.Enqueue(new MessageData {
                            Type = MessageDataType.RES_SET_TREEVIEW_CHILDREN,
                            Data = UpdateTreeViewNode(id, 1)
                        });
                        queue.Enqueue(new MessageData {
                            Type = MessageDataType.RES_SET_TREEVIEW_CHILDREN,
                            Data = UpdateTreeViewNode(id, 2)
                        });

                        if (charactersInLoadingQueue.Count == 0)
                            cooldown = null;
                        else
                            cooldown.Restart();
                    }
                }
            }
            catch (NullReferenceException ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                ExceptionReporter.ReportException(ex, "treeview.Update()");
            }
        }
    }
}
