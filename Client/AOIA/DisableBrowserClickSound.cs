﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using log4net;
using EvilsoftCommons.Exceptions;

namespace ItemsLight.AOIA {
    class DisableBrowserClickSound {
        private static ILog logger = LogManager.GetLogger(typeof(DisableBrowserClickSound));

        const int FEATURE_DISABLE_NAVIGATION_SOUNDS = 21;
        const int SET_FEATURE_ON_PROCESS = 0x00000002;

        [DllImport("urlmon.dll")]
        [PreserveSig]
        [return: MarshalAs(UnmanagedType.Error)]
        static extern int CoInternetSetFeatureEnabled(
            int FeatureEntry,
            [MarshalAs(UnmanagedType.U4)] int dwFlags,
            bool fEnable);

        static void DisableClickSounds() {
            CoInternetSetFeatureEnabled(
                FEATURE_DISABLE_NAVIGATION_SOUNDS,
                SET_FEATURE_ON_PROCESS,
                true);
        }

        public DisableBrowserClickSound() {
            try {
                DisableClickSounds();
            }

            catch (Exception e) {
                logger.Warn(e.Message);
                logger.Warn(e.StackTrace);
                ExceptionReporter.ReportException(e);
            }
        }
    }
}
