﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using ItemsLight.AOIA.Parsers;
using ItemsLight.Dao;
using EvilsoftCommons.Exceptions;

namespace ItemsLight.AOIA {
    public class ParseClientMessage {
        static ILog logger = LogManager.GetLogger(typeof(ParseClientMessage));
        CharacterDao characterDao = new CharacterDao();
        ItemDao itemDao = new ItemDao();
        
        public bool MSG_GIVE_TO_NPC(AOGiveToNPC msg) {
            uint fromContainerId = Inventory.GetFromContainerId(msg.CharacterId, msg.invItemGiven.type, msg.invItemGiven.containerTempId);
            uint toContainer = (uint)InventoryIds.INV_TRADE;
            if (msg.direction == 1)
                toContainer = (uint)InventoryIds.INV_TOONINV;

            uint nextFreeInvSlot = itemDao.FindNextAvailableContainerSlot(msg.CharacterId, toContainer);
            itemDao.UpdateParentAndSlotForItem(toContainer, nextFreeInvSlot, fromContainerId, msg.invItemGiven.itemSlotId, msg.CharacterId);

#if DEBUG
            //logger.DebugFormat()
#endif

            return true;
        }


        public bool MSG_WITH_AWESOMIUM_COOKIES(byte[] bdata) {
            try {
                AOMessageBase aomsg = new AOMessageBase(bdata);
                uint id1 = aomsg.EntityId;
                uint id2 = aomsg.popInteger();
                uint characterId = aomsg.m_serverid;
                GMITracker.Set(characterId, id1, id2);
#if DEBUG
                // SENSITIVE, DO NOT INCLUDE IN PLAYER LOGS -- PLAYERS MAY POST ON FORUM
                logger.Debug("AWESOMIUM: " + characterId + ": " + id1 + " " + id2);
#endif
            }
            catch (Exception ex) {
                logger.Warn(ex.Message);
                ExceptionReporter.ReportException(ex, "MSG_WITH_AWESOMIUM_COOKIES");
            }

            return true;
        }

        public bool MSG_END_NPC_TRADE(AOEndNPCTrade msg) {
            if (msg.operation == 0 || msg.operation == 1) {
                
                uint shopCapacity = 35;
                uint shopContainer = (uint)InventoryIds.INV_TRADE;
                itemDao.AddItemsReceivedFromShop(shopCapacity, msg.CharacterId, shopContainer);
            }
            return true;
        }


        public bool MSG_CHAR_OPERATION(CharacterAction msg) {

            switch (msg.operationId) {
                case 52: // split stack
                    {
                        uint fromContainerId = Inventory.GetFromContainerId(msg.CharacterId, msg.itemToDelete.type, msg.itemToDelete.containerTempId);
                        if (fromContainerId == 0)
                            return false;

                        uint nextFreeInvSlot = itemDao.FindNextAvailableContainerSlot(msg.CharacterId, fromContainerId);
                        

                        LegacySQL.RENAME_ME_PLEASE(msg.CharacterId, fromContainerId, msg.itemId.high, nextFreeInvSlot, msg.itemToDelete.itemSlotId);
                
                        
                        itemDao.SplitStacks(msg.CharacterId, msg.itemId.high, fromContainerId, msg.itemToDelete.itemSlotId);
                    }
                    break;


                case 112: // Client deleted an item
                    {
                        uint fromContainerId = Inventory.GetFromContainerId(msg.CharacterId, msg.itemToDelete.type, msg.itemToDelete.containerTempId);
                        if (fromContainerId == 0)
                            return false;
                        itemDao.DeleteSpecificItemInSlot(msg.CharacterId, fromContainerId, msg.itemId.low, msg.itemId.high, msg.itemToDelete.itemSlotId);
                    }
                    break;
            }

            return true;
        }
        
            
    }
}
