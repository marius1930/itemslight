﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using log4net;
using ItemsLight.AOIA.Misc;
using ItemsLight.AOIA;
using ItemsLight.Model;
using EvilsoftCommons.Exceptions;

namespace ItemsLight {
    public partial class WriteSummaryToScripts : Form {
        static ILog logger = LogManager.GetLogger("WriteSummaryToScripts");
        private static Font verdana = new Font("Verdana", 10);
        private static Dictionary<int, string> PixelDict;
        private static Graphics graphics;
        private static int spaceKeyWidth = 0;
        private static int[] columnSizes = new int[] { 135, 52, 78, 78, 55, 165 };

        public WriteSummaryToScripts(List<Character> entries) {
            InitializeComponent();
            try {
                if (entries.Count > 0) {
                    graphics = this.CreateGraphics();
                    run(entries);
                }
            }
            catch (Exception ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                ExceptionReporter.ReportException(ex, "WriteSummaryToScripts");
            }

            
        }


        /// <summary>
        /// Measure the length/width of a string in pixels
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        protected static int _MeasureDisplayStringWidth(string text) {
            if (text == "")
                return 0;

            StringFormat format = new StringFormat(StringFormat.GenericDefault);
            RectangleF rect = new RectangleF(0, 0, 1000, 1000);
            CharacterRange[] ranges = { new CharacterRange(0, text.Length) };
            Region[] regions = new Region[1];

            format.SetMeasurableCharacterRanges(ranges);
            format.FormatFlags = StringFormatFlags.MeasureTrailingSpaces;

            regions = graphics.MeasureCharacterRanges(text, verdana, rect, format);
            rect = regions[0].GetBounds(graphics);

            return (int)(rect.Right);
        }


        /// <summary>
        /// Set the pixel value if it does not already exist
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        private static void SetPixelValue(int key, string value) {
            if (!PixelDict.ContainsKey(key))
                PixelDict[key] = value;
        }


        /// <summary>
        /// Populate a dictionary of the pixel-width of character combinations
        /// 
        /// </summary>
        public static void PopulatePixelDictionary() {
            if (PixelDict != null)
                return;
            PixelDict = new Dictionary<int, string>();

            spaceKeyWidth = _MeasureDisplayStringWidth(" ");
            for (int i = char.MinValue; i <= Math.Min(127, (int)char.MaxValue); i++) {
                char c = Convert.ToChar(i);
                if (!char.IsControl(c)) {
                    var w = _MeasureDisplayStringWidth(c.ToString());
                    SetPixelValue(w, c.ToString());
                    
                }
            }

            //PixelDict[13] = PixelDict[14]; // Inaccurate, but spaces are useless
            PixelDict[9] = PixelDict[10]; // Inaccurate, but " breaks the bubble


            var values = PixelDict.Values.ToList();
            for (int i = 0; i < 14; i++) {
                var values2 = PixelDict.Values.ToList();
                foreach (var key1 in values2) {
                    foreach (var key2 in values) {
                        int w = _MeasureDisplayStringWidth(key1 + key2);
                        SetPixelValue(w, key1 + key2);
                    }
                }
            }


            SetPixelValue(9, PixelDict[8]); // Inaccurate
            SetPixelValue(10, PixelDict[11]); // Inaccurate
            SetPixelValue(20, PixelDict[19]); // Inaccurate

            SetPixelValue(24, PixelDict[12] + PixelDict[12]);
            SetPixelValue(25, PixelDict[14] + PixelDict[11]);
            SetPixelValue(26, PixelDict[14] + PixelDict[13]);
            SetPixelValue(27, PixelDict[19] + PixelDict[8]);
            SetPixelValue(28, PixelDict[14] + PixelDict[14]);
            SetPixelValue(29, PixelDict[15] + PixelDict[14]);
            SetPixelValue(30, PixelDict[15] + PixelDict[15]);



            for (int idx = 31; idx < 500; idx++) {
                int mod = ((idx / 10) - 2) * 10;
                SetPixelValue(idx, PixelDict[idx - 14] + PixelDict[14]);
            }
        }



        /// <summary>
        /// Pad the input text with black garbage characters, to align the data properly ingame.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="len"></param>
        /// <param name="missmatch"></param>
        /// <param name="postpad"></param>
        /// <returns></returns>
        private static string Pad(string s, int len, out int missmatch, bool postpad = true) {
            var currentLength = _MeasureDisplayStringWidth(s);
            int diff = len - currentLength;

            int spaces = 1;
            while (diff > spaceKeyWidth && spaces != 0) {
                spaces = (5 + diff) / spaceKeyWidth;
                if (postpad)
                    s += new string(' ', spaces);
                else
                    s = new string(' ', spaces) + s;
                currentLength = _MeasureDisplayStringWidth(s);
                diff = len - currentLength;
            }
            

            currentLength = _MeasureDisplayStringWidth(s);
            diff = len - currentLength;

            if (PixelDict.ContainsKey(diff)) {
                missmatch = len - _MeasureDisplayStringWidth(s + PixelDict[diff]);
                if (postpad)
                    return string.Format("{0}<font color='#000'>{1}</font>", s, PixelDict[diff]).Replace("%", "%%");
                else
                    return string.Format("<font color='#000'>{1}</font>{0}", s, PixelDict[diff]).Replace("%", "%%");
            }
            else {
                missmatch = len - currentLength;
            }
            return s;
        }


        /// <summary>
        /// Format the input data into a padded string for use in an AO chat bubble
        /// </summary>
        /// <param name="name"></param>
        /// <param name="level"></param>
        /// <param name="credits"></param>
        /// <param name="profession"></param>
        /// <param name="location"></param>
        /// <param name="lastplayed"></param>
        /// <returns></returns>
        private static string format(string name, int level, string credits, string gmi, string profession, string location, string lastplayed) {
            int missmatch = 0;
            string sLevel;
            int column = 0;
            {
                int t;
                name = Pad(name, columnSizes[column++], out t);
                missmatch += t;
            }

            {
                int t;
                sLevel = Pad(level.ToString(), columnSizes[column++] + missmatch, out t, true);
                missmatch += t;
            }
            {
                int t;
                credits = Pad(credits, columnSizes[column++] + missmatch, out t, true);
                missmatch += t;
            }
            {
                int t;
                gmi = Pad(gmi, columnSizes[column++] + missmatch, out t, true);
                missmatch += t;
            }
            lastplayed = Pad(lastplayed, columnSizes[column++], out missmatch);
            if (!string.IsNullOrEmpty(location))
                location = Pad(location, columnSizes[column++], out missmatch, false);

            return string.Format("{0}{1}{2}{3}{4}{5}", name, sLevel, credits, gmi, lastplayed, location);
        }


        /// <summary>
        /// Iterate over each entry and store a padded string for use ingame.
        /// </summary>
        /// <param name="entries"></param>
        /// <returns></returns>
        static List<string> EntriesToPaddedStrings(List<Character> entries) {

            string[] professionlist = {"?", "Soldier", "MA", "Eng", "Fixer", "Agent", 
                                    "Adv", "Trader", "Crat", 
                                    "Enf", "Doc", "NT", "MP", "?13", "Keeper", "Shade"};


            List<string> paddedStrings = new List<string>();
            foreach (var character in entries) {
                string location;

                string credits = ScriptUtilities.FormatAOCurrency(character.Credits);
                string gmi = ScriptUtilities.FormatAOCurrency(character.CreditsGMI);

                if (character.ZoneId == 0)
                    location = "";
                else {
                    location = ZoneClassifier.Classify(character.ZoneId, character.PositionX, character.PositionZ);
                    if (character.SubZoneId != 0)
                        location = string.Format("{0} ({1})", ZoneClassifier.ClassifySubzone(character.SubZoneId), location);
                }

                string lastSeen = ScriptUtilities.FormatLastSeen(character.LastSeen, false);

                string profession = (character.ProfessionId < professionlist.Length && character.ProfessionId >= 0) ? professionlist[character.ProfessionId] : "?";
                string paddedLine = format(character.Name, character.Level, credits, gmi, profession, location, lastSeen) + "<br>";
                paddedStrings.Add(paddedLine);
            }
            return paddedStrings;
        }


        /// <summary>
        /// Returns a dictionary of [filename, script content]
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, string> GetScripts(List<string> paddedStrings, string header) {
            Dictionary<string, string> result = new Dictionary<string, string>();
            var totalLength = paddedStrings.Select(x => x.Length).Sum();

            const int blobLength = 3640;
            string whoreout = "<br><br><a href='chatcmd:///start http://ribbs.dreamcrash.org/ialight/'>Get yours now!</a>";
            if (totalLength < blobLength) {
                string blob = paddedStrings.Aggregate((i, j) => i + j);
                if (totalLength + whoreout.Length >= blobLength)
                    whoreout = "";

                string data = string.Format("<a href=\"text://{0}{1}{2}\">Click here for a Summary</a>", header, blob, whoreout)
                    .Replace("\n", "")
                    .Replace("\r", "");

                result["summary"] = data;
                result["esummary"] = "/text " + data;
            }
                
            // Multiple scripts
            else {
                List<string> splitscript = new List<string>();

                int i = 0;
                string blob = "";
                while (i < paddedStrings.Count) {
                    if (blob.Length + paddedStrings[i].Length <= blobLength) {
                        blob += paddedStrings[i];
                    }
                    else {
                        splitscript.Add(blob);
                        blob = "";
                    }
                    i++;
                }
                if (blob.Length > 0)
                    splitscript.Add(blob);

                for (int j = 0; j < splitscript.Count; j++) {
                    string data = string.Format("<a href=\"text://{0}{1}\">Click here for a Summary {2}</a>", header, splitscript[j], (j + 1) + "/" + splitscript.Count)
                        .Replace("\n", "")
                        .Replace("\r", "");

                    string extra = "";
                    string extrae = "";
                    if (j != splitscript.Count - 1) {
                        extra = "\n/summary" + (j + 1);
                        extrae = "\n/esummary" + (j + 1);
                    }

                    if (j > 0) {
                        result["summary" + j] = data + extra;
                        result["esummary" + j] = "/text " + data + extrae;
                    }
                    else {
                        result["summary"] = data + extra;
                        result["esummary"] = "/text " + data + extrae;
                    }

                }
            }

            return result;
        }



        static void run(List<Character> entries) {

            PopulatePixelDictionary();


            // Create the header and calculate column width for the first column
            string header = "";
            {

                var longestCharacterName = entries.Max(m => _MeasureDisplayStringWidth(m.Name)) + 15;
                columnSizes[0] = longestCharacterName;
                columnSizes[3] = _MeasureDisplayStringWidth("99 minutes ago") + 15;

                int tmp3432432;
                int column = 0;
                var t0 = Pad("Name", columnSizes[column++], out tmp3432432).Replace("Name", "<font color='#EFE4B0'>Name</font>");
                var t1 = Pad("Level", columnSizes[column++], out tmp3432432).Replace("Level", "<font color='#EFE4B0'>Level</font>");
                //var t2 = Pad("Profession", columnSizes[column++], out tmp3432432).Replace("Profession", "<font color='#EFE4B0'>Profession</font>");
                var t3 = Pad("Credits", columnSizes[column++], out tmp3432432).Replace("Credits", "<font color='#EFE4B0'>Credits</font>");
                var t4 = Pad("GMI", columnSizes[column++], out tmp3432432).Replace("GMI", "<font color='#EFE4B0'>GMI</font>");
                var t5 = Pad("Last Seen", columnSizes[column++], out tmp3432432).Replace("Last Seen", "<font color='#EFE4B0'>Last Seen</font>");
                var t6 = Pad("Location", columnSizes[column++], out tmp3432432, false).Replace("Location", "<font color='#EFE4B0'>Location</font>");
                header = t0 + t1 + t3 + t4 + t5 + t6 + "<br>";
            }

            List<string> paddedStrings = EntriesToPaddedStrings(entries);
            var scripts = GetScripts(paddedStrings, header);


            try {
                // Write to file
                foreach (String folder in AppdataPathFinder.ListAnarchyDirectories()) {
                    Directory.CreateDirectory(Path.Combine(folder, "scripts"));
                    
                    foreach (var filename in scripts.Keys) {
                        File.WriteAllText(Path.Combine(folder, "scripts", filename), scripts[filename]);
                    }
                }
            }
            catch (IOException e) {
                logger.Warn(e.Message);
                logger.Warn(e.StackTrace);
            }
            catch (Exception e) {
                logger.Warn(e.Message);
                logger.Warn(e.StackTrace);
                ExceptionReporter.ReportException(e);
            }

        }

        /// <summary>
        /// We have no interesting showing an actual form ;)
        /// It was only created for the Graphics object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WriteSummaryToScripts_Load(object sender, EventArgs e) {
            this.Close();
        }
    }
}
