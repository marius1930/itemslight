﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using log4net;

public class ListViewColumnSorterQL : ListViewColumnSorter {

    protected override int CustomCompare(string a, string b) {
        try {
            if (ColumnToSort == 1) {
                return ObjectCompare.Compare(float.Parse(a), float.Parse(b));
            }
        }
        catch (Exception) {
            // Ignored, just do a regular compare
        }

        return ObjectCompare.Compare(a, b);
    }
}
public class ListViewColumnSorterIdentifyView : ListViewColumnSorter {
    private static ILog logger = LogManager.GetLogger(typeof(ListViewColumnSorterIdentifyView));

    protected override int CustomCompare(string a, string b) {
        try {
            if (ColumnToSort == 0 && a.StartsWith("#")) {
                int diff = a.Substring(5).CompareTo(b.Substring(5));
                if (diff != 0)
                    return diff;

                int ia = int.Parse(a.Substring(1, 1));
                int ib = int.Parse(b.Substring(1, 1));

                return ia - ib;
            }
            if (ColumnToSort == 2) {
                return ObjectCompare.Compare(float.Parse(a), float.Parse(b));
            }
        }
        catch (Exception ex) {
            // Ignored, just do a regular compare
            logger.Warn(ex.Message);
        }

        return ObjectCompare.Compare(a, b);
    }
}

public class ListViewColumnSorterBuffItems : ListViewColumnSorter {

    protected override int CustomCompare(string a, string b) {
        try {
            if (ColumnToSort == 1) {
                return ObjectCompare.Compare(float.Parse(a), float.Parse(b));
            }
            else if (ColumnToSort == 3) {
                return ObjectCompare.Compare(float.Parse(a.Replace("+", "")), float.Parse(b.Replace("+", "")));
            }
        }
        catch (Exception) {
            // Ignored, just do a regular compare
        }

        return ObjectCompare.Compare(a, b);
    }
}


public class ListViewColumnSorter : IComparer {
    /// <summary>
    /// Specifies the column to be sorted
    /// </summary>
    protected int ColumnToSort;
    /// <summary>
    /// Specifies the order in which to sort (i.e. 'Ascending').
    /// </summary>
    private SortOrder OrderOfSort;
    /// <summary>
    /// Case insensitive comparer object
    /// </summary>
    protected CaseInsensitiveComparer ObjectCompare;

    /// <summary>
    /// Class constructor.  Initializes various elements
    /// </summary>
    public ListViewColumnSorter() {
        // Initialize the column to '0'
        ColumnToSort = 0;

        // Initialize the sort order to 'none'
        OrderOfSort = SortOrder.None;

        // Initialize the CaseInsensitiveComparer object
        ObjectCompare = new CaseInsensitiveComparer();
    }

    protected virtual int CustomCompare(string a, string b) {
        return ObjectCompare.Compare(a, b); ;
    }

    /// <summary>
    /// This method is inherited from the IComparer interface.  It compares the two objects passed using a case insensitive comparison.
    /// </summary>
    /// <param name="x">First object to be compared</param>
    /// <param name="y">Second object to be compared</param>
    /// <returns>The result of the comparison. "0" if equal, negative if 'x' is less than 'y' and positive if 'x' is greater than 'y'</returns>
    public int Compare(object x, object y) {
        int compareResult;
        ListViewItem listviewX, listviewY;

        // Cast the objects to be compared to ListViewItem objects
        listviewX = (ListViewItem)x;
        listviewY = (ListViewItem)y;


        // Compare the two items
        string a = listviewX.SubItems[ColumnToSort].Text;
        string b = listviewY.SubItems[ColumnToSort].Text;
/*
 * // Cheap hack to order by QL, no longer applicable
        if (ColumnToSort == 0) {
            a += listviewX.SubItems[ColumnToSort + 1].Text;
            b += listviewY.SubItems[ColumnToSort + 1].Text;
        }*/

        compareResult = CustomCompare(a, b);


        // Calculate correct return value based on object comparison
        if (OrderOfSort == SortOrder.Ascending) {
            // Ascending sort is selected, return normal result of compare operation
            return compareResult;
        }
        else if (OrderOfSort == SortOrder.Descending) {
            // Descending sort is selected, return negative result of compare operation
            return (-compareResult);
        }
        else {
            // Return '0' to indicate they are equal
            return 0;
        }
    }

    /// <summary>
    /// Gets or sets the number of the column to which to apply the sorting operation (Defaults to '0').
    /// </summary>
    public int SortColumn {
        set {
            ColumnToSort = value;
        }
        get {
            return ColumnToSort;
        }
    }

    /// <summary>
    /// Gets or sets the order of sorting to apply (for example, 'Ascending' or 'Descending').
    /// </summary>
    public SortOrder Order {
        set {
            OrderOfSort = value;
        }
        get {
            return OrderOfSort;
        }
    }

}