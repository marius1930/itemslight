﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ItemsLight.AOIA {
    public class ContainerNode : TreeNode {
        public long ContainerId { get; set; }
        public long CharacterId { get; set; }
    }

    public class AOIAImportRequest {
        public string Path { get; set; }
        public bool ClearExistingDatabase { get; set; }
    }
    public enum RequestMessage {
        ERROR,
        SEARCH_ITEM,
        OPEN_CONTAINER,
        REFRESH_TREEVIEW,
        RESTART_BACKPACK_RENAMER,
        IMPORT_AOIA,
        RENAME_CONTAINER,
        IDENTIFY_ITEMS,
        UPDATE_PATTERNS,
        LOAD_SINGLE_PATTERN,
        BUFF_ITEMS,
        SEARCH_ITEM_ID,
        DELETE_CHARACTER,
        ARCHIVE_CHARACTER,

        CREATE_BACKUP_IMMEDIATELY,
        UPDATE_BACKPACK_DATABASE,

        GET_TREEVIEW_CHILDREN,
        GET_TREEVIEW_CHILDREN_AND_SELECT,

        UPDATE_SUMMARY,

        SHOW_SYMBIANTS,
        SHOW_EQUIPPED_ITEM,
        REQ_CHARACTER,

        WRITE_ITEMS_SCRIPT,
        //UPDATE_AO_ITEM_DATABASE,

    }
    public enum MessageDataType {
        ERROR,

        RES_CONTAINER_DATA,
        RES_SEARCH_RESULT,
        RES_CHARACTER_TREE,

        RES_READY_TO_BEGIN,
        RES_SHOW_PROGRESSBAR,
        RES_SET_DIMENSIONS,

        RES_IDENTIFY_DATA,


        RES_SET_CHARACTER_LIST,
        RES_UPDATE_PATTERNS,
        RES_LOAD_SINGLE_PATTERN,

        RES_BUFF_ITEMS,
        //RES_EXCEPTION,

        RES_UPDATE_SUMMARY,

        RES_GMI_SALE,

        RES_SET_STATUS_MESSAGE,
        RES_SET_TREEVIEW_CHILDREN,
        RES_SET_TREEVIEW_SELECTION,
        
        
        RES_SHOW_EQUIPPED_ITEM,

        RES_SHOW_SYMBIANTS,
        RES_CHARACTER
    }

    public class GMISale {
        public long characterId;
        public string name;
        public long amount;
    }

    public struct MessageData {
        public MessageDataType Type { get; set; }
        public object Data { get; set; }
    }
    public struct MessageDataRequest {
        public RequestMessage Type { get; set; }
        public object Data { get; set; }
    }

    public struct ContainerTreeNode {
        public string label;
        public List<ContainerTreeNode> children;
        public long containerId;
        public long characterId;

        public void update(TreeNodeCollection nodes) {
            nodes.Add(new ContainerNode { Text = label, ContainerId = containerId, CharacterId = characterId });
            if (children != null && children.Count > 0) {
                foreach (ContainerTreeNode node in children) {
                    node.update(nodes[nodes.Count - 1].Nodes);
                }
            }
        }
    };
}
