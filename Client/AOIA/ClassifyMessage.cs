﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ItemsLight.AOIA.Parsers;
using log4net;
using EvilsoftCommons.Exceptions;

namespace ItemsLight.AOIA {
    public class ClassifyMessage {
        static ILog logger = LogManager.GetLogger(typeof(ClassifyMessage));

        private static ParseServerMessage sm = new ParseServerMessage();
        private static ParseClientMessage cm = new ParseClientMessage();


        private static string toString(byte[] data) {
            string s = "";
            foreach (byte b in data) {
                s += b + " ";
            }
            return s;
        }
        public static void CustomWndProc(byte[] data, int cbData) {

            if (data.Length <= 24) {
                // Too small to even start processing
                return;
            }


            AOMessageBase aomsg;

            // Protocol changes? May get a packet smaller than were expecting, just report it and move on.
            try {
                aomsg = new AOMessageBase(data);
            }
            catch (IndexOutOfRangeException ex) {
                var dataset = string.Join(" ", string.Join(",", data).Split(',')
                                    .Select(x => x.PadLeft(3, '0')));

                ExceptionReporter.ReportException(ex, dataset);
                return;
            }

            try {
                if (cbData == 2) {
                    switch (aomsg.messageId()) {
                        case (uint)MessageId.MSG_GIVE_TO_NPC:
                            cm.MSG_GIVE_TO_NPC(new AOGiveToNPC(data));
                            break;

                        case (uint)MessageId.MSG_END_NPC_TRADE:
                            cm.MSG_END_NPC_TRADE(new AOEndNPCTrade(data));
                            break;

                        case (uint)MessageId.MSG_CHAR_OPERATION:
                            cm.MSG_CHAR_OPERATION(new CharacterAction(data));
                            break;
                        case (uint)MessageId.MSG_WITH_AWESOMIUM_COOKIES:
                            cm.MSG_WITH_AWESOMIUM_COOKIES(data);
                            break;
                    }
                }
                else if (cbData == 1) {
#if DEBUG
                    /*{
                        for (int i = 0; i < data.Length - 3; i++) {

                            byte[] bytes = new byte[] { (byte)data[i], (byte)data[i + 1], (byte)data[i+2], (byte)data[i + 3] };
                            if (BitConverter.IsLittleEndian)
                                Array.Reverse(bytes);
                            var b = BitConverter.ToUInt32(bytes, 0);

                            if (b == 710) {
                                logger.Debug("Possible zone: 710 at byte " + i + " in message " + aomsg.messageId());
                            }
                        }
                    }*/

                    /*
                    {
                        var tmpX = new HashSet<uint>(new uint[] { 490475292, 675889264, 1363747104, (uint)MsgIds.MSG_CHAR_OPERATION, 1968115989, 1245782078, (uint)MsgIds.MSG_UNKNOWN_1, (uint)MsgIds.MSG_PROGRAM_TERMINATED, (uint)MsgIds.SMSG_RELATED_TO_CORPSESPAWN });
                        if (tmpX.Contains(aomsg.messageId())) {
                            var a = string.Join(",", data).Split(',')
                                             .Select(x => x.PadLeft(3, '0'));
                            logger.DebugFormat("{0}: {1}", aomsg.messageId().ToString("X"), string.Join(" ", a));
                        }
                        else if (aomsg.messageId() == (uint)MsgIds.SMSG_CORPSESPAWN) {
                            var a = string.Join(",", data).Split(',').Select(x => x.PadLeft(3, '0'));
                            logger.DebugFormat("SMSG_POSSIBLE_CORPSESPAWN: {0}", string.Join(" ", a));
                        }
                    }
                    {
                        var tmpX = new HashSet<uint>(new uint[] { 911278200, 0xf00ba });
                        if (tmpX.Contains(aomsg.messageId())) {
                            var a = string.Join(",", data).Split(',')
                                             .Select(x => x.PadLeft(3, '0'));
                            logger.DebugFormat("{0}: {1}", aomsg.messageId().ToString("X"), string.Join(" ", a));
                        }
                        else if (aomsg.messageId() == (uint)MsgIds.MSG_CONTAINER) {
                            var a = string.Join(",", data).Split(',').Select(x => x.PadLeft(3, '0'));
                            logger.DebugFormat("MSG_CONTAINER({1}): {0}", string.Join(" ", a), data.Length);
                        }
                        else if (aomsg.messageId() == (uint)MsgIds.MSG_OPENBACKPACK) {
                            var a = string.Join(",", data).Split(',').Select(x => x.PadLeft(3, '0'));
                            logger.DebugFormat("MSG_OPENBACKPACK: {0}", string.Join(" ", a));
                        }
                    }*/
                    /*var tmpX = new HashSet<uint>(new uint[] { 1314089334, 691028809 });
                    var tmpY = new HashSet<uint>(new uint[] { 859514983, 894457412 });
                    if (tmpY.Contains(aomsg.messageId())) {
                        {
                            int toFind = 95577; // lockpick
                            var indices = aomsg.FindInteger(toFind);
                            foreach (int index in indices) {
                                logger.DebugFormat("{0} found found at {1}", toFind, index);
                            }
                        }
                        {
                            int toFind = 161699; // nano programming interface
                            var indices = aomsg.FindInteger(toFind);
                            foreach (int index in indices) {
                                logger.DebugFormat("{0} found found at {1}", toFind, index);
                            }
                        }

                        var a = string.Join(",", data).Split(',')
                                         .Select(x => x.PadLeft(3, '0'));

                        string message = string.Format("message {0}: {1}", aomsg.messageId(), string.Join(" ", a));
                        logger.Debug(message);
                        

                    }*/


                    /*
                    var tmp = new HashSet<uint>(new uint[] { 11, 1, 638531185, 656095851, 1410404643, 911278200, 1581741936 });
                    if (!tmp.Contains(aomsg.messageId())) {
                        string message = string.Format("?-message {0}: {1}", aomsg.messageId(), string.Join(" ", data));
                        logger.Debug(message);            
                        
                    }*/

#endif
                    switch (aomsg.messageId()) {

                        case (uint)MessageId.MSG_CONTAINER:
                            sm.MSG_CONTAINER(data);
                            break;

                        case (uint)MessageId.MSG_BACKPACK:
                            sm.MSG_BACKPACK(data);
                            break;

                        case (uint)MessageId.MSG_BANK:
                            sm.MSG_BANK(data);
                            break;

                        case (uint)MessageId.SMSG_UNKNOWN_WITH_ZONEID0:
                            sm.SMSG_UNKNOWN_WITH_ZONEID0(data);
                            break;

                        case (uint)MessageId.SMSG_UNKNOWN_WITH_ZONEID1:
                            sm.SMSG_UNKNOWN_WITH_ZONEID1(data);
                            break;

                        case (uint)MessageId.SMSG_UNKNOWN_WITH_ZONEID2:
                            sm.SMSG_UNKNOWN_WITH_ZONEID2(data);
                            break;


                        case (uint)MessageId.SMSG_MOVED:
                            sm.SMSG_MOVED(data);
                            break;

                        case (uint)MessageId.MSG_MOB_SYNC:
                            sm.MSG_MOB_SYNC(data);
                            break;

                        case (uint)MessageId.MSG_CHAR_INFO:
                            sm.MSG_CHAR_INFO(data);
                            break;

                        case (uint)MessageId.MSG_ITEM_MOVE:
                            sm.MSG_ITEM_MOVE(data);
                            break;

                        case (uint)MessageId.MSG_ITEM_BOUGHT:
                            sm.MSG_ITEM_BOUGHT(data);
                            break;


                        case (uint)MessageId.MSG_CHAR_OPERATION:
                            sm.MSG_CHAR_OPERATION(data);
                            break;

                        case (uint)MessageId.SMSG_CORPSESPAWN:
                            sm.SMSG_CORPSESPAWN(data);
                            break;

                        case (uint)MessageId.MSG_SHOP_TRANSACTION:
                            sm.MSG_SHOP_TRANSACTION(data);
                            break;

                        case (uint)MessageId.MSG_PARTNER_TRADE:
                            sm.MSG_PARTNER_TRADE(data);
                            break;

                        case (uint)MessageId.MSG_SPAWN_REWARD:
                            sm.MSG_SPAWN_REWARD(data);
                            break;

                        case (uint)MessageId.MSG_GIVE_TO_NPC:
                            sm.MSG_GIVE_TO_NPC(data);
                            break;

                        case (uint)MessageId.MSG_ACCEPT_NPC_TRADE:
                            sm.MSG_ACCEPT_NPC_TRADE(data);
                            break;


                        case (uint)MessageId.MSG_FULLSYNC:
                            sm.MSG_FULLSYNC(data);
                            break;


                        case (uint)MessageId.SMSG_OPEN_MAIL:
                            sm.MSG_OPEN_MAIL(data);
                            break;

                        case 1:
                        case 11:
                        case 638531185:
                        case 911278200:
                            break; // Common spam

                        case 992544625:
                            break; // Emotes like /ymca and /bow

                        case 959724648:
                        case 623988077:
                        case 724778350:
                        case 1296367892:
                        case 1347702041:
                            break; // Havn't looked into it, happens when I cast a nano

                        case 490475292:
                        case 675889264:
                        case 1363747104:
                        case 1968115989:
                        case 1245782078:
                            break; // Haven't looked into it, happens when I did brawl and killed a mob

                        case 1174417174:
                        case 1949180692:
                        case 425609582:
                            break; // Prolonged combat

                        case 1381132376: {
                                int x = 9;

                            }
                            break;

                        case 541676156: {
                                int x = 9;
                            }
                            break;

                        default:
                            //logger.Debug("Ignored message " + aomsg.messageId());
                            break;
                    }

                }
            }
            catch (Exception ex) {
                logger.Warn(ex.Message);
                ExceptionReporter.ReportException(ex, string.Format("ClassifyMessage {0}, {1}", cbData, aomsg.messageId()));
            }
        }
    }
}
