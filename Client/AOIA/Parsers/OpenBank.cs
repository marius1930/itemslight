﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.Parsers {
    public class OpenBank : AOMessageBase {
        public OpenBank(byte[] data) : base(data) {
            
            for (int i = 0; i < unknown2.Length; i++)
                unknown2[i] = popChar();

            _mass = popInteger();

            for (int i = 0; i < mass; i++) {
                ContItem item = new ContItem(this);
                items.Add(item);
            }
        }

        
        //uint   unknown1;      // 0x01000000 wtf?
        char[] unknown2 = new char[1];
        private uint _mass;          // 1009*items+1009
        public uint mass {
            get {
                return (_mass - 1009) / 1009;
            }
        }
        public List<ContItem> items = new List<ContItem>();
    }


    public struct ContItem {		// Size 0x20
        public uint	index;
        public ushort flags;	// 0x0021 ?? 1=normal item? 2=backpack 32 = nodrop? 34=nodrop bp?
        public ushort stack;
        public AoObjectId containerid;
        public AoObjectId itemid;		// low + high id
        public uint	ql;
        public uint	nullval;
        public ContItem(Parser p) {
            index = p.popInteger();
            flags = p.popShort();
            stack = p.popShort();
            containerid = AoObjectId.spawn(p);
            itemid = AoObjectId.spawn(p);

            ql = p.popInteger();
            nullval = p.popInteger();
        }
    };
    public struct ContEnd {
        public AoObjectId containerId;
        public uint	tempContainerId;	// Virtual id of the container
        public uint	nullval;
    };
}
