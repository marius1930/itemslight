﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.Parsers {
    public class AOTradeTransaction : AOMessageBase {
        public AOTradeTransaction(byte[] data) : base(data) {
            unknown1 = popChar();
            unknown2 = popInteger();
            operationId = popChar();
            fromId = AoObjectId.spawn(this);
            itemToTrade = new InvItemId(this);
        }
        

		//00 00 00 00 01 05 00 00 c3 50 67 a6 de 6a 00 00 00 68 00 00 00 54
		char	unknown1;//01
		uint	unknown2;//00 00 00 01 
        public char operationId;//01=accept, 02=decline,03=?start?, 04=commit,05=add item,06=remove item
		public AoObjectId		fromId;
        public InvItemId itemToTrade;

    }



}
