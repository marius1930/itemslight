﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.Parsers {
    public class AOItemMoved : AOMessageBase {
        public AOItemMoved(byte[] data) : base(data) {
            unknown1 = popChar();
            fromItem = new InvItemId(this);
            toContainer = AoObjectId.spawn(this);
            unknown3 = popShort();
            targetSlot = popShort();
        }
        
		char	unknown1; //00
        public InvItemId fromItem;

		//unsigned short	unknown2;//00 00 (part of fromType maybe)
		//unsigned short	fromType;
		//unsigned short	fromContainerTempId;// If backpack: the slot id in the inventory that holds the container
		//unsigned short	fromItemSlotId;//The slot id of the item in its container/inventory.
        public AoObjectId toContainer;
		ushort	unknown3; //00
		public ushort	targetSlot; //6f (invalid) except when moving to wear window.
    }
}
