﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace ItemsLight.AOIA.Parsers {
    public class ReceivedMail : AOMessageBase {
        static ILog logger = LogManager.GetLogger(typeof(ReceivedMail));
        public const int MESSAGE_READ = 9;
        public const int MESSAGE_LOOT = 11;
        public const int MESSAGE_WITH_CREDITS = 1394264;
        public const int MESSAGE_WITH_CREDITS_PAD = 1394266;
        public const int LIST_MESSAGES = 5489703;
        public const uint UNKNOWN = uint.MaxValue;


        char type1;
        public uint Type2 { get; protected set; }
        public uint MailId { get; protected set; }
        public uint Flag { get; protected set; }
        
        public uint Credits { get; protected set; }

        public ReceivedMail(byte[] data)
            : base(data) {
            if (pos() != 28) {
                logger.Warn("Mail message did not start at index 28");
            }

            popShort();
            type1 = popChar();


            skip(4);

            // Sometimes it contains nothing, perhaps a listmode/index with 0 mails?
            if (this.Empty()) {
                Type2 = ReceivedMail.UNKNOWN;
            }
            else {

                MailId = popInteger();

                Type2 = popInteger();

                if ((Type2 == MESSAGE_READ || Type2 == MESSAGE_LOOT) && type1 != 4)
                    logger.Warn("Type2 is READ/LOOT but type1 != 4");

                if ((Type2 == MESSAGE_WITH_CREDITS || Type2 == MESSAGE_WITH_CREDITS_PAD) && type1 != 2)
                    logger.Warn("Type2 is READ/LOOT but type1 != 4");

                if (Type2 == MESSAGE_WITH_CREDITS_PAD || Type2 == MESSAGE_WITH_CREDITS) {
                    uint len1 = popShort();
                    skip(len1); // Message content or character name

                    // Message subject
                    uint subjectLength = popShort();
                    skip(subjectLength);

                    skip(9);
                    Flag = popInteger();

                    // 11008 empty
                    // 2304 has credits [may be zero]
                    // 2816 "Loot all" clicked, credits no longer valid

                    if (Flag != 2816)
                        Credits = popInteger();
                }
            }
        }
        
    }
}
