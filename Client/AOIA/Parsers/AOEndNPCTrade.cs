﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.Parsers {
    public class AOEndNPCTrade : AOMessageBase {
        public AOEndNPCTrade(byte[] data) : base(data) {
            unknown1 = popChar();
            operationId = popShort();
            npcID = AoObjectId.spawn(this);
            operation = popInteger();
            unknown2 = popInteger();
        }
        
        
		protected char unknown1;//00
		ushort operationId;//02??
        public AoObjectId npcID;//??
		public uint	operation;//00 00 00 00 if closing, 00 00 00 01 if accepting
		uint	unknown2;//00 00 00 00
    }
}
