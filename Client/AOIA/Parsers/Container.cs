﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.Parsers {
    public class Container : AOMessageBase {
        public Container(byte[] data) : base(data) {
            unknown1 = popInteger();
            numslots = popChar();
            unknown2 = popInteger();
            _mass = popInteger();

            for (int i = 0; i < mass; i++) {
                ContItem item = new ContItem(this);
                items.Add(item);
            }

            contEnd = new ContEnd();
            contEnd.containerId = AoObjectId.spawn(this);
            contEnd.tempContainerId = popInteger();
            contEnd.nullval = popInteger();
        }

        public uint mass {
            get { return (_mass - 1009) / 1009; }
        }
        

        uint	unknown1;	// 0x01000000 object type?
        char	numslots;	// 0x15 for a backpack

        // This seems to be 0x00000003 for owned backpacks and 0x00000002 for loot containers
        // introduced after 16.1 or so..
        public uint unknown2 { get; set; }

        uint _mass;	      // mass? seems related to number of items in backpack. 1009 + 1009*numitems

        public List<ContItem> items = new List<ContItem>();
        public ContEnd contEnd;
    }



}
