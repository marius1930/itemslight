﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.Parsers {
    public class AOGiveToNPC : AOMessageBase {
        public AOGiveToNPC(byte[] data) : base(data) {
            unknown1 = popChar();
            operationId = popShort();
            npcID.low = popInteger();
            npcID.high = popInteger();
            direction = popInteger();
            unknown3 = popInteger();
            unknown4 = popInteger();
            invItemGiven = new InvItemId(this);
        }
        
        
		protected char unknown1;//00
		protected ushort operationId;//02??
        public AoObjectId npcID;//??
		public uint direction;//00 00 00 00 if giving, 00 00 00 01 if removing an item
		protected uint unknown3;//00 00 00 00
		protected uint unknown4;//00 00 00 00
        public InvItemId invItemGiven;
    }
}
