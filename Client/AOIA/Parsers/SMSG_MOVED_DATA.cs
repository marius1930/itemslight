﻿using ItemsLight.AOIA.Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA {
    public class SMSG_MOVED_DATA : AOMessageBase {
        public enum TYPE {
            START_FRONT = 1, STOP_FRONT = 2,
            START_BACK = 3, STOP_BACK = 4,
            START_SRIGHT = 5, STOP_SRIGHT = 6,
            START_SLEFT = 7, STOP_SLEFT = 8,
            START_ROT_RIGHT = 10, STOP_ROT_RIGHT = 11,
            START_ROT_LEFT = 13, STOP_ROT_LEFT = 14,
            JUMP = 15, ROTATE_RELATED = 22
        };

        public SMSG_MOVED_DATA(byte[] data) : base(data) {
		    unknown0 = popChar();
		    state = (TYPE)popChar();
		
		    // 4*4 bytes unknown ,first row
		    uint[] tmp = new uint[]{0,0};
		    tmp[0] = popInteger(); // 0
            unknown_d0 = popFloat(); // float/double? increments on look right, decrements on look left


		    tmp[0] = popInteger(); // 0
            unknown_d1 = popFloat(); // float/double? increments on look left, decrements on look right


		    // Grab coordinates
            x = popFloat();
            z = popFloat();
            y = popFloat();
	    }


        char unknown0;
        public TYPE state;
        public float x, z, y;

        // Angles?
        float unknown_d0;
        float unknown_d1;
    };
}
