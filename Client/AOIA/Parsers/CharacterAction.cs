﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.Parsers {
    public class CharacterAction : AOMessageBase {
        public CharacterAction(byte[] data) : base(data) {
            unknown1 = popChar();
            operationId = popInteger();
            unknown3 = popInteger();
            itemToDelete = new InvItemId(this);
            itemId = AoObjectId.spawn(this);
            zeroes2 = popShort();
        }
        
		//00 00 00 00 70 00 00 00 00 00 00 00 68 00 00 00 4e 00 01 3f 5d 00 01 3f 5c 00 00
		char	unknown1; //01
		public uint	operationId; //00 00 00 70 
		uint	unknown3; //00 00 00 00
        public InvItemId itemToDelete;
        public AoObjectId itemId;
		ushort	zeroes2; //00 00

    }
}
