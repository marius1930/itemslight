﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.Parsers {
    public class OpenBackpack : AOMessageBase {
        public OpenBackpack(byte[] data) : base(data) {
            unknown1 = popChar();
            unknown2 = popInteger();
            ownerId = AoObjectId.spawn(this);
            zone = popInteger();
            unCons_f424f = popInteger();
            unZeroes1 = popInteger();
            operationId = popChar();
            invSlot = popChar();
            unknMass = popInteger();
            
            unZeroes2 = popInteger();
            flags = popInteger(); ;

            unknownKey17 = popInteger();
            itemKey1 = popInteger();

            unknownKey2bd = popInteger();
            ql = popInteger();

            unknownKey2be = popInteger();
            itemKeyLow = popInteger();

            unknownKey2bf = popInteger();
            itemKeyHigh = popInteger();

            unknownKey19c = popInteger();
            unknown01 = popInteger();
        }
        
        
		//*00 00 00 00 0b 00 00 c3 50 44 bb 17 ae 
		char	unknown1; //0x00
		uint	unknown2; //0x00 00 00 0b  always (rk1 and 2)
		public AoObjectId		ownerId;
		uint	zone; //id of the zone you're in
		uint	unCons_f424f;
		uint	unZeroes1;
		public char	operationId; //65=bought bp, 01=opened bp, 0e=?, what is got bp in trade?
		public char	invSlot; //or 6f "find a spot" if you buy one
		uint	unknMass;//1b 97 gives short msg (1009*7) , 1f 88 (1009*8) gives long. probably number of key/val pairs following.
		
		uint	unZeroes2;
		public uint	flags; //seems to be a bitwise bp flag (probably unique, nodrop etc)

		uint	unknownKey17;//always 0x17!
		uint	itemKey1;

		uint	unknownKey2bd;//always 0x2bd!
        public uint ql;

		uint	unknownKey2be;//always 0x2be!
        public uint itemKeyLow;//I guess

		uint	unknownKey2bf;//always 0x2bf!
		public uint	itemKeyHigh;//I guess

		uint	unknownKey19c;//always 0x19c!
		uint	unknown01;//always 0x01!
    }
}
