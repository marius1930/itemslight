﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.Parsers {
    public class AOFullCharacterMessage : AOMessageBase {
        public AOFullCharacterMessage(byte[] data) : base(data) {
            popChar();  // padding?
            popInteger(); // version field (0x19)

            uint count = pop3F1Count();
            for (uint i = 0; i < count; ++i) {
                m_inventory.Add(new AOContainerItem(this));
            }

            // Read uploaded nano programs
            count = pop3F1Count();
            skip(4 * count); // instead of storing nanos
            /*for (uint i = 0; i < count; ++i) {
                m_nanos.Add(popInteger());
            }*/

            count = pop3F1Count();
            // Perk and stats information follows

            // unknown
            for (uint i = 0; i < 3; ++i) {
                uint key = popInteger();
                uint val = popInteger();
                for (uint j = 0; j < val; ++j) {
                    //00 00 00 01 00 00 d6 f3 00 00 01 cd 00 00 00 5a
                    //00 00 00 50 00 00 00 01 00 00 d6 f3 00 00 01 91
                    //00 00 00 f0 00 00 00 ed
                    skip(sizeof(uint) * 5);
                }
            }


            // Stats map (32 bit id, 32 bit value)
            count = pop3F1Count();
            for (uint i = 0; i < count; ++i) {

                uint key = popInteger();
                uint val = popInteger();
                stats[key] = val;
            }

            // Stats map (32 bit id, 32 bit value)
            count = pop3F1Count();
            for (uint i = 0; i < count; ++i) {

                uint key = popInteger();
                uint val = popInteger();
                stats[key] = val;
            }

            // Stats map (8 bit id, 8 bit value)
            count = pop3F1Count();
            for (uint i = 0; i < count; ++i) {

                char key = popChar();
                char val = popChar();
                stats[key] = val;
            }

            // Stats map (8 bit id, 16 bit value)
            count = pop3F1Count();
            for (uint i = 0; i < count; ++i) {

                char key = popChar();
                uint val = (uint)popShort();
                stats[key] = val;
            }
        }


        public List<AOContainerItem> m_inventory = new List<AOContainerItem>();
        public Dictionary<uint, uint> stats = new Dictionary<uint, uint>();
        //List<uint> m_nanos;
        //Dictionary<uint, uint> m_stats;        
    }


    public struct AOContainerItem {
        public AOContainerItem(Parser p) {
            index = p.popInteger();
            flags = p.popShort();
            stack = p.popShort();
            containerid = AoObjectId.spawn(p);
            itemid = AoObjectId.spawn(p);
            ql = p.popInteger();
            p.skip(4);
        }

        public uint index;
        public ushort stack;
        public ushort flags;
        public AoObjectId containerid;
        public AoObjectId itemid;
        public uint ql;
    }



}
