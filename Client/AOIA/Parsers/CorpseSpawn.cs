﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace ItemsLight.AOIA.Parsers {
    public class CorpseSpawn : AOMessageBase {
        private static ILog logger = LogManager.GetLogger(typeof(CorpseSpawn));

        public CorpseSpawn(byte[] data) : base(data) {
            if (data.Length >= 194) {
                var p = pos();
                var offset = 190 - pos();
                skip(163);
                MonsterId = popInteger();
                logger.DebugFormat("Skipped {0}(163) bytes, started at {1}", offset, p);
                
            }
        }

        public long MonsterId { get; set; }

    }
}
