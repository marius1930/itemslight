﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.Parsers {
    public class AOPartnerTradeItem : AOMessageBase {
        public AOPartnerTradeItem(byte[] data) : base(data) {
            unknown1 = popChar();
            itemid = AoObjectId.spawn(this);
            ql = popInteger();
            flags = popShort();
            stack = popShort();
            operationId = popInteger();
            partnerInvItem = new InvItemId(this);
            myInvItemNotUsed = new InvItemId(this);
        }
        		//00 00 01 d1 35 00 01 d1 35 00 00 00 01 00 00 00 01|00 00 00 55|00 00 00 68 00 00 00 4f |00 00 00 00 00 00 00 00 
		char	unknown1;//01
        public AoObjectId itemid;
        public uint ql;
        public ushort flags;
        public ushort stack;
        public uint operationId;//55=Add, 56=remove
		public InvItemId		partnerInvItem;
        public InvItemId myInvItemNotUsed; 
    }
}
