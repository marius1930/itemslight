using System;
using System.Collections.Generic;
using System.Linq;

namespace ItemsLight.AOIA.Parsers {
    public struct NPCTradeRejectedItem {
        public AoObjectId itemid;
        public uint ql;
        public uint unknown1;//49 96 02 d2

        public static NPCTradeRejectedItem spawn(Parser p) {
            NPCTradeRejectedItem obj = new NPCTradeRejectedItem();
            obj.itemid = AoObjectId.spawn(p);
            obj.ql = p.popInteger();
            obj.unknown1 = p.popInteger();
            return obj;
        }
    };
}
