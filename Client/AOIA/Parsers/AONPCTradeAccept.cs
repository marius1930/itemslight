﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.Parsers {
    public class AONPCTradeAccept : AOMessageBase {
        public AONPCTradeAccept(byte[] data) : base(data) {
            unknown1 = popChar();
            operationId = popShort();
            npcID = AoObjectId.spawn(this);
            itemCount = popInteger();

            items = new List<NPCTradeRejectedItem>();
            for (uint i = 0; i < itemCount; i++) {
                items.Add(NPCTradeRejectedItem.spawn(this));
            }
        }
        
		char unknown1; //00
        public ushort operationId; //02??
		public AoObjectId npcID; //??
        public uint itemCount;
        public List<NPCTradeRejectedItem> items;

    }
}