﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.Parsers {
    public class BoughtItemFromShop : AOMessageBase {
        public BoughtItemFromShop(byte[] data) : base(data) {
            unknown1 = popChar();
            itemid = AoObjectId.spawn(this);
            ql = popInteger();
            flags = popShort();
            stack = popShort();
        }
        
        
		//df df 00 0a 00 01 00 2d 00 00 0b c4 67 a6 de 6a 05 2e 2f 0c 00 00 c3 50 67 a6 de 6a 00 00 00 54 69 00 00 54 69 00 00 00 01 00 00 01 90 
		char	unknown1;//00
		public AoObjectId		itemid;
        public uint ql;
        public ushort flags;
        public ushort stack;
    }
}
