﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using log4net;
using System.Diagnostics;
using System.ComponentModel;
using System.Globalization;

namespace ItemsLight {
    public class AppdataPathFinder {

        public static List<String> ListAnarchyDirectories() {
            String appdata = Environment.GetEnvironmentVariable("LocalAppData");
            String fullpath = Path.Combine(new string[] { appdata, "funcom" });
            if (Directory.Exists(fullpath))
                return ListAnarchyDirectories(fullpath);

            // Try to locate it via the GUID
            const string FOLDERID_LocalAppData = "F1B32785-6FBA-4FCF-9D55-7B8E7F157091";
            Guid FOLDERID_LocalAppDataGuid = new Guid(FOLDERID_LocalAppData);
            string location = ItemsLight.Cloud.CloudWatcher.GetKnownFolderPath(FOLDERID_LocalAppDataGuid);
            if (Directory.Exists(location)) {
                fullpath = Path.Combine(new string[] { location, "funcom" });
                if (Directory.Exists(fullpath))
                    return ListAnarchyDirectories(fullpath);
            }

            return new List<String>();
        }

        /// <summary>
        /// List all the appdata folders for Anarchy Online
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static List<String> ListAnarchyDirectories(String path) {
            List<String> anarchyDirectory = new List<String>();
            if (Directory.Exists(path)) {
                string[] directories = Directory.GetDirectories(path);


                if (directories.Any(s => Path.GetFileName(s).Equals("prefs", StringComparison.InvariantCultureIgnoreCase))) {
                    anarchyDirectory.Add(path);
                    return anarchyDirectory;
                }

                foreach (string d in Directory.GetDirectories(path)) {
                    anarchyDirectory.AddRange(ListAnarchyDirectories(d));
                }
            }
            return anarchyDirectory;
        }


        public static String AnyAnarchyDirectory() {
            return (string)Properties.Settings.Default["pathToAnarchy"];
        }



        /// <summary>
        /// Check whetever we have a valid path to anarchy online [does not check if the directory contains AO, only if it exists]
        /// </summary>
        /// <returns></returns>
        public static bool HasValidAnarchyPath() {
            if (string.IsNullOrEmpty((string)Properties.Settings.Default["pathToAnarchy"]))
                return false;

            if (!Directory.Exists((string)Properties.Settings.Default["pathToAnarchy"]))
                return false;

            String path = Path.Combine(new string[] { (string)Properties.Settings.Default["pathToAnarchy"], "cd_image", "data", "db", "ResourceDatabase.idx" });
            return File.Exists(path);
        }


    }

}