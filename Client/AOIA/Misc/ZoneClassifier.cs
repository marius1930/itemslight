﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA {

    public class ZoneClassifier {
        class Playfield {
            public long zoneid;
            public string name;
            public float x = 0;
            public float z = 0;
            public float area = float.MaxValue / 3;
            public bool DistanceSq(float x2, float z2) {
                float d = (x - x2) * (x - x2) + (z - z2) * (z - z2);
                return d <= area * area;
            }
        }

        /// <summary>
        /// Check whatever a zone id is fully qualified (not the resource ID of an instanced area)
        /// </summary>
        /// <param name="zoneid"></param>
        /// <returns></returns>
        public static bool IsFullyQualifiedZone(long zoneid) {
            var playfields = Classifier;
            return playfields.Exists(u => u.zoneid == zoneid);
        }

        private static List<Playfield> __subzoneplayfields;
        public static string ClassifySubzone(long subzoneid) {
            if (__subzoneplayfields == null) {
                __subzoneplayfields = new List<Playfield>();
                // Not fully qualified
                __subzoneplayfields.Add(new Playfield { zoneid = 1180, name = "Basic Market" });
                __subzoneplayfields.Add(new Playfield { zoneid = 1181, name = "Advanced Market" });
                __subzoneplayfields.Add(new Playfield { zoneid = 1182, name = "Superior Market" });


                __subzoneplayfields.Add(new Playfield { zoneid = 1183, name = "Basic Store" });
                __subzoneplayfields.Add(new Playfield { zoneid = 1184, name = "Advanced Store" });
                __subzoneplayfields.Add(new Playfield { zoneid = 1185, name = "Superior Store" });

                __subzoneplayfields.Add(new Playfield { zoneid = 1189, name = "Special/Advanced Market" });
                __subzoneplayfields.Add(new Playfield { zoneid = 1190, name = "Finest Edition" });
                __subzoneplayfields.Add(new Playfield { zoneid = 1191, name = "Finest Edition?" }); // Guessing
                __subzoneplayfields.Add(new Playfield { zoneid = 1192, name = "Finest Edition" });
                __subzoneplayfields.Add(new Playfield { zoneid = 1187, name = "Fair Trade" });
                __subzoneplayfields.Add(new Playfield { zoneid = 2064, name = "Implant Store" });
                
                __subzoneplayfields.Add(new Playfield { zoneid = 1186, name = "General Store" });
                __subzoneplayfields.Add(new Playfield { zoneid = 4704, name = "Tower Shop" });
                __subzoneplayfields.Add(new Playfield { zoneid = 954, name = "Old School Noob spawning area" });
                __subzoneplayfields.Add(new Playfield { zoneid = 1193, name = "Trader Shop" });
                __subzoneplayfields.Add(new Playfield { zoneid = 4805, name = "CoH Instance" });
                __subzoneplayfields.Add(new Playfield { zoneid = 1941, name = "Foremans" });
                __subzoneplayfields.Add(new Playfield { zoneid = 362, name = "Static, map: tinker tower" });
                __subzoneplayfields.Add(new Playfield { zoneid = 6010, name = "Serenity Islands" });
                __subzoneplayfields.Add(new Playfield { zoneid = 8009, name = "Crashed Alien Ship" }); // Login screen (?)
                __subzoneplayfields.Add(new Playfield { zoneid = 7101, name = "Freighter headed to ICC" });
                
            }

            var possibles = __subzoneplayfields.Where(u => u.zoneid == subzoneid);

            foreach (var t in possibles)
                return t.name;

            if (subzoneid > 19000)
                return "Indoor PF / Store";

            if (subzoneid == 0)
                return string.Empty;
            return "Unknown area " + subzoneid;
        }

        public static string Classify(long zoneid, float x, float z) {
            var playfields = Classifier;
            var possibles = playfields.Where(u => u.zoneid == zoneid && u.DistanceSq(x, z));
            if (possibles.Any()) {
                possibles = possibles.OrderBy(u => u.area);
            }

            foreach (var t in possibles)
                return t.name;

            if (zoneid > 19000)
                return "Indoor PF / Store";

            if (zoneid == 0)
                return string.Empty;

            return "Unknown area " + zoneid;
        }

        private static List<Playfield> __playfields;
        private static List<Playfield> Classifier {
            get {
                if (__playfields == null) {
                    __playfields = new List<Playfield>();

                    __playfields.Add(new Playfield { zoneid = 6010, name = "Serenity Islands" });
                    __playfields.Add(new Playfield { zoneid = 800, name = "Borealis Grid", x = 636, z = 726, area = 22 });
                    __playfields.Add(new Playfield { zoneid = 800, name = "Borealis GMI", x = 695, z = 652, area = 25 });
                    __playfields.Add(new Playfield { zoneid = 800, name = "Borealis Wompah", x = 680, z = 540, area = 20 });
                    __playfields.Add(new Playfield { zoneid = 800, name = "Borealis FT", x = 662, z = 611, area = 15 });
                    __playfields.Add(new Playfield { zoneid = 7013, name = "Borealis Antiques (BS)" });
                    __playfields.Add(new Playfield { zoneid = 647, name = "TOTW (Greater Tir County)", x = 1814, z = 2713, area = 50 });
                    __playfields.Add(new Playfield { zoneid = 647, name = "General Store near TOTW", x = 1908, z = 2257, area = 50 });

                    __playfields.Add(new Playfield { zoneid = 4320, name = "Alappaa", x = 1187, z = 496, area = 50 });
                    __playfields.Add(new Playfield { zoneid = 4320, name = "Penumbra Purity (Clan temple)", x = 870, z = 2080, area = 250 });
                    __playfields.Add(new Playfield { zoneid = 4321, name = "Two-Time Maloney (Pen)", x = 2309, z = 1984, area = 50 });
                    __playfields.Add(new Playfield { zoneid = 4321, name = "Ergo Penumbra", x = 2170, z = 2443, area = 150 });
                    __playfields.Add(new Playfield { zoneid = 4872, name = "Adonis Dryads (ARK1)", x = 1687, z = 2834, area = 350 });
                    __playfields.Add(new Playfield { zoneid = 4872, name = "Adonis Dryads (Island/Ark2)", x = 1118, z = 2641, area = 350 });
                    __playfields.Add(new Playfield { zoneid = 665, name = "Crypt of Home (BS)", x = 357, z = 2215, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 665, name = "BS Omni Wompah", x = 2339, z = 2242, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 795, name = "TLR Omni Wompah", x = 2078, z = 731, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 635, name = "2HO Omni Wompah", x = 783, z = 1614, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 610, name = "SAV Omni Wompah", x = 1149, z = 2347, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 630, name = "20K Omni Wompah", x = 1250, z = 2308, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 615, name = "SFH Omni Wompah", x = 1588, z = 2157, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 615, name = "Smuggler's Den Entrance", x = 1753, z = 871, area = 50 });
                    __playfields.Add(new Playfield { zoneid = 620, name = "Mercs (EFP)", x = 739, z = 1376, area = 300 });


                    __playfields.Add(new Playfield { zoneid = 605, name = "Hollow Island - Entrance", x = 2148, z = 693, area = 25 });
                    __playfields.Add(new Playfield { zoneid = 605, name = "Hollow Island - Beach", x = 2242, z = 671, area = 40 });

                    __playfields.Add(new Playfield { zoneid = 125, name = "Smugglers Den (SFH)" });
                    __playfields.Add(new Playfield { zoneid = 125, name = "Smugglers Den - Safe Spot", x = 354, z = 265, area = 40 });
                    __playfields.Add(new Playfield { zoneid = 125, name = "Smugglers Den - At Queen", x = 59, z = 298, area = 40 });
                    
                    __playfields.Add(new Playfield { zoneid = 566, name = "Warden Ystanes (Newland City)", x = 401, z = 362, area = 20 });

                    __playfields.Add(new Playfield { zoneid = 625, name = "Acidic Prisoner (Milky Way)", x = 1460, z = 1715, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 625, name = "Burning Prisoner (Milky Way)", x = 375, z = 725, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 625, name = "Dangerous Prisoner (Milky Way)", x = 4565, z = 370, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 625, name = "Deadly Prisoner (Milky Way)", x = 765, z = 1025, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 625, name = "Diseased Prisoner (Milky Way)", x = 3625, z = 580, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 625, name = "Explosive Prisoner (Milky Way)", x = 1060, z = 925, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 625, name = "Murderous Prisoner (Milky Way)", x = 1925, z = 1875, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 625, name = "Poisonous Prisoner (Milky Way)", x = 2330, z = 2210, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 625, name = "Silenced Prisoner (Milky Way)", x = 2915, z = 300, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 625, name = "Spiritual Prisoner (Milky Way)", x = 880, z = 630, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 625, name = "Temporal Prisoner (Milky Way)", x = 3150, z = 2125, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 625, name = "Umbral Prisoner (Milky Way)", x = 4950, z = 810, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 620, name = "EFP - Medusa Crater", x = 490, z = 3800, area = 300 });
                    __playfields.Add(new Playfield { zoneid = 4872, name = "Dark Ruins - Adonis", x = 1690, z = 542, area = 50 });
                    __playfields.Add(new Playfield { zoneid = 4605, name = "Ergo - Inferno", x = 2800, z = 3375, area = 80 });
                    __playfields.Add(new Playfield { zoneid = 540, name = "OA Hill", x = 430, z = 400, area = 80 });
                    __playfields.Add(new Playfield { zoneid = 540, name = "OA Grid", x = 510, z = 570, area = 50 });

                    __playfields.Add(new Playfield { zoneid = 540, name = "OA Grid", x = 511, z = 569, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 730, name = "Rome Red Grid", x = 251, z = 318, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 730, name = "Rome Red Wompah", x = 359, z = 318, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 735, name = "Rome Blue Agency", x = 644, z = 315, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 7011, name = "Omni Agency (Rome Blue)" });
                    __playfields.Add(new Playfield { zoneid = 565, name = "Meetmerdere Grid", x = 1590, z = 2830, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 560, name = "Mort Borgs", x = 1378, z = 608, area = 120 });
                    __playfields.Add(new Playfield { zoneid = 560, name = "Hope (in Mort)", x = 2846, z = 1897, area = 120 });

                    __playfields.Add(new Playfield { zoneid = 655, name = "ICC - Subway", x = 3303, z = 838, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 655, name = "ICC - Enterprice Shop (Veteran)", x = 3206, z = 820, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 655, name = "ICC - Zyvania Bagh (SoM teleport)", x = 3158, z = 900, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 655, name = "ICC - Peacekeeper Constad (DBQ)", x = 3277, z = 920, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 655, name = "ICC - TOTW Portal", x = 3213, z = 789, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 655, name = "ICC - Maddy Cardile / Karrec quest ", x = 3337, z = 930, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 655, name = "ICC - Annoying dude / Karrec quest", x = 3185, z = 962, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 655, name = "ICC", x = 3245, z = 864, area = 300 });
                    __playfields.Add(new Playfield { zoneid = 710, name = "Trade Grid", x = 408, z = 576, area = 30 });
                    __playfields.Add(new Playfield { zoneid = 710, name = "Finest Edition, West Omni Trade", x = 232, z = 492, area = 10 });
                    __playfields.Add(new Playfield { zoneid = 710, name = "Advanced Store, West Omni Trade", x = 231, z = 429, area = 10 });
                    __playfields.Add(new Playfield { zoneid = 710, name = "Advanced Store, East Omni-Trade", x = 577, z = 432, area = 10 });
                    __playfields.Add(new Playfield { zoneid = 710, name = "Superior Store, East Omni-Trade", x = 575, z = 455, area = 10 });
                    __playfields.Add(new Playfield { zoneid = 710, name = "Advanced Store, NE Corner, Omni-Trade", x = 574, z = 560, area = 10 });
                    __playfields.Add(new Playfield { zoneid = 560, name = "Sentinels (Mort)", x = 1937, z = 1255, area = 60 });
                    __playfields.Add(new Playfield { zoneid = 4311, name = "Nasc 2M", x = 185, z = 425, area = 150 });
                    __playfields.Add(new Playfield { zoneid = 4311, name = "Nasc LOTV", x = 438, z = 1865, area = 150 });
                    __playfields.Add(new Playfield { zoneid = 4311, name = "Nasc Crippler Cave", x = 580, z = 1665, area = 150 });
                    __playfields.Add(new Playfield { zoneid = 4544, name = "Ely Tinker Tower (Entrance)", x = 284, z = 1000, area = 50 });
                    
                    __playfields.Add(new Playfield { zoneid = 4544, name = "Static instance, Elysium, Domeview", x = 782, z = 846, area = 50 });
                    


                    __playfields.Add(new Playfield { zoneid = 6553, name = "God forsaken NPE Noob Area" });
                    __playfields.Add(new Playfield { zoneid = 152, name = "The Grid" });
                    __playfields.Add(new Playfield { zoneid = 4107, name = "Fixer Grid" });
                    for (int i = 1; i <= 15; i++)
                        __playfields.Add(new Playfield { zoneid = 3059 + i, name = string.Format("Backyard {0} - Newland City", i) });

                    for (int i = 1; i <= 10; i++)
                        __playfields.Add(new Playfield { zoneid = 3079 + i, name = string.Format("Backyard {0} - Borealis", i) });

                    for (int i = 1; i <= 18; i++)
                        __playfields.Add(new Playfield { zoneid = 3119 + i, name = string.Format("Backyard {0} - OA", i) });

                    for (int i = 1; i <= 20; i++)
                        __playfields.Add(new Playfield { zoneid = 3019 + i, name = string.Format("Highrise {0} - Trade", i) });

                    __playfields.Add(new Playfield { zoneid = 1407, name = "By2? - neutralapartment" });
                    

                    // Regular zones
                    __playfields.Add(new Playfield { zoneid = 7012, name = "Clan Agency - OA" });
                    __playfields.Add(new Playfield { zoneid = 760, name = "4 Holes" });
                    __playfields.Add(new Playfield { zoneid = 4873, name = "Adonis Abyss" });
                    __playfields.Add(new Playfield { zoneid = 4872, name = "Adonis City" });
                    __playfields.Add(new Playfield { zoneid = 4877, name = "Adonis Hallway" });
                    __playfields.Add(new Playfield { zoneid = 585, name = "Aegean" });
                    __playfields.Add(new Playfield { zoneid = 4336, name = "Alappaa" });
                    __playfields.Add(new Playfield { zoneid = 4337, name = "Albtraum" });
                    __playfields.Add(new Playfield { zoneid = 4374, name = "Alien Playfield - Sector 10" });
                    __playfields.Add(new Playfield { zoneid = 4365, name = "Alien Playfield - Sector 13" });
                    __playfields.Add(new Playfield { zoneid = 4366, name = "Alien Playfield - Sector 28" });
                    __playfields.Add(new Playfield { zoneid = 4367, name = "Alien Playfield - Sector 35" });
                    __playfields.Add(new Playfield { zoneid = 4370, name = "Alien Playfield - Sector 42" });
                    __playfields.Add(new Playfield { zoneid = 655, name = "Andromeda (ICC)" });
                    __playfields.Add(new Playfield { zoneid = 550, name = "Athen Shire" });
                    __playfields.Add(new Playfield { zoneid = 505, name = "Avalon" });
                    __playfields.Add(new Playfield { zoneid = 660, name = "Bay of Rome" });
                    __playfields.Add(new Playfield { zoneid = 605, name = "Belial Forest" });
                    __playfields.Add(new Playfield { zoneid = 800, name = "Borealis" });
                    __playfields.Add(new Playfield { zoneid = 510, name = "Broken Crest" });
                    __playfields.Add(new Playfield { zoneid = 665, name = "Broken Shores" });
                    __playfields.Add(new Playfield { zoneid = 4005, name = "Inf South: Burning Marshes" });
                    __playfields.Add(new Playfield { zoneid = 4689, name = "Cama's Sanctuary" });
                    __playfields.Add(new Playfield { zoneid = 120, name = "Camelot Castle" });
                    __playfields.Add(new Playfield { zoneid = 590, name = "Central Artery Valley" });
                    __playfields.Add(new Playfield { zoneid = 670, name = "Clondyke" });
                    __playfields.Add(new Playfield { zoneid = 556, name = "Coast of Peace" });
                    __playfields.Add(new Playfield { zoneid = 656, name = "Coast of Tranquility" });
                    __playfields.Add(new Playfield { zoneid = 4687, name = "Dalja's Sanctuary" });
                    __playfields.Add(new Playfield { zoneid = 595, name = "Deep Artery Valley" });
                    __playfields.Add(new Playfield { zoneid = 555, name = "Dry Wastes" });
                    __playfields.Add(new Playfield { zoneid = 4543, name = "East of Elysium" });
                    __playfields.Add(new Playfield { zoneid = 620, name = "Eastern Fouls Plain" });
                    __playfields.Add(new Playfield { zoneid = 4542, name = "Elysium" });
                    __playfields.Add(new Playfield { zoneid = 4679, name = "Enel's Sanctuary" });
                    __playfields.Add(new Playfield { zoneid = 685, name = "Galway Country" });
                    __playfields.Add(new Playfield { zoneid = 687, name = "Galway Shire" });
                    __playfields.Add(new Playfield { zoneid = 765, name = "Gangly Mountains" });
                    __playfields.Add(new Playfield { zoneid = 4676, name = "Garden of Aban" });
                    __playfields.Add(new Playfield { zoneid = 4688, name = "Garden of Cama" });
                    __playfields.Add(new Playfield { zoneid = 4686, name = "Garden of Dalja" });
                    __playfields.Add(new Playfield { zoneid = 4678, name = "Garden of Enel" });
                    __playfields.Add(new Playfield { zoneid = 4684, name = "Garden of Gilthar" });
                    __playfields.Add(new Playfield { zoneid = 4692, name = "Garden of Lord Galahad" });
                    __playfields.Add(new Playfield { zoneid = 4694, name = "Garden of Lord Mordeth" });
                    __playfields.Add(new Playfield { zoneid = 4682, name = "Garden of Ocra" });
                    __playfields.Add(new Playfield { zoneid = 4696, name = "Garden of Redeemed Pandemonium" });
                    __playfields.Add(new Playfield { zoneid = 4683, name = "Garden of Roch" });
                    __playfields.Add(new Playfield { zoneid = 4680, name = "Garden of Shere" });
                    __playfields.Add(new Playfield { zoneid = 4677, name = "Garden of Thrak" });
                    __playfields.Add(new Playfield { zoneid = 4697, name = "Garden of Unredeemed Pandemonium" });
                    __playfields.Add(new Playfield { zoneid = 4690, name = "Garden of Vanja" });
                    __playfields.Add(new Playfield { zoneid = 4685, name = "Gilthar's Sanctuary" });
                    __playfields.Add(new Playfield { zoneid = 770, name = "Great Cathedral Park" });
                    __playfields.Add(new Playfield { zoneid = 717, name = "Greater Omni Forest" });
                    __playfields.Add(new Playfield { zoneid = 647, name = "Greater Tir County" });
                    __playfields.Add(new Playfield { zoneid = 152, name = "Grid" });
                    __playfields.Add(new Playfield { zoneid = 791, name = "Holes in the Wall" });
                    __playfields.Add(new Playfield { zoneid = 4582, name = "ICC Shuttleport" });
                    __playfields.Add(new Playfield { zoneid = 4605, name = "Inferno North" });
                    __playfields.Add(new Playfield { zoneid = 690, name = "Jobe" });
                    __playfields.Add(new Playfield { zoneid = 4560, name = "Jobe Gateway" });
                    __playfields.Add(new Playfield { zoneid = 4531, name = "Jobe Harbour" });
                    __playfields.Add(new Playfield { zoneid = 4532, name = "Jobe Market" });
                    __playfields.Add(new Playfield { zoneid = 4530, name = "Jobe Platform" });
                    __playfields.Add(new Playfield { zoneid = 4533, name = "Jobe Plaza" });
                    __playfields.Add(new Playfield { zoneid = 4001, name = "Jobe Research" });
                    __playfields.Add(new Playfield { zoneid = 4313, name = "Jobe Training Biosphere" });
                    __playfields.Add(new Playfield { zoneid = 775, name = "Kapar" });
                    __playfields.Add(new Playfield { zoneid = 4693, name = "Lord Galahad's Sanctuary" });
                    __playfields.Add(new Playfield { zoneid = 4695, name = "Lord Mordeth's Sanctuary" });
                    __playfields.Add(new Playfield { zoneid = 4881, name = "Lower Scheol" });
                    __playfields.Add(new Playfield { zoneid = 695, name = "Lush Fields" });
                    __playfields.Add(new Playfield { zoneid = 625, name = "Milky Way" });
                    __playfields.Add(new Playfield { zoneid = 560, name = "Mort" });
                    __playfields.Add(new Playfield { zoneid = 696, name = "Mutant Domain" });
                    __playfields.Add(new Playfield { zoneid = 4310, name = "Nascence Frontier" });
                    __playfields.Add(new Playfield { zoneid = 4312, name = "Nascence Swamp" });
                    __playfields.Add(new Playfield { zoneid = 4311, name = "Nascence Wilds" });
                    __playfields.Add(new Playfield { zoneid = 780, name = "Nebula Mountains" });
                    __playfields.Add(new Playfield { zoneid = 567, name = "Newland" });
                    __playfields.Add(new Playfield { zoneid = 566, name = "Newland City" });
                    __playfields.Add(new Playfield { zoneid = 565, name = "Newland Desert" });
                    __playfields.Add(new Playfield { zoneid = 4544, name = "North of Elysium" });
                    __playfields.Add(new Playfield { zoneid = 4698, name = "Ocra's Sanctuary" });
                    __playfields.Add(new Playfield { zoneid = 540, name = "Old Athen City" });
                    __playfields.Add(new Playfield { zoneid = 705, name = "Omni-1 Entertainment" });
                    __playfields.Add(new Playfield { zoneid = 716, name = "Omni-1 Forest" });
                    __playfields.Add(new Playfield { zoneid = 700, name = "Omni-1 Headquarters" });
                    __playfields.Add(new Playfield { zoneid = 710, name = "Omni-1 Trade" });
                    __playfields.Add(new Playfield { zoneid = 4329, name = "Pandemonium Antenora" });
                    __playfields.Add(new Playfield { zoneid = 4328, name = "Pandemonium Caina" });
                    __playfields.Add(new Playfield { zoneid = 4331, name = "Pandemonium Judecca" });
                    __playfields.Add(new Playfield { zoneid = 4330, name = "Pandemonium Ptolemea" });
                    __playfields.Add(new Playfield { zoneid = 4006, name = "Penumbra" });
                    __playfields.Add(new Playfield { zoneid = 4629, name = "Penumbra" });
                    __playfields.Add(new Playfield { zoneid = 4320, name = "Penumbra Forest" });
                    __playfields.Add(new Playfield { zoneid = 4322, name = "Penumbra Hollows" });
                    __playfields.Add(new Playfield { zoneid = 4321, name = "Penumbra Valley" });
                    __playfields.Add(new Playfield { zoneid = 570, name = "Perpetual Wastelands" });
                    __playfields.Add(new Playfield { zoneid = 720, name = "Plains of Divaad" });
                    __playfields.Add(new Playfield { zoneid = 575, name = "Plains of Salt" });
                    __playfields.Add(new Playfield { zoneid = 630, name = "Pleasant Meadows" });
                    __playfields.Add(new Playfield { zoneid = 725, name = "Reck Peninsula" });
                    __playfields.Add(new Playfield { zoneid = 785, name = "Road to Kapar" });
                    __playfields.Add(new Playfield { zoneid = 4699, name = "Roch's Sanctuary" });
                    __playfields.Add(new Playfield { zoneid = 735, name = "Rome Blue District" });
                    __playfields.Add(new Playfield { zoneid = 740, name = "Rome Green District" });
                    __playfields.Add(new Playfield { zoneid = 730, name = "Rome Red District" });
                    __playfields.Add(new Playfield { zoneid = 520, name = "Sandsend" });
                    __playfields.Add(new Playfield { zoneid = 745, name = "Sea of Jobe" });
                    __playfields.Add(new Playfield { zoneid = 4681, name = "Shere's Sanctuary" });
                    __playfields.Add(new Playfield { zoneid = 4540, name = "South of Elysium" });
                    __playfields.Add(new Playfield { zoneid = 610, name = "Southern Artery Valley" });
                    __playfields.Add(new Playfield { zoneid = 615, name = "Southern Fouls Hills" });
                    __playfields.Add(new Playfield { zoneid = 525, name = "Sparta" });
                    __playfields.Add(new Playfield { zoneid = 1933, name = "Steps of Madness Dungeon" });
                    __playfields.Add(new Playfield { zoneid = 1932, name = "Instance/Dungeon - Not yet identified" }); // SoM?
                    __playfields.Add(new Playfield { zoneid = 1931, name = "Inside Temple of Three Winds" });
                    __playfields.Add(new Playfield { zoneid = 635, name = "Stret East Bank" });
                    __playfields.Add(new Playfield { zoneid = 790, name = "Stret West Bank" });
                    __playfields.Add(new Playfield { zoneid = 535, name = "The Big One" });
                    __playfields.Add(new Playfield { zoneid = 515, name = "The Endless Plains" });
                    __playfields.Add(new Playfield { zoneid = 795, name = "The Longest Road" });
                    __playfields.Add(new Playfield { zoneid = 750, name = "The Reck" });
                    __playfields.Add(new Playfield { zoneid = 755, name = "The Spur" });
                    __playfields.Add(new Playfield { zoneid = 580, name = "Three Craters" });
                    __playfields.Add(new Playfield { zoneid = 640, name = "Tir City" });
                    __playfields.Add(new Playfield { zoneid = 646, name = "Tir County" });
                    __playfields.Add(new Playfield { zoneid = 4364, name = "Unicorn Outpost" });
                    __playfields.Add(new Playfield { zoneid = 4368, name = "Unicorn Outpost - Lower level" });
                    __playfields.Add(new Playfield { zoneid = 4880, name = "Upper Scheol" });
                    __playfields.Add(new Playfield { zoneid = 650, name = "Upper Stret East Bank" });
                    __playfields.Add(new Playfield { zoneid = 4691, name = "Vanja's Sanctuary" });
                    __playfields.Add(new Playfield { zoneid = 600, name = "Varmit Woods" });
                    __playfields.Add(new Playfield { zoneid = 551, name = "Wailing Wastes" });
                    __playfields.Add(new Playfield { zoneid = 586, name = "Wartorn Valley" });
                    __playfields.Add(new Playfield { zoneid = 545, name = "West Athen City" });
                    __playfields.Add(new Playfield { zoneid = 4541, name = "West of Elysium" });
                }
                return __playfields;
            }
        }
    }
}
