﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA {
	
    public struct CharacterPosition {
        public float x;
        public float z;
        public uint zoneid;
        public long timestamp;
        public long characterId;
        public long subzoneid;

        public bool isRK {
            get {
                return PositionTracker.zones_rk.Contains(zoneid);
            }
        }
        public bool isSL {
            get {
                return PositionTracker.zones_sl.Contains(zoneid);
            }
        }
    }

    public class PositionTracker {
        public static uint[] zones_sl = new uint[]{4336 , 4337 , 4374 , 4365 , 4366 , 4367 , 4370 , 4543 , 4676 , 4688 , 4686 , 4678 , 4684 , 4692 , 4694 , 4682 , 4696 , 4683 , 4680 , 4677 , 4697 , 4690 , 4329 , 4328 , 4331 , 4330 , 4006 , 4629 , 4320 , 4322 , 4321 , 4681 , 4540 , 4364 , 4368 , 4691 , 4541 , 4544 , 4698 , 4542 , 4679 , 4693 , 4695 , 4881 , 4005 , 4689 , 4687 , 4873 , 4872 , 4877 , 4685 , 4582 , 4605 , 690 , 4560 , 4531 , 4532 , 4530 , 4533 , 4001 , 4313 , 4310 , 4312 , 4311 , 4880 , 4699};
        public static uint[] zones_rk = new uint[]{760 , 585 , 655 , 550 , 505 , 660 , 605 , 800 , 510 , 665 , 120 , 590 , 670 , 556 , 656 , 595 , 555 , 620 , 685 , 687 , 765 , 770 , 717 , 647 , 152 , 791 , 775 , 695 , 625 , 560 , 696 , 780 , 567 , 566 , 565 , 540 , 705 , 716 , 700 , 710 , 570 , 720 , 575 , 630 , 725 , 785 , 735 , 740 , 730 , 520 , 745 , 610 , 615 , 525 , 1933 , 635 , 790 , 535 , 515 , 795 , 750 , 755 , 580 , 640 , 646 , 650 , 600 , 551 , 586 , 545};

        private static Dictionary<long, CharacterPosition> positions = new Dictionary<long, CharacterPosition>();

        private static long getTimestamp() {
            return (long)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }


        public static void Initialize(long characterId, float x, float z, uint zoneid) {

            if (!positions.ContainsKey(characterId))
                positions[characterId] = new CharacterPosition { characterId = characterId };

            
            CharacterPosition pos = positions[characterId];
            pos.x = x;
            pos.z = z;
            pos.timestamp = 0;
            pos.zoneid = zoneid;
            positions[characterId] = pos;
        }
        public static void setPosition(long characterId, float x, float z) {
            if (!positions.ContainsKey(characterId))
                positions[characterId] = new CharacterPosition { characterId = characterId };

            CharacterPosition pos = positions[characterId];
            if (pos.subzoneid == 0) {
                pos.x = x;
                pos.z = z;
                pos.timestamp = getTimestamp();
                positions[characterId] = pos;
            }
        }

        public static void setZone(long characterId, uint zoneid) {
            if (!positions.ContainsKey(characterId))
                positions[characterId] = new CharacterPosition { characterId = characterId };

            CharacterPosition pos = positions[characterId];
            if (ZoneClassifier.IsFullyQualifiedZone(zoneid)) {
                pos.zoneid = zoneid;
                pos.subzoneid = 0;
            }
            else {
                pos.subzoneid = zoneid;
            }
            pos.timestamp = getTimestamp();
            positions[characterId] = pos;
        }


        /// <summary>
        /// Get how many milliseconds since the last update
        /// MaxValue if no hits are found
        /// </summary>
        /// <returns></returns>
        public static long getMostRecentTimestampDelta() {
            if (positions.Count == 0)
                return long.MaxValue;

            long timestamp = positions.Values.Max(i => i.timestamp);
            return getTimestamp() - timestamp;
        }

        public static IEnumerable<CharacterPosition> RecentPositions {
            get {
                long ts = getTimestamp() - (5 * 60 * 1000);
                IEnumerable<CharacterPosition> result = positions.Values.Where(e => e.timestamp >= ts);
                return result;
            }
        }

        public static IEnumerable<CharacterPosition> AllPositions {
            get {
                return positions.Values;
            }
        }
    }
}
