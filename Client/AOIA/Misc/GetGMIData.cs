﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using ItemsLight.AOIA;
using log4net;
using System.Text.RegularExpressions;
using ItemsLight.Model;
using ItemsLight.AOIA.Misc;
using ItemsLight.Dao;
using EvilsoftCommons.Exceptions;

namespace ItemsLight {
    public class GetGMIData {
        static ILog logger = LogManager.GetLogger("GetGMIData");
        private static MarketItemDao gmiDao = new MarketItemDao();

        private const string URL = "http://aomarket.funcom.com/marketLIVE/inventory";
        private static bool DownloadGMIData(long characterId, string cookie, out string responseString) {
            responseString = string.Empty;
            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(URL);
            if (httpWReq == null) {
                ExceptionReporter.ReportIssue("WebRequest.Create returned null");
                return false;
            }

            httpWReq.Method = "GET";
            httpWReq.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.2; en-US) AppleWebKit/533.3 (KHTML, like Gecko) Chrome/12.0.702.0 Safari/533.3 Awesomium/1.6.5";
            httpWReq.Accept = "application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
            httpWReq.Headers.Add(string.Format("X-Anarchy-Cookie: {0}", cookie));
            httpWReq.Headers.Add(string.Format("X-Anarchy-CharacterID: {0}", characterId));
            
            httpWReq.Headers.Add("X-Anarchy-ServerID: 3502");
            httpWReq.Headers.Add("X-Anarchy-Appearance: 2 1 0 40681");
            httpWReq.Headers.Add("Accept-Language: en-US,en;q=0.8");
            httpWReq.Headers.Add("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3");
            httpWReq.Headers.Add(string.Format("Cookie: dim=5; sid=0; user={0}", characterId));
            
            try {
                using (HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse()) {
                    if (response.StatusCode == HttpStatusCode.Forbidden) {
                        logger.Debug("Market returned 403 Forbidden, invalidating data for " + characterId);
                        GMITracker.Invalidate(characterId);
                        return false;
                    }
                    if (response.StatusCode != HttpStatusCode.OK)
                        return false;

                    responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    //logger.Debug(responseString);

                    return true;
                }
            }
            catch (WebException ex) {
                HttpWebResponse r = (HttpWebResponse)ex.Response;

                if (r.StatusCode == HttpStatusCode.Forbidden) {
                    logger.Debug("Market returned 403 Forbidden, invalidating data for " + characterId);
                    GMITracker.Invalidate(characterId);
                    return false;
                }
                else if (r.StatusCode == HttpStatusCode.Forbidden) {
                    logger.Debug("Market returned 500 Internal Server Error for " + characterId);
                    logger.Debug("This is probably OK");
                    GMITracker.Invalidate(characterId);
                    ExceptionReporter.ReportException(ex, "Handled just fine");
                    return false;
                }
                else {
                    logger.Warn(string.Format("Failed to contact GMI: {0}{1}", ex.Message, ex.StackTrace));
                    ExceptionReporter.ReportException(ex);
                    return false;
                }
            }
            catch (NullReferenceException ex) {
                logger.Warn("Unknown error occurred (nullptr)");
                ExceptionReporter.ReportException(ex, "The GMI NullReferenceException");
                return false;
            }
        }



        private static bool DownloadItemName(long characterId, long itemid, string cookie, out string responseString) {
            responseString = string.Empty;

            var raw = string.Format("{{\"cluster_id\":\"{0}\",\"template_ql\":\"0\"}}", itemid);
            byte[] data = new ASCIIEncoding().GetBytes(raw);

            // Create a web request, may in some undefined cases return null, no idea why.
            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create("http://aomarket.funcom.com/marketLIVE/get_item_visuals");
            if (httpWReq == null)
                return false;

            httpWReq.Method = "POST";
            httpWReq.ContentType = "application/json";
            httpWReq.ContentLength = data.Length;

            httpWReq.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.2; en-US) AppleWebKit/533.3 (KHTML, like Gecko) Chrome/12.0.702.0 Safari/533.3 Awesomium/1.6.5";
            httpWReq.Accept = "application/json, text/javascript, */*; q=0.01";
            httpWReq.Headers.Add(string.Format("X-Anarchy-Cookie: {0}", cookie));
            httpWReq.Headers.Add(string.Format("X-Anarchy-CharacterID: {0}", characterId));
            
            httpWReq.Headers.Add("X-Requested-With: XMLHttpRequest");
            httpWReq.Headers.Add("X-Anarchy-ServerID: 3502");
            httpWReq.Headers.Add("X-Anarchy-Appearance: 2 1 0 40681");
            httpWReq.Headers.Add("Accept-Language: en-US,en;q=0.8");
            httpWReq.Headers.Add("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3");
            httpWReq.Headers.Add(string.Format("Cookie: dim=5; sid=0; user={0}", characterId));

            try {
                using (Stream stream = httpWReq.GetRequestStream()) {
                    stream.Write(data, 0, data.Length);
                }
                using (HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse()) {
                    if (response.StatusCode == HttpStatusCode.Forbidden) {
                        logger.Debug("Market returned 403 Forbidden, invalidating data for " + characterId);
                        GMITracker.Invalidate(characterId);
                        return false;
                    }
                    if (response.StatusCode != HttpStatusCode.OK)
                        return false;

                    responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    //logger.Debug(responseString);

                    return true;
                }
            }
            catch (WebException ex) {
                HttpWebResponse r = (HttpWebResponse)ex.Response;

                if (r.StatusCode == HttpStatusCode.Forbidden) {
                    logger.Debug("Market returned 403 Forbidden, invalidating data for " + characterId);
                    GMITracker.Invalidate(characterId);
                    return false;
                }
                else {
                    logger.Warn(string.Format("Failed to contact GMI: {0}{1}", ex.Message, ex.StackTrace));
                    ExceptionReporter.ReportException(ex);
                    return false;
                }
            }
        }

        public class GMIResult {
            public string credits;
            public long availableSlots;
        }

        private static List<GMIItem> FindItems(string html, long characterId) {
            List<GMIItem> result = new List<GMIItem>();
            const string pattern = @"'cluster_id': (?<id>\d+), 'template_ql': (?<ql>\d+),";
            Regex re = new Regex(pattern);

            foreach (Match m in Regex.Matches(html, pattern)) {
                Console.WriteLine(string.Format("item id {0} QL {1}", m.Groups["id"], m.Groups["ql"]));
                try {
                    result.Add(
                        new GMIItem {
                            ItemId = long.Parse(m.Groups["id"].Value),
                            QL = int.Parse(m.Groups["ql"].Value),
                            CharacterId = characterId
                        });
                }
                catch (Exception) {

                }
            }

            return result;
        }


        /// <summary>
        /// Download the item name for a given item ID (gmi item id)
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="cookie"></param>
        public static void DownloadMissingItemNames(long characterId, string cookie) {
            try {

                var missingItemNames = gmiDao.GetItemsMissingName();
                foreach (var id in missingItemNames) {
                    // Download and parse item name here?
                    string response;
                    if (DownloadItemName(characterId, id, cookie, out response)) {
                        var itemlist = JObject.Parse(response);
                        gmiDao.UpdateName(id, (string)itemlist["name"]);
                    }
                    // Let's just bail, could be a 403 forbidden, cookie expired
                    else
                        break;                    
                }
            }
            catch (Exception ex) {
                logger.Warn(string.Format("Failed to parse GMI items: {0}{1}", ex.Message, ex.StackTrace));
                ExceptionReporter.ReportException(ex);
            }
        }

        
        private static GMIResult ParseItemdata(long characterId, string html) {
            // Store the GMI items for this character
            try {
                var items = FindItems(html, characterId);
                gmiDao.SaveGMIItems(items, characterId);
            }
            catch (Exception ex) {
                logger.Warn(string.Format("Failed to parse GMI items: {0}{1}", ex.Message, ex.StackTrace));
                ExceptionReporter.ReportException(ex);
            }

            if (!string.IsNullOrEmpty(html)) {
                try {
                    const string toFind = @"<p id=""inventoryCash"" class=""inline"">";

                    string match = html.Substring(html.IndexOf(toFind) + toFind.Length);
                    match = match.Substring(0, match.IndexOf("<"));
                    Regex re = new Regex(@"\((?<a1>\d+)/(?<a2>\d+) free\)");
                    string availables = re.Match(html).Groups["a1"].Value;
                    GMIResult result = new GMIResult { credits = match };
                    if (!long.TryParse(availables, out result.availableSlots))
                        result.availableSlots = -1;

                    return result;
                }
                catch (Exception ex) {
                    logger.Warn(string.Format("Failed to contact GMI: {0}{1}", ex.Message, ex.StackTrace));
                    ExceptionReporter.ReportException(ex);
                }
            }

            return null;
        }
        
        public static GMIResult DownloadItemdata(long characterId, string cookie) {
            try {
                string data = "";
                if (DownloadGMIData(characterId, cookie, out data)) {
                    return ParseItemdata(characterId, data);
                }
            }
            catch (NullReferenceException ex) {
                if (string.IsNullOrEmpty(cookie))
                    ExceptionReporter.ReportException(ex, "\"cookie\" is null");
                /* No freaking idea where this one comes from, just eat it and move on */
            }
            catch (WebException ex) {
                // Probably 502 bad gateway, no way to handle it, just ignore it.
                if (ex.Status == WebExceptionStatus.ProtocolError) {
                    logger.Debug(ex.Message);                    
                }
                else {
                    ExceptionReporter.ReportException(ex);
                    logger.Warn(ex.Message);
                }
            }
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex);
                logger.Warn(ex.Message);
            }
            return null;
        }
    }
}
