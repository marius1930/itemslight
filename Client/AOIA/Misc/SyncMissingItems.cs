﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using ItemsLight.AOIA;
using log4net;
using EvilsoftCommons.Exceptions;

namespace ItemsLight {
    public class SyncMissingItems {
        static ILog logger = LogManager.GetLogger(typeof(SyncMissingItems));
        private static List<Int64> ignoredObjects = new List<Int64>();

        private const string URL_SYNCDOWN = "http://ribbs.dreamcrash.org/ialight/aoia.php?";
        private static bool DownloadItemdata(IList<Int64> objects, out string responseString) {
            responseString = null;


            string parameters = "";
            for (int i = 0; i < Math.Min(100, objects.Count); i++) {
                if (!ignoredObjects.Contains(objects[i]))
                    parameters += string.Format("aoid[]={0}&", objects[i]);

                ignoredObjects.Add(objects[i]);
            }

            // This happens a lot!
            if (string.IsNullOrEmpty(parameters))
                return false;

            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(URL_SYNCDOWN + parameters);

            httpWReq.Method = "GET";

            try {
                using (HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse()) {
                    if (response.StatusCode != HttpStatusCode.OK)
                        return false;

                    responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();


                    logger.Info(string.Format("Requested itemdata for {0} items.", objects.Count));
                    logger.Info(parameters);
                    return true;
                }
            }
            catch (WebException ex) {
                logger.Warn(string.Format("Failed to sync items: {0}{1}", ex.Message, ex.StackTrace));
                ExceptionReporter.ReportException(ex);
                return false;
            }
        }

        private static List<DownloadedItemdata> ParseItemdata(string json) {
            List<DownloadedItemdata> parsed = new List<DownloadedItemdata>();
            if (json.Length > 0) {
                try {
                    var itemlist = JArray.Parse(json);

                    foreach (var item in itemlist) {
                        parsed.Add(new DownloadedItemdata {
                            aoid = (long)item["aoid"],
                            properties = (uint)item["properties"],
                            name = (string)item["name"]
                        });
                    }
                }
                catch (Exception ex) {
                    ExceptionReporter.ReportException(ex, "parse json");
                    logger.Warn(string.Format("{0}, {1}", ex.Message, ex.StackTrace));
                }
            }

            logger.Info(string.Format("Parsed {0} items.", parsed.Count));
            return parsed;
        }

        public static List<DownloadedItemdata> DownloadItemdata(IList<Int64> objects) {
            string json = "";
            if (objects.Count > 0 && DownloadItemdata(objects, out json)) {
                return ParseItemdata(json);
            }
            else
                return new List<DownloadedItemdata>();
        }
    }
}
