﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace ItemsLight.AOIA.Misc {
    struct Monster {
        public string Name { get; set; }
        public int Spawntime { get; set; }
    }
    class MonsterNameTracker {
        static ILog logger = LogManager.GetLogger(typeof(MonsterNameTracker));
        private Dictionary<long, string> MonsterNameMapping = new Dictionary<long, string>();
        private Dictionary<long, long> MonsterContainerMapping = new Dictionary<long, long>();
        private List<Monster> monsters = new List<Monster>();

        private MonsterNameTracker() {
            monsters.Add(new Monster { Name = "Mooching Dryad", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Snaking Dryad", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Lurking Dryad", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Skulking Dryad", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Stealing Dryad", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Slinking Dryad", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Muddy Dryad", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Peristaltic Abomination", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Peristaltic Aversion", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Skulking", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Smee", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Jukes", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Mull", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Turk", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Wicked Soul Dredge", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Sinful Soul Dredge", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Tearing Soul Dredge", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Scratching Soul Dredge", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Snatching Soul Dredge", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Gashing Soul Dredge", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Coiling Eremite", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Swirling Eremite", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Wounded Predator", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Limping Predator", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Waning Predator", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Pained Predator", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Limber Dryad", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Brutish Dryad", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Limber Dryad", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Disturbing Imp", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Screeching Imp", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Cacophonous Imp", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Asperous Imp", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Sadistic Soul Dredge", Spawntime = 25 }); // ely,problem solving device
            monsters.Add(new Monster { Name = "Lost Soul", Spawntime = 25 }); // ely,problem solving device
            monsters.Add(new Monster { Name = "Waning Soul", Spawntime = 25 }); // ely,problem solving device
            monsters.Add(new Monster { Name = "Comatosed Soul", Spawntime = 25 }); // ely,problem solving device
            monsters.Add(new Monster { Name = "Xark", Spawntime = 240 }); // 
            monsters.Add(new Monster { Name = "Razor", Spawntime = 240 }); // 
            monsters.Add(new Monster { Name = "Breaker Reijo", Spawntime = 25 }); // pen
            monsters.Add(new Monster { Name = "Death Wing", Spawntime = 25 }); // pen
            monsters.Add(new Monster { Name = "Maar", Spawntime = 25 }); // pen
            monsters.Add(new Monster { Name = "Stubby Stalker", Spawntime = 25 }); // pen
            monsters.Add(new Monster { Name = "Pudgy Stalker", Spawntime = 25 }); // pen
            monsters.Add(new Monster { Name = "Hefty Rafter", Spawntime = 25 }); // pen
            monsters.Add(new Monster { Name = "Anansi's Abettor", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Devoted Spirit Hunter", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Nyame's Drudge", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Tiunissik", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Fanatic Spirit Hunter", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Tiunissik's Shadow", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Torrid Spirit", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Anansi's Left Hand", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Anansi's Right Hand", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Conflagrant Spirit", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Malah-Furcifera", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Isham", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Noxious Eremite", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Fetid Eremite", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Hebiel", Spawntime = 25 });

            monsters.Add(new Monster { Name = "Zias", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Tdecin", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Ossuz", Spawntime = 25 }); // ely
            monsters.Add(new Monster { Name = "Athlar", Spawntime = 25 }); // ely
            
            monsters.Add(new Monster { Name = "Stark", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Vile Spirit", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Black", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Skylight", Spawntime = 25 });
            monsters.Add(new Monster { Name = "Nippy Slither", Spawntime = 25 });
        }


        private static MonsterNameTracker instance;
        public static MonsterNameTracker Instance {
            get {
                if (instance == null)
                    instance = new MonsterNameTracker();
                return instance;
            }
        }

        /// <summary>
        /// Get the name of a monster, based on a containerId (loot window)
        /// </summary>
        /// <param name="containerId"></param>
        /// <returns></returns>
        public string MapContainerToMonster(long containerId) {
            if (MonsterContainerMapping.ContainsKey(containerId)) {
                long characterId = MonsterContainerMapping[containerId];
                if (MonsterNameMapping.ContainsKey(characterId)) {
                    return MonsterNameMapping[characterId];
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Map a monster with a container (corpse => loot container)
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="containerId"></param>
        public void MapMonsterToContainer(long characterId, long containerId) {
            MonsterContainerMapping[containerId] = characterId;
            if (MonsterNameMapping.ContainsKey(characterId)) {
                if (monsters.Any(e => MonsterNameMapping[characterId].StartsWith(e.Name))) {
                    logger.Debug("Mob killed: " + MonsterNameMapping[characterId]);
                }
            }
        }

        /// <summary>
        /// Store the name for a monster
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="name"></param>
        public void MapNameToMonster(long characterId, string name) {
            MonsterNameMapping[characterId] = name;
        }

        /// <summary>
        /// Clears the cache
        /// Should be safe to do once every zone
        /// </summary>
        public void Clear() {
            MonsterNameMapping.Clear();
            MonsterContainerMapping.Clear();
        }
    }
}
