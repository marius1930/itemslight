﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using log4net;
using System.Diagnostics;
using System.ComponentModel;
using System.Globalization;
using ItemsLight.Model;
using NMemory;
using NMemory.Tables;
using System.Threading;
using EvilsoftCommons.Exceptions;


namespace ItemsLight.AOIA.Misc {
    class BackpackRenamer {
        private static Dictionary<String, HashSet<long>> previouslyUpdatedContainers = new Dictionary<String, HashSet<long>>();
        private static ILog logger = LogManager.GetLogger(typeof(BackpackRenamer));
        private static BackgroundWorker bw;


        /// <summary>
        /// Get the most recently modified file in a specific folder
        /// Useful for detecting which character folder is the newest, in case of paid character transfers across accounts.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static DateTime GetMostRecentModified(string path) {
            try {
                var directory = new DirectoryInfo(path);
                var myFile = directory.GetFiles()
                         .OrderByDescending(f => f.LastWriteTime)
                         .First();

                return myFile.LastWriteTime;
            }
            catch (Exception ex) {
                logger.Warn(ex.Message);
                return default(DateTime);
            }
        }

        /// <summary>
        /// Map all account:characterid instances from the provided AO appdata folder
        /// </summary>
        /// <param name="path"></param>
        /// <param name="accountCharid"></param>
        public static void MapAccountCharid(String path, ref Dictionary<long, String> accountCharid) {
            string[] accounts = Directory.GetDirectories(Path.Combine(path, "Prefs"));

            Dictionary<long, DateTime> lastModifiedAccount = new Dictionary<long, DateTime>();


            foreach (String account in accounts) {
                string[] charids = Directory.GetDirectories(account);

                foreach (String charid in charids) {
                    String fn = Path.GetFileName(charid);

                    if (fn.StartsWith("Char")) {
                        var timestamp = GetMostRecentModified(Path.Combine(path, "Prefs", account, charid));


                        long id;
                        if (long.TryParse(fn.ToLower().Replace("char", ""), out id)) {
                            if (!lastModifiedAccount.ContainsKey(id) || timestamp > lastModifiedAccount[id]) {
                                accountCharid[id] = Path.GetFileName(account.ToLower());
                                lastModifiedAccount[id] = timestamp;
                            }
                            else {
                                /*
                                logger.Warn(string.Format("Ignored account {0} with timestamp {1} at path {2}, because another timestamp was newer ({3})",
                                    account, timestamp.ToString("dd-MM-yyyy HH:mm"), Path.Combine(path, "Prefs", account, charid), lastModifiedAccount[id].ToString("dd-MM-yyyy HH:mm")));
                                */
                            }
                        }
                        else {
                            logger.Debug("Could not parse character id from " + fn);
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Creates a new dummy container file to name an ingame backpack
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="containerName"></param>
        private static void CreateContainerFile(String filename, String containerName) {
            String data = string.Format("<Archive code=\"0\">\r\n    <String name=\"container_name\" value='&quot;{0}&quot;' />\r\n</Archive>", containerName);
            Directory.CreateDirectory(Path.GetDirectoryName(filename));
            File.WriteAllText(filename, data);
        }

        /// <summary>
        /// Renames an existing backpack, unless it has previously been renamed
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="containerName"></param>
        private static bool UpdateExistingContainerFile(String filename, String containerName, bool forced) {
            String data = File.ReadAllText(filename);
            String insert = string.Format("\r\n    <String name=\"container_name\" value='&quot;{0}&quot;' />", containerName);
            if (!data.Contains("container_name")) {
                int idx = 1 + data.IndexOf('>');
                data = data.Insert(idx, insert);
                File.WriteAllText(filename, data);
                return true;
            }
            else if (forced) {
                string[] dataset = File.ReadAllLines(filename);
                for (int i = 0; i < dataset.Length; i++) {
                    if (dataset[i].Contains("container_name")) {
                        if (containerName.Length > 0)
                            dataset[i] = insert;
                        else
                            dataset[i] = "";
                    }
                }

                File.WriteAllLines(filename, dataset);
                return true;

            }
            return false;
        }


        /// <summary>
        /// Renames the container, unless previously renamed.
        /// </summary>
        /// <param name="account"></param>
        /// <param name="containerName"></param>
        /// <param name="containerIds"></param>
        /// <param name="characterId"></param>
        public static bool RenameBackpack(String account, String containerName, long containerId, long characterId, bool forced) {
            bool success = false;
            try {
                foreach (String folder in AppdataPathFinder.ListAnarchyDirectories()) {
                    String charpath = Path.Combine(new string[] { folder, "Prefs", account, "Char" + characterId });
                    if (!Directory.Exists(charpath))
                        continue;

                    if (!previouslyUpdatedContainers.ContainsKey(folder))
                        previouslyUpdatedContainers[folder] = new HashSet<long>();


                    String containerPath = Path.Combine(charpath, "Containers");
                    if (forced || !previouslyUpdatedContainers[folder].Contains(containerId)) {
                        previouslyUpdatedContainers[folder].Add(containerId);

                        if (!File.Exists(Path.Combine(containerPath, string.Format("Container_51017x{0}.xml", containerId)))) {
                            CreateContainerFile(Path.Combine(containerPath, string.Format("Container_51017x{0}.xml", containerId)), containerName);
                            success = true;
                            logger.Info(string.Format("Renamed container {0} to {1}", containerId, containerName));
                        }
                        else {
                            if (UpdateExistingContainerFile(Path.Combine(containerPath, string.Format("Container_51017x{0}.xml", containerId)), containerName, forced)) {
                                success = true;
                                logger.Info(string.Format("Renamed container {0} to {1}", containerId, containerName));
                            }
                            else {
                                //logger.Debug("Skipped container rename on " + containerId + " to " + containerName + ", has a custom name.");
                            }
                        }
                    }
                }
            }
            catch (IOException) { /* Totally OK */ }
            catch (Exception e) {
                logger.Warn(e.Message + e.StackTrace);
                ExceptionReporter.ReportException(e);
            }
            return success;
        }



        class BackpackCacheEntry {
            public long Id { get; set; }
            public string account { get; set; }
            public long characterId { get; set; }
            public long containerId { get; set; }
            public long expiration { get; set; }
            public string name { get; set; }
        }
        private class BackpackNameCache : Database {
            public BackpackNameCache() {
                this.Entries = base.Tables.Create<BackpackCacheEntry, long>(p => p.Id, new IdentitySpecification<BackpackCacheEntry>(x => x.Id));
            }

            public ITable<BackpackCacheEntry> Entries { get; private set; }

            // Delete expired entities
            public void Clean() {
                var ts = (long)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                var toRemove = new List<BackpackCacheEntry>(Entries.Where(m => m.expiration < ts));
                for (int i = 0; i < toRemove.Count; i++)
                    Entries.Delete(toRemove[i]);
            }
        }


        private static BackpackNameCache db = new BackpackNameCache();

        public static string GetBackpackName(String account, long characterId, long containerId) {            
            if (string.IsNullOrEmpty(account))
                return string.Empty;

            var ts = (long)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            foreach (var existing in db.Entries.Where(m => m.characterId == characterId && m.containerId == containerId && m.expiration > ts && m.account.Equals(account))) {
                return existing.name;
            }

            try {
                const String f = "container_name\" value='&quot;";

                foreach (String folder in AppdataPathFinder.ListAnarchyDirectories()) {
                    String containerPath = Path.Combine(new string[] { folder, "Prefs", account, "Char" + characterId, "Containers" });
                    if (!Directory.Exists(containerPath))
                        continue;

                    string filename = Path.Combine(containerPath, string.Format("Container_51017x{0}.xml", containerId));
                    if (File.Exists(filename)) {
                        String[] data = File.ReadAllLines(filename);
                        
                        
                        foreach (String line in data) {
                            if (line.Contains("container_name")) {
                                int idx = line.IndexOf(f) + f.Length;

                                int length = line.IndexOf("&quot;'", idx) - idx;
                                if (length > 0) {
                                    string name = line.Substring(idx, length)
                                        .Replace("&apos;", "'")
                                        .Replace("&amp;", "&")
                                        .Replace("amp;", "&")
                                        .Replace("&quot;", "'")
                                        .Replace("&lt;", "<")
                                        .Replace("&gt;", ">");

                                    db.Entries.Insert(new BackpackCacheEntry { name = name, account = account, characterId = characterId, containerId = containerId, expiration = ts + 5*1000*3 });
                                    db.Clean();
                                    return name;
                                }
                                else
                                    return string.Empty;
                            }
                        }
                    }
                }
            }
            catch (IOException) {
                /* Happens quite often, used by another process */
            }
            catch (Exception e) {
                logger.Warn(e.Message + e.StackTrace);
                ExceptionReporter.ReportException(e);
            }

            return string.Empty;
        }



        public static void RenameBackpacksAsync(IList<Item> containers, RunWorkerCompletedEventHandler bw_RunWorkerCompleted) {
            try {
                bw = new BackgroundWorker();
                bw.DoWork += new DoWorkEventHandler(bw_DoWork);
                bw.RunWorkerCompleted += bw_RunWorkerCompleted;
                bw.WorkerSupportsCancellation = true;
                bw.RunWorkerAsync(containers);
            }
            catch (NullReferenceException ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                ExceptionReporter.ReportException(ex, string.Format("RenameBackpacksAsync({0}, {1})", containers == null, bw_RunWorkerCompleted == null));
            }
        }

        
        private static void bw_DoWork(object sender, DoWorkEventArgs e) {
            BackgroundWorker worker = sender as BackgroundWorker;

            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "BackpackRenamer";


            List<Item> containers = e.Argument as List<Item>;
            if (!worker.CancellationPending) {
                RenameBackpacks(containers);
                Thread.Sleep(1000 * 120);
            }
        }


        private static void RenameBackpacks(IList<Item> containers) {
            try {
                Dictionary<long, String> accountMap = new Dictionary<long, String>();

                foreach (String folder in AppdataPathFinder.ListAnarchyDirectories()) {
                    MapAccountCharid(folder, ref accountMap);
                }
                

                uint idx = (uint)Properties.Settings.Default["backpackIdx"];
                uint initial = idx;
                
                foreach (var container in containers) {
                    if (container.Owner == null) continue; // Orphaned backpack
                    if (container.Name == null) continue; // "Should" never happen

                    long charid = container.Owner.Id;
                    long containerId = container.Children; // Is this correct?
                    
                    string bpname;
                    if ((bool)Properties.Settings.Default["postfixbags"])
                        bpname = string.Format("{1} ({0})", idx, container.Name.Replace("\"", "").Replace("'", ""));
                    else
                        bpname = string.Format("#{0} {1}", idx, container.Name.Replace("\"", "").Replace("'", ""));

                    if (accountMap.ContainsKey(charid)) {
                        if (BackpackRenamer.RenameBackpack(accountMap[charid], bpname, containerId, charid, false)) {
                            idx++;
                        }
                    }
                }
                

                System.Threading.Thread.Sleep(50);
                

                logger.Debug("Backpack renaming complete..");
                if (initial != idx) {
                    Properties.Settings.Default["backpackIdx"] = idx;
                    Properties.Settings.Default.Save();
                }
            }
            catch (NullReferenceException e) {
                logger.Warn(e.Message);
                logger.Warn(e.StackTrace);
                ExceptionReporter.ReportException(e, string.Format("RenameBackpacks({0})", containers == null));
            }
            catch (Exception e) {
                logger.Warn(e.Message + e.StackTrace);
                ExceptionReporter.ReportException(e);
            }
        }
    }
}

