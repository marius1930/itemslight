﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight {
    public class GMITracker {
        struct GMIData {
            public uint a;
            public uint b;
        }
        private static Dictionary<long, GMIData> gmimap = new Dictionary<long, GMIData>();

        public static bool TryGet(long characterId, out string cookie) {
            if (gmimap.ContainsKey(characterId)) {
                cookie = string.Format("{0} {1}", gmimap[characterId].a, gmimap[characterId].b);
                return true;
            }
            else {
                cookie = string.Empty;
                return false;
            }
        }

        public static void Set(long characterId, uint a, uint b) {
            gmimap[characterId] = new GMIData {
                a = a,
                b = b
            };
        }

        public static void Invalidate(long characterId) {
            gmimap.Remove(characterId);
        }

        public static List<long> CharacterIds {
            get {
                return new List<long>(gmimap.Keys);
            }
        }
    }
}
