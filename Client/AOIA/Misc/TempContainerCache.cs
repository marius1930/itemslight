﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA.Misc {
    public class TempContainerCache {
        private static TempContainerCache instance;
        Dictionary<UInt32, Dictionary<UInt32, uint>> m_containerIdCache = new Dictionary<UInt32, Dictionary<UInt32, uint>>();
        Dictionary<UInt32, Dictionary<UInt32, uint>> m_invSlotForTempItemCache = new Dictionary<UInt32, Dictionary<UInt32, uint>>();
        public static TempContainerCache Instance {
            get {
                if (instance == null)
                    instance = new TempContainerCache();
                return instance;
            }
        }


        private TempContainerCache() { }

        public void ClearTempContainerIdCache(uint charId) {
            //TODO: Clear cache for only 1 char (if duallogged)
            if (m_containerIdCache.ContainsKey(charId))
                m_containerIdCache.Clear();
        }




        public uint GetContainerId(uint charId, uint tempId) {
            if (m_containerIdCache.ContainsKey(charId)) {
                if (m_containerIdCache[charId].ContainsKey(tempId)) {
                    return m_containerIdCache[charId][tempId];
                }
            }

            return 0;
        }
        public uint GetItemSlotId(uint charId, uint tempId) {
            if (m_invSlotForTempItemCache.ContainsKey(charId)) {
                if (m_invSlotForTempItemCache[charId].ContainsKey(tempId)) {
                    return m_invSlotForTempItemCache[charId][tempId];
                }
            }

            return 0;
        }


        public void UpdateTempContainerId(uint charId, uint tempId, uint containerId) {
            //TRACE("UPDATE Temp Cont Id " << tempId << " / " << containerId);
            if (!m_containerIdCache.ContainsKey(charId)) {
                Dictionary<UInt32, uint> newCharIdCache = new Dictionary<UInt32, uint>();
                m_containerIdCache[charId] = newCharIdCache;
            }

            m_containerIdCache[charId][tempId] = containerId;
        }

        public void UpdateTempItemId(uint charId, uint itemTempId, uint slotId) {
            //TRACE("UPDATE Temp Cont Id " << tempId << " / " << containerId);
            if (!m_invSlotForTempItemCache.ContainsKey(charId)) {
                Dictionary<UInt32, uint> newCharIdCache = new Dictionary<UInt32, uint>();
                m_invSlotForTempItemCache[charId] = newCharIdCache;
            }

            m_invSlotForTempItemCache[charId][itemTempId] = slotId;
        }

        
    }
}
