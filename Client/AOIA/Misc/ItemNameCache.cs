﻿using ItemsLight.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using log4net;

namespace ItemsLight.AOIA.Misc {
    class ItemNameCache {
        static ILog logger = LogManager.GetLogger(typeof(ItemNameCache));
        private static List<DatabaseItem> cache;

        /// <summary>
        /// Load cache from file, if possible
        /// </summary>
        private static void LoadCache() {
            try {
                String appdata = Environment.GetEnvironmentVariable("LocalAppData");
                string fullpath = Path.Combine(appdata, "EvilSoft", "IALight", "cache");
                Directory.CreateDirectory(fullpath);
                fullpath = Path.Combine(fullpath, "itemdb");

                if (File.Exists(fullpath)) {
                    List<DatabaseItem> tmp = new List<DatabaseItem>();
                    var data = File.ReadAllLines(fullpath);
                    if (data.Length < 250000) {
                        File.Delete(fullpath);
                        logger.Info("Deleted local item cache, corrupted.");
                    }
                    else {
                        for (int i = 0; i < data.Length; i += 2) {
                            long id;
                            if (long.TryParse(data[i], out id))
                                tmp.Add(new DatabaseItem { Id = id, Name = data[i + 1] });
                        }

                        cache = tmp;
                        logger.Info("Loaded local item cache");
                    }
                }
            }
            catch (Exception ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
            }
        }

        /// <summary>
        /// Update the local cache
        /// </summary>
        private static void TriggerSetCache() {
            try {
                String appdata = Environment.GetEnvironmentVariable("LocalAppData");
                string fullpath = Path.Combine(appdata, "EvilSoft", "IALight", "cache");
                Directory.CreateDirectory(fullpath);
                fullpath = Path.Combine(fullpath, "itemdb");


                if (File.Exists(fullpath) && cache == null) {
                    File.Delete(fullpath);
                }
                else if (cache != null) {
                    List<string> data = new List<string>();
                    foreach (var item in cache) {
                        data.Add(item.Id.ToString());
                        data.Add(item.Name);
                    }
                    File.WriteAllLines(fullpath, data);

                    logger.Info("Updated local item cache");
                }
            }
            catch (Exception ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
            }    
        }


        public static List<DatabaseItem> Cache {
            get {
                if (cache == null)
                    LoadCache();

                return cache;
            }
            set {
                cache = value;
                TriggerSetCache();
            }
        }

    }
}
