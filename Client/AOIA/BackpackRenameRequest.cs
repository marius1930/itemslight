﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.AOIA {
    class BackpackRenameRequest {
        public long CharacterId { get; set; }
        public long ContainerId { get; set; }
        public string Name { get; set; }
    }
}
