﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.Concurrent;
using ItemsLight.AOIA;
using log4net;

namespace ItemsLight {
    public class LoadingScreenManager : IDisposable {
        private static ILog logger = LogManager.GetLogger(typeof(LoadingScreenManager));
        private LoadingScreen instance;
        private ConcurrentQueue<MessageData> resultQueue;
        private Form parent;

        public LoadingScreenManager(ConcurrentQueue<MessageData> resultQueue, Form parent) {
            this.resultQueue = resultQueue;
            this.parent = parent;
        }


        public void Dispose() {
            if (instance != null) {
                try {
                    instance.Close();
                    instance = null;
                }
                catch (Exception ex) {
                    logger.Debug(ex.Message);

                }
            }

        }
        public void Hide() {
            if (instance != null) {
                try {
                    instance.Hide();
                }
                catch (Exception ex) {

                    logger.Debug(ex.Message);
                }
            }
        }


        private void LoadingSreenClosedCallback() {
            instance = null;
        }

        public void ShowIfAvailable() {
            if (instance != null) {
                try {
                    instance.Show(parent);
                }
                catch (Exception ex) {
                    logger.Debug(ex.Message);
                }
            }
        }


        public void Show(string message) {
            if (instance == null) {
                try {
                    instance = new LoadingScreen(resultQueue, LoadingSreenClosedCallback);
                    instance.StartPosition = FormStartPosition.Manual;
                    instance.Location = new Point(
                        parent.Location.X + (parent.Width - instance.Width) / 2,
                        parent.Location.Y + (parent.Height - instance.Height) / 2);
                    instance.Show(parent);
                }
                catch (Exception ex) {
                    logger.Debug(ex.Message);

                }
            }
            else {
                try {
                    instance.Label = message;
                    if (!instance.Visible)
                        instance.Show(parent);
                }
                catch (Exception ex) {
                    logger.Debug(ex.Message);

                }
            }

        }

    }

}