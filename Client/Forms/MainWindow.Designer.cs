﻿namespace ItemsLight {
    partial class MainWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Loading..");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.textboxSearch = new System.Windows.Forms.TextBox();
            this.treeContainerView = new System.Windows.Forms.TreeView();
            this.treeViewContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.renameF2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.archieveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listViewResults = new System.Windows.Forms.ListView();
            this.columnItem = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnQL = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnCharacter = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnBackpack = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listviewContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.viewItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToScriptsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.trayIconContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkForUpdatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectDimension = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.webBrowserSummary = new System.Windows.Forms.WebBrowser();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.comboBoxCharactersItemview = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbMaxQl = new System.Windows.Forms.TextBox();
            this.tbMinQl = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.listViewIdentify = new System.Windows.Forms.ListView();
            this.chType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.identifyViewContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.viewItemToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewBackpackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPagePatterns = new System.Windows.Forms.TabPage();
            this.listViewPatterns = new System.Windows.Forms.ListView();
            this.columnHeaderPBName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPBAvailability = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.webBrowserPatterns = new System.Windows.Forms.WebBrowser();
            this.comboBoxCharacters = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.patternClanGarden = new System.Windows.Forms.RadioButton();
            this.patternShowAll = new System.Windows.Forms.RadioButton();
            this.patternOmniGarden = new System.Windows.Forms.RadioButton();
            this.patternShowPartial = new System.Windows.Forms.RadioButton();
            this.patternShowCompletables = new System.Windows.Forms.RadioButton();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.buffItemsPanel = new System.Windows.Forms.Panel();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.symbiantMatcherPanel = new System.Windows.Forms.Panel();
            this.tabSettings = new System.Windows.Forms.TabPage();
            this.settingsPanel = new System.Windows.Forms.Panel();
            this.treeViewContext.SuspendLayout();
            this.listviewContextMenu.SuspendLayout();
            this.trayIconContext.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.identifyViewContext.SuspendLayout();
            this.tabPagePatterns.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // textboxSearch
            // 
            this.textboxSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textboxSearch.BackColor = System.Drawing.SystemColors.Window;
            this.textboxSearch.Enabled = false;
            this.textboxSearch.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxSearch.Location = new System.Drawing.Point(6, 3);
            this.textboxSearch.Name = "textboxSearch";
            this.textboxSearch.Size = new System.Drawing.Size(869, 26);
            this.textboxSearch.TabIndex = 1;
            this.textboxSearch.TextChanged += new System.EventHandler(this.textboxSearch_TextChanged);
            // 
            // treeContainerView
            // 
            this.treeContainerView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeContainerView.ContextMenuStrip = this.treeViewContext;
            this.treeContainerView.Enabled = false;
            this.treeContainerView.HideSelection = false;
            this.treeContainerView.Location = new System.Drawing.Point(6, 62);
            this.treeContainerView.Name = "treeContainerView";
            treeNode2.Name = "Node0";
            treeNode2.Text = "Loading..";
            this.treeContainerView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode2});
            this.treeContainerView.Size = new System.Drawing.Size(245, 478);
            this.treeContainerView.TabIndex = 3;
            this.treeContainerView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeContainerView_AfterSelect);
            // 
            // treeViewContext
            // 
            this.treeViewContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.renameF2ToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.archieveToolStripMenuItem});
            this.treeViewContext.Name = "treeViewContext";
            this.treeViewContext.Size = new System.Drawing.Size(141, 92);
            this.treeViewContext.Opening += new System.ComponentModel.CancelEventHandler(this.treeViewContext_Opening);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(140, 22);
            this.toolStripMenuItem2.Text = "Refresh";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // renameF2ToolStripMenuItem
            // 
            this.renameF2ToolStripMenuItem.Name = "renameF2ToolStripMenuItem";
            this.renameF2ToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.renameF2ToolStripMenuItem.Text = "Rename (F2)";
            this.renameF2ToolStripMenuItem.Click += new System.EventHandler(this.renameF2ToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // archieveToolStripMenuItem
            // 
            this.archieveToolStripMenuItem.Name = "archieveToolStripMenuItem";
            this.archieveToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.archieveToolStripMenuItem.Text = "Archive";
            this.archieveToolStripMenuItem.Click += new System.EventHandler(this.archieveToolStripMenuItem_Click);
            // 
            // listViewResults
            // 
            this.listViewResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewResults.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnItem,
            this.columnQL,
            this.columnCharacter,
            this.columnBackpack,
            this.columnLocation});
            this.listViewResults.ContextMenuStrip = this.listviewContextMenu;
            this.listViewResults.FullRowSelect = true;
            this.listViewResults.HideSelection = false;
            this.listViewResults.Location = new System.Drawing.Point(257, 62);
            this.listViewResults.Name = "listViewResults";
            this.listViewResults.Size = new System.Drawing.Size(795, 478);
            this.listViewResults.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewResults.TabIndex = 4;
            this.listViewResults.UseCompatibleStateImageBehavior = false;
            this.listViewResults.View = System.Windows.Forms.View.Details;
            this.listViewResults.SelectedIndexChanged += new System.EventHandler(this.listViewResults_SelectedIndexChanged);
            // 
            // columnItem
            // 
            this.columnItem.Text = "Item";
            this.columnItem.Width = 257;
            // 
            // columnQL
            // 
            this.columnQL.Text = "QL";
            this.columnQL.Width = 49;
            // 
            // columnCharacter
            // 
            this.columnCharacter.Text = "Character";
            this.columnCharacter.Width = 130;
            // 
            // columnBackpack
            // 
            this.columnBackpack.Text = "Backpack";
            this.columnBackpack.Width = 141;
            // 
            // columnLocation
            // 
            this.columnLocation.Text = "Location";
            this.columnLocation.Width = 105;
            // 
            // listviewContextMenu
            // 
            this.listviewContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewItemToolStripMenuItem,
            this.toolStripMenuItem1,
            this.writeToScriptsToolStripMenuItem});
            this.listviewContextMenu.Name = "listviewContextMenu";
            this.listviewContextMenu.Size = new System.Drawing.Size(154, 70);
            this.listviewContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.listviewContextMenu_Opening);
            // 
            // viewItemToolStripMenuItem
            // 
            this.viewItemToolStripMenuItem.Name = "viewItemToolStripMenuItem";
            this.viewItemToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.viewItemToolStripMenuItem.Text = "View Item";
            this.viewItemToolStripMenuItem.Click += new System.EventHandler(this.viewItemToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.toolStripMenuItem1.Text = "View backpack";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // writeToScriptsToolStripMenuItem
            // 
            this.writeToScriptsToolStripMenuItem.Name = "writeToScriptsToolStripMenuItem";
            this.writeToScriptsToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.writeToScriptsToolStripMenuItem.Text = "Write to scripts";
            this.writeToScriptsToolStripMenuItem.Click += new System.EventHandler(this.writeToScriptsToolStripMenuItem_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "AO:Light minimized to tray";
            this.notifyIcon1.ContextMenuStrip = this.trayIconContext;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "IA:Light";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // trayIconContext
            // 
            this.trayIconContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.trayIconContext.Name = "trayIconContext";
            this.trayIconContext.Size = new System.Drawing.Size(104, 48);
            this.trayIconContext.Opening += new System.ComponentModel.CancelEventHandler(this.trayIconContext_Opening);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1070, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkForUpdatesToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // checkForUpdatesToolStripMenuItem
            // 
            this.checkForUpdatesToolStripMenuItem.Name = "checkForUpdatesToolStripMenuItem";
            this.checkForUpdatesToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.checkForUpdatesToolStripMenuItem.Text = "Check for updates";
            this.checkForUpdatesToolStripMenuItem.Click += new System.EventHandler(this.checkForUpdatesToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // selectDimension
            // 
            this.selectDimension.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selectDimension.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectDimension.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectDimension.Location = new System.Drawing.Point(6, 32);
            this.selectDimension.Name = "selectDimension";
            this.selectDimension.Size = new System.Drawing.Size(245, 26);
            this.selectDimension.TabIndex = 8;
            this.selectDimension.SelectedIndexChanged += new System.EventHandler(this.selectDimension_SelectedIndexChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 603);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1070, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPagePatterns);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabSettings);
            this.tabControl1.Location = new System.Drawing.Point(4, 28);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1066, 572);
            this.tabControl1.TabIndex = 10;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.webBrowserSummary);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1058, 546);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Summary";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // webBrowserSummary
            // 
            this.webBrowserSummary.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowserSummary.IsWebBrowserContextMenuEnabled = false;
            this.webBrowserSummary.Location = new System.Drawing.Point(0, 0);
            this.webBrowserSummary.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowserSummary.Name = "webBrowserSummary";
            this.webBrowserSummary.Size = new System.Drawing.Size(1055, 543);
            this.webBrowserSummary.TabIndex = 1;
            this.webBrowserSummary.Url = new System.Uri("about:blank", System.UriKind.Absolute);
            this.webBrowserSummary.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser2_DocumentCompleted);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.comboBoxCharactersItemview);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.tbMaxQl);
            this.tabPage1.Controls.Add(this.tbMinQl);
            this.tabPage1.Controls.Add(this.selectDimension);
            this.tabPage1.Controls.Add(this.treeContainerView);
            this.tabPage1.Controls.Add(this.textboxSearch);
            this.tabPage1.Controls.Add(this.listViewResults);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1058, 546);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Items";
            // 
            // comboBoxCharactersItemview
            // 
            this.comboBoxCharactersItemview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxCharactersItemview.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCharactersItemview.FormattingEnabled = true;
            this.comboBoxCharactersItemview.Location = new System.Drawing.Point(874, 36);
            this.comboBoxCharactersItemview.Name = "comboBoxCharactersItemview";
            this.comboBoxCharactersItemview.Size = new System.Drawing.Size(176, 21);
            this.comboBoxCharactersItemview.TabIndex = 12;
            this.comboBoxCharactersItemview.SelectedIndexChanged += new System.EventHandler(this.comboBoxCharactersItemview_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(254, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "QL Range:";
            // 
            // tbMaxQl
            // 
            this.tbMaxQl.Location = new System.Drawing.Point(346, 36);
            this.tbMaxQl.MaxLength = 3;
            this.tbMaxQl.Name = "tbMaxQl";
            this.tbMaxQl.Size = new System.Drawing.Size(29, 20);
            this.tbMaxQl.TabIndex = 10;
            this.tbMaxQl.Text = "500";
            this.tbMaxQl.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbMaxQl.TextChanged += new System.EventHandler(this.tbMaxQl_TextChanged);
            // 
            // tbMinQl
            // 
            this.tbMinQl.Location = new System.Drawing.Point(315, 36);
            this.tbMinQl.MaxLength = 3;
            this.tbMinQl.Name = "tbMinQl";
            this.tbMinQl.Size = new System.Drawing.Size(29, 20);
            this.tbMinQl.TabIndex = 9;
            this.tbMinQl.Text = "1";
            this.tbMinQl.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbMinQl.TextChanged += new System.EventHandler(this.tbMinQl_TextChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button9);
            this.tabPage2.Controls.Add(this.button8);
            this.tabPage2.Controls.Add(this.button7);
            this.tabPage2.Controls.Add(this.button6);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.listViewIdentify);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1058, 546);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Identify";
            // 
            // button2
            // 
            this.button2.Image = global::ItemsLight.Properties.Resources.viralbots;
            this.button2.Location = new System.Drawing.Point(487, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(54, 54);
            this.button2.TabIndex = 15;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button9
            // 
            this.button9.Image = global::ItemsLight.Properties.Resources.ingot;
            this.button9.Location = new System.Drawing.Point(427, 6);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(54, 54);
            this.button9.TabIndex = 14;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Image = global::ItemsLight.Properties.Resources.misc;
            this.button8.Location = new System.Drawing.Point(367, 6);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(54, 54);
            this.button8.TabIndex = 13;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Image = global::ItemsLight.Properties.Resources.arulsaba;
            this.button7.Location = new System.Drawing.Point(307, 6);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(54, 54);
            this.button7.TabIndex = 12;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Image = global::ItemsLight.Properties.Resources.ofab_weapon;
            this.button6.Location = new System.Drawing.Point(247, 6);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(54, 54);
            this.button6.TabIndex = 11;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Image = global::ItemsLight.Properties.Resources.body;
            this.button5.Location = new System.Drawing.Point(187, 6);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(54, 54);
            this.button5.TabIndex = 10;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Image = global::ItemsLight.Properties.Resources.aiweapon;
            this.button4.Location = new System.Drawing.Point(127, 6);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(54, 54);
            this.button4.TabIndex = 9;
            this.button4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Image = global::ItemsLight.Properties.Resources.building;
            this.button3.Location = new System.Drawing.Point(67, 6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(54, 54);
            this.button3.TabIndex = 8;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Image = global::ItemsLight.Properties.Resources.Clump;
            this.button1.Location = new System.Drawing.Point(7, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(54, 54);
            this.button1.TabIndex = 7;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listViewIdentify
            // 
            this.listViewIdentify.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewIdentify.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chType,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.listViewIdentify.ContextMenuStrip = this.identifyViewContext;
            this.listViewIdentify.FullRowSelect = true;
            this.listViewIdentify.HideSelection = false;
            this.listViewIdentify.Location = new System.Drawing.Point(7, 66);
            this.listViewIdentify.Name = "listViewIdentify";
            this.listViewIdentify.Size = new System.Drawing.Size(1045, 474);
            this.listViewIdentify.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewIdentify.TabIndex = 5;
            this.listViewIdentify.UseCompatibleStateImageBehavior = false;
            this.listViewIdentify.View = System.Windows.Forms.View.Details;
            // 
            // chType
            // 
            this.chType.Text = "Type";
            this.chType.Width = 190;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Item";
            this.columnHeader1.Width = 157;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "QL";
            this.columnHeader2.Width = 32;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Character";
            this.columnHeader3.Width = 103;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Backpack";
            this.columnHeader4.Width = 101;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Location";
            this.columnHeader5.Width = 95;
            // 
            // identifyViewContext
            // 
            this.identifyViewContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewItemToolStripMenuItem2,
            this.viewBackpackToolStripMenuItem});
            this.identifyViewContext.Name = "identifyViewContext";
            this.identifyViewContext.Size = new System.Drawing.Size(153, 48);
            // 
            // viewItemToolStripMenuItem2
            // 
            this.viewItemToolStripMenuItem2.Name = "viewItemToolStripMenuItem2";
            this.viewItemToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.viewItemToolStripMenuItem2.Text = "View Item";
            this.viewItemToolStripMenuItem2.Click += new System.EventHandler(this.viewItemToolStripMenuItem2_Click);
            // 
            // viewBackpackToolStripMenuItem
            // 
            this.viewBackpackToolStripMenuItem.Name = "viewBackpackToolStripMenuItem";
            this.viewBackpackToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.viewBackpackToolStripMenuItem.Text = "View Backpack";
            this.viewBackpackToolStripMenuItem.Click += new System.EventHandler(this.viewBackpackToolStripMenuItem_Click);
            // 
            // tabPagePatterns
            // 
            this.tabPagePatterns.BackColor = System.Drawing.SystemColors.Control;
            this.tabPagePatterns.Controls.Add(this.listViewPatterns);
            this.tabPagePatterns.Controls.Add(this.webBrowserPatterns);
            this.tabPagePatterns.Controls.Add(this.comboBoxCharacters);
            this.tabPagePatterns.Controls.Add(this.label3);
            this.tabPagePatterns.Controls.Add(this.label2);
            this.tabPagePatterns.Controls.Add(this.groupBox1);
            this.tabPagePatterns.Location = new System.Drawing.Point(4, 22);
            this.tabPagePatterns.Name = "tabPagePatterns";
            this.tabPagePatterns.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePatterns.Size = new System.Drawing.Size(1058, 546);
            this.tabPagePatterns.TabIndex = 2;
            this.tabPagePatterns.Text = "Patterns";
            this.tabPagePatterns.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // listViewPatterns
            // 
            this.listViewPatterns.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listViewPatterns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderPBName,
            this.columnHeaderPBAvailability});
            this.listViewPatterns.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewPatterns.Location = new System.Drawing.Point(6, 209);
            this.listViewPatterns.Name = "listViewPatterns";
            this.listViewPatterns.Size = new System.Drawing.Size(213, 334);
            this.listViewPatterns.TabIndex = 6;
            this.listViewPatterns.UseCompatibleStateImageBehavior = false;
            this.listViewPatterns.View = System.Windows.Forms.View.Details;
            this.listViewPatterns.SelectedIndexChanged += new System.EventHandler(this.listViewPatterns_SelectedIndexChanged);
            // 
            // columnHeaderPBName
            // 
            this.columnHeaderPBName.Text = "Boss";
            this.columnHeaderPBName.Width = 145;
            // 
            // columnHeaderPBAvailability
            // 
            this.columnHeaderPBAvailability.Text = "Availability";
            this.columnHeaderPBAvailability.Width = 45;
            // 
            // webBrowserPatterns
            // 
            this.webBrowserPatterns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowserPatterns.IsWebBrowserContextMenuEnabled = false;
            this.webBrowserPatterns.Location = new System.Drawing.Point(225, 0);
            this.webBrowserPatterns.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.webBrowserPatterns.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowserPatterns.Name = "webBrowserPatterns";
            this.webBrowserPatterns.Size = new System.Drawing.Size(834, 546);
            this.webBrowserPatterns.TabIndex = 5;
            this.webBrowserPatterns.Url = new System.Uri("about:blank", System.UriKind.Absolute);
            // 
            // comboBoxCharacters
            // 
            this.comboBoxCharacters.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCharacters.FormattingEnabled = true;
            this.comboBoxCharacters.Location = new System.Drawing.Point(68, 27);
            this.comboBoxCharacters.Name = "comboBoxCharacters";
            this.comboBoxCharacters.Size = new System.Drawing.Size(151, 21);
            this.comboBoxCharacters.TabIndex = 4;
            this.comboBoxCharacters.SelectedIndexChanged += new System.EventHandler(this.comboBoxCharacters_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Character:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Filter settings:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.patternClanGarden);
            this.groupBox1.Controls.Add(this.patternShowAll);
            this.groupBox1.Controls.Add(this.patternOmniGarden);
            this.groupBox1.Controls.Add(this.patternShowPartial);
            this.groupBox1.Controls.Add(this.patternShowCompletables);
            this.groupBox1.Location = new System.Drawing.Point(9, 56);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(210, 144);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Patterns";
            // 
            // patternClanGarden
            // 
            this.patternClanGarden.AutoSize = true;
            this.patternClanGarden.Location = new System.Drawing.Point(6, 111);
            this.patternClanGarden.Name = "patternClanGarden";
            this.patternClanGarden.Size = new System.Drawing.Size(93, 17);
            this.patternClanGarden.TabIndex = 9;
            this.patternClanGarden.Text = "Inferno G Clan";
            this.patternClanGarden.UseVisualStyleBackColor = true;
            this.patternClanGarden.CheckedChanged += new System.EventHandler(this.patternClanGarden_CheckedChanged);
            // 
            // patternShowAll
            // 
            this.patternShowAll.AutoSize = true;
            this.patternShowAll.Checked = true;
            this.patternShowAll.Location = new System.Drawing.Point(6, 19);
            this.patternShowAll.Name = "patternShowAll";
            this.patternShowAll.Size = new System.Drawing.Size(66, 17);
            this.patternShowAll.TabIndex = 5;
            this.patternShowAll.TabStop = true;
            this.patternShowAll.Text = "Show All";
            this.patternShowAll.UseVisualStyleBackColor = true;
            this.patternShowAll.CheckedChanged += new System.EventHandler(this.patternShowAll_CheckedChanged);
            // 
            // patternOmniGarden
            // 
            this.patternOmniGarden.AutoSize = true;
            this.patternOmniGarden.Location = new System.Drawing.Point(6, 88);
            this.patternOmniGarden.Name = "patternOmniGarden";
            this.patternOmniGarden.Size = new System.Drawing.Size(96, 17);
            this.patternOmniGarden.TabIndex = 8;
            this.patternOmniGarden.Text = "Inferno G Omni";
            this.patternOmniGarden.UseVisualStyleBackColor = true;
            this.patternOmniGarden.CheckedChanged += new System.EventHandler(this.patternOmniGarden_CheckedChanged);
            // 
            // patternShowPartial
            // 
            this.patternShowPartial.AutoSize = true;
            this.patternShowPartial.Location = new System.Drawing.Point(6, 42);
            this.patternShowPartial.Name = "patternShowPartial";
            this.patternShowPartial.Size = new System.Drawing.Size(89, 17);
            this.patternShowPartial.TabIndex = 6;
            this.patternShowPartial.Text = "Show Partials";
            this.patternShowPartial.UseVisualStyleBackColor = true;
            this.patternShowPartial.CheckedChanged += new System.EventHandler(this.patternShowPartial_CheckedChanged);
            // 
            // patternShowCompletables
            // 
            this.patternShowCompletables.AutoSize = true;
            this.patternShowCompletables.Location = new System.Drawing.Point(6, 65);
            this.patternShowCompletables.Name = "patternShowCompletables";
            this.patternShowCompletables.Size = new System.Drawing.Size(118, 17);
            this.patternShowCompletables.TabIndex = 7;
            this.patternShowCompletables.Text = "Show Completables";
            this.patternShowCompletables.UseVisualStyleBackColor = true;
            this.patternShowCompletables.CheckedChanged += new System.EventHandler(this.patternShowCompletables_CheckedChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.buffItemsPanel);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1058, 546);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Buffs";
            this.tabPage4.UseVisualStyleBackColor = true;
            this.tabPage4.Click += new System.EventHandler(this.tabPage4_Click);
            // 
            // buffItemsPanel
            // 
            this.buffItemsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buffItemsPanel.Location = new System.Drawing.Point(-1, 1);
            this.buffItemsPanel.Name = "buffItemsPanel";
            this.buffItemsPanel.Size = new System.Drawing.Size(1061, 545);
            this.buffItemsPanel.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.symbiantMatcherPanel);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1058, 546);
            this.tabPage3.TabIndex = 5;
            this.tabPage3.Text = "Symbiants";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // symbiantMatcherPanel
            // 
            this.symbiantMatcherPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.symbiantMatcherPanel.BackColor = System.Drawing.Color.Transparent;
            this.symbiantMatcherPanel.Location = new System.Drawing.Point(-4, 0);
            this.symbiantMatcherPanel.Name = "symbiantMatcherPanel";
            this.symbiantMatcherPanel.Size = new System.Drawing.Size(1062, 550);
            this.symbiantMatcherPanel.TabIndex = 22;
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.settingsPanel);
            this.tabSettings.Location = new System.Drawing.Point(4, 22);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabSettings.Size = new System.Drawing.Size(1058, 546);
            this.tabSettings.TabIndex = 6;
            this.tabSettings.Text = "Settings";
            this.tabSettings.UseVisualStyleBackColor = true;
            // 
            // settingsPanel
            // 
            this.settingsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.settingsPanel.Location = new System.Drawing.Point(-4, 0);
            this.settingsPanel.Name = "settingsPanel";
            this.settingsPanel.Size = new System.Drawing.Size(1062, 550);
            this.settingsPanel.TabIndex = 0;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 625);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindow";
            this.Text = "IA:Light";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.treeViewContext.ResumeLayout(false);
            this.listviewContextMenu.ResumeLayout(false);
            this.trayIconContext.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.identifyViewContext.ResumeLayout(false);
            this.tabPagePatterns.ResumeLayout(false);
            this.tabPagePatterns.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabSettings.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textboxSearch;
        private System.Windows.Forms.TreeView treeContainerView;
        private System.Windows.Forms.ListView listViewResults;
        private System.Windows.Forms.ColumnHeader columnItem;
        private System.Windows.Forms.ColumnHeader columnQL;
        private System.Windows.Forms.ColumnHeader columnCharacter;
        private System.Windows.Forms.ColumnHeader columnBackpack;
        private System.Windows.Forms.ColumnHeader columnLocation;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip listviewContextMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip treeViewContext;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem viewItemToolStripMenuItem;
        private System.Windows.Forms.ComboBox selectDimension;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView listViewIdentify;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ToolStripMenuItem renameF2ToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPagePatterns;
        private System.Windows.Forms.ComboBox comboBoxCharacters;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton patternClanGarden;
        private System.Windows.Forms.RadioButton patternShowAll;
        private System.Windows.Forms.RadioButton patternOmniGarden;
        private System.Windows.Forms.RadioButton patternShowPartial;
        private System.Windows.Forms.RadioButton patternShowCompletables;
        private System.Windows.Forms.WebBrowser webBrowserPatterns;
        private System.Windows.Forms.ListView listViewPatterns;
        private System.Windows.Forms.ColumnHeader columnHeaderPBName;
        private System.Windows.Forms.ColumnHeader columnHeaderPBAvailability;
        private System.Windows.Forms.ContextMenuStrip trayIconContext;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.WebBrowser webBrowserSummary;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem archieveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkForUpdatesToolStripMenuItem;
        private System.Windows.Forms.TextBox tbMaxQl;
        private System.Windows.Forms.TextBox tbMinQl;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ColumnHeader chType;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ContextMenuStrip identifyViewContext;
        private System.Windows.Forms.ToolStripMenuItem viewItemToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem viewBackpackToolStripMenuItem;
        private System.Windows.Forms.Panel symbiantMatcherPanel;
        private System.Windows.Forms.ComboBox comboBoxCharactersItemview;
        private System.Windows.Forms.Panel buffItemsPanel;
        private System.Windows.Forms.ToolStripMenuItem writeToScriptsToolStripMenuItem;
        private System.Windows.Forms.TabPage tabSettings;
        private System.Windows.Forms.Panel settingsPanel;
        
    }
}