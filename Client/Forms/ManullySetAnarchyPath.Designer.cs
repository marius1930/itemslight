﻿namespace ItemsLight {
    partial class ManullySetAnarchyPath {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManullySetAnarchyPath));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonBrowse = new FirefoxButton();
            this.firefoxH11 = new FirefoxH1();
            this.buttonOk = new FirefoxButton();
            this.buttonCancel = new FirefoxButton();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(10, 56);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(297, 20);
            this.textBox1.TabIndex = 6;
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.EnabledCalc = true;
            this.buttonBrowse.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.buttonBrowse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(68)))), ((int)(((byte)(80)))));
            this.buttonBrowse.Location = new System.Drawing.Point(313, 56);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(87, 20);
            this.buttonBrowse.TabIndex = 13;
            this.buttonBrowse.Text = "Browse..";
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click_1);
            // 
            // firefoxH11
            // 
            this.firefoxH11.AutoSize = true;
            this.firefoxH11.Font = new System.Drawing.Font("Segoe UI Semibold", 20F);
            this.firefoxH11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(88)))), ((int)(((byte)(100)))));
            this.firefoxH11.Location = new System.Drawing.Point(3, 3);
            this.firefoxH11.Name = "firefoxH11";
            this.firefoxH11.Size = new System.Drawing.Size(429, 37);
            this.firefoxH11.TabIndex = 12;
            this.firefoxH11.Text = "Configure path to Anarchy Online";
            // 
            // buttonOk
            // 
            this.buttonOk.EnabledCalc = true;
            this.buttonOk.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.buttonOk.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(68)))), ((int)(((byte)(80)))));
            this.buttonOk.Location = new System.Drawing.Point(10, 93);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(192, 32);
            this.buttonOk.TabIndex = 11;
            this.buttonOk.Text = "Save";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click_1);
            // 
            // buttonCancel
            // 
            this.buttonCancel.EnabledCalc = true;
            this.buttonCancel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.buttonCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(68)))), ((int)(((byte)(80)))));
            this.buttonCancel.Location = new System.Drawing.Point(208, 93);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(192, 32);
            this.buttonCancel.TabIndex = 10;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // ManullySetAnarchyPath
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 139);
            this.Controls.Add(this.buttonBrowse);
            this.Controls.Add(this.firefoxH11);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ManullySetAnarchyPath";
            this.Text = "Configure path to Anarchy Online";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ManullySetAnarchyPath_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private FirefoxButton buttonCancel;
        private FirefoxButton buttonOk;
        private FirefoxH1 firefoxH11;
        private FirefoxButton buttonBrowse;
    }
}