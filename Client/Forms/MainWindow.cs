﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.Concurrent;
using System.Runtime.InteropServices;
using System.IO;
using log4net;
using ItemsLight.AOIA;
using System.Reflection;
using AutoUpdaterDotNET;
using System.Diagnostics;
using ItemsLight.Model;
using ItemsLight.AOIA.HelperStructs;
using ItemsLight.Cloud;
using ItemsLight.Forms;
using EvilsoftCommons.DllInjector;
using EvilsoftCommons.Exceptions;
using EvilsoftCommons.UI;


namespace ItemsLight {
    public struct ByteAndType {
        public byte[] Data;
        public int Type;
    }


    public partial class MainWindow : Form {
        ConcurrentQueue<MessageDataRequest> requestQueue;
        ConcurrentQueue<MessageData> resultQueue;
        private static ConcurrentQueue<ByteAndType> messageQueue;
        
        private RegisterWindow window;
        Action<RegisterWindow.DataAndType> registerWindowDelegate;

        private delegate void InvokeDelegate();
        private static ILog logger = LogManager.GetLogger(typeof(MainWindow));
        private LoadingScreenManager loadingScreen;

        private System.Windows.Forms.Timer m_delayedTextChangedTimer;
        private ListViewColumnSorterQL columnSorterMainView;
        private ListViewColumnSorter columnSorterIdentifyView;
        private ListViewColumnSorter columnSorterPatternList;
        private Boolean canExit = false;
        private bool canExitOnWorkerThread = false;
        private DateTime lastAutomaticUpdateCheck = default(DateTime);

        private System.Timers.Timer timerProcessMessageQueue;

        private TextboxHoverFocusHighlight searchBarHighlight;
        private int lastSelectedDimensionIndex = 0;

        private MainBackgroundThread workerThread;
        private int comboBoxCharactersLastSelectedIndex = 0;
        private int comboBoxCharactersItemviewLastSelectedIndex = 0;
        private SymbiantMatcher matcher;
        private BuffItems buffItemsTab;
        private const string UPDATE_XML = "http://ribbs.dreamcrash.org/ialight/version2.xml";


        private int _dimension;
        private int Dimension {
            get {
                return _dimension;
            }
            set {
                _dimension = value;
                if (buffItemsTab != null)
                    buffItemsTab.Dimension = value;
            }
        }

        private void CustomWndProc(RegisterWindow.DataAndType bt) {
            messageQueue.Enqueue(new ByteAndType { Data = bt.Data, Type = bt.Type });
        }



        public MainWindow() {
            InitializeComponent();
            this.requestQueue = new ConcurrentQueue<MessageDataRequest>();
            this.resultQueue = new ConcurrentQueue<MessageData>();
            MainWindow.messageQueue = new ConcurrentQueue<ByteAndType>();

            registerWindowDelegate = CustomWndProc;
            window = new RegisterWindow("ItemAssistantWindowClass", registerWindowDelegate);


            this.SizeChanged += OnMinimizeWindow;
            this.KeyPreview = true;
            this.KeyUp += new KeyEventHandler(KeyEvent);

            // Start minimized
            /*notifyIcon1.Visible = true;
            notifyIcon1.BalloonTipText = "Test";
            notifyIcon1.ShowBalloonTip(500);*/

            // Sorting for main view
            columnSorterMainView = new ListViewColumnSorterQL();
            this.listViewResults.ListViewItemSorter = columnSorterMainView;
            this.listViewResults.ColumnClick += listViewResults_ColumnClick;
            
            // Sorting for identify view
            columnSorterIdentifyView = new ListViewColumnSorterIdentifyView();
            this.listViewIdentify.ListViewItemSorter = columnSorterIdentifyView;
            this.listViewIdentify.ColumnClick += listViewIdentify_ColumnClick;
            columnSorterIdentifyView.Order = SortOrder.Ascending;
            this.listViewIdentify.Sort();

            columnSorterPatternList = new ListViewColumnSorter();
            this.listViewPatterns.ListViewItemSorter = columnSorterPatternList;
            this.listViewPatterns.ColumnClick += listViewPatterns_ColumnClick;



            

            this.FormClosing += MainWindow_FormClosing;


            // Initialize dimensions to "any"
            selectDimension.Items.Add(new ComboBoxItem(new Character { Id = -1, Name = "Any" }));
            selectDimension.SelectedIndex = 0;
            

            // Selection change on results should show N of M items
            listViewResults.ItemSelectionChanged += listViewResults_ItemSelectionChanged;
            listViewIdentify.ItemSelectionChanged += listViewIdentify_ItemSelectionChanged;
            

            // Initial character, to not have an empty list
            comboBoxCharacters.Items.Add(new ComboBoxItem(new Character { Id = 0, Name = "-" }));
            comboBoxCharacters.SelectedItem = 0;


            comboBoxCharactersItemview.Items.Add(new ComboBoxItem(new Character { Id = 0, Name = "-" }));
            comboBoxCharactersItemview.SelectedItem = 0;

                                    
                        



            // Open links in a new window (web browser, patterns)
            this.webBrowserPatterns.Navigating += webBrowserPatterns_Navigating;
            this.webBrowserSummary.Navigating += webBrowserSummary_Navigating;
            
            
            // Highlight search bar on focus/mouse-over
            searchBarHighlight = new TextboxHoverFocusHighlight(textboxSearch);
                        
        }

        void webBrowserGMI_Navigated(object sender, WebBrowserNavigatedEventArgs e) {
            // http://stackoverflow.com/questions/12587728/changing-the-html-in-a-webbrowser-before-it-is-displayed-to-the-user
        }

        void webBrowserSummary_Navigating(object sender, WebBrowserNavigatingEventArgs e) {
            if (!e.Url.ToString().Contains("blank")) {
                e.Cancel = true;
            }
            if (e.Url.ToString().Contains("refresh")) {
                RequestRefreshSummary();
            }
        }



        void listViewResults_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e) {
            toolStripStatusLabel.Text = string.Format("{0} of {1} items selected", listViewResults.SelectedItems.Count, listViewResults.Items.Count);
        }
        void listViewIdentify_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e) {
            toolStripStatusLabel.Text = string.Format("{0} of {1} items selected", listViewIdentify.SelectedItems.Count, listViewIdentify.Items.Count);
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e) {
            if (e.CloseReason == CloseReason.UserClosing) {

                // Either the worker thread has signaled OK, or the user has requested it twice.
                if (canExitOnWorkerThread || workerThread == null) {
                    e.Cancel = false;
                }
                else {
                    if (!canExit && (bool)Properties.Settings.Default["closetotray"]) {
                        this.Hide();
                        loadingScreen.Hide();

                        notifyIcon1.Visible = true;
                        e.Cancel = true;
                    }
                    else {
                        try {
                            if (workerThread != null)
                                workerThread.Dispose(); 
                        }
                        catch (Exception) { }
                        workerThread = null;
                        timerProcessMessageQueue.Stop();
                        e.Cancel = true;
                    }
                }
            }
        }



        /// <summary>
        /// Sort search results / backpack contents
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listViewResults_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e) {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == columnSorterMainView.SortColumn) {
                // Reverse the current sort direction for this column.
                if (columnSorterMainView.Order == SortOrder.Ascending) {
                    columnSorterMainView.Order = SortOrder.Descending;
                }
                else {
                    columnSorterMainView.Order = SortOrder.Ascending;
                }
            }
            else {
                // Set the column number that is to be sorted; default to ascending.
                columnSorterMainView.SortColumn = e.Column;
                columnSorterMainView.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            this.listViewResults.Sort();
        }



        private void listViewPatterns_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e) {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == columnSorterPatternList.SortColumn) {
                // Reverse the current sort direction for this column.
                if (columnSorterPatternList.Order == SortOrder.Ascending) {
                    columnSorterPatternList.Order = SortOrder.Descending;
                }
                else {
                    columnSorterPatternList.Order = SortOrder.Ascending;
                }
            }
            else {
                // Set the column number that is to be sorted; default to ascending.
                columnSorterPatternList.SortColumn = e.Column;
                columnSorterPatternList.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            this.listViewPatterns.Sort();
        }

        
        /// <summary>
        /// Sort the items in the Identify view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listViewIdentify_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e) {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == columnSorterIdentifyView.SortColumn) {
                // Reverse the current sort direction for this column.
                if (columnSorterIdentifyView.Order == SortOrder.Ascending) {
                    columnSorterIdentifyView.Order = SortOrder.Descending;
                }
                else {
                    columnSorterIdentifyView.Order = SortOrder.Ascending;
                }
            }
            else {
                // Set the column number that is to be sorted; default to ascending.
                columnSorterIdentifyView.SortColumn = e.Column;
                columnSorterIdentifyView.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            this.listViewIdentify.Sort();
        }


        private void TimedMessageProcessing(Object source, System.Timers.ElapsedEventArgs e) {
            processMessagesInQueueDELEGATED();
        }

        private void textboxSearch_TextChanged(object sender, EventArgs e) {
            var value = textboxSearch.Text;
            if (value.Length >= 3) {
                if (m_delayedTextChangedTimer != null) {
                    m_delayedTextChangedTimer.Stop();
                }

                m_delayedTextChangedTimer = new System.Windows.Forms.Timer();
                m_delayedTextChangedTimer.Tick += new EventHandler(HandleDelayedTextChangedTimerTick);
                m_delayedTextChangedTimer.Interval = 800;
                m_delayedTextChangedTimer.Start();
            }
            
        }

        private void HandleDelayedTextChangedTimerTick(object sender, EventArgs e) {
            if (m_delayedTextChangedTimer != null) {
                m_delayedTextChangedTimer.Stop();
                m_delayedTextChangedTimer = null;
            }

            var value = textboxSearch.Text;
            if (value.Length >= 3) {
                int minQl;
                int maxQl;
                if (!int.TryParse(tbMinQl.Text, out minQl))
                    minQl = -5000;
                if (!int.TryParse(tbMaxQl.Text, out maxQl))
                    maxQl = 5000;

                long characterId = getSelectedCharacterForItemSearch();
                
                int dimension = Dimension;
                if (characterId > 0)
                    dimension = -1;
                
                // Send the dimension, if any.
                // If somehow searching before the dimensions update, this may be none.                
                requestQueue.Enqueue(new MessageDataRequest { 
                    Type = RequestMessage.SEARCH_ITEM,
                    Data = new SearchRequest { Search = value, CharacterId = characterId, Dimension = dimension, MinQL = minQl, MaxQL = maxQl }
                });
                

                logger.Info("Searching on criteria: " + value);
            }
        }


        /// <summary>
        /// Process all messages received from the worker thread
        /// </summary>
        private void processMessagesInQueue() {

            try {
                while (MessagePump.pump(window.hwnd)) { /* Process all messages from AO */ }
            }
            catch (OutOfMemoryException e) {
                ExceptionReporter.ReportException(e, "MesssagePump");
            }
            
            if (!resultQueue.IsEmpty) {
                MessageData obj;
                if (resultQueue.TryDequeue(out obj)) {
                    
                    try {
                        switch (obj.Type) {
                            case MessageDataType.RES_SEARCH_RESULT:
                                UpdateContentFromContainer((List<Item>)obj.Data);
                                break;

                            case MessageDataType.RES_CHARACTER_TREE:
                                updateContainerTree((List<ContainerTreeNode>)obj.Data);
                                break;

                            case MessageDataType.RES_SET_TREEVIEW_CHILDREN:
                                UpdateTreeviewChildren((ContainerUpdateNode)obj.Data);
                                break;

                            case MessageDataType.RES_SET_TREEVIEW_SELECTION:
                                // Select the given bag
                                {
                                    CharacterAndContainer data = (CharacterAndContainer)obj.Data;
                                    TreeviewSelectContainer(treeContainerView.Nodes, data.ContainerId, data.CharacterId);
                                    tabControl1.SelectedTab = tabPage1;
                                }
                                break;

                            case MessageDataType.RES_GMI_SALE:
                                {
                                    GMISale sale = (GMISale)obj.Data;
                                    notifyIcon1.Visible = true;
                                    notifyIcon1.BalloonTipText = string.Format("{0} sold an item on GMI for {1}", sale.name, ScriptUtilities.FormatAOCurrency(sale.amount));
                                    notifyIcon1.ShowBalloonTip(500);
                                }
                                break;

                            case MessageDataType.RES_CONTAINER_DATA:
                                UpdateContentFromContainer((List<Item>)obj.Data);
                                break;

                            case MessageDataType.RES_UPDATE_SUMMARY: 
                                {
                                    UpdateSummaryHtml((List<Character>)obj.Data);

                                    if ((bool)Properties.Settings.Default["summaryscript"]) {
                                        WriteSummaryToScripts f = new WriteSummaryToScripts((List<Character>)obj.Data);
                                        f.Show();
                                    }
                                }

                                break;

                            case MessageDataType.RES_SET_STATUS_MESSAGE:
                                this.toolStripStatusLabel.Text = (string)obj.Data;
                                break;

                            case MessageDataType.RES_CHARACTER:
                                matcher.Character = (Character)obj.Data;
                                break;

                            case MessageDataType.RES_SHOW_EQUIPPED_ITEM:
                                matcher.EquippedItem =  obj.Data == null ? null : (Item)obj.Data;
                                break;

                            case MessageDataType.RES_READY_TO_BEGIN: 
                                {
                                    logger.Debug("Loading screen discarded..");
                                    textboxSearch.Enabled = true;
                                    listViewResults.Enabled = true;
                                    treeContainerView.Enabled = true;
                                    loadingScreen.Dispose();

#if !DEBUG
                                    CloudWatcher provider = new CloudWatcher();
                                    CloudProviderEnum currentProvider = (CloudProviderEnum)Properties.Settings.Default["cloud"];
                                    if (provider.Providers.Count > 0 && currentProvider == CloudProviderEnum.UNINITIALIZED) {
                                        CloudPicker picker = new CloudPicker(requestQueue);
                                        picker.ShowDialog();
                                    }
#endif
                                }
                                break;

                            case MessageDataType.RES_SHOW_PROGRESSBAR: {
                                    logger.Debug("Loading screen requested..");
                                    textboxSearch.Enabled = false;
                                    listViewResults.Enabled = false;
                                    treeContainerView.Enabled = false;

                                    loadingScreen.Show(obj.Data as string);

                                }
                                break;

                            case MessageDataType.RES_LOAD_SINGLE_PATTERN: {
                                    PatternLookupResult result = (PatternLookupResult)obj.Data;
                                    UpdatePatternWebview(result.Name, result.Pieces, result.Total, result.Drops, result.Locations);
                                }
                                break;

                            case MessageDataType.RES_SET_DIMENSIONS: 
                                {
                                    selectDimension.Items.Clear();
                                    List<Dimension> dimensions = (List<Dimension>)obj.Data;

                                    int preferredDimensionIndex = 0;
                                    int i = 0;
                                    foreach (Dimension dim in dimensions) {
                                        selectDimension.Items.Add(new ComboBoxItem(dim.Id, dim.name));
                                        if (dim.Id == 13)
                                            preferredDimensionIndex = i;
                                        i++;
                                    }
                                    selectDimension.SelectedIndex = preferredDimensionIndex;
                                }
                                break;

                            case MessageDataType.RES_BUFF_ITEMS: 
                                {
                                    buffItemsTab.UpdateBuffItems((List<Item>)obj.Data);
                                }
                                break;

                            case MessageDataType.RES_SHOW_SYMBIANTS:
                                matcher.Result = (IList<Item>)obj.Data;
                                break;

                            case MessageDataType.RES_IDENTIFY_DATA: 
                                {
                                    List<Item> items = (List<Item>)obj.Data;
                                    List<ListViewItem> temp = new List<ListViewItem>();
                                    foreach (var item in items) {
                                        ListViewItem lvi = new ListViewItem(item.Extras);
                                        lvi.SubItems.Add(item.Name);
                                        lvi.Tag = string.Format("{0}:{1}:{2}", item.ParentContainerId, item.LowKey, item.CharacterId);
                                        lvi.SubItems.Add("" + item.QL);
                                        lvi.SubItems.Add(item.Owner.Name);
                                        lvi.SubItems.Add(item.ParentContainerName);
                                        lvi.SubItems.Add(item.ParentInventory);

                                        temp.Add(lvi);
                                    }

                                    listViewIdentify.BeginUpdate();
                                    listViewIdentify.Items.Clear();
                                    listViewIdentify.Items.AddRange(temp.ToArray());
                                    listViewIdentify.EndUpdate();

                                    toolStripStatusLabel.Text = string.Format("{0} of {1} items selected", 0, items.Count);
                                }
                                break;

                            case MessageDataType.RES_SET_CHARACTER_LIST:
                                {
                                    var items = (List<ComboBoxItem>)obj.Data;
                                    items.Insert(0, new ComboBoxItem(new Character { Id = 0, Name = "-" }));
                                    /*
                                    foreach (var item in (List<ComboBoxItem>)obj.Data) {
                                        comboBoxCharacters.Items.Add(item);
                                        comboBoxCharactersItemview.Items.Add(item);
                                    }*/

                                    var arr = items.ToArray();


                                    comboBoxCharacters.Items.Clear();
                                    comboBoxCharacters.Items.AddRange(arr);
                                    comboBoxCharacters.SelectedIndex = 0;


                                    comboBoxCharactersItemview.Items.Clear();
                                    comboBoxCharactersItemview.Items.AddRange(arr);
                                    comboBoxCharactersItemview.SelectedIndex = 0;

                                    // Update the duplicate control in the pattern matcher
                                    if (matcher != null) {
                                        matcher.Characters = arr;
                                    }
                                }
                                break;


                            case MessageDataType.RES_UPDATE_PATTERNS: 
                                {
                                    Dictionary<string, Pattern> patterns = (Dictionary<string, Pattern>)obj.Data;
                                    listViewPatterns.BeginUpdate();
                                    listViewPatterns.Items.Clear();
                                    foreach (var key in patterns.Keys) {
                                        ListViewItem lvi = new ListViewItem(key);
                                        lvi.SubItems.Add("" + patterns[key].Total);

                                        listViewPatterns.Items.Add(lvi);
                                    }
                                    listViewPatterns.EndUpdate();
                                }
                                break;

                        }
                    }
                    catch (OutOfMemoryException e) {
                        ExceptionReporter.ReportException(e, "ProcessMessage " + obj.Type);
                    }
                }
            }
        }



        /// <summary>
        /// Process all messages received from the worker thread
        /// DELEGATED
        /// </summary>
        private void processMessagesInQueueDELEGATED() {


            try {
                if (resultQueue.Count > 0) {
                    try {
                        if (this.InvokeRequired) {
                            this.BeginInvoke(new InvokeDelegate(processMessagesInQueue));
                        }
                        else
                            processMessagesInQueue();
                    }
                    catch (OutOfMemoryException e) {
                        ExceptionReporter.ReportException(e);
                    }
                }
            }
            catch (Exception e) {
                logger.Warn(e.Message + e.StackTrace);
                ExceptionReporter.ReportException(e);
            }
        }


        /// <summary>
        /// Update the container view
        /// </summary>
        /// <param name="items"></param>
        private void UpdateContentFromContainer(List<Item> items) {
            logger.DebugFormat("Updating listview with {0} items", items.Count);
            List<ListViewItem> temp = new List<ListViewItem>();

            foreach (var item in items) {
                var name = item.Name;
                ListViewItem lvi = new ListViewItem(name);
                lvi.Tag = string.Format("{0}:{1}:{2}", item.ParentContainerId, item.LowKey, item.Id);
                lvi.SubItems.Add("" + item.QL);
                lvi.SubItems.Add(item.Owner.Name);
                lvi.SubItems.Add(item.ParentContainerName);
                lvi.SubItems.Add(item.ParentInventory);
                temp.Add(lvi);
            }

            
            listViewResults.BeginUpdate();
            listViewResults.Items.Clear();
            listViewResults.Items.AddRange(temp.ToArray());
            listViewResults.EndUpdate();

            listViewResults_ItemSelectionChanged(null, null);
            tabControl1.SelectedTab = tabPage1;
        }


        /// <summary>
        /// Get node expansion state before refreshing a treeview, to retain selection
        /// </summary>
        /// <param name="treeNodes"></param>
        /// <param name="expandedNodes"></param>
        void GetExpandedNodes(TreeNodeCollection treeNodes, List<CharacterAndContainer> expandedNodes) {
            foreach (TreeNode node in treeNodes) {
                if (node.IsExpanded) {
                    ContainerNode cnode = (ContainerNode)node;
                    expandedNodes.Add(new CharacterAndContainer { CharacterId = cnode.CharacterId, ContainerId = cnode.ContainerId });

                    if (node.Nodes != null && node.Nodes.Count > 0)
                        GetExpandedNodes(node.Nodes, expandedNodes);
                }
            }
        }

        /// <summary>
        /// Expand the previously expanded nodes, to retain selection
        /// </summary>
        /// <param name="treeNodes"></param>
        /// <param name="expandedNodes"></param>
        void SetExpandedNodes(TreeNodeCollection treeNodes, List<CharacterAndContainer> expandedNodes) {
            foreach (TreeNode node in treeNodes) {
                ContainerNode cnode = (ContainerNode)node;

                foreach (var enode in expandedNodes) {
                    if (enode.CharacterId == cnode.CharacterId && enode.ContainerId == cnode.ContainerId)
                        cnode.Expand();
                }

                if (node.Nodes != null && node.Nodes.Count > 0)
                    SetExpandedNodes(node.Nodes, expandedNodes);

            }
        }
        void TreeviewSelectContainer(TreeNodeCollection treeNodes, long containerId, long characterId) {
            foreach (TreeNode node in treeNodes) {
                ContainerNode cnode = (ContainerNode)node;


                if (containerId < 14 && characterId == cnode.CharacterId && containerId == cnode.ContainerId)
                    treeContainerView.SelectedNode = cnode;
                
                else if (containerId >= 14 && containerId == cnode.ContainerId)
                    treeContainerView.SelectedNode = cnode;
                
                else if (node.Nodes != null && node.Nodes.Count > 0)
                    TreeviewSelectContainer(node.Nodes, containerId, characterId);

            }
        }

        /// <summary>
        /// Update the character/inventory tree hierarchy 
        /// </summary>
        /// <param name="nodes"></param>
        private void updateContainerTree(List<ContainerTreeNode> nodes) {
            long containerId = 0;
            long characterId = 0;

            {
                ContainerNode node = treeContainerView.SelectedNode as ContainerNode;
                if (node != null) {
                    containerId = node.ContainerId;
                    characterId = node.CharacterId;
                }
            }
            List<CharacterAndContainer> expandedNodes = new List<CharacterAndContainer>();
            GetExpandedNodes(treeContainerView.Nodes, expandedNodes);

            treeContainerView.BeginUpdate();
            treeContainerView.Nodes.Clear();
            foreach (ContainerTreeNode node in nodes) {
                node.update(treeContainerView.Nodes);
            }
            treeContainerView.EndUpdate();

            
            TreeviewSelectContainer(treeContainerView.Nodes, containerId, characterId);
            SetExpandedNodes(treeContainerView.Nodes, expandedNodes);

        }


        private void treeContainerView_AfterSelect(object sender, TreeViewEventArgs e) {
            ContainerNode node = treeContainerView.SelectedNode as ContainerNode;
            if (node != null) {
                logger.Debug(string.Format("Clicked on tree: Container \"{0}\" = {1}, charid: {2}", node.Text, node.ContainerId, node.CharacterId));

                // If both container and character is 0, we've clicked on the account.
                if (!(node.ContainerId == 0 && node.CharacterId == 0)) {
                    requestQueue.Enqueue(new MessageDataRequest {
                        Type = RequestMessage.OPEN_CONTAINER,
                        Data = new CharacterAndContainer {
                            CharacterId = node.CharacterId,
                            ContainerId = node.ContainerId
                        }
                    });
                }
            }
        }


        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e) {
            this.Visible = true;
            notifyIcon1.Visible = false;
            this.WindowState = FormWindowState.Normal;
        }


        /// <summary>
        /// Minimize to tray
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMinimizeWindow(object sender, object e) {
            try {
                if (this.WindowState == FormWindowState.Minimized) {
                    this.Hide();
                    loadingScreen.Hide();

                    notifyIcon1.Visible = true;
                }
                else if (this.WindowState == FormWindowState.Normal) {
                    notifyIcon1.Visible = false;

                    loadingScreen.ShowIfAvailable();

                    Boolean canSendCrashreport = (bool)Properties.Settings.Default["canSendCrashreport"];
                    if (canSendCrashreport && (DateTime.Now - lastAutomaticUpdateCheck).TotalHours > 36) {
                        AutoUpdater.LetUserSelectRemindLater = true;
                        AutoUpdater.RemindLaterTimeSpan = RemindLaterFormat.Days;
                        AutoUpdater.RemindLaterAt = 7;
                        AutoUpdater.CheckForUpdateEvent -= AutoUpdater_CheckForUpdateEvent;
                        AutoUpdater.Start(UPDATE_XML);
                        lastAutomaticUpdateCheck = DateTime.Now;
                    }
                    else if (!canSendCrashreport) {
                        logger.Info("Automatic updates disabled due to configuration settings.");
                    }
                }
            }
            catch (Exception ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
            }
        }

        private void listViewResults_SelectedIndexChanged(object sender, EventArgs e) {

        }

        private void listviewContextMenu_Opening(object sender, CancelEventArgs e) {
            e.Cancel = listViewResults.SelectedItems.Count <= 0;
        }






        /// <summary>
        /// View this specific bag
        /// Activated by right click on context menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItem1_Click(object sender, EventArgs e) {
            
            try {
                if (listViewResults.SelectedItems.Count > 0) {
                    string[] tags = listViewResults.FocusedItem.Tag.ToString().Split(':');
                    
                    long containerId = long.Parse(tags[0]);
                    if (containerId > 100) {
                        requestQueue.Enqueue(new MessageDataRequest {
                            Type = RequestMessage.OPEN_CONTAINER,
                            Data = new CharacterAndContainer { CharacterId = 0, ContainerId = containerId }
                        });

                        // Select the given bag
                        TreeviewSelectContainer(treeContainerView.Nodes, containerId, 0);
                    }
                }
            }
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex, "toolStripMenuItem1_Click");
                MessageBox.Show("Something went horribly wrong!");
            }
        }


        /// <summary>
        /// Context -> View item details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void viewItemToolStripMenuItem_Click(object sender, EventArgs e) {
            try {
                if (listViewResults.SelectedItems.Count > 0) {
                    string[] tags = listViewResults.FocusedItem.Tag.ToString().Split(':');
                    if (tags.Length < 3) {
                        logger.WarnFormat("listViewResults tag length is {0}, expected 3", tags.Length);
                        ExceptionReporter.ReportIssue(string.Format("listViewResults tag length is {0}, expected 3, tag is {1}", tags.Length, listViewResults.FocusedItem.Tag));
                    }
                    else {
                        long itemId = long.Parse(tags[1]);
                        int ql = int.Parse(listViewResults.FocusedItem.SubItems[1].Text);
                        System.Diagnostics.Process.Start(GetItemDatabaseURL(itemId, ql));
                    }
                }
            }
            catch (System.ComponentModel.Win32Exception) {
                MessageBox.Show("Could not find a default browser to open website in");
            }
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex, "viewItemToolStripMenuItem_Click");
                MessageBox.Show("Something went horribly wrong!");
            }
        }


        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            canExit = true;
            this.Close();
        }


        /// <summary>
        /// Show context menu for treeview
        /// Hide the 'rename' functionality if a backpack is not selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeViewContext_Opening(object sender, CancelEventArgs e) {
            e.Cancel = false;
            ContainerNode node = treeContainerView.SelectedNode as ContainerNode;
            renameF2ToolStripMenuItem.Visible = (node != null && node.ContainerId > 20);
            deleteToolStripMenuItem.Visible = (node != null && node.ContainerId == 0 && node.CharacterId != 0);
            archieveToolStripMenuItem.Visible = (node != null && node.ContainerId == 0 && node.CharacterId != 0);
        }

        /// <summary>
        /// Refresh treeview (rclick on treeview context)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItem2_Click(object sender, EventArgs e) {
            requestQueue.Enqueue(new MessageDataRequest { Type = RequestMessage.REFRESH_TREEVIEW, Data = Dimension });
        }


        private void aboutToolStripMenuItem_Click(object sender, EventArgs e) {
            var version = Assembly.GetExecutingAssembly().GetName().Version;
            MessageBox.Show(
@"IA:Light by Eviltrox
Based on the original AOIA by Morten Fjeldstad

BETA VERSION (" + version.Major + "." + version.Minor + "." + version.Build + "." + version.Revision + ")"
                + "\nYour UUID is " + UUID.Value
                );
        }

        [DllImport("kernel32")]
        extern static UInt64 GetTickCount64();

        /// <summary>
        /// Start minimized
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Load(object sender, EventArgs e) {
            // Log exceptions
            ExceptionReporter.EnableLogUnhandledOnThread();

            this.loadingScreen = new LoadingScreenManager(resultQueue, this);
            SendRequest(RequestMessage.REFRESH_TREEVIEW, Dimension);

            if (System.Threading.Thread.CurrentThread.Name == null)
                System.Threading.Thread.CurrentThread.Name = "Main UI";


            // Start up the background worker
            StartWorkerThread();


            // Set a timer to process messages, in case the background thread is too busy. (there may be messages it never dispatched)
            timerProcessMessageQueue = new System.Timers.Timer();
            timerProcessMessageQueue.Elapsed += TimedMessageProcessing;
            timerProcessMessageQueue.Interval = 1000;
            timerProcessMessageQueue.AutoReset = true;
            timerProcessMessageQueue.Start();

            RequestRefreshSummary();

            string[] tipOfTheDay = {
                "Did you know you can use the /aoia command ingame to see available options?",
                "Did you know you can use the /ia command ingame to see available options?",
                "Did you know you can use /WP ingame to list your characters waypoint?",
                "Did you know you can use /summary ingame to see character details?",
                "Did you know you can rightclick on a backpack to rename it from within IA?",
                "Did you know you can click F2 to rename backpacks inside IA?",
                "Did you know I had to include \"Tip of the day\" because people were unaware of features?",
                "Did you know that any issues you may run into can be reported on the forum?",
                "Did you know that /raidloot can be used to create loot lists?",
                "Did you know that... oh wait, you do.."
            };
            toolStripStatusLabel.Text = tipOfTheDay[new Random().Next(tipOfTheDay.Length)];


            treeContainerView.NodeMouseClick += (s, args) => treeContainerView.SelectedNode = args.Node;

            loadingScreen.Show("");

            Boolean canSendCrashreport = (bool)Properties.Settings.Default["canSendCrashreport"];
            if (!canSendCrashreport) {
                logger.Info("Automatic updates disabled due to configuration settings.");
            }

            // Only check for automatic updates if the system has been running for >5minutes, eg not on startup.
            else if (GetTickCount64() > 5*60*1000 && (DateTime.Now - lastAutomaticUpdateCheck).TotalHours > 36) {
                AutoUpdater.LetUserSelectRemindLater = true;
                AutoUpdater.RemindLaterTimeSpan = RemindLaterFormat.Days;
                AutoUpdater.RemindLaterAt = 7;
                AutoUpdater.CheckForUpdateEvent -= AutoUpdater_CheckForUpdateEvent;
                AutoUpdater.Start(UPDATE_XML);
                lastAutomaticUpdateCheck = DateTime.Now;
            }

            System.Threading.ThreadPool.QueueUserWorkItem(m => ExceptionReporter.ReportUsage() );

            //treeContainerView.NodeMouseClick += treeContainerView_NodeMouseClick;
            treeContainerView.BeforeExpand += tv_BeforeExpand;

            // Update the pattern view [perhaps this should be done on the first navigate?]
            UpdatePatternViews();
                        


            // Set the symbiant matcher form inside the tab panel
            {
                matcher = new SymbiantMatcher(requestQueue, toolStripStatusLabel);
                matcher.TopLevel = false;
                symbiantMatcherPanel.Controls.Add(matcher);
                matcher.Show();
            }

            // Set the "Buff Items" form inside the tab panel
            {
                buffItemsTab = new BuffItems(toolStripStatusLabel, SendRequest);
                buffItemsTab.Dimension = Dimension;
                buffItemsTab.TopLevel = false;
                buffItemsPanel.Controls.Add(buffItemsTab);
                buffItemsTab.Show();
            }

            // Set the settings window to the panel
            {
                var m = new SettingsWindow(SendRequest);
                m.TopLevel = false;
                settingsPanel.Controls.Add(m);
                m.Show();
            }

        }


        /// <summary>
        /// Request the data for this node (unless it already has children)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void tv_BeforeExpand(object sender, TreeViewCancelEventArgs e) {
            ContainerNode node = (ContainerNode)e.Node;
            if (node.Nodes.Count == 0) {
                requestQueue.Enqueue(new MessageDataRequest {
                    Type = RequestMessage.GET_TREEVIEW_CHILDREN,
                    Data = new CharacterAndContainer { CharacterId = node.CharacterId, ContainerId = node.ContainerId }
                });
            }
            // If we clicked on the player, and the "Inventory" node has no children... assume its uninitialized
            else if (node.ContainerId == 0) {
                ContainerNode subnode = (ContainerNode)node.Nodes[0];
                if (subnode.ContainerId == 2 && subnode.Nodes.Count == 0) {
                    SendRequest(RequestMessage.GET_TREEVIEW_CHILDREN,
                        new CharacterAndContainer { CharacterId = node.CharacterId, ContainerId = 1 }
                    );
                    SendRequest(RequestMessage.GET_TREEVIEW_CHILDREN,
                        new CharacterAndContainer { CharacterId = node.CharacterId, ContainerId = 2 }
                    );

                }
            }
        }

        private void UpdateTreeviewChildren(ContainerUpdateNode data) {
            UpdateTreeviewChildren(treeContainerView.Nodes, data);
        }

        /// <summary>
        /// Recursively look for the given node to update (and update it)
        /// </summary>
        /// <param name="treeNodes"></param>
        /// <param name="data"></param>
        void UpdateTreeviewChildren(TreeNodeCollection treeNodes, ContainerUpdateNode data) {
            foreach (TreeNode node in treeNodes) {
                ContainerNode cnode = (ContainerNode)node;

                if (data.CharacterId == cnode.CharacterId && data.ContainerId == cnode.ContainerId) {
                    var abc = data.nodes.ToArray();
                    if (cnode.Nodes.Count == 0)
                        cnode.Nodes.AddRange(abc);
                }
                else {
                    if (node.Nodes != null && node.Nodes.Count > 0) {
                        UpdateTreeviewChildren(node.Nodes, data);
                    }
                }

            }
        }

        private void StartWorkerThread() {
            QueueGUI q = new QueueGUI() { 
                requestQueue = requestQueue, 
                messageQueue = messageQueue,
                resultQueue = resultQueue 
            };
            workerThread = new MainBackgroundThread(q, backgroundWorker1_RunWorkerCompleted, bw_ProgressChanged);

        }
        void bw_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            processMessagesInQueueDELEGATED();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            if (e != null && e.Error != null && e.Error.Message != null) {
                long memory = GC.GetTotalMemory(true);
                int numQueuedMessages = messageQueue.Count;
                string tmp = string.Format("alloc: {0}b, raw:byte[]: {1}, q:in: {2}, q:out: {3}", memory, messageQueue.Count, resultQueue.Count, requestQueue.Count);
                if (e.Error is OutOfMemoryException) {
                    logger.Fatal("Background worker failed!");
                    logger.Fatal(e.Error.Message);
                    logger.Fatal(e.Error.StackTrace);

                    ExceptionReporter.ReportException(e.Error, string.Format("Auto starting.. {0}", tmp), true);
                    StartWorkerThread();
                }
                else {
                    logger.Fatal("Background worker failed!");
                    logger.Fatal(e.Error.Message);
                    logger.Fatal(e.Error.StackTrace);
                    ExceptionReporter.ReportException(e.Error, string.Format("Total worker crash, {0}", tmp), true);

                    if (MessageBox.Show(string.Format("Fatal error - IA worker thread has crashed.\n{0}\n\n\nDo you wish to try to continue running AOIA?", e.Error.Message), "Error", MessageBoxButtons.OKCancel) == DialogResult.OK) {
                        StartWorkerThread();
                    }
                    else {
                        canExitOnWorkerThread = true;
                        this.Close();
                    }
                }
            }
            else {
                canExitOnWorkerThread = true;
                this.Close();
            }
        }


        void LoadingScreenClosed(object sender, FormClosedEventArgs e) {
            //this.loadingScreen = null;
        }


        private void selectDimension_SelectedIndexChanged(object sender, EventArgs e) {
            if (selectDimension.SelectedItem != null) {
                Dimension = (int)((ComboBoxItem)selectDimension.SelectedItem).Value;
            }
            else {
                Dimension = -1;
            }

            if (selectDimension.SelectedIndex != lastSelectedDimensionIndex) {
                SendRequest(RequestMessage.REFRESH_TREEVIEW, Dimension);
                lastSelectedDimensionIndex = selectDimension.SelectedIndex;
                RequestRefreshSummary();
            }

        }





        /// <summary>
        /// Context -> Search GMI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkGMIToolStripMenuItem_Click(object sender, EventArgs e) {
            try {
                if (listViewResults.SelectedItems.Count > 0) {
                    string tag = (string)listViewResults.FocusedItem.Text;
                    System.Diagnostics.Process.Start("http://aogmi.com/search/" + tag);
                }
            }
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex, "checkGMIToolStripMenuItem_Click");
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                MessageBox.Show("Something went horribly wrong!");
            }
        }




        private void ActivateRenameBackpack() {
            ContainerNode node = treeContainerView.SelectedNode as ContainerNode;
            if (node != null) {
                var containerId = node.ContainerId;
                var characterId = node.CharacterId;
                if (containerId > 100) {
                    var currentName = node.Text;
                    RenameContainerDialog diag = new RenameContainerDialog(currentName);
                    if (diag.ShowDialog() == DialogResult.OK) {
                        var newName = diag.Label.Replace("\"", "").Replace("'", "");
                        var request = new BackpackRenameRequest {
                            CharacterId = characterId,
                            ContainerId = containerId,
                            Name = newName
                        };
                        SendRequest(RequestMessage.RENAME_CONTAINER, request);
                    }
                }
            }

        }
        private void KeyEvent(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.F2) {
                ActivateRenameBackpack();
            }
        }

        private void renameF2ToolStripMenuItem_Click(object sender, EventArgs e) {
            ActivateRenameBackpack();
        }
        


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabPage3_Click(object sender, EventArgs e) {

        }

        /// <summary>
        /// Update the "summary" webview
        /// </summary>
        /// <param name="html"></param>
        private void DisplaySummaryHtml(string html) {
            webBrowserSummary.Navigate("about:blank");
            if (webBrowserSummary.Document != null) {
                webBrowserSummary.Document.Write(string.Empty);
            }
            webBrowserSummary.DocumentText = html;
        }



        /// <summary>
        /// Update the summary view
        /// </summary>
        /// <param name="entries"></param>
        private void UpdateSummaryHtml(List<Character> entries) {
            if (!File.Exists(@"html\summary.html")) {
                MessageBox.Show("Somehow the summary page HTML seems to be missing!\n\nA reinstall is required! :'(");
                return;
            }
            string html = File.ReadAllText(@"html\summary.html");

            long totalContainers = 0;
            long totalItems = 0;
            long totalCreditsChar = 0;
            long totalCreditsGmi = 0;
            long totalCredits = 0;


            long numCharacters = entries.Count;

            if (entries.Count > 0) {

                string chartable = "";
                foreach (var character in entries) {

                    string lastSeen = ScriptUtilities.FormatLastSeen(character.LastSeen, true, "Unknown");


                    string location = ZoneClassifier.Classify(character.ZoneId, character.PositionX, character.PositionZ);
                    if (character.SubZoneId != 0)
                        location = string.Format("{0} ({1})", ZoneClassifier.ClassifySubzone(character.SubZoneId), location);


                    string profession = character.Profession;
                    

                    string level = (character.Level == 0) ? "" : character.Level.ToString();

                    chartable += string.Format(@"
                            <tr>
                                <td>{0}</td>
                                <td>{5}</td>
                                <td>{4}</td>
                                <td>{1}</td>
                                <td>{6}</td>
                                <td>{7}</td>
                                <td>{2}</td>
                                <td>{3}</td>
                            </tr>
                            ", character.Name, ScriptUtilities.FormatAOCurrency(character.Credits), location, lastSeen, level, profession,
                             ScriptUtilities.FormatAOCurrency(character.CreditsGMI), ScriptUtilities.FormatVP(character.VP));

                }
                html = html.Replace("{characters}", chartable);

                totalContainers = entries.Sum(m => m.NumContainers);
                totalItems = entries.Sum(m => m.NumItems);
                totalCreditsChar = entries.Sum(m => m.Credits);
                totalCreditsGmi = entries.Sum(m => m.CreditsGMI);
                totalCredits = totalCreditsChar + totalCreditsGmi;


                numCharacters = entries.Count;

            }
            else {

                html = html.Replace("{characters}", "");
            }
            ;

            html = html.Replace("{totalCharacters}", numCharacters.ToString());
            html = html.Replace("{totalCreditsChar}", ScriptUtilities.FormatAOCurrency(totalCreditsChar));
            html = html.Replace("{totalCreditsGmi}", ScriptUtilities.FormatAOCurrency(totalCreditsGmi));
            html = html.Replace("{totalCredits}", ScriptUtilities.FormatAOCurrency(totalCredits));
            html = html.Replace("{totalItems}", string.Format("{0:# ###}", totalItems));
            html = html.Replace("{totalContainers}", string.Format("{0:# ###}", totalContainers));
            DisplaySummaryHtml(html);
        }


        /// <summary>
        /// Request a refresh of the "Summary HTML" page
        /// </summary>
        public void RequestRefreshSummary() {
            SendRequest(RequestMessage.UPDATE_SUMMARY, Dimension);
        }
        

        public static string GetItemDatabaseURL(long itemId, int ql) {
            if (itemId > 272000 || (bool)Properties.Settings.Default["auno"] == false)
                return string.Format("https://aoitems.com/item/{0}/{1}", itemId, ql);
            else
                return string.Format("http://auno.org/ao/db.php?id={0}&ql={1}", itemId, ql);
        }

        /// <summary>
        /// Update the "pattern" webview
        /// </summary>
        /// <param name="html"></param>
        private void DisplayPatternHtml(string html) {
            webBrowserPatterns.Navigate("about:blank");
            if (webBrowserPatterns.Document != null) {
                webBrowserPatterns.Document.Write(string.Empty);
            }
            webBrowserPatterns.DocumentText = html;
        }

        private void UpdatePatternWebview(string name, IList<PatternPiece> pieces, float total, ICollection<Symbiant> drops, IList<PocketBossLocation> locations) {
            if (!File.Exists(@"html\patterns.html"))
                return;
            string html = File.ReadAllText(@"html\patterns.html");
            html = html.Replace("{toon}", "all characters"); // TODO:
            html = html.Replace("{pb}", name);
            if (locations.Count > 0) {
                html = html.Replace("{playfield}", locations[0].Playfield);
                html = html.Replace("{location}", locations[0].Location);
                html = html.Replace("{whom}", locations[0].Bosses);
            }
            else {
                html = html.Replace("{playfield}", "Unknown");
                html = html.Replace("{location}", "Unknown");
                html = html.Replace("{whom}", "Unknown");
            }
            html = html.Replace("{sum}", string.Format("{0:f2}", total).Replace(',', '.')); // TODO:


            string itemtable = "";
            foreach (var piece in pieces) {
                itemtable += string.Format(@"
	<tr>
		<td>{0}</td>
		<td>{1}</td>
		<td>{2}</td>
		<td>{3}</td>
	</tr>", piece.Owner.Name, piece.Container, piece.ParentContainer, piece.Type);
            }
            html = html.Replace("{items}", itemtable);





            Dictionary<string, string> professions = new Dictionary<string, string>();
            professions["Artillery"] = "Adv/Agt/Fix/Sol/Trader";
            professions["Extermination"] = "Crat/Eng/MP/Trader";
            professions["Infantry"] = "Adv/Enf/Keep/MA";
            professions["Support"] = "Adv/Doc/Fix/Keep/MA/MP/Trader";
            professions["Control"] = "Crat/Eng/MP/Trader";

            string loottable = "";
            foreach (var item in drops) {
                // Shorten the name if we're in a small window
                string symbname = item.Name;
                if (this.Width <= 1165)
                    symbname = symbname.Replace("Unit Aban", "");
                loottable += string.Format(@"
	<tr>
		<td><a href='{3}'>QL{0}</a></td>
		<td><a href='{3}'>{1}</a></td>
		<td>{2}</td>
	</tr>", item.QL, symbname, professions[item.Type], GetItemDatabaseURL(item.AOID, item.QL));
            }
            html = html.Replace("{loot}", loottable);

            DisplayPatternHtml(html);
        }


        /// <summary>
        /// Open links in a new window, web-patterns
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void webBrowserPatterns_Navigating(object sender, WebBrowserNavigatingEventArgs e) {
            if (!e.Url.ToString().Contains("blank")) {
                //cancel the current event
                e.Cancel = true;

                //this opens the URL in the user's default browser
                Process.Start(e.Url.ToString());
            }
        }



        /// <summary>
        /// Update the pattern page
        /// </summary>
        private void UpdatePatternViews() {
            long characterId = getSelectedCharacter();



            PatternRequest request = new PatternRequest { CharacterId = characterId, Dimension = Dimension };
            if (patternShowAll.Checked)
                request.set = PatternRequest.PatternSet.SHOW_ALL;
            else if (patternShowPartial.Checked)
                request.set = PatternRequest.PatternSet.SHOW_PARTIAL;
            else if (patternShowCompletables.Checked)
                request.set = PatternRequest.PatternSet.SHOW_COMPLETEABLE;
            else if (patternOmniGarden.Checked)
                request.set = PatternRequest.PatternSet.SHOW_OMNI;
            else if (patternClanGarden.Checked)
                request.set = PatternRequest.PatternSet.SHOW_CLAN;


            SendRequest(RequestMessage.UPDATE_PATTERNS, request);
        }

        private void patternShowAll_CheckedChanged(object sender, EventArgs e) {
            RadioButton rb = sender as RadioButton;
            if (rb != null && rb.Checked)
                UpdatePatternViews();
        }

        private void patternShowPartial_CheckedChanged(object sender, EventArgs e) {
            RadioButton rb = sender as RadioButton;
            if (rb != null && rb.Checked)
                UpdatePatternViews();
        }

        private void patternShowCompletables_CheckedChanged(object sender, EventArgs e) {
            RadioButton rb = sender as RadioButton;
            if (rb != null && rb.Checked)
                UpdatePatternViews();
        }

        private void patternOmniGarden_CheckedChanged(object sender, EventArgs e) {
            RadioButton rb = sender as RadioButton;
            if (rb != null && rb.Checked)
                UpdatePatternViews();
        }

        private void patternClanGarden_CheckedChanged(object sender, EventArgs e) {
            RadioButton rb = sender as RadioButton;
            if (rb != null && rb.Checked)
                UpdatePatternViews();
        }

        
        private void comboBoxCharacters_SelectedIndexChanged(object sender, EventArgs e) {
            if (comboBoxCharactersLastSelectedIndex != comboBoxCharacters.SelectedIndex) {
                UpdatePatternViews();
                comboBoxCharactersLastSelectedIndex = comboBoxCharacters.SelectedIndex;
            }
        }
        
        private void comboBoxCharactersItemview_SelectedIndexChanged(object sender, EventArgs e) {
            if (comboBoxCharactersItemviewLastSelectedIndex != comboBoxCharactersItemview.SelectedIndex) {
                long characterId = getSelectedCharacterForItemSearch();

                var value = textboxSearch.Text;
                if (value.Length >= 3) {
                    int minQl;
                    int maxQl;
                    if (!int.TryParse(tbMinQl.Text, out minQl))
                        minQl = -5000;
                    if (!int.TryParse(tbMaxQl.Text, out maxQl))
                        maxQl = 5000;

                    int dimension = Dimension;
                    if (characterId != 0)
                        dimension = -1;

                    requestQueue.Enqueue(new MessageDataRequest {
                        Type = RequestMessage.SEARCH_ITEM,
                        Data = new SearchRequest { Search = value, Dimension = dimension, CharacterId = characterId, MinQL = minQl, MaxQL = maxQl }
                    });
                }

                comboBoxCharactersItemviewLastSelectedIndex = comboBoxCharactersItemview.SelectedIndex;
            }
        
        }
        


        private long getSelectedCharacter() {
            try {
                if (comboBoxCharacters.SelectedItem != null && comboBoxCharacters.SelectedIndex >= 0) {
                    return (long)((ComboBoxItem)comboBoxCharacters.SelectedItem).Value;
                }
            }
            catch (NullReferenceException ex) {
                ExceptionReporter.ReportException(ex);
            }
            catch (ArgumentOutOfRangeException ex) {
                ExceptionReporter.ReportException(ex);
            }
            return 0;
        }
        private long getSelectedCharacterForItemSearch() {
            try {
                if (comboBoxCharactersItemview.SelectedItem != null && comboBoxCharactersItemview.SelectedIndex >= 0) {
                    return (long)((ComboBoxItem)comboBoxCharactersItemview.SelectedItem).Value;
                }
            }
            catch (NullReferenceException ex) {
                ExceptionReporter.ReportException(ex);
            }
            catch (ArgumentOutOfRangeException ex) {
                ExceptionReporter.ReportException(ex);
            }
            return 0;
        }

        private void listViewPatterns_SelectedIndexChanged(object sender, EventArgs e) {
            try {
                long characterId = getSelectedCharacter();
                if (listViewPatterns.SelectedIndices.Count > 0) {

                    SendRequest(RequestMessage.LOAD_SINGLE_PATTERN,
                        new PatternRequest {
                            CharacterId = characterId,
                            Name = listViewPatterns.SelectedItems[0].Text,
                            Dimension = Dimension
                        }
                    );
                }
            }
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex, "Pattern match click");
            }
        }


        private void trayIconContext_Opening(object sender, CancelEventArgs e) {
            e.Cancel = false;
        }


        /// <summary>
        /// Tray icon context: Exit program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem1_Click(object sender, EventArgs e) {
            this.canExit = true;
            this.Close();
        }

        /// <summary>
        /// Tray icon context: Show window / Restore window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openToolStripMenuItem_Click(object sender, EventArgs e) {
            notifyIcon1.Visible = false;

            loadingScreen.ShowIfAvailable();

            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void tabPage4_Click(object sender, EventArgs e) {
        }

        private void buffresultContext_Opening(object sender, CancelEventArgs e) {
            e.Cancel = false;
        }


        private void webBrowser2_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e) {

        }


        private void tabPageGMI_Click(object sender, EventArgs e) {
        }

        void tabControl1_SelectedIndexChanged(object sender, EventArgs e) {
            if (tabControl1.SelectedTab == tabPagePatterns) {
                if (listViewPatterns.Items.Count == 0) {
                    UpdatePatternViews();
                }
            }
        }


        private void deleteToolStripMenuItem_Click(object sender, EventArgs e) {
            
            ContainerNode node = treeContainerView.SelectedNode as ContainerNode;
            if (node != null && node.ContainerId == 0) {
                var characterId = node.CharacterId;

                if (MessageBox.Show("Are you sure you wish to permanently delete the data for this character?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                    SendRequest(RequestMessage.DELETE_CHARACTER, characterId);
                }
            }
        }

        private void archieveToolStripMenuItem_Click(object sender, EventArgs e) {
            
            ContainerNode node = treeContainerView.SelectedNode as ContainerNode;
            if (node != null && node.CharacterId != 0) {
                var containerId = node.ContainerId;
                var characterId = node.CharacterId; 
                SendRequest(RequestMessage.ARCHIVE_CHARACTER, characterId);
            }
        }

        private void checkForUpdatesToolStripMenuItem_Click(object sender, EventArgs e) {            
            Boolean canSendCrashreport = (bool)Properties.Settings.Default["canSendCrashreport"];
            if (!canSendCrashreport) {
                MessageBox.Show("Automatic updates disabled due to configuration settings.");
            }
            else {
                AutoUpdater.LetUserSelectRemindLater = true;
                AutoUpdater.RemindLaterTimeSpan = RemindLaterFormat.Days;
                AutoUpdater.CheckForUpdateEvent += AutoUpdater_CheckForUpdateEvent;
                AutoUpdater.RemindLaterAt = 7;
                AutoUpdater.Start(UPDATE_XML);
            }
        }


        void AutoUpdater_CheckForUpdateEvent(UpdateInfoEventArgs args) {

            if (args.IsUpdateAvailable) {
                AutoUpdater.CheckForUpdateEvent -= AutoUpdater_CheckForUpdateEvent;
                AutoUpdater.Start(UPDATE_XML);
                //message = string.Format("There is a new version available!\nCurrent version is {0}, your version is {1}", args.CurrentVersion, args.InstalledVersion);
            }
            else
                MessageBox.Show("Your version is up-to-date!", "Automatic Updates", MessageBoxButtons.OK, MessageBoxIcon.Information);

            
        }

        private void tbMinQl_TextChanged(object sender, EventArgs e) {
            int tmp;
            if (textboxSearch.Text.Length > 3 && (tbMinQl.Text.Length == 0 || int.TryParse(tbMinQl.Text, out tmp)))
                textboxSearch_TextChanged(null, null);
        }

        private void tbMaxQl_TextChanged(object sender, EventArgs e) {
            int tmp;
            if (textboxSearch.Text.Length > 3 && (tbMaxQl.Text.Length == 0 || int.TryParse(tbMaxQl.Text, out tmp)))
                textboxSearch_TextChanged(null, null);
        }


        private void RequestIdentify(IdentifyType type) {
            requestQueue.Enqueue(new MessageDataRequest {
                Type = RequestMessage.IDENTIFY_ITEMS,
                Data = new UpdateIdentifyRequest {
                    Type = type,
                    Dimension = Dimension
                }
            });
        }
        private void button1_Click(object sender, EventArgs e) {
            RequestIdentify(IdentifyType.CLUMPS);
        }

        private void button3_Click(object sender, EventArgs e) {
            RequestIdentify(IdentifyType.BUILDINGS);
        }

        private void button4_Click(object sender, EventArgs e) {
            RequestIdentify(IdentifyType.AI_WEAPONS);
        }

        private void button5_Click(object sender, EventArgs e) {
            RequestIdentify(IdentifyType.OFAB_BODY);
        }

        private void button6_Click(object sender, EventArgs e) {
            RequestIdentify(IdentifyType.OFAB_WEAPON);
        }

        private void button7_Click(object sender, EventArgs e) {
            RequestIdentify(IdentifyType.ARULSABA);
        }

        private void button8_Click(object sender, EventArgs e) {
            RequestIdentify(IdentifyType.MISC);
        }

        private void button9_Click(object sender, EventArgs e) {
            RequestIdentify(IdentifyType.INGOT);
        }

        
        private void button2_Click(object sender, EventArgs e) {
            RequestIdentify(IdentifyType.VIRALBOTS);
        }




        private void viewItemToolStripMenuItem2_Click(object sender, EventArgs e) {
            try {
                if (listViewIdentify.SelectedItems.Count > 0) {
                    string[] tags = listViewIdentify.FocusedItem.Tag.ToString().Split(':');
                    
                    long itemId = long.Parse(tags[1]);
                    int ql = int.Parse(listViewIdentify.FocusedItem.SubItems[2].Text);

                    System.Diagnostics.Process.Start(GetItemDatabaseURL(itemId, ql));
                }
            }
            catch (System.ComponentModel.Win32Exception) {
                MessageBox.Show("Could not find a default browser to open website in");
            }
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex, "viewItemToolStripMenuItem_Click");
                MessageBox.Show("Something went horribly wrong!");
            }
        }

        private void SendRequest(RequestMessage type, Object data = null) {
            requestQueue.Enqueue(new MessageDataRequest {
                Type = type,
                Data = data
            });
        }

        private void viewBackpackToolStripMenuItem_Click(object sender, EventArgs e) {
            try {
                if (listViewIdentify.SelectedItems.Count > 0) {
                    string[] tags = listViewIdentify.FocusedItem.Tag.ToString().Split(':');
                    long containerId = long.Parse(tags[0]);
                    long characterId = long.Parse(tags[2]);

                    if (containerId > 100) {
                        
                        SendRequest(RequestMessage.GET_TREEVIEW_CHILDREN_AND_SELECT,
                            new UpdateTreeviewNodeRequest { CharacterId = characterId, ContainerId = 1, ContainerToSelect = containerId }
                        );
                        SendRequest(RequestMessage.GET_TREEVIEW_CHILDREN_AND_SELECT,
                            new UpdateTreeviewNodeRequest { CharacterId = characterId, ContainerId = 2, ContainerToSelect = containerId }
                        );

                        SendRequest(RequestMessage.OPEN_CONTAINER,
                            new CharacterAndContainer { CharacterId = 0, ContainerId = containerId }
                        );


                        // Select the given bag
                        TreeviewSelectContainer(treeContainerView.Nodes, containerId, characterId);

                        tabControl1.SelectedTab = tabPage1;
                    }
                }
            }
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex, "viewBackpackToolStripMenuItem_Click");
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                MessageBox.Show("Something went horribly wrong!");
            }
        }


        private void writeToScriptsToolStripMenuItem_Click(object sender, EventArgs e) {
            List<long> ids = new List<long>();
            foreach (ListViewItem item in listViewResults.SelectedItems) {
                string[] tags = item.Tag.ToString().Split(':');
                long id;
                if (long.TryParse(tags[2], out id)) {
                    ids.Add(id);
                }

            }

            if (ids.Count > 0)
                SendRequest(RequestMessage.WRITE_ITEMS_SCRIPT, ids);
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e) {

        }




    }
}
