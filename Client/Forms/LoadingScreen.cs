﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.Concurrent;
using ItemsLight.AOIA;

namespace ItemsLight {


    public partial class LoadingScreen : Form {
        private ConcurrentQueue<MessageData> resultQueue;
        private Action onClose;
        public string Label {
            set {
                this.label.Text = value;
            }
        }



        public LoadingScreen(ConcurrentQueue<MessageData> resultQueue, Action onClose) {
            InitializeComponent();
            this.resultQueue = resultQueue;
            //this.FormClosing += LoadingScreen_FormClosing;
            this.FormClosed += LoadingScreen_FormClosed;
        }

        void LoadingScreen_FormClosed(object sender, FormClosedEventArgs e) {
            if (onClose != null) {
                var tmp = onClose;
                onClose = null;
                tmp();
            }
        }

        private void LoadingScreen_FormClosing(object sender, FormClosingEventArgs e) {
            //e.Cancel = !canExit;
            if (e.Cancel) {
                //resultQueue.Enqueue(new MessageData { type = MessageDataType.RES_READY_TO_BEGIN });
            }
        }
    }
    
}
