﻿using ItemsLight.AOIA;
using ItemsLight.AOIA.HelperStructs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using log4net;
using System.Collections.Concurrent;
using ItemsLight.Model;
using EvilsoftCommons.Exceptions;

namespace ItemsLight.Forms {
    partial class SymbiantMatcher : Form {
        private static ILog logger = LogManager.GetLogger(typeof(SymbiantMatcher));
        ConcurrentQueue<MessageDataRequest> requestQueue;
        private string lastSelectedSlot = "";
        private ToolStripStatusLabel toolStripStatusLabel;
        private ListViewColumnSorterQL sorter;
        private Item equippedItem;

        public SymbiantMatcher(ConcurrentQueue<MessageDataRequest> requestQueue, ToolStripStatusLabel toolStripStatusLabel) {
            InitializeComponent();
            this.requestQueue = requestQueue;
            this.toolStripStatusLabel = toolStripStatusLabel;
        }

        /// <summary>
        /// Set the character list
        /// </summary>
        public ComboBoxItem[] Characters {
            set {
                this.cbSymbCharacter.Items.Clear();
                this.cbSymbCharacter.Items.AddRange(value);
                this.cbSymbCharacter.SelectedIndex = 0;
            }
        }

        public Item EquippedItem {
            set {
                linkEquippedItem.Enabled = value != null;

                if (value != null) {
                    linkEquippedItem.Text = string.Format("QL{0} {1}", value.QL, value.Name);
                }
                else {
                    linkEquippedItem.Text = "Slot empty";
                }

                equippedItem = value;
            }
        }

        public Character Character {
            set {
                lbLevel.Text = value.Level.ToString();
                lbProfession.Text = value.Profession;
            }
        }

        public IList<Item> Result {
            set {
                bool hasEquippedSymbiant = equippedItem != null && equippedItem.Name.Contains("Symbiant");
                List<ListViewItem> temp = new List<ListViewItem>();
                foreach (var item in value) {
                    var name = item.Name;
                    ListViewItem lvi = new ListViewItem(name);
                    lvi.Tag = string.Format("{0}:{1}:{2}", item.ParentContainerId, item.LowKey, item.Owner.Id);
                    lvi.SubItems.Add("" + item.QL);
                    lvi.SubItems.Add(item.Owner.Name);
                    lvi.SubItems.Add(item.ParentContainerName);
                    lvi.SubItems.Add(item.ParentInventory);

                    if (hasEquippedSymbiant) {
                        // Treat extermination as QL x 0.7
                        if (item.Name.Contains("Extermination Unit")) {
                            if (equippedItem.QL < item.QL * 0.7f) {
                                lvi.ForeColor = Color.Green;
                            }
                        }
                        // Prefer [already equipped] syndicate if the other item is < QL230
                        else if (equippedItem.Name.Contains("Syndicate Brain Symbiant")) {
                            if (item.QL > 230) {
                                lvi.ForeColor = Color.Green;
                            }
                        }
                        // Treat [this item] syndicate as a QL230
                        else if (item.Name.Contains("Syndicate Brain Symbiant")) {
                            if (equippedItem.QL < 230) {
                                lvi.ForeColor = Color.Green;
                            }
                        }
                        else if (equippedItem.QL < item.QL) {
                            lvi.ForeColor = Color.Green;
                        }
                        // Treat symbiants <50 QLs of current as obsolete
                        else if (equippedItem.QL - item.QL > 50)
                            lvi.ForeColor = Color.LightGray;
                    }

                    temp.Add(lvi);
                }

                listView1.BeginUpdate();
                listView1.Items.Clear();
                listView1.Items.AddRange(temp.ToArray());
                listView1.EndUpdate();

                if (temp.Count == 0)
                    toolStripStatusLabel.Text = "No symbiants found";
                else
                    listView1_SelectedIndexChanged(null, null);
            }
        }

        /// <summary>
        /// Sort search results / backpack contents
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listViewResults_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e) {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == sorter.SortColumn) {
                // Reverse the current sort direction for this column.
                if (sorter.Order == SortOrder.Ascending) {
                    sorter.Order = SortOrder.Descending;
                }
                else {
                    sorter.Order = SortOrder.Ascending;
                }
            }
            else {
                // Set the column number that is to be sorted; default to ascending.
                sorter.SortColumn = e.Column;
                sorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            this.listView1.Sort();
        }


        /// <summary>
        /// Get the selected character on the symbiants screen
        /// This is likely temporary
        /// </summary>
        /// <returns></returns>
        private long GetSelectedCharacterForSymbiants() {
            try {
                if (cbSymbCharacter.SelectedItem != null && cbSymbCharacter.SelectedIndex >= 0) {
                    return (long)((ComboBoxItem)cbSymbCharacter.SelectedItem).Value;
                }
            }
            catch (NullReferenceException ex) {
                ExceptionReporter.ReportException(ex);
            }
            catch (ArgumentOutOfRangeException ex) {
                ExceptionReporter.ReportException(ex);
            }

            return 0;
        }


        private void SendRequest(RequestMessage type, Object data = null) {
            requestQueue.Enqueue(new MessageDataRequest {
                Type = type,
                Data = data
            });
        }



        private void button_symb_head_Click_1(object sender, EventArgs e) {
            button_symb_any("head");
        }

        private int translate(string slot) {
            switch (slot) {
                case "eye":
                    return 33;
                case "head":
                    return 34;
                case "ear":
                    return 35;
                case "rarm":
                    return 36;
                case "chest":
                    return 37;
                case "larm":
                    return 38;
                case "rwrist":
                    return 39;
                case "waist":
                    return 40;
                case "lwrist":
                    return 41;
                case "rhand":
                    return 42;
                case "legs":
                    return 43;
                case "lhand":
                    return 44;
                case "feet":
                    return 45;

                default:
                    return 0;
            }
        }

        private void button_symb_any(string slot) {
            long characterId = GetSelectedCharacterForSymbiants();
            if (!string.IsNullOrEmpty(slot) && characterId != 0) {

                SendRequest(RequestMessage.SHOW_EQUIPPED_ITEM,
                    new CharacterAndContainer {
                        ContainerId = translate(slot),
                        CharacterId = characterId
                    });
                
                SendRequest(RequestMessage.SHOW_SYMBIANTS,
                    new ShowSymbiantsRequest {
                        Slot = slot,
                        CharacterId = characterId,
                        ShowAvailableOnly = cbOnlyAvailable.Checked && cbOwnedOnly.Checked,
                        ShowOwnedOnly = cbOwnedOnly.Checked
                    });

                lastSelectedSlot = slot;
            }
        }



        private void button_symb_chest_Click(object sender, EventArgs e) {
            button_symb_any("chest");
        }

        private void button_symb_eyes_Click(object sender, EventArgs e) {
            button_symb_any("eye");
        }

        private void button_symb_waist_Click(object sender, EventArgs e) {
            button_symb_any("waist");
        }

        private void button_symb_legs_Click_1(object sender, EventArgs e) {
            button_symb_any("legs");
        }

        private void button_symb_feet_Click(object sender, EventArgs e) {
            button_symb_any("feet");
        }

        private void leftarm_Click(object sender, EventArgs e) {
            button_symb_any("larm");
        }

        private void bLeftWrist_Click(object sender, EventArgs e) {
            button_symb_any("lwrist");
        }

        private void button19_Click(object sender, EventArgs e) {
            button_symb_any("rwrist");
        }

        private void bRightArm_Click(object sender, EventArgs e) {
            button_symb_any("rarm");
        }

        private void bRightHand_Click(object sender, EventArgs e) {
            button_symb_any("rhand");
        }

        private void bLeftHand_Click(object sender, EventArgs e) {
            button_symb_any("lhand");
        }

        private void bEars_Click(object sender, EventArgs e) {
            button_symb_any("ear");
        }

        private void SymbiantMatcher_Load(object sender, EventArgs e) {
            this.Dock = DockStyle.Fill;
            sorter = new ListViewColumnSorterQL();
            this.listView1.ListViewItemSorter = sorter;
            this.listView1.ColumnClick += listViewResults_ColumnClick;
            sorter.Order = SortOrder.Descending;
            sorter.SortColumn = 1;
            this.listView1.Sort();

            EquippedItem = null;
        }

        private void cbOnlyAvailable_CheckedChanged(object sender, EventArgs e) {
            button_symb_any(lastSelectedSlot);
        }

        /// <summary>
        /// Refresh search, and disable 'available only' if unchecked.
        /// No point checking if its unused, if its unowned.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbOwnedOnly_CheckedChanged(object sender, EventArgs e) {
            button_symb_any(lastSelectedSlot);
            cbOnlyAvailable.Enabled = cbOwnedOnly.Checked;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e) {
            toolStripStatusLabel.Text = string.Format("{0} of {1} items selected", listView1.SelectedItems.Count, listView1.Items.Count);
        }

        private void cbSymbCharacter_SelectedIndexChanged(object sender, EventArgs e) {
            long characterId = GetSelectedCharacterForSymbiants();
            if (characterId != 0)
                SendRequest(RequestMessage.REQ_CHARACTER, characterId);

            button_symb_any(lastSelectedSlot);
        }

        private void linkEquippedItem_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            if (equippedItem != null) {                
                try {
                    System.Diagnostics.Process.Start(MainWindow.GetItemDatabaseURL(equippedItem.LowKey, equippedItem.QL));                    
                }
                catch (Exception ex) {
                    ExceptionReporter.ReportException(ex, "linkEquippedItem_LinkClicked");
                    logger.Warn(ex.Message);
                    logger.Warn(ex.StackTrace);
                    MessageBox.Show("Something went horribly wrong!");
                }
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e) {
            e.Cancel = false;
        }

        private void viewItemToolStripMenuItem_Click(object sender, EventArgs e) {
            try {
                if (listView1.SelectedItems.Count > 0) {
                    string[] tags = listView1.FocusedItem.Tag.ToString().Split(':');
                    
                    long itemId = long.Parse(tags[1]);
                    int ql = int.Parse(listView1.FocusedItem.SubItems[1].Text);

                    System.Diagnostics.Process.Start(MainWindow.GetItemDatabaseURL(itemId, ql));
                }
            }
            catch (System.ComponentModel.Win32Exception) {
                MessageBox.Show("Could not find a default browser to open website in");
            }
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex, "viewItemToolStripMenuItem_Click");
                MessageBox.Show("Something went horribly wrong!");
            }
        }

        private void viewBackpackToolStripMenuItem_Click(object sender, EventArgs e) {
            try {
                if (listView1.SelectedItems.Count > 0) {
                    string[] tags = listView1.FocusedItem.Tag.ToString().Split(':');
                    long containerId = long.Parse(tags[0]);
                    long characterId = long.Parse(tags[2]);

                    if (containerId > 100) {

                        SendRequest(RequestMessage.GET_TREEVIEW_CHILDREN_AND_SELECT,
                            new UpdateTreeviewNodeRequest { CharacterId = characterId, ContainerId = 1, ContainerToSelect = containerId }
                        );
                        SendRequest(RequestMessage.GET_TREEVIEW_CHILDREN_AND_SELECT,
                            new UpdateTreeviewNodeRequest { CharacterId = characterId, ContainerId = 2, ContainerToSelect = containerId }
                        );

                        SendRequest(RequestMessage.OPEN_CONTAINER,
                            new CharacterAndContainer { CharacterId = 0, ContainerId = containerId }
                        );
                    }
                }
            }
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex, "viewBackpackToolStripMenuItem_Click");
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                MessageBox.Show("Something went horribly wrong!");
            }
        }

        private void checkGMIToolStripMenuItem_Click(object sender, EventArgs e) {
            try {
                if (listView1.SelectedItems.Count > 0) {
                    string tag = (string)listView1.FocusedItem.Text;
                    System.Diagnostics.Process.Start("http://aogmi.com/search/" + tag);
                }
            }
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex, "checkGMIToolStripMenuItem_Click");
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                MessageBox.Show("Something went horribly wrong!");
            }
        }
    }
}
