﻿namespace ItemsLight.Forms {
    partial class BuffItems {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.listViewBuffs = new System.Windows.Forms.ListView();
            this.chItem = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chQL = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAbility = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chModifier = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chSlot = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chCharacter = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chBackpack = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buffresultContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.viewItemToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.findItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buffsSlot = new System.Windows.Forms.ComboBox();
            this.buffsProfession = new System.Windows.Forms.ComboBox();
            this.buffsBreed = new System.Windows.Forms.ComboBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.buffresultContext.SuspendLayout();
            this.SuspendLayout();
            // 
            // listViewBuffs
            // 
            this.listViewBuffs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewBuffs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chItem,
            this.chQL,
            this.chAbility,
            this.chModifier,
            this.chSlot,
            this.chCharacter,
            this.chBackpack,
            this.chLocation});
            this.listViewBuffs.ContextMenuStrip = this.buffresultContext;
            this.listViewBuffs.Location = new System.Drawing.Point(202, 4);
            this.listViewBuffs.Name = "listViewBuffs";
            this.listViewBuffs.Size = new System.Drawing.Size(710, 467);
            this.listViewBuffs.TabIndex = 21;
            this.listViewBuffs.UseCompatibleStateImageBehavior = false;
            this.listViewBuffs.View = System.Windows.Forms.View.Details;
            // 
            // chItem
            // 
            this.chItem.Text = "Item";
            this.chItem.Width = 186;
            // 
            // chQL
            // 
            this.chQL.Text = "QL";
            this.chQL.Width = 40;
            // 
            // chAbility
            // 
            this.chAbility.Text = "Ability";
            this.chAbility.Width = 61;
            // 
            // chModifier
            // 
            this.chModifier.Text = "Modifier";
            this.chModifier.Width = 58;
            // 
            // chSlot
            // 
            this.chSlot.Text = "Slot";
            this.chSlot.Width = 68;
            // 
            // chCharacter
            // 
            this.chCharacter.Text = "Character";
            this.chCharacter.Width = 77;
            // 
            // chBackpack
            // 
            this.chBackpack.Text = "Backpack";
            this.chBackpack.Width = 76;
            // 
            // chLocation
            // 
            this.chLocation.Text = "Location";
            // 
            // buffresultContext
            // 
            this.buffresultContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewItemToolStripMenuItem1,
            this.findItemToolStripMenuItem});
            this.buffresultContext.Name = "buffresultContext";
            this.buffresultContext.Size = new System.Drawing.Size(127, 48);
            // 
            // viewItemToolStripMenuItem1
            // 
            this.viewItemToolStripMenuItem1.Name = "viewItemToolStripMenuItem1";
            this.viewItemToolStripMenuItem1.Size = new System.Drawing.Size(126, 22);
            this.viewItemToolStripMenuItem1.Text = "View Item";
            this.viewItemToolStripMenuItem1.Click += new System.EventHandler(this.viewItemToolStripMenuItem1_Click);
            // 
            // findItemToolStripMenuItem
            // 
            this.findItemToolStripMenuItem.Name = "findItemToolStripMenuItem";
            this.findItemToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.findItemToolStripMenuItem.Text = "Find Item";
            this.findItemToolStripMenuItem.Click += new System.EventHandler(this.findItemToolStripMenuItem_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Breed:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Profession:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Slot:";
            // 
            // buffsSlot
            // 
            this.buffsSlot.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.buffsSlot.FormattingEnabled = true;
            this.buffsSlot.Items.AddRange(new object[] {
            "Any",
            "Back",
            "Body",
            "Feet",
            "Hands",
            "Head",
            "Legs",
            "Misc",
            "Nano",
            "NCU",
            "Neck",
            "Rings",
            "Shoulder",
            "Sleeve",
            "Utility",
            "Weapon",
            "Wrists"});
            this.buffsSlot.Location = new System.Drawing.Point(75, 60);
            this.buffsSlot.Name = "buffsSlot";
            this.buffsSlot.Size = new System.Drawing.Size(121, 21);
            this.buffsSlot.TabIndex = 14;
            this.buffsSlot.SelectedIndexChanged += new System.EventHandler(this.buffsSlot_SelectedIndexChanged);
            // 
            // buffsProfession
            // 
            this.buffsProfession.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.buffsProfession.Enabled = false;
            this.buffsProfession.FormattingEnabled = true;
            this.buffsProfession.Items.AddRange(new object[] {
            "Any",
            "Adventurer",
            "Agent",
            "Bureaucrat",
            "Doctor",
            "Enforcer",
            "Engineer",
            "Fixer",
            "Keeper",
            "Martial artist",
            "Meta-physicist",
            "Nano-technician",
            "Shade",
            "Soldier",
            "Trader"});
            this.buffsProfession.Location = new System.Drawing.Point(75, 6);
            this.buffsProfession.Name = "buffsProfession";
            this.buffsProfession.Size = new System.Drawing.Size(121, 21);
            this.buffsProfession.TabIndex = 13;
            this.buffsProfession.SelectedIndexChanged += new System.EventHandler(this.buffsProfession_SelectedIndexChanged);
            // 
            // buffsBreed
            // 
            this.buffsBreed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.buffsBreed.Enabled = false;
            this.buffsBreed.FormattingEnabled = true;
            this.buffsBreed.Items.AddRange(new object[] {
            "Any",
            "Atrox",
            "Solitus",
            "Opifex",
            "Nanomage"});
            this.buffsBreed.Location = new System.Drawing.Point(75, 33);
            this.buffsBreed.Name = "buffsBreed";
            this.buffsBreed.Size = new System.Drawing.Size(121, 21);
            this.buffsBreed.TabIndex = 12;
            this.buffsBreed.SelectedIndexChanged += new System.EventHandler(this.buffsBreed_SelectedIndexChanged);
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 87);
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBox1.Size = new System.Drawing.Size(184, 381);
            this.listBox1.TabIndex = 23;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // BuffItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(924, 483);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.listViewBuffs);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buffsSlot);
            this.Controls.Add(this.buffsProfession);
            this.Controls.Add(this.buffsBreed);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BuffItems";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "BuffItems";
            this.Load += new System.EventHandler(this.BuffItems_Load);
            this.buffresultContext.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewBuffs;
        private System.Windows.Forms.ColumnHeader chItem;
        private System.Windows.Forms.ColumnHeader chQL;
        private System.Windows.Forms.ColumnHeader chAbility;
        private System.Windows.Forms.ColumnHeader chModifier;
        private System.Windows.Forms.ColumnHeader chSlot;
        private System.Windows.Forms.ColumnHeader chCharacter;
        private System.Windows.Forms.ColumnHeader chBackpack;
        private System.Windows.Forms.ColumnHeader chLocation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox buffsSlot;
        private System.Windows.Forms.ComboBox buffsProfession;
        private System.Windows.Forms.ComboBox buffsBreed;
        private System.Windows.Forms.ContextMenuStrip buffresultContext;
        private System.Windows.Forms.ToolStripMenuItem viewItemToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem findItemToolStripMenuItem;
        private System.Windows.Forms.ListBox listBox1;
    }
}