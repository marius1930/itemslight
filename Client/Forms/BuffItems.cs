﻿using EvilsoftCommons.Exceptions;
using ItemsLight.AOIA;
using ItemsLight.AOIA.HelperStructs;
using ItemsLight.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ItemsLight.Forms {
    partial class BuffItems : Form {
        private ToolStripLabel toolStripStatusLabel;
        private Action<RequestMessage, object> SendRequest;
        private ListViewColumnSorterBuffItems columnSorterBuffList;
        public int Dimension { get; set; }

        public BuffItems(ToolStripLabel toolStripStatusLabel, Action<RequestMessage, object> SendRequest) {
            InitializeComponent();
            this.toolStripStatusLabel = toolStripStatusLabel;
            this.SendRequest = SendRequest;


            listViewBuffs.ItemSelectionChanged += listViewBuffs_ItemSelectionChanged;

            columnSorterBuffList = new ListViewColumnSorterBuffItems();
            this.listViewBuffs.ListViewItemSorter = columnSorterBuffList;
            this.listViewBuffs.ColumnClick += listViewBuffs_ColumnClick;
        }

        void listViewBuffs_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e) {
            toolStripStatusLabel.Text = string.Format("{0} of {1} items selected", listViewBuffs.SelectedItems.Count, listViewBuffs.Items.Count);
        }

        

        private void UpdateBuffitemsView() {
            if (listBox1.SelectedItems == null || listBox1.SelectedItems.Count == 0)
                return;

            
            string profession = (string)buffsProfession.SelectedItem;
            string breed = (string)buffsBreed.SelectedItem;
            string slot = (string)buffsSlot.SelectedItem;
            

            HashSet<int> items = new HashSet<int>();
            foreach (var item in listBox1.SelectedItems) {
                var value = ((ComboBoxItem)item).Value;
                items.Add((int)value);
            }
            RequestBuffItems request = new RequestBuffItems {
                Dimension = Dimension,
                Profession = profession,
                Breed = breed,
                Slot = slot,
                Skills = items
            };

            SendRequest(RequestMessage.BUFF_ITEMS, request);
        }

        private void buffsAbility_SelectedIndexChanged(object sender, EventArgs e) {
            UpdateBuffitemsView();
        }

        private void buffsProfession_SelectedIndexChanged(object sender, EventArgs e) {
            UpdateBuffitemsView();
        }

        private void buffsBreed_SelectedIndexChanged(object sender, EventArgs e) {
            UpdateBuffitemsView();
        }

        private void buffsSlot_SelectedIndexChanged(object sender, EventArgs e) {
            UpdateBuffitemsView();
        }

        private void radioShowAvailable_CheckedChanged(object sender, EventArgs e) {
            RadioButton rb = sender as RadioButton;
            if (rb != null && rb.Checked)
                UpdateBuffitemsView();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e) {
            RadioButton rb = sender as RadioButton;
            if (rb != null && rb.Checked)
                UpdateBuffitemsView();
        }



        public void UpdateBuffItems(List<Item> items) {

            List<int> stats = new List<int>();
            List<string> abilities = new List<string>();
            foreach (var item in listBox1.SelectedItems) {
                var m = ((ComboBoxItem)item);
                abilities.Add(m.Text);
                stats.Add((int)m.Value);
            }

            string abilitiesTag = string.Join("/", abilities);


            listViewBuffs.BeginUpdate();
            listViewBuffs.Items.Clear();
            foreach (var item in items) {
                List<int> modifiers = new List<int>();
                foreach (int stat in stats) {
                    modifiers.Add(item.GetSkill(stat));
                }

                string slot = "Unknown " + item.EquipSlot;
                if (Inventory.SlotMap.ContainsKey((int)item.EquipSlot))
                    slot = Inventory.SlotMap[(int)item.EquipSlot];

                ListViewItem lvi = new ListViewItem(item.Name);
                lvi.Tag = "0:" + item.LowKey;
                lvi.SubItems.Add("" + item.QL);
                lvi.SubItems.Add(abilitiesTag);
                lvi.SubItems.Add("+" + string.Join("/", modifiers));
                lvi.SubItems.Add(slot);
                lvi.SubItems.Add((item.Owner == null) ? "" : item.Owner.Name);
                lvi.SubItems.Add(item.ParentContainerName);
                lvi.SubItems.Add(item.ParentInventory);

                listViewBuffs.Items.Add(lvi);
            }
            listViewBuffs.EndUpdate();

            listViewBuffs_ItemSelectionChanged(null, null);
        }

        private void BuffItems_Load(object sender, EventArgs e) {
            this.Dock = DockStyle.Fill;
                        
            buffsProfession.SelectedIndex = 0;
            buffsBreed.SelectedIndex = 0;
            buffsSlot.SelectedIndex = 0;

            this.listBox1.Items.Add(new ComboBoxItem(17, "Agility"));
            this.listBox1.Items.Add(new ComboBoxItem(20, "Sense"));
            this.listBox1.Items.Add(new ComboBoxItem(19, "Intelligence"));
            this.listBox1.Items.Add(new ComboBoxItem(21, "Psychic"));
            this.listBox1.Items.Add(new ComboBoxItem(16, "Strength"));
            this.listBox1.Items.Add(new ComboBoxItem(18, "Stamina"));

            this.listBox1.Items.Add(new ComboBoxItem(124, "Treatment"));

            this.listBox1.Items.Add(new ComboBoxItem(101, "Mult. Melee"));
            this.listBox1.Items.Add(new ComboBoxItem(134, "Mult. Ranged"));// TODO:
            this.listBox1.Items.Add(new ComboBoxItem(161, "Comp. Lit"));


            this.listBox1.Items.Add(new ComboBoxItem(148, "Burst"));
            this.listBox1.Items.Add(new ComboBoxItem(167, "Fullauto"));
            this.listBox1.Items.Add(new ComboBoxItem(151, "Aimed Shot"));
            this.listBox1.Items.Add(new ComboBoxItem(150, "Fling shot"));


            this.listBox1.Items.Add(new ComboBoxItem(112, "Pistol"));
            this.listBox1.Items.Add(new ComboBoxItem(114, "SMG"));
            this.listBox1.Items.Add(new ComboBoxItem(115, "Shotgun"));
            this.listBox1.Items.Add(new ComboBoxItem(116, "Assault Rifle"));
            this.listBox1.Items.Add(new ComboBoxItem(113, "Rifle"));
        }


        /// <summary>
        /// Context => View item
        /// Opens up the auno / aoitems website
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void viewItemToolStripMenuItem1_Click(object sender, EventArgs e) {
            try {
                if (listViewBuffs.SelectedItems.Count > 0) {
                    string tag = (string)listViewBuffs.FocusedItem.Tag;
                    tag = tag.Substring(tag.IndexOf(':') + 1);
                    long itemId = long.Parse(tag);
                    int ql = int.Parse(listViewBuffs.FocusedItem.SubItems[1].Text);

                    System.Diagnostics.Process.Start(MainWindow.GetItemDatabaseURL(itemId, ql));
                }
            }
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex, "viewItemToolStripMenuItem1_Click");
                MessageBox.Show("Something went horribly wrong!");
            }
        }

        /// <summary>
        /// Context => Find item
        /// Searches for the specific item type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void findItemToolStripMenuItem_Click(object sender, EventArgs e) {
            try {
                if (listViewBuffs.SelectedItems.Count > 0) {
                    string tag = (string)listViewBuffs.FocusedItem.Tag;
                    tag = tag.Substring(tag.IndexOf(':') + 1);
                    long itemId = long.Parse(tag);
                    SendRequest(RequestMessage.SEARCH_ITEM_ID,
                        new SearchRequestById { Search = itemId, Dimension = Dimension }
                    );
                }
            }
            catch (Exception ex) {
                ExceptionReporter.ReportException(ex, "findItemToolStripMenuItem_Click");
                MessageBox.Show("Something went horribly wrong!");
            }
        }



        /// <summary>
        /// Sorting callback for the listview columns
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listViewBuffs_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e) {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == columnSorterBuffList.SortColumn) {
                // Reverse the current sort direction for this column.
                if (columnSorterBuffList.Order == SortOrder.Ascending) {
                    columnSorterBuffList.Order = SortOrder.Descending;
                }
                else {
                    columnSorterBuffList.Order = SortOrder.Ascending;
                }
            }
            else {
                // Set the column number that is to be sorted; default to ascending.
                columnSorterBuffList.SortColumn = e.Column;
                columnSorterBuffList.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            this.listViewBuffs.Sort();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e) {
            if (listBox1.SelectedItems != null && listBox1.SelectedItems.Count > 0) {
                UpdateBuffitemsView();
            }
        }
    }
}
