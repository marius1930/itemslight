﻿namespace ItemsLight.Forms {
    partial class SymbiantMatcher {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.viewItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewBackpackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkGMIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cbSymbCharacter = new System.Windows.Forms.ComboBox();
            this.button_symb_feet = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button_symb_legs = new System.Windows.Forms.Button();
            this.bLeftArm = new System.Windows.Forms.Button();
            this.bLeftWrist = new System.Windows.Forms.Button();
            this.button_symb_waist = new System.Windows.Forms.Button();
            this.bRightArm = new System.Windows.Forms.Button();
            this.bRightHand = new System.Windows.Forms.Button();
            this.button_symb_chest = new System.Windows.Forms.Button();
            this.bEars = new System.Windows.Forms.Button();
            this.bLeftHand = new System.Windows.Forms.Button();
            this.button_symb_eyes = new System.Windows.Forms.Button();
            this.button_symb_head = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cbOnlyAvailable = new System.Windows.Forms.CheckBox();
            this.cbOwnedOnly = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbLevel = new System.Windows.Forms.Label();
            this.lbProfession = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.linkEquippedItem = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10});
            this.listView1.ContextMenuStrip = this.contextMenuStrip1;
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(220, 25);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(692, 450);
            this.listView1.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Item";
            this.columnHeader6.Width = 257;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "QL";
            this.columnHeader7.Width = 49;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Character";
            this.columnHeader8.Width = 130;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Backpack";
            this.columnHeader9.Width = 141;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Location";
            this.columnHeader10.Width = 105;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewItemToolStripMenuItem,
            this.viewBackpackToolStripMenuItem,
            this.checkGMIToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(153, 70);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // viewItemToolStripMenuItem
            // 
            this.viewItemToolStripMenuItem.Name = "viewItemToolStripMenuItem";
            this.viewItemToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.viewItemToolStripMenuItem.Text = "View Item";
            this.viewItemToolStripMenuItem.Click += new System.EventHandler(this.viewItemToolStripMenuItem_Click);
            // 
            // viewBackpackToolStripMenuItem
            // 
            this.viewBackpackToolStripMenuItem.Name = "viewBackpackToolStripMenuItem";
            this.viewBackpackToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.viewBackpackToolStripMenuItem.Text = "View Backpack";
            this.viewBackpackToolStripMenuItem.Click += new System.EventHandler(this.viewBackpackToolStripMenuItem_Click);
            // 
            // checkGMIToolStripMenuItem
            // 
            this.checkGMIToolStripMenuItem.Enabled = false;
            this.checkGMIToolStripMenuItem.Name = "checkGMIToolStripMenuItem";
            this.checkGMIToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.checkGMIToolStripMenuItem.Text = "Check GMI";
            this.checkGMIToolStripMenuItem.Click += new System.EventHandler(this.checkGMIToolStripMenuItem_Click);
            // 
            // cbSymbCharacter
            // 
            this.cbSymbCharacter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSymbCharacter.FormattingEnabled = true;
            this.cbSymbCharacter.Location = new System.Drawing.Point(6, 19);
            this.cbSymbCharacter.Name = "cbSymbCharacter";
            this.cbSymbCharacter.Size = new System.Drawing.Size(185, 21);
            this.cbSymbCharacter.TabIndex = 0;
            this.cbSymbCharacter.SelectedIndexChanged += new System.EventHandler(this.cbSymbCharacter_SelectedIndexChanged);
            // 
            // button_symb_feet
            // 
            this.button_symb_feet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_symb_feet.Image = global::ItemsLight.Properties.Resources.s_feet;
            this.button_symb_feet.Location = new System.Drawing.Point(75, 251);
            this.button_symb_feet.Name = "button_symb_feet";
            this.button_symb_feet.Size = new System.Drawing.Size(52, 52);
            this.button_symb_feet.TabIndex = 12;
            this.button_symb_feet.UseVisualStyleBackColor = true;
            this.button_symb_feet.Click += new System.EventHandler(this.button_symb_feet_Click);
            // 
            // button19
            // 
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button19.Image = global::ItemsLight.Properties.Resources.s_rightwrist;
            this.button19.Location = new System.Drawing.Point(16, 137);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(54, 54);
            this.button19.TabIndex = 6;
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button_symb_legs
            // 
            this.button_symb_legs.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_symb_legs.Image = global::ItemsLight.Properties.Resources.s_legs;
            this.button_symb_legs.Location = new System.Drawing.Point(76, 194);
            this.button_symb_legs.Margin = new System.Windows.Forms.Padding(0);
            this.button_symb_legs.Name = "button_symb_legs";
            this.button_symb_legs.Size = new System.Drawing.Size(52, 52);
            this.button_symb_legs.TabIndex = 10;
            this.button_symb_legs.UseVisualStyleBackColor = true;
            this.button_symb_legs.Click += new System.EventHandler(this.button_symb_legs_Click_1);
            // 
            // bLeftArm
            // 
            this.bLeftArm.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bLeftArm.Image = global::ItemsLight.Properties.Resources.s_leftarm1;
            this.bLeftArm.Location = new System.Drawing.Point(133, 78);
            this.bLeftArm.Name = "bLeftArm";
            this.bLeftArm.Size = new System.Drawing.Size(55, 55);
            this.bLeftArm.TabIndex = 5;
            this.bLeftArm.UseVisualStyleBackColor = true;
            this.bLeftArm.Click += new System.EventHandler(this.leftarm_Click);
            // 
            // bLeftWrist
            // 
            this.bLeftWrist.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bLeftWrist.Image = global::ItemsLight.Properties.Resources.s_leftwrist;
            this.bLeftWrist.Location = new System.Drawing.Point(134, 137);
            this.bLeftWrist.Name = "bLeftWrist";
            this.bLeftWrist.Size = new System.Drawing.Size(54, 54);
            this.bLeftWrist.TabIndex = 8;
            this.bLeftWrist.UseVisualStyleBackColor = true;
            this.bLeftWrist.Click += new System.EventHandler(this.bLeftWrist_Click);
            // 
            // button_symb_waist
            // 
            this.button_symb_waist.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_symb_waist.Image = global::ItemsLight.Properties.Resources.s_waist;
            this.button_symb_waist.Location = new System.Drawing.Point(76, 139);
            this.button_symb_waist.Name = "button_symb_waist";
            this.button_symb_waist.Size = new System.Drawing.Size(52, 52);
            this.button_symb_waist.TabIndex = 7;
            this.button_symb_waist.UseVisualStyleBackColor = true;
            this.button_symb_waist.Click += new System.EventHandler(this.button_symb_waist_Click);
            // 
            // bRightArm
            // 
            this.bRightArm.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bRightArm.Image = global::ItemsLight.Properties.Resources.s_rightarm;
            this.bRightArm.Location = new System.Drawing.Point(16, 81);
            this.bRightArm.Name = "bRightArm";
            this.bRightArm.Size = new System.Drawing.Size(54, 54);
            this.bRightArm.TabIndex = 3;
            this.bRightArm.UseVisualStyleBackColor = true;
            this.bRightArm.Click += new System.EventHandler(this.bRightArm_Click);
            // 
            // bRightHand
            // 
            this.bRightHand.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bRightHand.Image = global::ItemsLight.Properties.Resources.s_righthand;
            this.bRightHand.Location = new System.Drawing.Point(15, 194);
            this.bRightHand.Name = "bRightHand";
            this.bRightHand.Size = new System.Drawing.Size(54, 54);
            this.bRightHand.TabIndex = 9;
            this.bRightHand.UseVisualStyleBackColor = true;
            this.bRightHand.Click += new System.EventHandler(this.bRightHand_Click);
            // 
            // button_symb_chest
            // 
            this.button_symb_chest.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_symb_chest.Image = global::ItemsLight.Properties.Resources.s_chest;
            this.button_symb_chest.Location = new System.Drawing.Point(75, 82);
            this.button_symb_chest.Name = "button_symb_chest";
            this.button_symb_chest.Size = new System.Drawing.Size(52, 49);
            this.button_symb_chest.TabIndex = 4;
            this.button_symb_chest.UseVisualStyleBackColor = true;
            this.button_symb_chest.Click += new System.EventHandler(this.button_symb_chest_Click);
            // 
            // bEars
            // 
            this.bEars.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bEars.Image = global::ItemsLight.Properties.Resources.s_ears;
            this.bEars.Location = new System.Drawing.Point(134, 22);
            this.bEars.Margin = new System.Windows.Forms.Padding(0);
            this.bEars.Name = "bEars";
            this.bEars.Size = new System.Drawing.Size(54, 54);
            this.bEars.TabIndex = 2;
            this.bEars.TabStop = false;
            this.bEars.UseVisualStyleBackColor = true;
            this.bEars.Click += new System.EventHandler(this.bEars_Click);
            // 
            // bLeftHand
            // 
            this.bLeftHand.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bLeftHand.Image = global::ItemsLight.Properties.Resources.s_lefthand;
            this.bLeftHand.Location = new System.Drawing.Point(133, 193);
            this.bLeftHand.Name = "bLeftHand";
            this.bLeftHand.Size = new System.Drawing.Size(55, 55);
            this.bLeftHand.TabIndex = 11;
            this.bLeftHand.UseVisualStyleBackColor = true;
            this.bLeftHand.Click += new System.EventHandler(this.bLeftHand_Click);
            // 
            // button_symb_eyes
            // 
            this.button_symb_eyes.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_symb_eyes.Image = global::ItemsLight.Properties.Resources.s_eyes;
            this.button_symb_eyes.Location = new System.Drawing.Point(19, 26);
            this.button_symb_eyes.Margin = new System.Windows.Forms.Padding(0);
            this.button_symb_eyes.Name = "button_symb_eyes";
            this.button_symb_eyes.Size = new System.Drawing.Size(48, 47);
            this.button_symb_eyes.TabIndex = 0;
            this.button_symb_eyes.UseVisualStyleBackColor = true;
            this.button_symb_eyes.Click += new System.EventHandler(this.button_symb_eyes_Click);
            // 
            // button_symb_head
            // 
            this.button_symb_head.FlatAppearance.BorderSize = 0;
            this.button_symb_head.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_symb_head.Image = global::ItemsLight.Properties.Resources.s_head;
            this.button_symb_head.Location = new System.Drawing.Point(78, 26);
            this.button_symb_head.Margin = new System.Windows.Forms.Padding(0);
            this.button_symb_head.Name = "button_symb_head";
            this.button_symb_head.Size = new System.Drawing.Size(48, 48);
            this.button_symb_head.TabIndex = 1;
            this.button_symb_head.TabStop = false;
            this.button_symb_head.UseVisualStyleBackColor = true;
            this.button_symb_head.Click += new System.EventHandler(this.button_symb_head_Click_1);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::ItemsLight.Properties.Resources.symbiants;
            this.pictureBox1.Location = new System.Drawing.Point(11, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(183, 293);
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // cbOnlyAvailable
            // 
            this.cbOnlyAvailable.AutoSize = true;
            this.cbOnlyAvailable.Checked = true;
            this.cbOnlyAvailable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbOnlyAvailable.Location = new System.Drawing.Point(6, 46);
            this.cbOnlyAvailable.Name = "cbOnlyAvailable";
            this.cbOnlyAvailable.Size = new System.Drawing.Size(93, 17);
            this.cbOnlyAvailable.TabIndex = 1;
            this.cbOnlyAvailable.Text = "Only Available";
            this.cbOnlyAvailable.UseVisualStyleBackColor = true;
            this.cbOnlyAvailable.CheckedChanged += new System.EventHandler(this.cbOnlyAvailable_CheckedChanged);
            // 
            // cbOwnedOnly
            // 
            this.cbOwnedOnly.AutoSize = true;
            this.cbOwnedOnly.Checked = true;
            this.cbOwnedOnly.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbOwnedOnly.Enabled = false;
            this.cbOwnedOnly.Location = new System.Drawing.Point(6, 69);
            this.cbOwnedOnly.Name = "cbOwnedOnly";
            this.cbOwnedOnly.Size = new System.Drawing.Size(84, 17);
            this.cbOwnedOnly.TabIndex = 2;
            this.cbOwnedOnly.Text = "Only Owned";
            this.cbOwnedOnly.UseVisualStyleBackColor = true;
            this.cbOwnedOnly.CheckedChanged += new System.EventHandler(this.cbOwnedOnly_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbSymbCharacter);
            this.groupBox1.Controls.Add(this.cbOwnedOnly);
            this.groupBox1.Controls.Add(this.cbOnlyAvailable);
            this.groupBox1.Location = new System.Drawing.Point(2, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 92);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_symb_head);
            this.groupBox2.Controls.Add(this.button_symb_eyes);
            this.groupBox2.Controls.Add(this.button_symb_feet);
            this.groupBox2.Controls.Add(this.bLeftHand);
            this.groupBox2.Controls.Add(this.button19);
            this.groupBox2.Controls.Add(this.bEars);
            this.groupBox2.Controls.Add(this.button_symb_legs);
            this.groupBox2.Controls.Add(this.button_symb_chest);
            this.groupBox2.Controls.Add(this.bLeftArm);
            this.groupBox2.Controls.Add(this.bRightHand);
            this.groupBox2.Controls.Add(this.bLeftWrist);
            this.groupBox2.Controls.Add(this.bRightArm);
            this.groupBox2.Controls.Add(this.button_symb_waist);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Location = new System.Drawing.Point(2, 154);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(203, 321);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Symbiant";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbLevel);
            this.groupBox3.Controls.Add(this.lbProfession);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(2, 100);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(203, 51);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Character";
            // 
            // lbLevel
            // 
            this.lbLevel.AutoSize = true;
            this.lbLevel.Location = new System.Drawing.Point(92, 16);
            this.lbLevel.Name = "lbLevel";
            this.lbLevel.Size = new System.Drawing.Size(0, 13);
            this.lbLevel.TabIndex = 3;
            // 
            // lbProfession
            // 
            this.lbProfession.AutoSize = true;
            this.lbProfession.Location = new System.Drawing.Point(93, 31);
            this.lbProfession.Name = "lbProfession";
            this.lbProfession.Size = new System.Drawing.Size(0, 13);
            this.lbProfession.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Profession:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Level:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(219, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Currently equipped:";
            // 
            // linkEquippedItem
            // 
            this.linkEquippedItem.AutoSize = true;
            this.linkEquippedItem.Location = new System.Drawing.Point(319, 8);
            this.linkEquippedItem.Name = "linkEquippedItem";
            this.linkEquippedItem.Size = new System.Drawing.Size(55, 13);
            this.linkEquippedItem.TabIndex = 5;
            this.linkEquippedItem.TabStop = true;
            this.linkEquippedItem.Text = "linkLabel1";
            this.linkEquippedItem.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkEquippedItem_LinkClicked);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(723, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(198, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Green items are recommended upgrades";
            // 
            // SymbiantMatcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(924, 483);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.linkEquippedItem);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SymbiantMatcher";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "SymbiantMatcher";
            this.Load += new System.EventHandler(this.SymbiantMatcher_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.Button button_symb_feet;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button_symb_legs;
        private System.Windows.Forms.Button bLeftArm;
        private System.Windows.Forms.Button bLeftWrist;
        private System.Windows.Forms.Button button_symb_waist;
        private System.Windows.Forms.Button bRightArm;
        private System.Windows.Forms.Button bRightHand;
        private System.Windows.Forms.Button button_symb_chest;
        private System.Windows.Forms.Button bEars;
        private System.Windows.Forms.Button bLeftHand;
        private System.Windows.Forms.Button button_symb_eyes;
        private System.Windows.Forms.ComboBox cbSymbCharacter;
        private System.Windows.Forms.Button button_symb_head;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox cbOnlyAvailable;
        private System.Windows.Forms.CheckBox cbOwnedOnly;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbLevel;
        private System.Windows.Forms.Label lbProfession;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel linkEquippedItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem viewItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewBackpackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkGMIToolStripMenuItem;
        private System.Windows.Forms.Label label4;

    }
}