﻿using ItemsLight.AOIA;
using ItemsLight.Cloud;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ItemsLight.Forms {
    public partial class CloudPicker : Form {
        ConcurrentQueue<MessageDataRequest> requestQueue;
        public CloudPicker(ConcurrentQueue<MessageDataRequest> requestQueue) {
            InitializeComponent();
            this.requestQueue = requestQueue;
        }
        
        private void CloudPicker_Load(object sender, EventArgs e) {
            CloudWatcher provider = new CloudWatcher();
            dropbox.Enabled = provider.Providers.Any(m => m.Provider == CloudProviderEnum.DROPBOX);
            gdrive.Enabled = provider.Providers.Any(m => m.Provider == CloudProviderEnum.GOOGLE_DRIVE);
            skydrive.Enabled = provider.Providers.Any(m => m.Provider == CloudProviderEnum.ONEDRIVE);
        }

        private void dropbox_Click(object sender, EventArgs e) {
            Properties.Settings.Default["cloud"] = (int)CloudProviderEnum.DROPBOX;
            Properties.Settings.Default.Save();
            requestQueue.Enqueue(new MessageDataRequest { Type = RequestMessage.CREATE_BACKUP_IMMEDIATELY });
            this.Close();
        }

        private void skydrive_Click(object sender, EventArgs e) {
            Properties.Settings.Default["cloud"] = (int)CloudProviderEnum.ONEDRIVE;
            Properties.Settings.Default.Save();
            requestQueue.Enqueue(new MessageDataRequest { Type = RequestMessage.CREATE_BACKUP_IMMEDIATELY });
            this.Close();
        }

        private void gdrive_Click(object sender, EventArgs e) {
            Properties.Settings.Default["cloud"] = (int)CloudProviderEnum.GOOGLE_DRIVE;
            Properties.Settings.Default.Save();
            requestQueue.Enqueue(new MessageDataRequest { Type = RequestMessage.CREATE_BACKUP_IMMEDIATELY });
            this.Close();
        }

        private void none_Click(object sender, EventArgs e) {
            Properties.Settings.Default["cloud"] = (int)CloudProviderEnum.NONE;
            Properties.Settings.Default.Save();
            this.Close();
        }
    }
}
