﻿using ItemsLight.AOIA;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ItemsLight.Forms {
    public partial class SettingsWindow : Form {
        private Action<RequestMessage, object> SendRequest;

        public SettingsWindow(Action<RequestMessage, object> SendRequest) {
            InitializeComponent();
            this.SendRequest = SendRequest;

            Panel x;

        }

        private void SettingsWindow_Load(object sender, EventArgs e) {
            this.Dock = DockStyle.Fill;

            cbWriteSummary.Checked = (bool)Properties.Settings.Default["summaryscript"];
            cbCrashReports.Checked = (bool)Properties.Settings.Default["canSendCrashreport"];
            cbCloseToTray.Checked = (bool)Properties.Settings.Default["closetotray"];
            cbWriteWaypoints.Checked = (bool)Properties.Settings.Default["waypoints"];

            // Postfix or prefix?
            if ((bool)Properties.Settings.Default["postfixbags"]) {
                radioPostfix.Checked = true;
            }
            else {
                radioPrefix.Checked = true;
            }

            // Prefer auno or aoitems?
            if ((bool)Properties.Settings.Default["auno"]) {
                radioAuno.Checked = true;
            }
            else {
                radioAoItems.Checked = true;
            }
        }

        private void btnSetAnarchyPath_Click(object sender, EventArgs e) {
            ManullySetAnarchyPath import = new ManullySetAnarchyPath();
            if (import.ShowDialog() == DialogResult.OK) {
                MessageBox.Show("Path to anarchy online updated!\n\nYou will need to restart IA before the changes fully take effect");
                //canExit = true; // TODO:
                this.Close();
            }
        }

        private void cbCrashReports_CheckedChanged(object sender, EventArgs e) {
            FirefoxCheckBox item = sender as FirefoxCheckBox;
            Properties.Settings.Default["canSendCrashreport"] = item.Checked;
            Properties.Settings.Default.Save();
        }

        private void cbWriteSummary_CheckedChanged(object sender, EventArgs e) {
            FirefoxCheckBox item = sender as FirefoxCheckBox;
            Properties.Settings.Default["summaryscript"] = item.Checked;
            Properties.Settings.Default.Save();
        }

        private void cbCloseToTray_CheckedChanged(object sender, EventArgs e) {
            Properties.Settings.Default["closetotray"] = cbCloseToTray.Checked;
            Properties.Settings.Default.Save();
        }

        private void cbWriteWaypoints_CheckedChanged(object sender, EventArgs e) {
            FirefoxCheckBox item = sender as FirefoxCheckBox;
            Properties.Settings.Default["waypoints"] = item.Checked;
            Properties.Settings.Default.Save();
        }

        private void radioPrefix_CheckedChanged(object sender, EventArgs e) {
            FirefoxRadioButton item = sender as FirefoxRadioButton;
            if (item.Checked) {
                Properties.Settings.Default["postfixbags"] = false;
                Properties.Settings.Default.Save();
            }
        }

        private void radioPostfix_CheckedChanged(object sender, EventArgs e) {
            FirefoxRadioButton item = sender as FirefoxRadioButton;
            if (item.Checked) {
                Properties.Settings.Default["postfixbags"] = true;
                Properties.Settings.Default.Save();
            }
        }

        private void btnUpdateBackpacks_Click(object sender, EventArgs e) {
            SendRequest(RequestMessage.UPDATE_BACKPACK_DATABASE, null);
        }

        private void btnViewLogs_Click(object sender, EventArgs e) {
            String appdata = Environment.GetEnvironmentVariable("LocalAppData");
            string dir = Path.Combine(appdata, "EvilSoft", "IALight");
            Process.Start("file://" + dir);
        }

        private void radioAuno_CheckedChanged(object sender, EventArgs e) {
            FirefoxRadioButton item = sender as FirefoxRadioButton;
            if (item.Checked) {
                Properties.Settings.Default["auno"] = true;
                Properties.Settings.Default.Save();
            }
        }

        private void radioAoItems_CheckedChanged(object sender, EventArgs e) {
            FirefoxRadioButton item = sender as FirefoxRadioButton;
            if (item.Checked) {
                Properties.Settings.Default["auno"] = false;
                Properties.Settings.Default.Save();
            }
        }

        private void panelBox4_Click(object sender, EventArgs e) {

        }

        private void buttonDeveloper_Click(object sender, EventArgs e) {
            System.Diagnostics.Process.Start("http://forums.anarchy-online.com/private.php?do=newpm&u=702799");
        }

        private void buttonForum_Click(object sender, EventArgs e) {
            System.Diagnostics.Process.Start("http://forums.anarchy-online.com/showthread.php?614989-Item-Assistant");
        }

        /// <summary>
        /// Menu => Import database from AOIA
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImport_Click(object sender, EventArgs e) {
            IAImport import = new IAImport();
            if (import.ShowDialog() == DialogResult.OK) {
                bool reset = import.WipeExisting;
                String fullpath = Path.Combine(import.Path, "ItemAssistant.db");
                if (!File.Exists(fullpath))
                    MessageBox.Show("The selected folder is not a valid install of Item Assistant.", "error", MessageBoxButtons.OK);
                else
                    SendRequest(RequestMessage.IMPORT_AOIA, new AOIAImportRequest { Path = fullpath, ClearExistingDatabase = reset });
            }
        }

        

    }
}
