﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ItemsLight.AOIA.Misc;
using System.IO;

namespace ItemsLight {
    public partial class ManullySetAnarchyPath : Form {
        public ManullySetAnarchyPath() {
            InitializeComponent();
        }

        private void ManullySetAnarchyPath_Load(object sender, EventArgs e) {
            if (!string.IsNullOrEmpty(Properties.Settings.Default["pathToAnarchy"] as String)) {
                this.textBox1.Text = Properties.Settings.Default["pathToAnarchy"] as String;
            }
        }

        
        private void buttonCancel_Click_1(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
            this.Close();

        }

        private void buttonOk_Click_1(object sender, EventArgs e) {
            if (Directory.Exists(this.textBox1.Text)
                && (File.Exists(Path.Combine(this.textBox1.Text, "Anarchy.exe")) || File.Exists(Path.Combine(this.textBox1.Text, "Client.exe")) || File.Exists(Path.Combine(this.textBox1.Text, "AnarchyOnline.exe")))) {
                Properties.Settings.Default["pathToAnarchy"] = this.textBox1.Text;
                Properties.Settings.Default.Save();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else {
                MessageBox.Show("Could not locate anarchy online in specified directory!");
            }
        }

        private void buttonBrowse_Click_1(object sender, EventArgs e) {
            FolderBrowserDialog diag = new FolderBrowserDialog();
            if (diag.ShowDialog() == DialogResult.OK) {
                this.DialogResult = DialogResult.None;
                textBox1.Text = diag.SelectedPath;
            }

        }

        private void buttonCancel_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
            this.Close();

        }


    }
}
