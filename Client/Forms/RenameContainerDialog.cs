﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ItemsLight {
    public partial class RenameContainerDialog : Form {
        private string label;
        public string Label {
            get {
                return label;
            }
        }

        public RenameContainerDialog(string label) {
            InitializeComponent();
            this.label = label;
            textBox1.Text = label;
        }

        private void buttonCancel_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonRename_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e) {
            this.label = textBox1.Text;
        }
    }
}
