﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

internal abstract class ThemeContainerControl : ContainerControl {
    protected Graphics G;

    protected Bitmap B;

    private bool _NoRounding;

    private Rectangle _Rectangle;

    private LinearGradientBrush _Gradient;

    public bool NoRounding {
        get {
            return this._NoRounding;
        }
        set {
            this._NoRounding = value;
            this.Invalidate();
        }
    }

    public ThemeContainerControl() {
        this.SetStyle(ControlStyles.UserPaint | ControlStyles.Opaque | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
        this.B = new Bitmap(1, 1);
        this.G = Graphics.FromImage(this.B);
    }

    public void AllowTransparent() {
        this.SetStyle(ControlStyles.Opaque, false);
        this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
    }

    public abstract void PaintHook();

    protected sealed override void OnPaint(PaintEventArgs e) {
        if (this.Width != 0) {
            if (this.Height == 0) {
                return;
            }
            this.PaintHook();
            e.Graphics.DrawImage(this.B, 0, 0);
        }
    }

    protected override void OnSizeChanged(EventArgs e) {
        if (this.Width != 0 && this.Height != 0) {
            this.B = new Bitmap(this.Width, this.Height);
            this.G = Graphics.FromImage(this.B);
            this.Invalidate();
        }
        base.OnSizeChanged(e);
    }

    protected void DrawCorners(Color c, Rectangle rect) {
        if (this._NoRounding) {
            return;
        }
        this.B.SetPixel(rect.X, rect.Y, c);
        checked {
            this.B.SetPixel(rect.X + (rect.Width - 1), rect.Y, c);
            this.B.SetPixel(rect.X, rect.Y + (rect.Height - 1), c);
            this.B.SetPixel(rect.X + (rect.Width - 1), rect.Y + (rect.Height - 1), c);
        }
    }

    protected void DrawBorders(Pen p1, Pen p2, Rectangle rect) {
        checked {
            this.G.DrawRectangle(p1, rect.X, rect.Y, rect.Width - 1, rect.Height - 1);
            this.G.DrawRectangle(p2, rect.X + 1, rect.Y + 1, rect.Width - 3, rect.Height - 3);
        }
    }

    protected void DrawGradient(Color c1, Color c2, int x, int y, int width, int height, float angle) {
        this._Rectangle = new Rectangle(x, y, width, height);
        this._Gradient = new LinearGradientBrush(this._Rectangle, c1, c2, angle);
        this.G.FillRectangle(this._Gradient, this._Rectangle);
    }
}
