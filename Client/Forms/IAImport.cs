﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ItemsLight.AOIA.Misc;
using EvilsoftCommons.DllInjector;

namespace ItemsLight {
    public partial class IAImport : Form {
        public IAImport() {
            InitializeComponent();
        }

        private string path;
        public string Path {
            get {
                return path;
            }
        }

        public bool WipeExisting {
            get;
            protected set;
        }

        private void buttonCancel_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonBrowse_Click(object sender, EventArgs e) {
            FolderBrowserDialog diag = new FolderBrowserDialog();
            if (diag.ShowDialog() == DialogResult.OK) {
                this.DialogResult = DialogResult.None;
                path = diag.SelectedPath;
                textBox1.Text = path;
            }
        }

        private void IAImport_Load(object sender, EventArgs e) {
            WipeExisting = checkBox1.Checked;

            string path;
            /*if (AppdataPathFinder.FindPathToIA(out path)) {
                this.path = path;
                textBox1.Text = path;
            }*/
        }
        HashSet<uint> pids = DllInjector.FindProcessForWindow("Anarchy client");

        private void checkBox1_CheckedChanged(object sender, EventArgs e) {
            WipeExisting = checkBox1.Checked;
        }

        private void textBox1_TextChanged(object sender, EventArgs e) {
            this.path = textBox1.Text;
        }
    }
}
