﻿namespace ItemsLight.Forms {
    partial class CloudPicker {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dropbox = new System.Windows.Forms.Button();
            this.gdrive = new System.Windows.Forms.Button();
            this.skydrive = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.none = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dropbox
            // 
            this.dropbox.Image = global::ItemsLight.Properties.Resources.dropbox;
            this.dropbox.Location = new System.Drawing.Point(12, 25);
            this.dropbox.Name = "dropbox";
            this.dropbox.Size = new System.Drawing.Size(54, 54);
            this.dropbox.TabIndex = 2;
            this.dropbox.UseVisualStyleBackColor = true;
            this.dropbox.Click += new System.EventHandler(this.dropbox_Click);
            // 
            // gdrive
            // 
            this.gdrive.Image = global::ItemsLight.Properties.Resources.gdrive;
            this.gdrive.Location = new System.Drawing.Point(132, 25);
            this.gdrive.Name = "gdrive";
            this.gdrive.Size = new System.Drawing.Size(54, 54);
            this.gdrive.TabIndex = 1;
            this.gdrive.UseVisualStyleBackColor = true;
            this.gdrive.Click += new System.EventHandler(this.gdrive_Click);
            // 
            // skydrive
            // 
            this.skydrive.Image = global::ItemsLight.Properties.Resources.onedrive;
            this.skydrive.Location = new System.Drawing.Point(72, 25);
            this.skydrive.Name = "skydrive";
            this.skydrive.Size = new System.Drawing.Size(54, 54);
            this.skydrive.TabIndex = 0;
            this.skydrive.UseVisualStyleBackColor = true;
            this.skydrive.Click += new System.EventHandler(this.skydrive_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(235, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Pick your cloud provider for automatic backups..";
            // 
            // none
            // 
            this.none.Location = new System.Drawing.Point(192, 25);
            this.none.Name = "none";
            this.none.Size = new System.Drawing.Size(54, 54);
            this.none.TabIndex = 7;
            this.none.Text = "None";
            this.none.UseVisualStyleBackColor = true;
            this.none.Click += new System.EventHandler(this.none_Click);
            // 
            // CloudPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(257, 90);
            this.Controls.Add(this.none);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dropbox);
            this.Controls.Add(this.gdrive);
            this.Controls.Add(this.skydrive);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(273, 129);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(273, 129);
            this.Name = "CloudPicker";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pick your cloud provider..";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.CloudPicker_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button skydrive;
        private System.Windows.Forms.Button gdrive;
        private System.Windows.Forms.Button dropbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button none;
    }
}