﻿namespace ItemsLight.Forms {
    partial class SettingsWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panelBox4 = new PanelBox();
            this.btnUpdateBackpacks = new FirefoxButton();
            this.btnViewLogs = new FirefoxButton();
            this.btnSetAnarchyPath = new FirefoxButton();
            this.panelBox2 = new PanelBox();
            this.radioPostfix = new FirefoxRadioButton();
            this.radioPrefix = new FirefoxRadioButton();
            this.cbCloseToTray = new FirefoxCheckBox();
            this.cbWriteSummary = new FirefoxCheckBox();
            this.cbWriteWaypoints = new FirefoxCheckBox();
            this.cbCrashReports = new FirefoxCheckBox();
            this.panelBox1 = new PanelBox();
            this.radioAuno = new FirefoxRadioButton();
            this.radioAoItems = new FirefoxRadioButton();
            this.panelBox3 = new PanelBox();
            this.panelBox5 = new PanelBox();
            this.buttonImport = new FirefoxButton();
            this.buttonDeveloper = new FirefoxButton();
            this.buttonForum = new FirefoxButton();
            this.panelBox4.SuspendLayout();
            this.panelBox2.SuspendLayout();
            this.panelBox1.SuspendLayout();
            this.panelBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBox4
            // 
            this.panelBox4.Controls.Add(this.btnUpdateBackpacks);
            this.panelBox4.Controls.Add(this.btnViewLogs);
            this.panelBox4.Controls.Add(this.btnSetAnarchyPath);
            this.panelBox4.Font = new System.Drawing.Font("Segoe UI Semibold", 20F);
            this.panelBox4.HeaderHeight = 40;
            this.panelBox4.Location = new System.Drawing.Point(323, 20);
            this.panelBox4.Name = "panelBox4";
            this.panelBox4.NoRounding = false;
            this.panelBox4.Size = new System.Drawing.Size(230, 184);
            this.panelBox4.TabIndex = 25;
            this.panelBox4.Text = "Actions";
            this.panelBox4.TextLocation = "5; 0";
            this.panelBox4.Click += new System.EventHandler(this.panelBox4_Click);
            // 
            // btnUpdateBackpacks
            // 
            this.btnUpdateBackpacks.EnabledCalc = true;
            this.btnUpdateBackpacks.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnUpdateBackpacks.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(68)))), ((int)(((byte)(80)))));
            this.btnUpdateBackpacks.Location = new System.Drawing.Point(15, 53);
            this.btnUpdateBackpacks.Name = "btnUpdateBackpacks";
            this.btnUpdateBackpacks.Size = new System.Drawing.Size(192, 32);
            this.btnUpdateBackpacks.TabIndex = 8;
            this.btnUpdateBackpacks.Text = "Detect new Backpacks";
            this.btnUpdateBackpacks.Click += new System.EventHandler(this.btnUpdateBackpacks_Click);
            // 
            // btnViewLogs
            // 
            this.btnViewLogs.EnabledCalc = true;
            this.btnViewLogs.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnViewLogs.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(68)))), ((int)(((byte)(80)))));
            this.btnViewLogs.Location = new System.Drawing.Point(15, 129);
            this.btnViewLogs.Name = "btnViewLogs";
            this.btnViewLogs.Size = new System.Drawing.Size(192, 32);
            this.btnViewLogs.TabIndex = 5;
            this.btnViewLogs.Text = "View Logs";
            this.btnViewLogs.Click += new System.EventHandler(this.btnViewLogs_Click);
            // 
            // btnSetAnarchyPath
            // 
            this.btnSetAnarchyPath.EnabledCalc = true;
            this.btnSetAnarchyPath.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSetAnarchyPath.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(68)))), ((int)(((byte)(80)))));
            this.btnSetAnarchyPath.Location = new System.Drawing.Point(15, 91);
            this.btnSetAnarchyPath.Name = "btnSetAnarchyPath";
            this.btnSetAnarchyPath.Size = new System.Drawing.Size(192, 32);
            this.btnSetAnarchyPath.TabIndex = 6;
            this.btnSetAnarchyPath.Text = "Configure path to Anarchy";
            this.btnSetAnarchyPath.Click += new System.EventHandler(this.btnSetAnarchyPath_Click);
            // 
            // panelBox2
            // 
            this.panelBox2.Controls.Add(this.radioPostfix);
            this.panelBox2.Controls.Add(this.radioPrefix);
            this.panelBox2.Font = new System.Drawing.Font("Segoe UI Semibold", 20F);
            this.panelBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.panelBox2.HeaderHeight = 40;
            this.panelBox2.Location = new System.Drawing.Point(22, 151);
            this.panelBox2.Name = "panelBox2";
            this.panelBox2.NoRounding = false;
            this.panelBox2.Size = new System.Drawing.Size(280, 118);
            this.panelBox2.TabIndex = 21;
            this.panelBox2.Text = "Backpack Renaming";
            this.panelBox2.TextLocation = "5; 0";
            // 
            // radioPostfix
            // 
            this.radioPostfix.Bold = false;
            this.radioPostfix.Checked = false;
            this.radioPostfix.EnabledCalc = true;
            this.radioPostfix.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radioPostfix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(78)))), ((int)(((byte)(90)))));
            this.radioPostfix.Location = new System.Drawing.Point(3, 80);
            this.radioPostfix.Name = "radioPostfix";
            this.radioPostfix.Size = new System.Drawing.Size(160, 27);
            this.radioPostfix.TabIndex = 10;
            this.radioPostfix.Text = "Postfix Bag names";
            this.radioPostfix.CheckedChanged += new FirefoxRadioButton.CheckedChangedEventHandler(this.radioPostfix_CheckedChanged);
            // 
            // radioPrefix
            // 
            this.radioPrefix.Bold = false;
            this.radioPrefix.Checked = false;
            this.radioPrefix.EnabledCalc = true;
            this.radioPrefix.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radioPrefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(78)))), ((int)(((byte)(90)))));
            this.radioPrefix.Location = new System.Drawing.Point(3, 47);
            this.radioPrefix.Name = "radioPrefix";
            this.radioPrefix.Size = new System.Drawing.Size(160, 27);
            this.radioPrefix.TabIndex = 11;
            this.radioPrefix.Text = "Prefix Bag names";
            this.radioPrefix.CheckedChanged += new FirefoxRadioButton.CheckedChangedEventHandler(this.radioPrefix_CheckedChanged);
            // 
            // cbCloseToTray
            // 
            this.cbCloseToTray.Bold = false;
            this.cbCloseToTray.Checked = false;
            this.cbCloseToTray.EnabledCalc = true;
            this.cbCloseToTray.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cbCloseToTray.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(78)))), ((int)(((byte)(90)))));
            this.cbCloseToTray.Location = new System.Drawing.Point(27, 357);
            this.cbCloseToTray.Name = "cbCloseToTray";
            this.cbCloseToTray.Size = new System.Drawing.Size(163, 29);
            this.cbCloseToTray.TabIndex = 9;
            this.cbCloseToTray.Text = "Close to Tray";
            this.cbCloseToTray.CheckedChanged += new FirefoxCheckBox.CheckedChangedEventHandler(this.cbCloseToTray_CheckedChanged);
            // 
            // cbWriteSummary
            // 
            this.cbWriteSummary.Bold = false;
            this.cbWriteSummary.Checked = false;
            this.cbWriteSummary.EnabledCalc = true;
            this.cbWriteSummary.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cbWriteSummary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(78)))), ((int)(((byte)(90)))));
            this.cbWriteSummary.Location = new System.Drawing.Point(27, 423);
            this.cbWriteSummary.Name = "cbWriteSummary";
            this.cbWriteSummary.Size = new System.Drawing.Size(163, 29);
            this.cbWriteSummary.TabIndex = 4;
            this.cbWriteSummary.Text = "Write summary script";
            this.cbWriteSummary.CheckedChanged += new FirefoxCheckBox.CheckedChangedEventHandler(this.cbWriteSummary_CheckedChanged);
            // 
            // cbWriteWaypoints
            // 
            this.cbWriteWaypoints.Bold = false;
            this.cbWriteWaypoints.Checked = false;
            this.cbWriteWaypoints.EnabledCalc = true;
            this.cbWriteWaypoints.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cbWriteWaypoints.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(78)))), ((int)(((byte)(90)))));
            this.cbWriteWaypoints.Location = new System.Drawing.Point(27, 390);
            this.cbWriteWaypoints.Name = "cbWriteWaypoints";
            this.cbWriteWaypoints.Size = new System.Drawing.Size(185, 29);
            this.cbWriteWaypoints.TabIndex = 3;
            this.cbWriteWaypoints.Text = "Write waypoints script";
            this.cbWriteWaypoints.CheckedChanged += new FirefoxCheckBox.CheckedChangedEventHandler(this.cbWriteWaypoints_CheckedChanged);
            // 
            // cbCrashReports
            // 
            this.cbCrashReports.Bold = false;
            this.cbCrashReports.Checked = false;
            this.cbCrashReports.EnabledCalc = true;
            this.cbCrashReports.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cbCrashReports.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(78)))), ((int)(((byte)(90)))));
            this.cbCrashReports.Location = new System.Drawing.Point(27, 324);
            this.cbCrashReports.Name = "cbCrashReports";
            this.cbCrashReports.Size = new System.Drawing.Size(163, 29);
            this.cbCrashReports.TabIndex = 0;
            this.cbCrashReports.Text = "Send crash reports";
            this.cbCrashReports.CheckedChanged += new FirefoxCheckBox.CheckedChangedEventHandler(this.cbCrashReports_CheckedChanged);
            // 
            // panelBox1
            // 
            this.panelBox1.Controls.Add(this.radioAuno);
            this.panelBox1.Controls.Add(this.radioAoItems);
            this.panelBox1.Font = new System.Drawing.Font("Segoe UI Semibold", 20F);
            this.panelBox1.HeaderHeight = 40;
            this.panelBox1.Location = new System.Drawing.Point(22, 20);
            this.panelBox1.Name = "panelBox1";
            this.panelBox1.NoRounding = false;
            this.panelBox1.Size = new System.Drawing.Size(280, 125);
            this.panelBox1.TabIndex = 22;
            this.panelBox1.Text = "Items Website";
            this.panelBox1.TextLocation = "5; 0";
            // 
            // radioAuno
            // 
            this.radioAuno.Bold = false;
            this.radioAuno.Checked = false;
            this.radioAuno.EnabledCalc = true;
            this.radioAuno.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radioAuno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(78)))), ((int)(((byte)(90)))));
            this.radioAuno.Location = new System.Drawing.Point(8, 49);
            this.radioAuno.Name = "radioAuno";
            this.radioAuno.Size = new System.Drawing.Size(160, 27);
            this.radioAuno.TabIndex = 2;
            this.radioAuno.Text = "Auno";
            this.radioAuno.CheckedChanged += new FirefoxRadioButton.CheckedChangedEventHandler(this.radioAuno_CheckedChanged);
            // 
            // radioAoItems
            // 
            this.radioAoItems.Bold = false;
            this.radioAoItems.Checked = false;
            this.radioAoItems.EnabledCalc = true;
            this.radioAoItems.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radioAoItems.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(78)))), ((int)(((byte)(90)))));
            this.radioAoItems.Location = new System.Drawing.Point(8, 82);
            this.radioAoItems.Name = "radioAoItems";
            this.radioAoItems.Size = new System.Drawing.Size(160, 27);
            this.radioAoItems.TabIndex = 1;
            this.radioAoItems.Text = "AOItems";
            this.radioAoItems.CheckedChanged += new FirefoxRadioButton.CheckedChangedEventHandler(this.radioAoItems_CheckedChanged);
            // 
            // panelBox3
            // 
            this.panelBox3.Font = new System.Drawing.Font("Segoe UI Semibold", 20F);
            this.panelBox3.HeaderHeight = 40;
            this.panelBox3.Location = new System.Drawing.Point(22, 275);
            this.panelBox3.Name = "panelBox3";
            this.panelBox3.NoRounding = false;
            this.panelBox3.Size = new System.Drawing.Size(280, 190);
            this.panelBox3.TabIndex = 24;
            this.panelBox3.Text = "Settings";
            this.panelBox3.TextLocation = "5; 0";
            // 
            // panelBox5
            // 
            this.panelBox5.Controls.Add(this.buttonImport);
            this.panelBox5.Controls.Add(this.buttonDeveloper);
            this.panelBox5.Controls.Add(this.buttonForum);
            this.panelBox5.Font = new System.Drawing.Font("Segoe UI Semibold", 20F);
            this.panelBox5.HeaderHeight = 40;
            this.panelBox5.Location = new System.Drawing.Point(323, 210);
            this.panelBox5.Name = "panelBox5";
            this.panelBox5.NoRounding = false;
            this.panelBox5.Size = new System.Drawing.Size(230, 188);
            this.panelBox5.TabIndex = 26;
            this.panelBox5.Text = "Misc";
            this.panelBox5.TextLocation = "5; 0";
            // 
            // buttonImport
            // 
            this.buttonImport.EnabledCalc = true;
            this.buttonImport.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.buttonImport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(68)))), ((int)(((byte)(80)))));
            this.buttonImport.Location = new System.Drawing.Point(15, 130);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(192, 32);
            this.buttonImport.TabIndex = 29;
            this.buttonImport.Text = "Import from AOIA";
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // buttonDeveloper
            // 
            this.buttonDeveloper.EnabledCalc = true;
            this.buttonDeveloper.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.buttonDeveloper.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(68)))), ((int)(((byte)(80)))));
            this.buttonDeveloper.Location = new System.Drawing.Point(15, 54);
            this.buttonDeveloper.Name = "buttonDeveloper";
            this.buttonDeveloper.Size = new System.Drawing.Size(192, 32);
            this.buttonDeveloper.TabIndex = 28;
            this.buttonDeveloper.Text = "Contact Developer";
            this.buttonDeveloper.Click += new System.EventHandler(this.buttonDeveloper_Click);
            // 
            // buttonForum
            // 
            this.buttonForum.EnabledCalc = true;
            this.buttonForum.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.buttonForum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(68)))), ((int)(((byte)(80)))));
            this.buttonForum.Location = new System.Drawing.Point(15, 92);
            this.buttonForum.Name = "buttonForum";
            this.buttonForum.Size = new System.Drawing.Size(192, 32);
            this.buttonForum.TabIndex = 27;
            this.buttonForum.Text = "Open Forum";
            this.buttonForum.Click += new System.EventHandler(this.buttonForum_Click);
            // 
            // SettingsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 483);
            this.Controls.Add(this.panelBox5);
            this.Controls.Add(this.panelBox4);
            this.Controls.Add(this.panelBox2);
            this.Controls.Add(this.cbCloseToTray);
            this.Controls.Add(this.cbWriteSummary);
            this.Controls.Add(this.cbWriteWaypoints);
            this.Controls.Add(this.cbCrashReports);
            this.Controls.Add(this.panelBox1);
            this.Controls.Add(this.panelBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsWindow";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.SettingsWindow_Load);
            this.panelBox4.ResumeLayout(false);
            this.panelBox2.ResumeLayout(false);
            this.panelBox1.ResumeLayout(false);
            this.panelBox5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private FirefoxCheckBox cbCrashReports;
        private FirefoxRadioButton radioAoItems;
        private FirefoxRadioButton radioAuno;
        private FirefoxCheckBox cbWriteWaypoints;
        private FirefoxCheckBox cbWriteSummary;
        private FirefoxButton btnViewLogs;
        private FirefoxButton btnSetAnarchyPath;
        private FirefoxButton btnUpdateBackpacks;
        private FirefoxCheckBox cbCloseToTray;
        private FirefoxRadioButton radioPostfix;
        private FirefoxRadioButton radioPrefix;
        private PanelBox panelBox2;
        private PanelBox panelBox1;
        private PanelBox panelBox3;
        private PanelBox panelBox4;
        private PanelBox panelBox5;
        private FirefoxButton buttonForum;
        private FirefoxButton buttonDeveloper;
        private FirefoxButton buttonImport;
    }
}