﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Model {
    public class GMIItem {
        protected virtual long Id { get; set; }
        public virtual long ItemId { get; set; }
        public virtual Character Owner { get; protected set; }
        public virtual String Name {
            get {
                if (db == null)
                    return "Unknown Item";
                return db.Name;
            }
        }

        public virtual long CharacterId { get; set; }
        public virtual int QL { get; set; }
        protected virtual GMIDatabaseItem db { get; set; }
    }
}
