﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Model {
    public class Character {
        private string owner;

        public virtual long Id {
            get;
            set;
        }
        public virtual long Credits {
            get;
            set;
        }
        public virtual long CreditsGMI {
            get;
            set;
        }
        public virtual long VP {
            get;
            set;
        }
        public virtual int Dimension {
            get;
            set;
        }
        public virtual string Name {
            get {
                if (string.IsNullOrEmpty(owner))
                    return Id.ToString();
                else
                    return owner;
            }
            set {
                owner = value;
            }
        }
        public virtual long ZoneId {
            get;
            set;
        }
        public virtual long SubZoneId {
            get;
            set;
        }
        public virtual float PositionX {
            get;
            set;
        }
        public virtual float PositionZ {
            get;
            set;
        }
        public virtual DateTime LastSeen {
            get;
            set;
        }
        public virtual int Level {
            get;
            set;
        }
        public virtual long ProfessionId {
            get;
            set;
        }
        public virtual string Profession {
            get {

                string[] professionlist = {"?", "Soldier", "Martial Artist", "Engineer", "Fixer", "Agent", 
                                        "Adventurer", "Trader", "Bureaucrat", 
                                        "Enforcer", "Doctor", "Nano-Technician", "Meta-Physicist", "?13", "Keeper", "Shade"};
                if (ProfessionId >= 0 && ProfessionId < professionlist.Length)
                    return professionlist[ProfessionId];
                else
                    return "?";
            }
        }

        // OBS: Unmapped properties
        public virtual long NumItems { get; set; }
        public virtual long NumContainers { get; set; }
    }
}
