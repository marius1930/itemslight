﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Model {
    /// <summary>
    /// Transient class
    /// </summary>
    public class PocketBoss {
        protected long Id { get; set; }
        protected int QL { get; set; }
        public virtual string Name { get; set; }
    }
}
