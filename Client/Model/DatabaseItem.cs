﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Model {
    class DatabaseItem {
        public virtual long Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int QL { get; set; }
        public virtual string Type { get; set; }
        public virtual long Flags { get; set; }

        /// <summary>
        /// Do not name "Properties", reserved keyword causing issues.
        /// </summary>
        public virtual long PropertyFlags { get; set; }
        public virtual long ISlot { get; set; }


        //public virtual IDictionary<int, int> Attributes { get; set; }
        public virtual ISet<AOItemEffect> Effects { get; set; }
    }
}
