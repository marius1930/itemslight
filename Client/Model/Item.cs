﻿using ItemsLight.AOIA;
using ItemsLight.AOIA.Misc;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Model {
    class Item {

        public virtual long Id { get; protected set; }
        public virtual long LowKey { get; set; }
        public virtual long HighKey { get; set; }

        private string name;
        public virtual string Name {
            get {
                if (!string.IsNullOrEmpty(name))
                    return name + StackStr;

                // Scaled name
                else if (InternalLow != null) {
                    if (InternalHigh != null) {
                        float m = (QL - InternalLow.QL) / (1.0f * InternalHigh.QL - InternalLow.QL);
                        if (m < .5)
                            return InternalLow.Name + StackStr;
                        else
                            return InternalHigh.Name + StackStr;
                    }
                    else {
                        // No high? No scale!
                        return InternalLow.Name;
                    }
                }

                else
                    return string.Format("Unknown item {0}", Id);
            }

            set {
                name = value;
            }
        }

        private long characterId;
        public virtual long CharacterId {
            set {
                characterId = value;
            }
            get {
                if (characterId == 0 && Owner != null)
                    return Owner.Id;
                else
                    return characterId;
            }
        }


        public virtual int QL { get; set; }

        /// <summary>
        /// ID of the parent container
        /// </summary>
        public virtual long ParentContainerId { get; set; }


        /// <summary>
        /// ID of the containing inventory (parent of parent)
        /// This field is not auto-mapped
        /// </summary>
        public virtual long ParentParentContainerId { get; set; }
        

        /// <summary>
        /// Name of the parent container
        /// </summary>
        public virtual string ParentContainerName { get; set; }


        public virtual long Children { get; set; } // To enter backpack
        public virtual Character Owner { get; set; }


        private string parentInventory;

        /// <summary>
        /// Inventory / Bank / Etc
        /// If ParentContainerId is set, this will get mapped automatically
        /// </summary>
        public virtual string ParentInventory {
            get {
                if (ParentContainerName == null && ParentParentContainerId == 0 && ParentContainerId <= 2)
                    return Inventory.MapLocation((InventoryIds)ParentContainerId, Slot);
                else if (ParentParentContainerId == 2 || ParentParentContainerId == 1)
                    return Inventory.MapLocationSimple((InventoryIds)ParentParentContainerId, Slot);                
                else
                    return parentInventory;
            }
            set {
                parentInventory = value;
            }
        } // Bank or Inventory

        private string StackStr {
            get {
                if (Stack <= 1)
                    return "";
                else
                    return string.Format(" ({0})", Stack);
            }
        }

        public virtual int Slot { get; set; }
        public virtual uint Stack { get; set; }
        public virtual long Flags { get; set; }

        public virtual string Extras { get; set; }

        public DatabaseItem InternalLow { protected get; set; }
        public DatabaseItem InternalHigh { protected get; set; }

        public virtual long EquipSlot {
            get {
                return InternalLow.ISlot;
            }
        }
        public int GetSkill(int skillId) {
            if (InternalHigh == null || InternalHigh.Effects == null || InternalLow == null || InternalLow.Effects == null)
                return int.MinValue;

            var effectHigh = InternalHigh.Effects.Where(m => m.Type == skillId).FirstOrDefault();
            var effectLow = InternalLow.Effects.Where(m => m.Type == skillId).FirstOrDefault();

            if (effectHigh == null || effectLow == null)
                return int.MinValue;
            else 
                return (int)(effectLow.Value + QL * Scalar * (effectHigh.Value - effectLow.Value));
        }

        public void InitializeEffects() {
            NHibernateUtil.Initialize(InternalHigh.Effects);
        }


        public virtual Pattern Piece { get; protected set; }


        /// <summary>
        /// Stat * QL * Scalar = Skill
        /// </summary>
        protected virtual float Scalar {
            get {
                if (InternalLow == null || InternalHigh == null)
                    return 1.0f;
                else if (LowKey == HighKey)
                    return 1.0f;
                else {
                    return 1.0f / (InternalHigh.QL - InternalLow.QL);
                }
            }
        }
    }
}
