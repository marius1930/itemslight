﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Model {
    class PocketBossLocation {
        public virtual string Playfield { get; protected set; }
        public virtual string Location { get; protected set; }
        public virtual string Bosses { get; protected set; }
        public virtual string Name { get; protected set; }
    }
}
