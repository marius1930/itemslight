﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Model {
    /// <summary>
    /// Transient class
    /// </summary>
     class Pattern {
        protected long Id { get; set; }
        public virtual string Piece { get; set; }
        public virtual string Name { get; set; }

        public virtual int a { get; set; }
        public virtual int b { get; set; }
        public virtual int c { get; set; }
        public virtual int d { get; set; }


        public virtual void Add(string type) {
            if (type.Contains("A"))
                a++;
            if (type.Contains("B"))
                b++;
            if (type.Contains("C"))
                c++;
            if (type.Contains("D"))
                d++;
        }

        public virtual float Total {
            get {
                int completes = Math.Min(Math.Min(Math.Min(a, b), c), d);
                float sum = completes;
                if (a > completes)
                    sum += 0.25f;
                if (b > completes)
                    sum += 0.25f;
                if (c > completes)
                    sum += 0.25f;
                if (d > completes)
                    sum += 0.25f;

                return sum;
            }
        }



    }
}
