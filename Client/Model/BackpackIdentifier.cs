﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Model {
    /// <summary>
    /// Used only to classify an item ID as a backpack
    /// Does in no way represent an owned backpack or its contents
    /// </summary>
    public class BackpackIdentifier {
        public long Id { get; set; }
    }
}
