﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Model {
    public class Item {
        public long LowKey { get; set; }
        public long HighKey { get; set; }
        public string Name { get; set; }

        public int QL { get; set; }
        public long parentContainerId { get; set; }
        public string parentContainer { get; set; }

        public long Children { get; set; } // To enter backpack
        public Character Owner { get; set; }
        
        
        public string ParentInventory { get; set; } // Bank or Inventory

        public uint Slot { get; set; }

        public string Extras { get; set; }
    }
}
