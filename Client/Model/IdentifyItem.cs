﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Model {
    class IdentifyItem {
        public virtual long LowKey { get; set; }
        public virtual long HighKey { get; set; }
        public virtual ICollection<Item> Items { get; set; }
    }
}
