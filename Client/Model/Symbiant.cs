﻿using ItemsLight.AOIA.Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Model {
    public class Symbiant {
        public virtual long Id { get; set; }

        private string name;
        public virtual string Name {
            get {
                return name;
            }
            set {
                name = value;
            }
        }
        public virtual string Slot { get; set; }
        public virtual int QL { get; set; }
        public virtual string Type { get; set; }
        public virtual long AOID { get; set; }
        protected string Origin { get; set; }

        
    }
}
