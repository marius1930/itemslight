﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Model {
    public class GMIDatabaseItem {
        public virtual long Id { get; set; }
        public virtual string Name { get; set; }
    }
}
