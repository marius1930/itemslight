﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemsLight.Model {
    public class DatabaseSetting {
        public virtual string Id { get; set; }
        public virtual string V1 { get; set; }
        public virtual long V2 { get; set; }
    }
}
