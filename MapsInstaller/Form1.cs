﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace MapsInstaller {
    public partial class Form1 : Form {

        public bool rk;
        public bool sl;
        public bool xan;

        public Form1() {
            InitializeComponent();
            this.DialogResult = DialogResult.Abort;
            this.FormClosing += Form1_FormClosing;
        }

        void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            rk = this.checkBox1.Checked;
            sl = this.checkBox2.Checked;
            xan = this.checkBox3.Checked;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            System.Diagnostics.Process.Start("http://mireiawen.net/ao/install-ao-maps");
        }

        private void Form1_Load(object sender, EventArgs e) {
            string path;
            if (FindPathToAnarchy(out path)) {
                textBox1.Text = path;
            }
        }

        private void button2_Click(object sender, EventArgs e) {
            bool isValidAoFolder = false;
            try {
                isValidAoFolder = File.Exists(Path.Combine(textBox1.Text, "Anarchy.exe"));
            }
            catch (Exception ex) {

            }

            if (!checkBox1.Checked && !checkBox2.Checked && !checkBox3.Checked) {
                MessageBox.Show("You have to choose at least one map!");
            }
            else if (!isValidAoFolder) {
                MessageBox.Show("Could not find Anarchy Online in the specified folder!");
            }
            else {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            FolderBrowserDialog diag = new FolderBrowserDialog();
            if (diag.ShowDialog() == DialogResult.OK) {
                this.DialogResult = DialogResult.None;
                textBox1.Text = diag.SelectedPath;
            }
        }



        private static bool FindPathToAnarchy(out string fullpath) {
            return BackpackRenamer.FindPathToAnarchy(false, out fullpath);
        }
    }
}
