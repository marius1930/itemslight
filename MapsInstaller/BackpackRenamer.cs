﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using log4net;
using System.Diagnostics;
using System.ComponentModel;

namespace ItemsLight.AOIA.Misc {
    public class BackpackRenamer {
        private static HashSet<long> previouslyUpdatedContainers = new HashSet<long>();
        private static ILog logger = LogManager.GetLogger(typeof(BackpackRenamer));
        private static BackgroundWorker bw = new BackgroundWorker();

        /// <summary>
        /// List all the appdata folders for Anarchy Online
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static List<String> ListAnarchyDirectories(String path) {
            List<String> anarchyDirectory = new List<String>();
            string[] directories = Directory.GetDirectories(path);

            
            if (directories.Any(s => Path.GetFileName(s).Equals("prefs", StringComparison.InvariantCultureIgnoreCase))) {
                anarchyDirectory.Add(path);
                return anarchyDirectory;
            }

            foreach (string d in Directory.GetDirectories(path)) {
                anarchyDirectory.AddRange(ListAnarchyDirectories(d));
            }
            return anarchyDirectory;
        }


        /// <summary>
        /// Map all account:characterid instances from the provided AO appdata folder
        /// </summary>
        /// <param name="path"></param>
        /// <param name="accountCharid"></param>
        public static void MapAccountCharid(String path, ref Dictionary<long, String> accountCharid) {
            string[] accounts = Directory.GetDirectories(Path.Combine(path, "Prefs"));
            foreach (String account in accounts) {
                string[] charids = Directory.GetDirectories(account);
                foreach (String charid in charids) {
                    String fn = Path.GetFileName(charid);

                    if (fn.StartsWith("Char")) {
                        long id;
                        if (long.TryParse(fn.ToLower().Replace("char", ""), out id)) {
                            accountCharid[id] = Path.GetFileName(account);
                        }
                        else {
                            logger.Debug("Could not parse character id from " + fn);
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Creates a new dummy container file to name an ingame backpack
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="containerName"></param>
        private static void CreateContainerFile(String filename, String containerName) {
            String data = "<Archive code=\"0\">\r\n    <String name=\"container_name\" value='&quot;" + containerName + "&quot;' />\r\n</Archive>";
            Directory.CreateDirectory(Path.GetDirectoryName(filename));
            File.WriteAllText(filename, data);
        }

        /// <summary>
        /// Renames an existing backpack, unless it has previously been renamed
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="containerName"></param>
        private static bool UpdateExistingContainerFile(String filename, String containerName, bool forced) {
            String data = File.ReadAllText(filename);
            String insert = "\r\n    <String name=\"container_name\" value='&quot;" + containerName + "&quot;' />";
            if (!data.Contains("container_name")) {
                int idx = 1 + data.IndexOf('>');
                data = data.Insert(idx, insert);
                File.WriteAllText(filename, data);
                return true;
            }
            else if (forced) {
                string[] dataset = File.ReadAllLines(filename);
                for (int i = 0; i < dataset.Length; i++) {
                    if (dataset[i].Contains("container_name")) {
                        if (containerName.Length > 0)
                            dataset[i] = insert;
                        else
                            dataset[i] = "";
                    }
                }

                File.WriteAllLines(filename, dataset);
                return true;

            }
            return false;
        }


        /// <summary>
        /// Renames the container, unless previously renamed.
        /// </summary>
        /// <param name="account"></param>
        /// <param name="containerName"></param>
        /// <param name="containerIds"></param>
        /// <param name="characterId"></param>
        public static bool RenameBackpacks(String account, String containerName, long containerId, long characterId, bool forced) {
            bool success = false;
            try {
                String appdata = Environment.GetEnvironmentVariable("LocalAppData");
                String fullpath = Path.Combine(new string[] { appdata, "funcom" });
                foreach (String folder in ListAnarchyDirectories(fullpath)) {
                    String charpath = Path.Combine(new string[] { folder, "Prefs", account, "Char" + characterId });
                    if (!Directory.Exists(charpath))
                        continue;


                    System.Threading.Thread.Sleep(200);

                    String containerPath = Path.Combine(charpath, "Containers");
                    if (!previouslyUpdatedContainers.Contains(containerId) || forced) {
                        previouslyUpdatedContainers.Add(containerId);

                        if (!File.Exists(Path.Combine(containerPath, "Container_51017x" + containerId + ".xml"))) {
                            CreateContainerFile(Path.Combine(containerPath, "Container_51017x" + containerId + ".xml"), containerName);
                            success = true;
                            logger.Info("Renamed container " + containerId + " to " + containerName);
                        }
                        else {
                            if (UpdateExistingContainerFile(Path.Combine(containerPath, "Container_51017x" + containerId + ".xml"), containerName, forced)) {
                                success = true;
                                logger.Info("Renamed container " + containerId + " to " + containerName);
                            }
                            else {
                                //logger.Debug("Skipped container rename on " + containerId + " to " + containerName + ", has a custom name.");
                            }
                        }
                    }
                }
            }
            catch (Exception e) {
                logger.Warn(e.Message + e.StackTrace);
                CrashReport.ReportCrash(e);
            }
            return success;
        }


        public static void CreateAOIAScriptIfRequired() {
            string script = @"/text <a href=""text://
                    <font color=#99CCFF>## IA:Light ##<br>
                    <br>
                    :: Announce ::<br>
                    <a href='chatcmd:///summary'>Character Details</a><br>
                    <a href='chatcmd:///wp'>Display Waypoints</a> (Online Characters)<br>
                    <a href='chatcmd:///awp'>Display Waypoints</a> (All Characters)<br>
                    <br>
                    :: Display ::<br>
                    <a href='chatcmd:///esummary'>View Character Details</a><br>
                    <a href='chatcmd:///ewp'>Display Waypoints</a> (Online Characters)<br>
                    <a href='chatcmd:///aewp'>Display Waypoints</a> (All Characters)<br>

                    "">IA:Light Menu</a>".Replace("\n", "").Replace("\r", "").Replace("                    ", "    ");
            String appdata = Environment.GetEnvironmentVariable("LocalAppData");
            String fullpath = Path.Combine(new string[] { appdata, "funcom" });

            try {
                // Write to file
                foreach (String folder in BackpackRenamer.ListAnarchyDirectories(fullpath)) {
                    Directory.CreateDirectory(Path.Combine(folder, "scripts"));
                    File.WriteAllText(Path.Combine(folder, "scripts", "aoia"), script);
                    File.WriteAllText(Path.Combine(folder, "scripts", "ia"), script);

                }
            }
            catch (Exception ex) {
                logger.Warn(ex.Message + ex.StackTrace);
                CrashReport.ReportCrash(ex);
            }
        }



        public static string GetBackpackName(String account, long characterId, long containerId) {
            try {
                String appdata = Environment.GetEnvironmentVariable("LocalAppData");
                String fullpath = Path.Combine(new string[] { appdata, "funcom" });

                foreach (String folder in ListAnarchyDirectories(fullpath)) {
                    String containerPath = Path.Combine(new string[] { folder, "Prefs", account, "Char" + characterId, "Containers" });
                    if (!Directory.Exists(containerPath))
                        continue;

                    string filename = Path.Combine(containerPath, "Container_51017x" + containerId + ".xml");
                    if (File.Exists(filename)) {
                        String[] data = File.ReadAllLines(filename);
                        //String insert = "\r\n    <String name=\"container_name\" value='&quot;" + containerName + "&quot;' />";
                        String f = "container_name\" value='&quot;";
                        foreach (String line in data) {
                            if (line.Contains("container_name")) {
                                int idx = line.IndexOf(f) + f.Length;

                                return line.Substring(idx, line.IndexOf("&quot;", idx) - idx);
                            }
                        }
                    }
                }
            }
            catch (Exception e) {
                logger.Warn(e.Message + e.StackTrace);
                CrashReport.ReportCrash(e);
            }
            
            return "";
        }


        public static bool FindPathToAnarchy() {
            string fullpath;
            if (FindPathToAnarchy(false, out fullpath)) {
                Properties.Settings.Default["pathToAnarchy"] = fullpath;
                Properties.Settings.Default.Save();
                return true;
            }
            return false;
        }
        public static bool FindPathToIA(out string path) {
            path = "";
            return FindPathToAnarchy(true, out path);
        }

        public static bool FindPathToAnarchy(bool aoia, out string fullpath) {
            fullpath = "";

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "find.exe";
            if (aoia)
                startInfo.Arguments = "aoia";
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;

            startInfo.CreateNoWindow = true;

            Process processTemp = new Process();
            processTemp.StartInfo = startInfo;
            processTemp.EnableRaisingEvents = true;
            try {
                processTemp.Start();
                while (!processTemp.StandardOutput.EndOfStream) {
                    string line = processTemp.StandardOutput.ReadLine();
                    if (!"-1".Equals(line)) {
                        if (Directory.Exists(line)) {
                            fullpath = Path.GetDirectoryName(line);
                            return true;
                        }

                        logger.Warn(string.Format("Find.exe returned \"{0}\" but the folder does not exist", line));
                    }

                }
            }
            catch (Exception) {
                return false;
            }

            return false;
        }

        [Obsolete("Can't find anyone using this")]
        public static void RenameBackpacksAsync(List<BackpackRenameRequest> containers, Action<string> UpdateLoadingScreenImmediately) {
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.WorkerSupportsCancellation = true;
            bw.RunWorkerAsync(new object[] { containers, UpdateLoadingScreenImmediately });
        }

        private static void bw_DoWork(object sender, DoWorkEventArgs e) {
            BackgroundWorker worker = sender as BackgroundWorker;

            object[] args = (object[])e.Argument;
            Action<string> UpdateLoadingScreenImmediately = args[1] as Action<string>;
            List<BackpackRenameRequest> containers = args[0] as List<BackpackRenameRequest>;
            if (!worker.CancellationPending) {
                RenameBackpacks(containers, UpdateLoadingScreenImmediately);
            }
        }

        public static void RenameBackpacks(List<BackpackRenameRequest> containers, Action<string> UpdateLoadingScreenImmediately) {
            try {
                String appdata = Environment.GetEnvironmentVariable("LocalAppData");
                String fullpath = Path.Combine(new string[] { appdata, "funcom" });
                //UpdateLoadingScreenImmediately("Detecting backpacks..");

                Dictionary<long, String> accountMap = new Dictionary<long, String>();

                foreach (String folder in BackpackRenamer.ListAnarchyDirectories(fullpath)) {
                    MapAccountCharid(folder, ref accountMap);
                }

                uint idx = (uint)Properties.Settings.Default["backpackIdx"];
                uint initial = idx;
                //var containers = SqlCE.Instance.CListAllContainers();

                //UpdateLoadingScreenImmediately("Renaming backpacks..");
                foreach (var container in containers) {
                    long charid = container.Item1;
                    long containerId = container.Item2;
                    string bpname = "#" + idx + " " + container.Item3;

                    if (accountMap.ContainsKey(charid)) {
                        if (BackpackRenamer.RenameBackpacks(accountMap[charid], bpname, containerId, charid, false))
                            idx++;
                    }
                }

                logger.Debug("Backpack renaming complete..");
                if (initial != idx) {
                    Properties.Settings.Default["backpackIdx"] = idx;
                    Properties.Settings.Default.Save();
                }
            }
            catch (Exception e) {
                logger.Warn(e.Message + e.StackTrace);
                CrashReport.ReportCrash(e);
            }
        }
    }
}

