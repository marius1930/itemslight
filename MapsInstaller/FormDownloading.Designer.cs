﻿namespace MapsInstaller {
    partial class FormDownloading {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDownloading));
            this.progressRk = new System.Windows.Forms.ProgressBar();
            this.progressXan = new System.Windows.Forms.ProgressBar();
            this.progressSl = new System.Windows.Forms.ProgressBar();
            this.labelRk = new System.Windows.Forms.Label();
            this.labelSl = new System.Windows.Forms.Label();
            this.labelXan = new System.Windows.Forms.Label();
            this.buttonNext = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // progressRk
            // 
            this.progressRk.Location = new System.Drawing.Point(179, 12);
            this.progressRk.Name = "progressRk";
            this.progressRk.Size = new System.Drawing.Size(316, 23);
            this.progressRk.TabIndex = 0;
            // 
            // progressXan
            // 
            this.progressXan.Location = new System.Drawing.Point(179, 70);
            this.progressXan.Name = "progressXan";
            this.progressXan.Size = new System.Drawing.Size(316, 23);
            this.progressXan.TabIndex = 1;
            // 
            // progressSl
            // 
            this.progressSl.Location = new System.Drawing.Point(179, 41);
            this.progressSl.Name = "progressSl";
            this.progressSl.Size = new System.Drawing.Size(316, 23);
            this.progressSl.TabIndex = 2;
            // 
            // labelRk
            // 
            this.labelRk.AutoSize = true;
            this.labelRk.Location = new System.Drawing.Point(12, 18);
            this.labelRk.Name = "labelRk";
            this.labelRk.Size = new System.Drawing.Size(133, 13);
            this.labelRk.TabIndex = 3;
            this.labelRk.Text = "Downloading Rubi-Ka map";
            // 
            // labelSl
            // 
            this.labelSl.AutoSize = true;
            this.labelSl.Location = new System.Drawing.Point(12, 47);
            this.labelSl.Name = "labelSl";
            this.labelSl.Size = new System.Drawing.Size(160, 13);
            this.labelSl.TabIndex = 4;
            this.labelSl.Text = "Downloading Shadowlands Map";
            // 
            // labelXan
            // 
            this.labelXan.AutoSize = true;
            this.labelXan.Location = new System.Drawing.Point(12, 76);
            this.labelXan.Name = "labelXan";
            this.labelXan.Size = new System.Drawing.Size(115, 13);
            this.labelXan.TabIndex = 5;
            this.labelXan.Text = "Downloading Xan Map";
            // 
            // buttonNext
            // 
            this.buttonNext.Enabled = false;
            this.buttonNext.Location = new System.Drawing.Point(433, 151);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(75, 23);
            this.buttonNext.TabIndex = 6;
            this.buttonNext.Text = "Next";
            this.buttonNext.UseVisualStyleBackColor = true;
            // 
            // FormDownloading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 176);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.labelXan);
            this.Controls.Add(this.labelSl);
            this.Controls.Add(this.labelRk);
            this.Controls.Add(this.progressSl);
            this.Controls.Add(this.progressXan);
            this.Controls.Add(this.progressRk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormDownloading";
            this.Text = "AO Map Installer - Downloading..";
            this.Load += new System.EventHandler(this.FormDownloading_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressRk;
        private System.Windows.Forms.ProgressBar progressXan;
        private System.Windows.Forms.ProgressBar progressSl;
        private System.Windows.Forms.Label labelRk;
        private System.Windows.Forms.Label labelSl;
        private System.Windows.Forms.Label labelXan;
        private System.Windows.Forms.Button buttonNext;
    }
}