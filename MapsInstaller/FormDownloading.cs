﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using System.Diagnostics;

namespace MapsInstaller {
    public partial class FormDownloading : Form {
        class MapDownload {
            public string URL;
            public string Filename;
            public ProgressBar progress;
        }

        private List<MapDownload> queue = new List<MapDownload>();
        private string URL_RK = "http://mireiawen.net/Files/Maps/AoRK_v2.1.zip";
        private string URL_SL = "http://mireiawen.net/Files/Maps/AoSL_v0.9.3.zip";
        private string URL_XAN = "http://mireiawen.net/Files/Maps/AoLX_v0.4.zip";


        public FormDownloading(bool rk, bool sl, bool xan) {
            InitializeComponent();

            List<ProgressBar> progressBars = new List<ProgressBar>();
            List<Label> labels = new List<Label>();

            if (rk) {
                progressBars.Add(progressRk);
                labels.Add(labelRk);
                queue.Add(new MapDownload {
                    URL = URL_RK,
                    Filename = "aork.zip",
                    progress = progressRk
                });
            }
            if (sl) {
                progressBars.Add(progressSl);
                labels.Add(labelSl);
                queue.Add(new MapDownload {
                    URL = URL_SL,
                    Filename = "aosl.zip",
                    progress = progressSl
                });
            }
            if (xan) {
                progressBars.Add(progressXan);
                labels.Add(labelXan);
                queue.Add(new MapDownload {
                    URL = URL_XAN,
                    Filename = "aoxan.zip",
                    progress = progressXan
                });
            }

            // Hide any elements not required
            this.progressRk.Visible = rk;
            this.progressSl.Visible = sl;
            this.progressXan.Visible = xan;
            this.labelRk.Visible = rk;
            this.labelSl.Visible = sl;
            this.labelXan.Visible = xan;

            // Position the progress bars
            // The positions may change if the user chooses not to install all the maps.
            {
                int y = 12;
                for (int i = 0; i < progressBars.Count; i++) {
                    var loc = progressBars[i].Location;
                    loc.Y = y;
                    y += 29;
                    progressBars[i].Location = loc;
                }
            }
            {
                int y = 18;
                for (int i = 0; i < labels.Count; i++) {
                    var loc = labels[i].Location;
                    loc.Y = y;
                    y += 29;
                    labels[i].Location = loc;
                }
            }

            this.FormClosing += FormDownloading_FormClosing;
        }

        void FormDownloading_FormClosing(object sender, FormClosingEventArgs e) {
            try {
                if (client != null)
                    client.CancelAsync();
            }
            catch (Exception) {

            }
        }


        /// <summary>
        /// Delete any corrupted downloaded files
        /// </summary>
        public static void VerifyDownloadedFiles() {
            try {
                if (File.Exists(Path.Combine(MainPath, "aork.zip"))) {
                    if (!"913e569744dccf7ad1d4947dbd013f16".Equals(GetMD5(Path.Combine(MainPath, "aork.zip")))) {
                        File.Delete(Path.Combine(MainPath, "aork.zip"));
                    }
                }

                if (File.Exists(Path.Combine(MainPath, "aosl.zip"))) {
                    if (!"7b3a056fd296119a72ffd547ee4e5d55".Equals(GetMD5(Path.Combine(MainPath, "aosl.zip")))) {
                        File.Delete(Path.Combine(MainPath, "aosl.zip"));
                    }
                }

                if (File.Exists(Path.Combine(MainPath, "aoxan.zip"))) {
                    if (!"6cff1baa7007169cd52bab16949672de".Equals(GetMD5(Path.Combine(MainPath, "aoxan.zip")))) {
                        File.Delete(Path.Combine(MainPath, "aoxan.zip"));
                    }
                }
            }
            catch (Exception ex) {

            }
        }
        private static string GetMD5(string filename) {
            using (var md5 = MD5.Create()) {
                using (var stream = File.OpenRead(filename)) {
                    byte[] bytedata = md5.ComputeHash(stream);
                    return BitConverter.ToString(bytedata).Replace("-", "").ToLower(); 
                }
            }
        }

        private static string MainPath {
            get {
                String appdata = Environment.GetEnvironmentVariable("LocalAppData");
                string fullpath = Path.Combine(appdata, "EvilSoft", "AOMaps", "maps");
                Directory.CreateDirectory(fullpath);
                return fullpath;
            }
        }
        
        private void FormDownloading_Load(object sender, EventArgs e) {
            VerifyDownloadedFiles();

            try {
                StartDownload();
            }
            catch (Exception ex) {
                MessageBox.Show("Error!\n" + ex.Message);
                this.Close();
            }
        }

        WebClient client;
        private void StartDownload() {
            if (queue.Count > 0 && client == null) {
                client = new WebClient();
                client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
                client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);
                client.DownloadFileAsync(new Uri(queue[0].URL), Path.Combine(MainPath, queue[0].Filename));
            }
        }

        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e) {
            //double bytesIn = double.Parse(e.BytesReceived.ToString());
            //double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            //double percentage = bytesIn / totalBytes * 100;
            //label2.Text = "Downloaded " + e.BytesReceived + " of " + e.TotalBytesToReceive;

            if (queue.Count > 0)
                queue[0].progress.Value = e.ProgressPercentage;
        }


        void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e) {
            if (queue.Count > 0)
                queue.RemoveAt(0);
            
            client = null;
            StartDownload();

            if (queue.Count == 0)
                buttonNext.Enabled = true;
        }

        private void CreateSymblinkJunktion(string path, string link) {
            string file = Path.Combine(path, "cd_image", "textures", "PlanetMap", link);
            string linkfile = Path.Combine(MainPath, link);
            ProcessStartInfo psInfo = new System.Diagnostics.ProcessStartInfo(string.Format("mklink /J {0} \"{1}\"", linkfile, path));

            psInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            psInfo.RedirectStandardOutput = true;
            psInfo.UseShellExecute = false;

            Process GetIPInfo;
            GetIPInfo = Process.Start(psInfo);

            // We want to get the output from standard output
            System.IO.StreamReader myOutput = GetIPInfo.StandardOutput;
            GetIPInfo.WaitForExit(3000);
        }
    }
}
