﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace MapsInstaller {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Form1 main = new Form1();
            Application.Run(main);
            if (main.DialogResult == DialogResult.OK && (main.rk || main.sl || main.xan)) {
                
                FormDownloading form = new FormDownloading(main.rk, main.sl, main.xan);
                Application.Run(form);
            }
        }
    }
}
